import pytest
from unittest.mock import Mock

from fair_ds_ap42.source import S3Bucket
from fair_ds_ap42.source.s3_bucket.s3_bucket_writer import (
    S3BucketWriter,
)


@pytest.fixture
def mock_bucket(s3):
    mock = Mock(spec=S3Bucket)
    mock.bucket_name = "testbucket"
    mock.client = s3
    return mock


def test_write(s3, mock_bucket):
    # Test that we can write a LocalDirectoryWriter object

    test_data = "test data"
    writer = S3BucketWriter(mock_bucket)
    writer.write("test_file.txt", test_data)

    assert (
        s3.get_object(Bucket="testbucket", Key="test_file.txt")["Body"].read()
        == b"test data"
    )


def test_write_json(s3, mock_bucket):
    # Test that we can write a LocalDirectoryWriter object

    test_data = {"test": "data"}
    writer = S3BucketWriter(mock_bucket)
    writer.write_json("test_file.txt", test_data)

    assert (
        s3.get_object(Bucket="testbucket", Key="test_file.txt")["Body"].read()
        == b'{"test": "data"}'
    )
