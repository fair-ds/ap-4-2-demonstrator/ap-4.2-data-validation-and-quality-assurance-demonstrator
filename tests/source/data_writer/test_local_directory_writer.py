from unittest.mock import patch

from fair_ds_ap42.source.local_directory.local_directory_writer import (
    LocalDirectoryWriter,
)


def test_write():
    # Test that we can write a LocalDirectoryWriter object

    test_data = "test data"
    writer = LocalDirectoryWriter()

    with patch("builtins.open"):
        writer.write("test_file.txt", test_data)
        # mock_textiowrapper.write.assert_called_once_with(test_data)


def test_write_json():
    # Test that we can write a LocalDirectoryWriter object

    test_data = {"test": "data"}
    writer = LocalDirectoryWriter()

    with patch("builtins.open"):
        writer.write("test_file.txt", test_data)
        # mock_textiowrapper.write.assert_called_once_with(test_data)
