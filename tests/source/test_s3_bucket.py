import pytest

from fair_ds_ap42.source import S3Bucket
from fair_ds_ap42.source.s3_bucket.readers import S3Reader


@pytest.fixture
def s3_bucket(s3):
    # Upload a file to the bucket
    s3.put_object(Bucket="testbucket", Key="test-file", Body="test")
    return S3Bucket(bucket_name="testbucket")


def test_exists(s3_bucket):
    # Assert that the file exists
    assert s3_bucket.exists("test-file")

    # Assert that the file does not exist
    assert not s3_bucket.exists("non-existing-file")


def test_get_size_in_bytes(s3_bucket):
    # Assert that the size is correct
    assert s3_bucket.get_size_in_bytes("test-file") == 4


def test_get_date_last_modified(s3, s3_bucket):
    # Assert that the date is correct
    assert (
        s3_bucket.get_date_last_modified("test-file")
        == s3.head_object(Bucket="testbucket", Key="test-file")["LastModified"]
    )


def test_get_contents(s3_bucket):
    # Assert that the contents are correct
    assert s3_bucket.get_contents("test-file") == "test"


def test_list_files(s3_bucket):
    # Assert that the file is in the list
    assert "test-file" in s3_bucket.list_files()


def test_bucket_list_files_no_prefix(s3):
    # If  we provide no prefix, we should get all files in the bucket
    s3.put_object(Bucket="testbucket", Key="file1", Body="test")
    s3.put_object(Bucket="testbucket", Key="my_dirA/file2", Body="test")
    s3.put_object(Bucket="testbucket", Key="my_dirB/file3", Body="test")

    s3_bucket = S3Bucket(bucket_name="testbucket")
    assert s3_bucket.list_files() == ["file1", "my_dirA/file2", "my_dirB/file3"]


def test_bucket_list_files_with_prefix(s3):
    # If we provide a prefix when creating the bucket, it should be used when listing files
    s3.put_object(Bucket="testbucket", Key="file1", Body="test")
    s3.put_object(Bucket="testbucket", Key="my_dirA/file2", Body="test")
    s3.put_object(Bucket="testbucket", Key="my_dirB/file3", Body="test")

    s3_bucket = S3Bucket(bucket_name="testbucket", prefix="my_dirA")
    assert s3_bucket.list_files() == ["my_dirA/file2"]


def test_list_files_pattern(s3, s3_bucket):
    # Upload two files to the bucket
    s3.put_object(Bucket="testbucket", Key="findme/test-file", Body="test")
    s3.put_object(Bucket="testbucket", Key="dont_findme/test-file2", Body="test")

    # A regex pattern that matches the first file but not the second
    pattern = r"findme\/.*"

    results = s3_bucket.list_files(pattern)

    # Assert that we only find the first file
    assert "findme/test-file" in results
    assert "dont_findme/test-file2" not in results


def test_write_json(s3_bucket):
    # Write a JSON file to the bucket
    s3_bucket.write_json("test-file.json", {"test": "test"})

    # Assert that the file exists
    assert s3_bucket.exists("test-file.json")

    # Assert that the file contains the correct data
    assert s3_bucket.get_contents("test-file.json") == '{"test": "test"}'


def test_get_reader(s3_bucket):
    result = s3_bucket.get_reader("test-file.csv")

    assert isinstance(result, S3Reader)
    assert result.filepath == "test-file.csv"
    assert result.delimiter == ","
    assert result.encoding == "utf-8"


def test_get_reader_with_format(s3_bucket):
    # Even though the file is a CSV, we can specify a different format
    result = s3_bucket.get_reader("test-file.csv", file_format="tsv")

    assert isinstance(result, S3Reader)
    assert result.filepath == "test-file.csv"
    assert result.delimiter == "\t"
    assert result.encoding == "utf-8"
