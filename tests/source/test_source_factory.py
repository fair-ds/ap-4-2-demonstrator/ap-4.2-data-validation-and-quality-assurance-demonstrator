from fair_ds_ap42.source import ISource
from fair_ds_ap42.source.source_factory import SourceFactory


def test_create_local_directory():
    source_dict = {"type": "local_directory", "location": "./my_dir"}

    source = SourceFactory.from_dict(**source_dict)

    assert isinstance(source, ISource)
    assert source.name == "local"
    assert source.location == "./my_dir"


def test_create_s3_bucket():
    source_dict = {"type": "s3_bucket", "bucket_name": "my_bucket"}

    source = SourceFactory.from_dict(**source_dict)

    assert isinstance(source, ISource)
    assert source.name == "s3"
    assert source.bucket_name == "my_bucket"


def test_create_url():
    source_dict = {"type": "url", "url": "https://my_url", "location": "./my_dir"}

    source = SourceFactory.from_dict(**source_dict)

    assert isinstance(source, ISource)
    assert source.name == "url"
    assert source.url == "https://my_url"
    assert source.location == "./my_dir"
