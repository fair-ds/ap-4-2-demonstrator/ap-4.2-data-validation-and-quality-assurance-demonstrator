import os
from pathlib import Path

import pytest

from fair_ds_ap42.source.local_directory.readers.local_csv_reader import LocalCSVReader
from fair_ds_ap42.source import S3Bucket, find_file_location


@pytest.fixture
def csv_file():
    file_name = "testfile.csv"
    file_path = os.path.join(os.getcwd(), file_name)
    data = "name,age\nJohn,25\nAlice,30\nBob,40\n"
    with open(file_path, "w") as file:
        file.write(data)
    yield file_name
    os.remove(file_path)


@pytest.fixture
def s3_bucket(s3):
    # Add a test file to the bucket
    data = "name,age\nJohn,25\nAlice,30\nBob,40"
    s3.put_object(Bucket="testbucket", Key="testfile.csv", Body=data)
    return S3Bucket(bucket_name="testbucket")


@pytest.fixture
def s3_csv_file(s3_bucket):
    return "testfile.csv"


def test_find_file_location_local_file(csv_file):
    reader = find_file_location(file_path=csv_file)
    assert reader is not None


def test_find_file_location_s3_file(s3_bucket, s3_csv_file):
    reader = find_file_location(file_path=s3_csv_file, s3_bucket=s3_bucket)
    assert reader is not None


def test_find_file_local_priority(csv_file, s3_bucket, s3_csv_file):
    """
    In the case where we have two files with identical names, one local and one in an s3 bucket,
    we should prioritize the local file
    """
    # Check that we have the same file name in both locations
    filename = "testfile.csv"
    assert Path(filename).exists()
    assert s3_bucket.exists(filename)

    reader = find_file_location(file_path=filename, s3_bucket=s3_bucket)
    assert reader is not None
    assert isinstance(reader, LocalCSVReader)
