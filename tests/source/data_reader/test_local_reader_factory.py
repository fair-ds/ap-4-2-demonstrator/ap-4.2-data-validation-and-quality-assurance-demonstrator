import pytest

from fair_ds_ap42.source.local_directory.readers import (
    LocalReaderFactory,
    LocalCSVReader,
    LocalParquetReader,
)


@pytest.fixture
def local_reader_factory():
    return LocalReaderFactory()


def test_create_reader_csv(local_reader_factory):
    file_path = "data.csv"
    reader = local_reader_factory.create_reader(file_path)
    assert isinstance(reader, LocalCSVReader)
    assert reader.delimiter == ","
    assert reader.encoding == "utf-8"


def test_create_reader_parquet(local_reader_factory):
    file_path = "data.parquet"
    reader = local_reader_factory.create_reader(file_path)
    assert isinstance(reader, LocalParquetReader)


def test_create_reader_unsupported_format(local_reader_factory):
    file_path = "data.txt"
    with pytest.raises(NotImplementedError):
        local_reader_factory.create_reader(file_path)
