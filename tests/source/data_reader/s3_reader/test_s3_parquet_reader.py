import pandas as pd
import pytest

from fair_ds_ap42.source.s3_bucket.readers.s3_parquet_reader import S3ParquetReader
from fair_ds_ap42.source import S3Bucket

TEST_DATAFRAME = pd.DataFrame({"name": ["John", "Alice", "Bob"], "age": [25, 30, 40]})


@pytest.fixture
def s3_bucket(s3):
    # Create a bucket
    s3.create_bucket(Bucket="testbucket")

    # Create a parquet file in our bucket we can read, and put some data in it
    s3.put_object(
        Bucket="testbucket",
        Key="testfile.parquet",
        Body=TEST_DATAFRAME.to_parquet(engine="pyarrow"),
    )

    return S3Bucket(bucket_name="testbucket")


@pytest.fixture
def s3_csv_reader(s3_bucket):
    return S3ParquetReader(filepath="testfile.parquet", s3_bucket=s3_bucket)


def test_read_data(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.read_data()

    # Assert that the data is correct
    pd.testing.assert_frame_equal(result, TEST_DATAFRAME)


def test_read_column(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.read_column("name")

    # Assert that the data is correct
    expected = pd.Series(["John", "Alice", "Bob"], name="name")
    assert result.equals(expected)


def test_get_fields(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.get_fields()

    # Assert that the data is correct
    expected = ["name", "age"]
    assert result == expected
