import pytest

from fair_ds_ap42.source.s3_bucket.readers.s3_json_reader import S3JSONReader
from fair_ds_ap42.source import S3Bucket


@pytest.fixture
def s3_bucket(s3):
    # Add a test file to the bucket
    data = '[{"name": "John", "age": 25}, {"name": "Alice", "age": 30}, {"name": "Bob", "age": 40}]'
    s3.put_object(Bucket="testbucket", Key="testfile.json", Body=data)
    return S3Bucket(bucket_name="testbucket")


@pytest.fixture
def s3_json_reader(s3_bucket):
    return S3JSONReader(filepath="testfile.json", s3_bucket=s3_bucket)


def test_read_dict(s3_json_reader):
    # Read the file using the s3_json_reader
    result = s3_json_reader.read_dict()

    # Assert that the data is correct
    expected = [
        {"name": "John", "age": 25},
        {"name": "Alice", "age": 30},
        {"name": "Bob", "age": 40},
    ]
    assert result == expected
