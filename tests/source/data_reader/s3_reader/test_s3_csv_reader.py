import pytest
from unittest.mock import MagicMock

import pandas as pd

from fair_ds_ap42.source.s3_bucket.readers.s3_csv_reader import S3CSVReader
from fair_ds_ap42.source import S3Bucket


@pytest.fixture
def s3_bucket(s3):
    # Add a test file to the bucket
    data = "name,age\nJohn,25\nAlice,30\nBob,40"
    s3.put_object(Bucket="testbucket", Key="testfile.csv", Body=data)
    return S3Bucket(bucket_name="testbucket")


@pytest.fixture
def s3_csv_reader(s3_bucket):
    return S3CSVReader(filepath="testfile.csv", s3_bucket=s3_bucket)


def test_read_data(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.read_data()

    # Assert that the data is correct
    expected = pd.DataFrame({"name": ["John", "Alice", "Bob"], "age": [25, 30, 40]})
    assert result.equals(expected)


def test_read_column(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.read_column("name")

    # Assert that the data is correct
    expected = pd.Series(["John", "Alice", "Bob"], name="name")
    assert result.equals(expected)


def test_get_fields(s3_csv_reader):
    # Read the file using the s3_csv_reader
    result = s3_csv_reader.get_fields()

    # Assert that the data is correct
    assert isinstance(result, list)
    expected = ["name", "age"]
    assert result == expected


# Test to confirm that we are reading data efficiently. We should only be requesting data from the
# S3 bucket once, and then storing it in memory.
def test_read_data_efficiency(s3_csv_reader):
    # Mock the s3_bucket.client.get_object method
    mock_s3_bucket = MagicMock(spec=S3Bucket)
    mock_s3_bucket.get_contents.return_value = "name,age\nJohn,25\nAlice,30\nBob,40"

    s3_csv_reader.s3_bucket = mock_s3_bucket

    # "read" the file
    s3_csv_reader.read_data()

    # Read the file again
    s3_csv_reader.read_data()

    # Assert that we only called the s3_bucket.client.get_object method once
    assert mock_s3_bucket.get_contents.call_count == 1
