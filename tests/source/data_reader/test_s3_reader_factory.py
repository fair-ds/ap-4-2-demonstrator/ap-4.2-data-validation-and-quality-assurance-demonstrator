import pytest

from fair_ds_ap42.source.s3_bucket.readers import (
    S3ReaderFactory,
    S3CSVReader,
    S3ParquetReader,
)
from fair_ds_ap42.source import S3Bucket


@pytest.fixture
def s3_bucket(s3):
    return S3Bucket(bucket_name="testbucket")


@pytest.fixture
def s3_reader_factory(s3_bucket):
    return S3ReaderFactory(s3_bucket=s3_bucket)


def test_create_reader_csv(s3_reader_factory):
    file_path = "data.csv"
    reader = s3_reader_factory.create_reader(file_path)
    assert isinstance(reader, S3CSVReader)
    assert reader.delimiter == ","
    assert reader.encoding == "utf-8"


def test_create_reader_parquet(s3_reader_factory):
    file_path = "data.parquet"
    reader = s3_reader_factory.create_reader(file_path)
    assert isinstance(reader, S3ParquetReader)


def test_create_reader_unsupported_format(s3_reader_factory):
    file_path = "data.txt"
    with pytest.raises(NotImplementedError):
        s3_reader_factory.create_reader(file_path)
