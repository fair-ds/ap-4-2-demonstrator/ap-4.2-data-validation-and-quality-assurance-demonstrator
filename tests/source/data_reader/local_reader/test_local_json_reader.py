import os
import tempfile

import pytest

from fair_ds_ap42.source.local_directory.readers.local_json_reader import (
    LocalJSONReader,
)


@pytest.fixture
def json_file():
    # create a dictionary
    data = '[{"name": "John", "age": 25}, {"name": "Alice", "age": 30}, {"name": "Bob", "age": 40}]'
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_file:
        temp_file.write(data)
        file_path = temp_file.name
    yield file_path
    os.remove(file_path)


@pytest.fixture
def local_json_reader(json_file):
    return LocalJSONReader(filepath=json_file)


def test_read_dict(local_json_reader):
    # Read the file using the local_json_reader
    result = local_json_reader.read_dict()

    # Assert that the data is correct
    expected = [
        {"name": "John", "age": 25},
        {"name": "Alice", "age": 30},
        {"name": "Bob", "age": 40},
    ]
    assert result == expected
