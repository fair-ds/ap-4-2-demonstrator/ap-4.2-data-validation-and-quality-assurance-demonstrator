import os
import tempfile

import pandas as pd
import pytest

from fair_ds_ap42.source.local_directory.readers.local_parquet_reader import (
    LocalParquetReader,
)


@pytest.fixture
def parquet_file_path():
    data = pd.DataFrame({"name": ["John", "Alice", "Bob"], "age": [25, 30, 40]})
    with tempfile.NamedTemporaryFile(suffix=".parquet", delete=False) as temp_file:
        data.to_parquet(temp_file.name)
        file_path = temp_file.name
    yield file_path
    os.remove(file_path)


@pytest.fixture
def local_parquet_reader(parquet_file_path):
    return LocalParquetReader(filepath=parquet_file_path)


def test_read_data(local_parquet_reader):
    # Read the file using the local_parquet_reader
    result = local_parquet_reader.read_data()

    # Assert that the data is correct
    expected = pd.DataFrame({"name": ["John", "Alice", "Bob"], "age": [25, 30, 40]})
    assert result.equals(expected)


def test_read_column(local_parquet_reader):
    # Read the file using the local_parquet_reader
    result = local_parquet_reader.read_column("name")

    # Assert that the data is correct
    expected = pd.Series(["John", "Alice", "Bob"], name="name")
    assert result.equals(expected)


def test_get_fields(local_parquet_reader):
    # Read the file using the local_parquet_reader
    result = local_parquet_reader.get_fields()

    # Assert that the data is correct
    expected = ["name", "age"]
    assert result == expected
