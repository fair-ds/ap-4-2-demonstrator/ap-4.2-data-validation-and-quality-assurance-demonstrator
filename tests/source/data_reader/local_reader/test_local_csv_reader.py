import os
import tempfile

import pandas as pd
import pytest

from fair_ds_ap42.source.local_directory.readers.local_csv_reader import LocalCSVReader


@pytest.fixture
def csv_file():
    data = "name,age\nJohn,25\nAlice,30\nBob,40\n"
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_file:
        temp_file.write(data)
        file_path = temp_file.name
    yield file_path
    os.remove(file_path)


@pytest.fixture
def local_csv_reader(csv_file):
    return LocalCSVReader(filepath=csv_file)


def test_read_data(local_csv_reader):
    # Read the file using the local_csv_reader
    result = local_csv_reader.read_data()

    # Assert that the data is correct
    expected = pd.DataFrame({"name": ["John", "Alice", "Bob"], "age": [25, 30, 40]})
    assert result.equals(expected)


def test_read_column(local_csv_reader):
    # Read the file using the local_csv_reader
    result = local_csv_reader.read_column("name")

    # Assert that the data is correct
    expected = pd.Series(["John", "Alice", "Bob"], name="name")
    assert result.equals(expected)


def test_get_fields(local_csv_reader):
    # Read the file using the local_csv_reader
    result = local_csv_reader.get_fields()

    # Assert that the data is correct
    expected = ["name", "age"]
    assert result == expected
