import pytest
from unittest.mock import Mock


from fair_ds_ap42.source.available_sources import (
    register_source,
    find_file,
    source_registry,
)
from fair_ds_ap42.source import LocalDirectory, S3Bucket


class MockSource(Mock):
    def __lt__(self, other):
        return self.sort_rank < other.sort_rank

    def __eq__(self, other):
        if not isinstance(other, MockSource):
            return False

        return self.sort_rank == other.sort_rank


@pytest.fixture
def local_directory():
    mock = MockSource(spec=LocalDirectory)
    mock.sort_rank = 1
    return mock


@pytest.fixture
def s3_bucket():
    mock = MockSource(spec=S3Bucket)
    mock.sort_rank = 2
    return mock


@pytest.fixture(autouse=True)
def reset_available_sources():
    if source_registry.available_sources is not None:
        source_registry.available_sources = []


def test_register_source(local_directory, s3_bucket):
    register_source(local_directory)
    register_source(s3_bucket)

    assert len(source_registry.available_sources) == 2
    assert source_registry.available_sources == [local_directory, s3_bucket]


def test_find_file(s3_bucket):
    s3_bucket.list_files.return_value = ["test_file"]

    register_source(s3_bucket)
    assert len(source_registry.available_sources) == 1
    assert find_file("test") == s3_bucket


def test_find_file_sorting(local_directory, s3_bucket):
    # If there are multiple sources that contain the same file, the source with the
    # lowest sort_rank should be returned.
    local_directory.list_files.return_value = ["test_file"]
    s3_bucket.list_files.return_value = ["test_file"]

    register_source(s3_bucket)
    register_source(local_directory)

    assert len(source_registry.available_sources) == 2
    assert find_file("test") == local_directory
