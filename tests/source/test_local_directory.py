import datetime
import pytest
from unittest.mock import patch

from fair_ds_ap42.source import LocalDirectory
from fair_ds_ap42.source.local_directory.readers import LocalReader


@pytest.fixture
def local_directory():
    return LocalDirectory(location="my_dir")


temp_file_setup = [
    {"filepaths": ["my_dir/file1.txt", "my_dir/file2.txt"]},
]


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
def test_exists(temporary_files):
    local_directory = LocalDirectory(location="my_dir")

    # Our temp files should exist
    assert local_directory.exists("file1.txt"), "file1.txt does not exist"
    assert local_directory.exists("file2.txt"), "file2.txt does not exist"

    # A non-existing file should not exist
    assert not local_directory.exists("non-existing-file"), "non-existing-file exists"


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
def test_get_size_in_bytes(temporary_files):
    local_directory = LocalDirectory(location="my_dir")

    result = local_directory.get_size_in_bytes("file1.txt")

    assert isinstance(result, int), "get_size_in_bytes did not return an int"
    assert (
        result == 4
    ), f"get_size_in_bytes did not return the correct size (expected 4, got {result})"


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
def test_get_date_last_modified(local_directory, temporary_files):
    with patch("pathlib.Path.stat") as mock_stat:
        mock_stat.return_value.st_mtime = datetime.datetime(
            2022, 1, 1, 0, 0
        ).timestamp()
        result = local_directory.get_date_last_modified(temporary_files[0])

    assert isinstance(
        result, datetime.datetime
    ), "get_date_last_modified did not return a datetime object"
    assert result == datetime.datetime(2022, 1, 1, 0, 0), (
        "get_date_last_modified did not return the correct date "
        f"(expected 2022-01-01 00:00:00, got {result})"
    )


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
def test_get_contents(local_directory, temporary_files):
    assert local_directory.get_contents(temporary_files[0]) == "abcd"


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
@pytest.mark.no_dir_setup
def test_list_files(local_directory, temporary_files):
    result = local_directory.list_files()

    assert isinstance(result, list)
    assert len(result) == 2
    assert temporary_files[0] in result
    assert temporary_files[1] in result


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
def test_list_files_with_pattern(local_directory, temporary_files):
    result = local_directory.list_files(pattern="file1.txt")

    assert isinstance(result, list)
    assert len(result) == 1
    assert temporary_files[0] in result
    assert temporary_files[1] not in result


def test_get_reader(local_directory):
    result = local_directory.get_reader("file1.csv")

    assert isinstance(result, LocalReader)
    assert result.filepath == "my_dir/file1.csv"
    assert result.delimiter == ","
    assert result.encoding == "utf-8"


def test_get_reader_with_format(local_directory):
    # Even though the file is a CSV, we can specify a different format
    result = local_directory.get_reader("file1.csv", file_format="tsv")

    assert isinstance(result, LocalReader)
    assert result.filepath == "my_dir/file1.csv"
    assert result.delimiter == "\t"
    assert result.encoding == "utf-8"


def test_is_child_of():
    # Check that we can determine when one local directory is a child of another local directory.

    dir1 = LocalDirectory(location="./my_dir")
    dir2 = LocalDirectory(location="./my_dir/child_dir")
    dir3 = LocalDirectory(location="./my_dir/child_dir/dirA/dirB/child_dir")

    assert not dir1.is_child_of(dir2)
    assert dir2.is_child_of(dir1)

    assert not dir1.is_child_of(dir3)
    assert not dir2.is_child_of(dir3)
    assert dir3.is_child_of(dir1)
    assert dir3.is_child_of(dir1)
    assert dir3.is_child_of(dir2)


def test_is_child_or_parent_of():
    # Check that we can determine when one local directory is a child or parent of another
    # local directory.

    dir1 = LocalDirectory(location="./my_dir")
    dir2 = LocalDirectory(location="./my_dir/child_dir")
    dir3 = LocalDirectory(location="./my_dir/child_dir/dirA/dirB/child_dir")
    dir4 = LocalDirectory(location="./other_dir")

    assert dir1.is_child_or_parent_of(dir2)
    assert dir2.is_child_or_parent_of(dir1)
    assert dir1.is_child_or_parent_of(dir3)
    assert dir2.is_child_or_parent_of(dir3)
    assert dir3.is_child_or_parent_of(dir1)
    assert dir3.is_child_or_parent_of(dir2)

    assert not dir1.is_child_or_parent_of(dir4)
    assert not dir2.is_child_or_parent_of(dir4)
    assert not dir3.is_child_or_parent_of(dir4)
    assert not dir4.is_child_or_parent_of(dir1)
    assert not dir4.is_child_or_parent_of(dir2)
    assert not dir4.is_child_or_parent_of(dir3)


# === DEPRECATING ===
@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
@pytest.mark.deprecated
def test_exists_no_location(temporary_files):
    local_directory = LocalDirectory()

    # Our temp files should exist
    assert local_directory.exists(temporary_files[0])
    assert local_directory.exists(temporary_files[1])

    # A non-existing file should not exist
    assert not local_directory.exists("non-existing-file")


@pytest.mark.parametrize("temporary_files", temp_file_setup, indirect=True)
@pytest.mark.deprecated
def test_get_size_in_bytes_no_location(temporary_files):
    local_directory = LocalDirectory()

    assert isinstance(local_directory.get_size_in_bytes(temporary_files[0]), int)


def test_write_json(local_directory):
    local_directory.write_json("file.json", {"key": "value"})

    with open("my_dir/file.json", "r") as file_obj:
        assert file_obj.read() == '{"key": "value"}'
