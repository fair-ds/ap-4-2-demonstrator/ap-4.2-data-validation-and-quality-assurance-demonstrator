import os

import pytest
from unittest.mock import patch

import pandas as pd

from fair_ds_ap42.source import URLSource
from fair_ds_ap42.source.local_directory.readers import LocalReader


@pytest.fixture
def url_source():
    return URLSource(url="http://www.example.com/data.csv", location="my_dir")


@pytest.fixture(autouse=True)
def patch_urlretrieve():
    def mock_urlretrieve_side_effect(url, filename=None, reporthook=None, data=None):
        # Instead of actually downloading, just return a dummy filename and status code
        # make sure the data directory exists
        os.makedirs("my_dir", exist_ok=True)

        # create a csv file in the data directory
        df = pd.DataFrame({"col1": [1, 2, 3], "col2": [4, 5, 6]})
        df.to_csv("my_dir/data.csv", index=False)

        return ("data.csv", 200)

    # Patch urllib.request.urlretrieve with the side effect function
    with patch(
        "urllib.request.urlretrieve", side_effect=mock_urlretrieve_side_effect
    ) as mock_urlretrieve:  # noqa: F841
        yield


def test_exists(url_source):
    url_source.retrieve_file()

    # Assert that the file exists
    assert url_source.exists("data.csv")

    # Assert that the file does not exist
    assert not url_source.exists("non-existing-file")


def test_url_doesnt_exist():
    # Check that we raise a proper error when the URL doesn't exist
    with pytest.raises(ValueError):
        with patch("urllib.request.urlretrieve") as mock_urlretrieve:
            mock_urlretrieve.side_effect = ValueError("URL does not exist")
            my_source = URLSource(
                url="http://www.example.com/non-existing-file.csv", location="my_dir"
            )
            my_source.retrieve_file()


def test_get_size_in_bytes(url_source):
    url_source.retrieve_file()
    result = url_source.get_size_in_bytes("data.csv")
    assert isinstance(result, int), "get_size_in_bytes did not return an int"


def test_list_files(url_source):
    assert url_source.list_files() == ["data.csv"]


def test_no_redundant_download(url_source):
    # If the file already exists, we should not download it again

    # make sure the data directory exists
    os.makedirs("my_dir", exist_ok=True)

    # Create a file in the data directory
    df = pd.DataFrame({"col1": [1, 2, 3], "col2": [4, 5, 6]})
    df.to_csv("my_dir/data.csv", index=False)

    # Assert that we don't download the file again
    with patch("urllib.request.urlretrieve") as mock_urlretrieve:
        mock_urlretrieve.return_value = ("data.csv", None)

        # Create a new URLSource object
        URLSource(url="http://www.example.com/data.csv", location="my_dir")

        mock_urlretrieve.assert_not_called()


def test_get_reader(url_source):
    reader = url_source.get_reader()

    assert isinstance(
        reader, LocalReader
    )  # Because we download the file, we use a LocalReader
    assert reader.filepath == "my_dir/data.csv"
    assert reader.delimiter == ","
    assert reader.encoding == "utf-8"

    assert isinstance(reader.get_size_in_bytes(), int)
