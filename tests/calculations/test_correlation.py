import pandas as pd
import pytest
from unittest.mock import MagicMock

from fair_ds_ap42.calculations.correlation import Correlation


@pytest.fixture
def mock_datafile():
    mock_df = pd.DataFrame({"A": [1, 2, 3], "B": [4, 5, 6], "C": [1, 9, 2]})
    mock_datafile = MagicMock()
    mock_datafile.get_dataframe.return_value = mock_df
    return mock_datafile


def test_calculate_correlations(mock_datafile):
    corr = Correlation(mock_datafile, "pearson")
    corr.calculate_correlations()
    mock_datafile.get_dataframe.assert_called_once()


def test_get_interesting_correlations(mock_datafile):
    corr = Correlation(mock_datafile, "pearson")
    expected_output = {("B", "A"): 1.0}
    assert corr.get_interesting_correlations() == expected_output


def test_get_interesting_correlations_user_min_r(mock_datafile):
    corr = Correlation(mock_datafile, "pearson")
    expected_output = {
        ("B", "A"): 1.0,
        ("C", "A"): 0.11470786693528087,
        ("C", "B"): 0.11470786693528087,
    }
    assert corr.get_interesting_correlations(min_r=0.1) == expected_output


def test_get_interesting_correlations_user_n(mock_datafile):
    corr = Correlation(mock_datafile, "pearson")
    expected_output = {
        ("B", "A"): 1.0,
        ("C", "A"): 0.11470786693528087,
    }
    assert corr.get_interesting_correlations(top_n=2, min_r=0.1) == expected_output
