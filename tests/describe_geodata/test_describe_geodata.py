# import json
# import os
# from pathlib import Path
# import pytest

# import pandas as pd
# import geopandas as gpd

# # from fair_ds_ap42.describe_geo import describe_geodata

# from tests.setups import Setups

# # A test Dataframe to use
# df = pd.DataFrame(
#     {
#         "my_data": [45, 84, 345, 12, 76, 34, 67, 4],
#         "lon": [10.5, 20.21, 30.865, None, 40.5678, 50.45923, 60.191254, 10.5],
#         "lat": [45.5, 80.21, -37.865, None, 41.5678, 59.45923, 60.191254, 45.5],
#     }
# )
# gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.lon, df.lat))


# @pytest.mark.skip(reason="Temp - environment mismatch")
# def test_get_description():
#     # Test
#     ret_val = describe_geodata(gdf.geometry)

#     assert isinstance(ret_val, dict)

#     required_columns = [
#         "Top",
#         "Count",
#         "Unique",
#         "Percent Unique",
#         "Missing Values",
#         "Percent Missing",
#         "Sample Values",
#     ]

#     for col in required_columns:
#         assert col in ret_val, f"return value missing column {col}"

#     assert ret_val["Top"] == [10.5, 45.5]
#     assert ret_val["Count"] == 8
#     assert ret_val["Unique"] == 6
#     assert ret_val["Percent Unique"] == (6 / 8)
#     assert ret_val["Missing Values"] == 1
#     assert ret_val["Percent Missing"] == (1 / 8)
#     assert len(ret_val["Sample Values"]) == 7


# @pytest.mark.skip(reason="Temp - environment mismatch")
# def test_save_description():
#     # Test
#     ret_val = describe_geodata(gdf.geometry)

#     with open("test.json", "w") as f:
#         json.dump(ret_val, f)

#     assert Path("test.json").exists()


# @pytest.mark.skipif("jh-admin" not in os.getcwd(), reason="Only run on local machine")
# @pytest.mark.skip(reason="Refer to Issue #2")
# def test_sample_data():
#     """Instead of requiring users to have their geo data in frictionless'
#     format, we can specify a setting indicating the columns to be used"""
#     # Biodiversity ============================================================
#     Setups.LoadData.setup("biodiv")

#     # Test
#     df = pd.read_csv("data/biodiv_cat.csv")
#     df["lon"] = df.coordinates.apply(lambda x: float(x.split(",")[0].strip()))
#     df["lat"] = df.coordinates.apply(lambda x: float(x.split(",")[1].strip()))
#     gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.lon, df.lat))

#     ret_val = describe_geodata(gdf.geometry)

#     assert isinstance(ret_val, dict)

#     with open("test_biodiv.json", "w") as f:
#         json.dump(ret_val, f)

#     assert Path("test_biodiv.json").exists()

#     # Car Rental ==============================================================
#     Setups.LoadData.setup("carrental")
#     df = pd.read_csv("data/CarRentalData.csv")
#     gdf = gpd.GeoDataFrame(
#         df,
#         geometry=gpd.points_from_xy(df["location.longitude"], df["location.latitude"]),
#     )

#     ret_val = describe_geodata(gdf.geometry)

#     assert isinstance(ret_val, dict)

#     with open("test_carrental.json", "w") as f:
#         json.dump(ret_val, f)

#     assert Path("test_carrental.json").exists()
