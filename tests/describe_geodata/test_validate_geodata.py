# import pytest

# from fair_ds_ap42.junit_xml_gen import JUnitTestSuites

# # from fair_ds_ap42.describe_geo import validate_geodata
# from fair_ds_ap42.settings import SettingsManager

# from tests.setups import Setups


# @pytest.mark.skip(reason="This section due for rework")
# def test_file_with_schema(get_data, get_schema):
#     # Setup
#     Setups.GenFile.data(get_data, schema=11)  # date, int, geopoint
#     Setups.GenFile.schema(get_schema, schema=11)

#     my_schemas = {"schemas/schema_1.json": ["data/file_1.csv"]}

#     Setups.GenFile.schemas_file(my_schemas)

#     # Test
#     validate_geodata("data/file_1.csv", "geopoint_1")

#     xml = JUnitTestSuites(filename=SettingsManager().validation_report)

#     assert xml.total_tests == 1
#     assert xml.worst_message == "SUCCESS"

#     testsuite = xml.get_testsuite("Validate Geodata|Describe Geo")
#     assert testsuite.num_tests == 1
#     assert testsuite.worst_message == "SUCCESS"

#     testcase = testsuite.get_testcase("data/file_1.csv|geo_qc_passed_geopoint_1")
#     assert testcase.name == (
#         '"data/file_1.csv": ' 'No Geospatial Quality Issues Noted in "geopoint_1"'
#     )
