from fair_ds_ap42.objects.datafile.datafile_factory import DataFileFactory
from fair_ds_ap42.objects.schema import Schema

from tests.util import gen_dummy_data, gen_schema


class Test_S3:
    def test_get_dataframe_bad_schema(self, s3):
        """
        If the user has specified the type incorrectly
        in the schema, we should still be able to load
        the dataframe.
        """
        df = gen_dummy_data()
        df.to_csv("file.csv")

        my_datafile = DataFileFactory.from_filepath("file.csv")
        schema_dict = gen_schema()
        schema_dict["fields"][5]["type"] = "integer"

        my_schema = Schema.from_raw_schema(schema_dict)
        my_datafile.assign_schema(my_schema)

        my_datafile.get_dataframe()
