import datetime

import pytest
from unittest.mock import Mock

from fair_ds_ap42.source import LocalDirectory
from fair_ds_ap42.objects.resource import Resource, remove_duplicate_resources


@pytest.fixture
def mock_resource(mock_local_source):
    def _gen_resource(filepath, source=mock_local_source):
        mock = Mock(spec=Resource)
        mock.filepath = filepath
        mock.source = source
        mock.size_in_bytes = 0
        mock.last_modified = datetime.datetime(2022, 1, 1)
        return mock

    return _gen_resource


def test_remove_duplicate_files(mock_resource):
    # Test that the remove_duplicate_files method correctly removes duplicate files

    # Create a pair of LocalDirectory sources, one of which is a child of another
    sourceA = LocalDirectory(location="./my_dir")
    sourceB = LocalDirectory(location="./my_dir/child_dir")

    # Create a list of files that contains duplicate resources
    resource1 = mock_resource("file1.csv", sourceB)
    resource2 = mock_resource("child_dir/file1.csv", sourceA)
    resource3 = mock_resource("file2.csv", sourceA)
    resource4 = mock_resource("file3.csv", sourceA)

    resources = [resource1, resource2, resource3, resource4]

    result = remove_duplicate_resources(resources)

    # We should have three resources left. The second, which is in a child directory, should be
    # removed from the list
    assert len(result) == 3
    assert result[0] == resource1
    assert result[1] == resource3
    assert result[2] == resource4
