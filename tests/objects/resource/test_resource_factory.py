import pytest

from fair_ds_ap42.objects.resource import Resource

# from fair_ds_ap42.objects.resource.resource_factory import ResourceFactory
from fair_ds_ap42.objects.resource.resource_factory import ResourceFactory


def test_init():
    # Test that we can initialize a ResourceFactory object

    # Initialize a ResourceFactory
    result = ResourceFactory()
    assert isinstance(result, ResourceFactory)


def test_get_resource(mock_source, mock_find_source):
    # Test that we can get a Resource object from a filepath when we have a source that can read
    # that filepath and a ResourceFactory that can create a Resource from that source.

    # Initialize a ResourceFactory
    factory = ResourceFactory()

    result = factory.from_filepath("file1.csv")

    # Check that we got a Resource object
    assert isinstance(result, Resource)


def test_get_resource_file_missing():
    # Test that we will raise an exception if we try to get a Resource object from a source where
    # the file does not exist.

    # Initialize a ResourceFactory
    factory = ResourceFactory()

    # Try to get a Resource object
    with pytest.raises(FileNotFoundError):
        factory.from_filepath("i_don_exist.csv")
