from fair_ds_ap42.objects.resource.get_resource_by_pattern import (
    get_resource_by_pattern,
)
from fair_ds_ap42.objects import DataFile, Schema
from fair_ds_ap42.objects.resource import Resource


def test_get_datafile_resource_by_extension(mock_find_source):
    # Test that we can get a Resource object from a filepath, and if that filepath extension
    # matches one of the extensions in the data_file_patterns property, we will get a DataFile
    # object.

    # Get a DataFile object
    result = get_resource_by_pattern("file1.csv", data_file_patterns=["*.csv"])

    # Check that we got a DataFile object
    assert isinstance(result, DataFile)


def test_get_schema_resource_by_extension(mock_find_source):
    # Test that we can get a Resource object from a filepath, and if that filepath extension
    # matches one of the extensions in the schema_file_patterns property, we will get a Schema
    # object.

    # Get a Schema object
    result = get_resource_by_pattern("schema1.json", schema_file_patterns=["*.json"])

    # Check that we got a Schema object
    assert isinstance(result, Schema)


def test_get_resource_by_extension_no_match(mock_find_source):
    # Test that we can get a Resource object from a filepath, and if that filepath extension
    # does not match any of the extensions in the data_file_patterns or schema_file_patterns
    # properties, we will get a normal Resource object.

    # Get a Resource object
    result = get_resource_by_pattern("my_file.txt")

    # Check that we got a Resource object
    assert isinstance(result, Resource)
