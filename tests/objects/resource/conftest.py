import datetime

import pytest
from unittest.mock import Mock, patch

from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.source import ISource


@pytest.fixture
def mock_source():
    # Create a mock Source
    mock = Mock(spec=ISource)

    mock.name = "Mock Source"
    mock.exists.side_effect = lambda filepath: filepath in [
        "file1.csv",
        "schema1.json",
        "my_file.txt",
    ]
    mock.get_size_in_bytes.side_effect = lambda filepath: 100
    mock.get_date_last_modified.side_effect = lambda filepath: datetime.datetime(
        2020, 1, 1
    )

    def get_reader(filepath):
        class MockDataReader(IDataReader, IJSONDataReader):
            pass

        mock = Mock(spec=MockDataReader)
        mock.filepath = filepath
        mock.read_dict.return_value = {}
        return mock

    mock.get_reader.side_effect = lambda filepath: get_reader(filepath)

    return mock


@pytest.fixture
def mock_find_source(mock_source):
    with patch(
        "fair_ds_ap42.objects.resource.resource_factory.ResourceFactory.find_source"
    ) as mock_find_source:
        mock_find_source.return_value = mock_source
        yield mock_find_source
