import datetime

import pytest
from unittest.mock import Mock

from fair_ds_ap42.objects.resource.resource import Resource
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source import ISource


@pytest.fixture
def mock_reader():
    mock = Mock(spec=IDataReader)
    mock.get_size_in_bytes.return_value = 42
    mock.get_date_last_modified.return_value = datetime.datetime(2020, 1, 1)
    yield mock


@pytest.fixture
def mock_source(mock_reader):
    mock = Mock(spec=ISource)
    mock.source_type = "local_directory"
    mock.location = "./my_dir"
    mock.to_json.return_value = {
        "type": "local_directory",
        "location": "./my_dir",
    }
    mock.get_reader.return_value = mock_reader
    return mock


def test_init(mock_source):
    # Test that we can initialize a Resource Object
    filepath = "file1.csv"
    source = mock_source

    my_resource = Resource(
        filepath=filepath,
        source=source,
    )

    assert my_resource.filepath == filepath
    assert my_resource.source.to_json() == {
        "type": "local_directory",
        "location": "./my_dir",
    }
    assert my_resource.size_in_bytes == 42
    assert my_resource.last_modified == datetime.datetime(2020, 1, 1)


def test_from_dict():
    # Test that we can iniitalize a Resource object from a dict
    filepath = "file1.csv"
    source = {"type": "local_directory", "location": "./my_dir"}
    size_in_bytes = 42
    last_modified = "2020-01-01T00:00:00Z"

    my_resource = Resource.from_dict(
        {
            "filepath": filepath,
            "source": source,
            "size_in_bytes": size_in_bytes,
            "last_modified": last_modified,
        }
    )

    assert my_resource.filepath == filepath
    assert isinstance(my_resource.source, ISource)
    source_dict = my_resource.source.to_json()
    assert source_dict["type"] == "local_directory"
    assert source_dict["location"] == "./my_dir"
    assert my_resource.size_in_bytes == size_in_bytes
    assert my_resource.last_modified == datetime.datetime(2020, 1, 1)


def test_as_dict(mock_source):
    # Test that we can convert a Resource object to a json object
    filepath = "file1.csv"
    size_in_bytes = 42
    last_modified = datetime.datetime(2020, 1, 1)

    my_resource = Resource(
        filepath=filepath,
        source=mock_source,
        size_in_bytes=size_in_bytes,
        last_modified=last_modified,
    )

    json_data = my_resource.as_dict()

    assert isinstance(json_data, dict)
    assert json_data == {
        "filepath": filepath,
        "source": {
            "type": "local_directory",
            "location": "./my_dir",
        },
        "size_in_bytes": size_in_bytes,
        "last_modified": "2020-01-01T00:00:00",
    }


def test_equality(mock_source):
    # Test that we can compare two Resource objects for equality
    resource1 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource2 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    assert resource1 == resource2


def test_inequality(mock_source):
    # Test that we can compare two Resource objects for inequality
    resource1 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource2 = Resource(
        filepath="file2.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    assert resource1 != resource2


def test_membership(mock_source):
    # Test that we can check if a Resource object is in a list
    resource1 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource2 = Resource(
        filepath="file2.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource3 = Resource(
        filepath="file3.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    assert resource1 in [resource1, resource2, resource3]
    assert resource2 not in [resource1, resource3]
    assert resource3 in [resource2, resource3]


def test_contains_modified(mock_source):
    # Test that we can determine when a list of Resources contains a modified version of a
    # Resource
    resource1 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource2 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=100,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    resource3 = Resource(
        filepath="file1.csv",
        source=mock_source,
        size_in_bytes=42,
        last_modified=datetime.datetime(2020, 1, 2),
    )

    # Test we find a modified version of the resource when size_in_bytes is different
    assert Resource.contains_modified_resource(resource1, [resource2])

    # Test we find a modified version of the resource when last_modified is different
    assert Resource.contains_modified_resource(resource1, [resource3])

    # We should not detect a modified version if the files are identical
    assert not Resource.contains_modified_resource(resource1, [resource1])
