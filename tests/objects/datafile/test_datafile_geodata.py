import pytest
from unittest.mock import MagicMock

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point

from fair_ds_ap42.objects.datafile import DataFile
from fair_ds_ap42.objects.data_column.exceptions import DataColumnFieldDoesntExist
from fair_ds_ap42.objects.schema import Schema
from fair_ds_ap42.objects.data_column import GeodataColumn
from fair_ds_ap42.source import ISource
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.itabular_data_reader import ITabularDataReader

from fair_ds_ap42.settings import settings
from fair_ds_ap42.settings.file_settings import FileSettings


@pytest.fixture
def mock_schema():
    schema_descriptor = {
        "fields": [
            {"name": "data", "type": "object"},
            {"name": "lat", "type": "number"},
            {"name": "lon", "type": "number"},
        ]
    }
    mock_schema = Schema.from_raw_schema(schema_descriptor)
    yield mock_schema


@pytest.fixture
def mock_reader():
    class MockReader(ITabularDataReader, IDataReader):
        pass

    mock = MagicMock(spec=MockReader)
    df = pd.DataFrame(
        {
            "data": ["a", "b", "c"],
            "lat": [1, 2, 3],
            "lon": [4, 5, 6],
        }
    )
    mock.read_data.return_value = df
    mock.read_column.side_effect = lambda col_name: df[col_name]
    mock.get_fields.return_value = list(df.columns)
    return mock


@pytest.fixture
def local_source(mock_reader):
    mock = MagicMock(spec=ISource)
    mock.get_reader.return_value = mock_reader
    return mock


def test_has_constructed_geodata(local_source):
    settings.per_file.files = [
        FileSettings(filename="test.csv", latitude="lat", longitude="lon")
    ]
    my_datafile = DataFile(filepath="test.csv", source=local_source)
    assert my_datafile.has_geodata
    assert my_datafile.has_constructed_geodata


def test_has_geodata_column(local_source, mock_schema):
    # If the datafile has constructed geodata, it should have a geodata column in the
    # "fields" property
    settings.per_file.files = [
        FileSettings(filename="test.csv", latitude="lat", longitude="lon")
    ]
    my_datafile = DataFile(filepath="test.csv", source=local_source)
    my_datafile.schema = mock_schema

    assert "_geodata" in my_datafile.fields

    # The expected value of the geodata column is a GeodataColumn object
    geo_column = my_datafile.get_column("_geodata")
    assert isinstance(geo_column, GeodataColumn)
    pd.testing.assert_series_equal(geo_column.data, pd.Series([(4, 1), (5, 2), (6, 3)]))


def test_has_no_geodata_column(local_source, mock_schema):
    # If the datafile has no constructed geodata, it should not have a geodata column, and we
    # should get an exception if we try to get the geodata column
    settings.per_file.files = [
        FileSettings(filename="test.csv", latitude="lat", longitude="lon")
    ]
    my_datafile = DataFile(filepath="no_geodata.csv", source=local_source)
    my_datafile.schema = mock_schema

    with pytest.raises(DataColumnFieldDoesntExist):
        my_datafile.get_column("_geodata")


def test_get_geodataframe(local_source, mock_schema):
    # If the datafile has constructed geodata, we should be able to get a geodataframe
    # from it with the get_geodataframe method

    settings.per_file.files = [
        FileSettings(filename="test.csv", latitude="lat", longitude="lon")
    ]
    my_datafile = DataFile(filepath="test.csv", source=local_source)
    my_datafile.schema = mock_schema

    gdf = my_datafile.get_geodataframe()

    assert isinstance(gdf, gpd.GeoDataFrame)


def test_get_geodataframe_no_geodata(local_source, mock_schema):
    # If the datafile has no constructed geodata, we should get an exception if we try to
    # get a geodataframe from it
    settings.per_file.files = [
        FileSettings(filename="test.csv", latitude="lat", longitude="lon")
    ]
    my_datafile = DataFile(filepath="no_geodata.csv", source=local_source)
    my_datafile.schema = mock_schema

    assert not my_datafile.has_constructed_geodata
    with pytest.raises(DataColumnFieldDoesntExist):
        my_datafile.get_geodataframe()


@pytest.fixture
def mock_geodata_schema():
    schema_descriptor = {
        "fields": [
            {"name": "data", "type": "object"},
            {"name": "coordinates", "type": "geopoint", "format": "array"},
        ]
    }
    mock_schema = Schema.from_raw_schema(schema_descriptor)
    yield mock_schema


@pytest.fixture
def mock_geodata_reader(mock_reader):
    class MockReader(ITabularDataReader, IDataReader):
        pass

    mock = MagicMock(spec=MockReader)
    df = pd.DataFrame(
        {
            "data": ["a", "b", "c"],
            "coordinates": [(1, 4), (2, 5), (3, 6)],
        }
    )
    mock.read_data.return_value = df
    mock.read_column.side_effect = lambda col_name: df[col_name]
    mock.get_fields.return_value = list(df.columns)
    return mock


def test_has_native_geodata(local_source, mock_geodata_reader, mock_geodata_schema):
    # If the user provided a datafile that contains geodata in one of the frictionless formats,
    # and the field is specified as such in the schema, then the datafile should indicate that
    # it has geodata, but that it does not have constructed geodata

    local_source.get_reader.return_value = mock_geodata_reader

    my_datafile = DataFile(filepath="has_geodata.csv", source=local_source)
    my_datafile.schema = mock_geodata_schema

    # We should have only the two columns in the datafile as fields
    assert len(my_datafile.fields) == 2
    assert "data" in my_datafile.fields
    assert "coordinates" in my_datafile.fields

    # Explicitly check that we don't have a geodata column
    assert "_geodata" not in my_datafile.fields

    # The datafile should indicate that it has geodata, but that it does not have constructed
    # geodata
    assert my_datafile.has_geodata
    assert not my_datafile.has_constructed_geodata

    # We should be able to get a geodataframe from the datafile
    gdf = my_datafile.get_geodataframe()
    assert isinstance(gdf, gpd.GeoDataFrame)
    # The geodataframe should have the two columns in the datafile, and an additional "geometry"
    # column that contains the geodata in shapely Point objects
    assert len(gdf.columns) == 3
    assert "data" in gdf.columns
    assert "coordinates" in gdf.columns
    assert "geometry" in gdf.columns

    assert gdf["geometry"].apply(lambda x: isinstance(x, Point)).all()
