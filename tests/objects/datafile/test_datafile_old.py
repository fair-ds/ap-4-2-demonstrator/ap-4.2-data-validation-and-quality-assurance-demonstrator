import datetime
import json
import os
from pathlib import Path

import pandas as pd
import pytest


from fair_ds_ap42.objects.schema import Schema
from fair_ds_ap42.objects.datafile.datafile_factory import DataFileFactory
from fair_ds_ap42.objects.data_column import (
    DataColumn,
    NumericColumn,
    TextColumn,
    GeodataColumn,
)
from fair_ds_ap42.objects.data_column.exceptions import DataColumnFieldDoesntExist
from fair_ds_ap42.utils.s3_utils import file_exists_s3

from fair_ds_ap42.settings import settings, SettingsManager

from tests.util import gen_dummy_data, gen_schema


def test_get_field_types():
    """We should be able to get a list of all of the field types present in
    the data file"""
    my_file = "data/file_1.csv"
    gen_dummy_data().to_csv(my_file)
    my_resource = DataFileFactory.from_filepath(my_file)
    my_schema = Schema.from_raw_schema(gen_schema())
    my_resource.assign_schema(my_schema)

    assert "numeric" in my_resource.field_types
    assert "text" in my_resource.field_types
    assert "date" in my_resource.field_types
    assert "boolean" in my_resource.field_types
    assert "geodata" in my_resource.field_types

    gen_dummy_data(42).to_csv(my_file)
    my_resource = DataFileFactory.from_filepath(my_file)
    my_schema = Schema.from_raw_schema(gen_schema(42))
    my_resource.assign_schema(my_schema)

    assert "numeric" in my_resource.field_types
    assert "categorical" in my_resource.field_types
    assert "date" not in my_resource.field_types
    assert "boolean" not in my_resource.field_types
    assert "geodata" in my_resource.field_types


class Test_LocalDirectory:
    @pytest.mark.parametrize("file_type", ["csv", "parquet"])
    def test_create_resource(self, file_type):
        # Setup
        my_file = f"data/test_file.{file_type}"
        df = gen_dummy_data()
        if file_type == "csv":
            df.to_csv(my_file)
        elif file_type == "parquet":
            df.to_parquet(my_file)

        # Test
        my_resource = DataFileFactory.from_filepath(my_file)

        assert my_resource.filepath == my_file
        assert my_resource.format == file_type
        assert my_resource.source.name == "local"
        assert set(df.columns).issubset(
            set(my_resource.fields)
        ), "fields parameter should have the same columns as the file"
        assert isinstance(my_resource.size_in_bytes, int)
        assert isinstance(my_resource.last_modified, datetime.datetime)

    @pytest.mark.parametrize("file_type", ["csv", "parquet"])
    def test_get_column(self, file_type):
        # Setup
        my_file = f"data/test_file.{file_type}"
        if file_type == "csv":
            gen_dummy_data().to_csv(my_file)
        elif file_type == "parquet":
            gen_dummy_data().to_parquet(my_file)

        my_schema = Schema.from_raw_schema(gen_schema())

        # Test
        my_resource = DataFileFactory.from_filepath(my_file)
        my_resource.assign_schema(my_schema)

        my_data = my_resource.get_column("float_1")
        assert isinstance(my_data, NumericColumn)
        assert my_data.description["Count"] == 10
        assert my_data.description["Unique"] == 10
        assert my_data.description["Missing"] == 0.0

        my_data = my_resource.get_column("str_1")
        assert isinstance(my_data, TextColumn)
        assert my_data.description["Count"] == 10
        assert my_data.description["Unique"] == 3
        assert my_data.description["Missing"] == 0

    def test_missing_file(self):
        # We should not be able to create a DataFile object from a file that does
        # not exist

        # Confirm Setup
        assert not Path("data/test_file.csv").exists()

        with pytest.raises(FileNotFoundError):
            DataFileFactory.from_filepath("data/test_file.csv")


class Test_S3Source:
    # TODO: @pytest.mark.parametrize("file_type", ["csv", "parquet"])
    @pytest.mark.parametrize("file_type", ["csv"])
    def test_create_resource(self, s3, file_type):
        # Setup
        s3.create_bucket(Bucket="testbucket")
        settings.storage.s3.bucket = "testbucket"

        my_file = f"data/test_file.{file_type}"
        df = gen_dummy_data()
        if file_type == "csv":
            df.to_csv(my_file)
        elif file_type == "parquet":
            df.to_parquet(my_file)
        all_columns = [df.index.name] + list(df.columns)

        s3.upload_file(my_file, "testbucket", my_file)
        os.remove(my_file)

        # Test
        my_resource = DataFileFactory.from_filepath(my_file)

        assert my_resource.filepath == my_file
        assert my_resource.format == file_type
        assert my_resource.source.name == "s3"

        assert set(all_columns) == set(
            my_resource.fields
        ), "fields parameter should have the same columns as the file"
        assert isinstance(my_resource.size_in_bytes, int)
        assert isinstance(my_resource.last_modified, datetime.datetime)

    # TODO: @pytest.mark.parametrize("file_type", ["csv", "parquet"])
    @pytest.mark.parametrize("file_type", ["csv"])
    def test_get_column(self, s3, file_type):
        # Setup
        my_file = f"data/test_file.{file_type}"
        if file_type == "csv":
            gen_dummy_data().to_csv(my_file)
        elif file_type == "parquet":
            gen_dummy_data().to_parquet(my_file)

        s3.create_bucket(Bucket="testbucket")
        settings.storage.s3.bucket = "testbucket"
        s3.upload_file(Filename=my_file, Bucket="testbucket", Key=my_file)
        os.remove(my_file)

        my_schema = Schema.from_raw_schema(gen_schema())

        # Test
        my_resource = DataFileFactory.from_filepath(my_file)
        my_resource.assign_schema(my_schema)

        with pytest.raises(DataColumnFieldDoesntExist):
            my_data = my_resource.get_column("name")

        my_data = my_resource.get_column("float_1")
        assert isinstance(my_data, DataColumn)
        assert isinstance(my_data, NumericColumn)
        assert my_data.description["Count"] == 10
        assert my_data.description["Unique"] == 10
        assert my_data.description["Missing"] == 0.0

        my_data = my_resource.get_column("str_1")
        assert isinstance(my_data, TextColumn)
        assert my_data.description["Count"] == 10
        assert my_data.description["Unique"] == 3
        assert my_data.description["Missing"] == 0

    def test_missing_file(self, s3):
        # We should not be able to create a DataFile object from a file that does
        # not exist

        # Confirm Setup
        s3.create_bucket(Bucket="testbucket")
        settings.storage.s3.bucket = "testbucket"
        assert not file_exists_s3("data/test_file.csv")

        with pytest.raises(FileNotFoundError):
            DataFileFactory.from_filepath("data/test_file.csv")


def test_unsupported_format():
    """
    We should be able to create a DataFile object with an invalid filetype,
    but trying to perform any operations where we get data should fail.
    """
    # Setup
    with open("data/file_1.txt", "w") as f:
        f.write("this is a file")

    # Confirm Setup
    assert Path("data/file_1.txt").exists()

    # Test
    with pytest.raises(NotImplementedError):
        DataFileFactory.from_filepath("data/file_1.txt")


def test_assign_schema_file():
    """
    When we have a schema file we want to associate with a data file, we should
    check to make sure it broadly applies to that file - i.e. that they share
    identical field names.
    """
    data_filepath = Path("data/test_file.csv")
    pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]}).to_csv(
        data_filepath, index=None
    )
    my_datafile = DataFileFactory.from_filepath(data_filepath)

    schema_filepath = Path("schemas/schema_1.json")
    schema_data = {
        "fields": [
            {"name": "id", "type": "integer"},
            {"name": "name", "type": "string"},
        ]
    }

    with open(schema_filepath, "w") as f:
        json.dump(schema_data, f)
    my_schema = Schema.from_file(schema_filepath)

    # We should be able to check that the schema will generally match the
    # data file
    assert my_datafile.compatible_schema(my_schema)

    # We should be able to add the schema to the datafile
    my_datafile.assign_schema(my_schema)
    assert my_datafile.schema == my_schema

    testcase_ids = [x.testcase_id for x in my_datafile.test_cases]
    assert "data_file_located_data/test_file.csv" in testcase_ids
    assert (
        "schema_applied_no_error_schemas/schema_1.json_data/test_file.csv"
        in testcase_ids
    )
    assert (
        "schema_applied_no_error_schemas/schema_1.json_data/test_file.csv"
        in testcase_ids
    )


def test_assign_bad_schema_file():
    """
    When we have a schema file and a data file that are not compatible, we
    should not be able to assign it to a datafile without an exception arising
    """

    data_filepath = Path("data/test_file.csv")
    pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]}).to_csv(
        data_filepath, index=None
    )
    my_datafile = DataFileFactory.from_filepath(data_filepath)

    schema_filepath = Path("schemas/schema_1.json")
    schema_data = {
        "fields": [
            {"name": "new_id", "type": "integer"},
            {"name": "name", "type": "string"},
        ]
    }

    with open(schema_filepath, "w") as f:
        json.dump(schema_data, f)
    my_schema = Schema.from_file(schema_filepath)

    # We should be able to check that the schema will generally match the
    # data file (it should not)
    assert not my_datafile.compatible_schema(my_schema)

    # We should still be able to add this schema to the datafile, but we
    # should see a error in the log
    my_datafile.assign_schema(my_schema)

    testcase_ids = [x.testcase_id for x in my_datafile.test_cases]

    assert "data_file_located_data/test_file.csv" in testcase_ids
    assert (
        "schema_file_fail_assigned_schemas/schema_1.json_data/test_file.csv"
        in testcase_ids
    )


class Test_Geodata:
    """
    Tests related to loading a data file with various formats of geodata
    """

    def test_simple(self):
        gen_dummy_data().to_csv("data/file_1.csv")
        my_datafile = DataFileFactory.from_filepath("data/file_1.csv")
        my_schema = Schema.from_raw_schema(gen_schema())
        my_datafile.assign_schema(my_schema)

        assert isinstance(my_datafile.get_column("geopoint_1"), GeodataColumn)
        assert "geodata" in my_datafile.field_types
        assert not my_datafile.has_constructed_geodata

    def test_merge_column_data(self, monkeypatch):
        """
        We want to be able to support the situation where a user has two
        columns of data - like "lat" and "lon" that should together make a
        geodata column.

        We should be able to do this as long as the user has specified the
        columns in the settings file.
        """
        from fair_ds_ap42.settings import settings

        my_settings = SettingsManager(
            config={
                "FILE_DETAILS": {
                    "FILE": [
                        {
                            "filename": "data/file_1.csv",
                            "latitude": "lat",
                            "longitude": "lon",
                        }
                    ]
                }
            }
        )
        monkeypatch.setattr(settings, "per_file", my_settings.per_file)

        pd.DataFrame(
            {
                "foo": [4, 7, 2],
                "bar": ["bizz", "fizz", "buzz"],
                "lat": [-75.2727, -49.1555, -36.4132],
                "lon": [38.4498, 91.0885, 57.2713],
            }
        ).to_csv("data/file_1.csv", index=None)

        my_datafile = DataFileFactory.from_filepath("data/file_1.csv")
        schema = {
            "fields": [
                {"name": "foo", "type": "integer"},
                {"name": "bar", "type": "string"},
                {"name": "lat", "type": "number"},
                {"name": "lon", "type": "number"},
            ]
        }
        my_schema = Schema.from_raw_schema(schema)
        my_datafile.assign_schema(my_schema)

        # We should be able to get all of the data as their own column
        assert isinstance(my_datafile.get_column("foo"), NumericColumn)
        assert isinstance(my_datafile.get_column("bar"), TextColumn)
        assert isinstance(my_datafile.get_column("lat"), NumericColumn)
        assert isinstance(my_datafile.get_column("lon"), NumericColumn)

        # There should be a special extra column called "geopoint" we can
        # access that is a zipped together version of the lat/lon data
        assert my_datafile.has_constructed_geodata
        assert "geodata" in my_datafile.field_types
        assert isinstance(my_datafile.get_column("_geodata"), GeodataColumn)

        # The underlying dataframe shouldn't change, this is all so that we can
        # create this column on the fly
        my_df = my_datafile.get_dataframe()
        assert set(my_df.columns) == {"foo", "bar", "lat", "lon"}


class Test_GetDataFrame:
    def test_missing_string_data(self):
        schema_data = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name_with_empty", "type": "string"},
                {
                    "name": "name_required",
                    "type": "string",
                    "constraints": {"required": True},
                },
            ]
        }
        my_schema = Schema.from_raw_schema(schema_data)
        pd.DataFrame(
            {
                "id": [1, 2, 3, 4, 5],
                "name_with_empty": ["foo", "bar", None, None, "fuzz"],
                "name_required": ["foo", "bar", None, None, "fuzz"],
            }
        ).to_csv("file_1.csv", index=None)
        my_datafile = DataFileFactory.from_filepath("file_1.csv")
        my_datafile.assign_schema(my_schema)

        assert not my_datafile.get_dataframe().name_with_empty.isnull().any()
        assert my_datafile.get_dataframe().name_required.isnull().sum() == 2


class Test_SelfValidation:
    def test_bad_type(self):
        df = gen_dummy_data()
        df.to_csv("file.csv")

        my_datafile = DataFileFactory.from_filepath("file.csv")
        schema_dict = gen_schema()
        schema_dict["fields"][5]["type"] = "integer"

        my_schema = Schema.from_raw_schema(schema_dict)
        my_datafile.assign_schema(my_schema)

        for testcase in my_datafile.test_cases:
            if "schema_violation_type-error" in testcase.testcase_id:
                assert testcase.fail_type == "ERROR"
