import datetime
import pytest
from unittest.mock import MagicMock

import pandas as pd

from fair_ds_ap42.objects import DataFile
from fair_ds_ap42.objects.schema import Schema
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.itabular_data_reader import ITabularDataReader
from fair_ds_ap42.source import ISource


from tests.test_utils.source_mocks import LocalDirectoryMock


@pytest.fixture
def mock_datareader():
    class MockDataReader(IDataReader, ITabularDataReader):
        pass

    mock = MagicMock(spec=MockDataReader)
    mock.filepath = "mock_filepath"
    mock.read_data.return_value = pd.DataFrame(
        {
            "bool_col": ["T", "F", "T", "F", "T"],
            "int_col": [1, 2, 3, 4, 5],
        }
    )
    mock.get_size_in_bytes.return_value = 100
    mock.get_date_last_modified.return_value = datetime.datetime(2020, 1, 1)
    return mock


@pytest.fixture
def mock_source(mock_datareader):
    mock = MagicMock(spec=ISource)
    mock.get_reader.return_value = mock_datareader
    return mock


@pytest.fixture
def mock_schema():
    mock = MagicMock(spec=Schema)
    mock.filepath = "mock_filepath"
    return mock


def test_get_dataframe_convert_bool(mock_source):
    # If we are given a dataframe that contains boolean values as strings, but the schema
    # provides a mapping to convert those strings to booleans, we should get a dataframe
    # with the column converted to booleans
    my_schema = Schema.from_raw_schema(
        {
            "fields": [
                {
                    "name": "bool_col",
                    "type": "boolean",
                    "trueValues": ["T"],
                    "falseValues": ["F"],
                },
                {"name": "int_col", "type": "integer"},
            ]
        }
    )

    my_datafile = DataFile(filepath="testfile", source=mock_source)
    my_datafile.schema = my_schema

    pd.testing.assert_frame_equal(
        my_datafile.get_dataframe(),
        pd.DataFrame(
            {
                "bool_col": [True, False, True, False, True],
                "int_col": [1, 2, 3, 4, 5],
            }
        ),
    )


def test_datafile_from_dict():
    # We should be able to create a DataFile from a dictionary that contains metadata about the
    # DataFile.

    # Create a dictionary
    my_dict = {
        "filepath": "file1.csv",
        "source": {"type": "local_directory", "location": "./my_dir"},
        "size_in_bytes": 100,
        "last_modified": "2020-01-01T00:00:00Z",
    }

    # Create a DataFile from the dictionary
    result = DataFile.from_dict(my_dict)

    # Check that the DataFile was created correctly
    assert result.filepath == "file1.csv"
    assert isinstance(result.source, ISource)
    assert result.source.name == "local"
    assert result.source.location == "./my_dir"
    assert result.size_in_bytes == 100
    assert result.last_modified == datetime.datetime(2020, 1, 1)


def test_datafile_equivalance():
    # Check that we can determine if two DataFiles are equivalent.
    mock_local_source = LocalDirectoryMock()

    # Create two DataFiles
    datafile1 = DataFile(
        filepath="file1.csv",
        source=mock_local_source,
        size_in_bytes=100,
        last_modified=datetime.datetime(2020, 1, 1),
    )
    datafile2 = DataFile(
        filepath="file1.csv",
        source=mock_local_source,
        size_in_bytes=100,
        last_modified=datetime.datetime(2020, 1, 1),
    )

    # Check that they are equivalent
    assert datafile1 == datafile2
