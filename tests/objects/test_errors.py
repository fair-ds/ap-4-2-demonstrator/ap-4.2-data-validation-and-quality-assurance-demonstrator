from fair_ds_ap42.objects.errors import DataFileTestCase

from fair_ds_ap42.junit_xml_gen import JUnitTestSuites


def test_success_error():
    err = DataFileTestCase(filename="file1.csv", case="success")

    assert err.testcase_id == "data_file_located_file1.csv"
    assert err.fail_type == "SUCCESS"
    assert err.msg == "Sucessfully Located Data File: file1.csv"
    assert err.testsuite_id == "locate_data"

    testsuites = JUnitTestSuites("test")

    err.add_to_xml(testsuites)

    testsuite = testsuites.get_testsuite("locate_data")
    testcase = testsuite.get_testcase("data_file_located_file1.csv")

    child_elements = [t for t in testcase.to_xml().findall("failure")]
    assert len(child_elements) == 0


def test_error_error():
    err = DataFileTestCase(filename="file1.csv", case="missing")

    assert err.testcase_id == "data_file_missing_file1.csv"
    assert err.fail_type == "ERROR"
    assert err.msg == "Unable to locate data file file1.csv"
    assert err.testsuite_id == "locate_data"

    testsuites = JUnitTestSuites("test")

    err.add_to_xml(testsuites)

    testsuite = testsuites.get_testsuite("locate_data")
    testcase = testsuite.get_testcase("data_file_missing_file1.csv")

    child_elements = [t for t in testcase.to_xml().findall("failure")]
    assert len(child_elements) == 1
