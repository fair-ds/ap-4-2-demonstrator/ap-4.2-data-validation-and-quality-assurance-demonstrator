import pytest
from unittest.mock import Mock

from fair_ds_ap42.objects.validator.schema_assignment_validator import (
    SchemasAssignmentValidator,
)


@pytest.fixture
def mock_schema_assignment_reader():
    # Create a mock IJSONDataReader that will return a mock SchemaAssignment
    mock_reader = Mock()
    mock_reader.filepath = "test.json"
    mock_reader.exists = True
    mock_reader.format == "json"
    mock_reader.file_format = "foo"
    mock_reader.read_dict.return_value = {"schemas": [], "data_files": []}
    return mock_reader


def test_file_found(mock_schema_assignment_reader):
    # Test that we properly detect when the file is found

    # Initialize a SchemasAssignmentValidator
    validator = SchemasAssignmentValidator(reader=mock_schema_assignment_reader)

    # Run the validator
    validator.validate_self()

    # Check that the validator has been run
    assert validator.validation_run

    # We should have one test case indicating that the file was found
    assert len(validator.test_cases) == 1
    test_case = validator.test_cases[0]
    assert test_case.testsuite_id == "locate_data"
    assert test_case.testcase_id == "schema_assignment_file_validated"


def test_missing_file(mock_schema_assignment_reader):
    # Test that we properly detect when the file is missing

    # Set the exists property to False
    mock_schema_assignment_reader.exists = False

    # Initialize a SchemasAssignmentValidator
    validator = SchemasAssignmentValidator(reader=mock_schema_assignment_reader)

    # Run the validator
    validator.validate_self()

    # Check that the validator has been run
    assert validator.validation_run

    # We should have one test case indicating that the file was missing
    assert len(validator.test_cases) == 1
    test_case = validator.test_cases[0]
    assert test_case.testsuite_id == "locate_data"
    assert test_case.testcase_id == "schema_assignment_file_missing"


def test_invalid_file(mock_schema_assignment_reader):
    # Test that we properly detect when the file is invalid in some way

    # Set the read_dict method to return somethingthat's not a dict
    mock_schema_assignment_reader.read_dict.return_value = "foo"

    # Initialize a SchemasAssignmentValidator
    validator = SchemasAssignmentValidator(reader=mock_schema_assignment_reader)

    # Run the validator
    validator.validate_self()

    # Check that the validator has been run
    assert validator.validation_run

    # We should have one test case indicating that the file was invalid
    assert len(validator.test_cases) == 1
    test_case = validator.test_cases[0]
    assert test_case.testsuite_id == "locate_data"
    assert test_case.testcase_id == "schema_assignment_file_invalid"
