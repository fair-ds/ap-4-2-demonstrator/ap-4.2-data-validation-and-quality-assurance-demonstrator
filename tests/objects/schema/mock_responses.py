# flake8: noqa: E501

# Mock Response for the /api/ontologies/{onto} endpoint
ONTOLOGY_RESPONSE = {
    "mock_endpoint": "http://example.com/api/test_ontology",
    "mock_response": {
        "ontologyId": "test_ontology",
        "loaded": "2020-01-01T00:00:00.000+0000",
        "updated": "2020-01-02T00:00:00.000+0000",
        "config": {
            "id": "http://my_onto.test.com/test_ontology/terms/",
            "title": "Test Ontology",
            "namespace": "test",
            "preferredPrefix": "TEST",
            "description": "This is a test ontology",
            "homepage": "http://my_onto.test.com",
            "version": "1.0.0",
            "license": {
                "url": "https://creativecommons.org/licenses/by/4.0/",
            },
        },
    },
}

# Mock Response for the /api/ontologies/{onto}/{termId}/terminstances endpoint
TERMINSTANCES_RESPONSE = {
    "mock_endpoint": "http://example.com/api/test_ontology/http%253A%252F%252Fmy_onto.test.com%252Ftest_ontology%252Fterms%252Ftest_ontology_class/terminstances",  # noqa
    "mock_response": {
        "_embedded": {
            "individuals": [
                {
                    "iri": "http://my_onto.test.com/test_ontology/terms/foo",
                    "label": "foo",
                    "description": ["This is a foo"],
                    "type": [
                        {
                            "description": ["This is a test type description"],
                        }
                    ],
                },
                {
                    "iri": "http://my_onto.test.com/test_ontology/terms/bar",
                    "label": "bar",
                    "description": ["This is a bar"],
                    "type": [
                        {
                            "description": ["This is a test type description"],
                        }
                    ],
                },
            ]
        }
    },
}
