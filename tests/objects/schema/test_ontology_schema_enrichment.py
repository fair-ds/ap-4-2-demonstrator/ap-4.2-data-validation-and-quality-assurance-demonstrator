import pytest
import requests_mock

import datetime

from fair_ds_ap42.objects.schema.ontology_schema_enrichment import (
    OntologySchemaEnrichment,
)
from fair_ds_ap42.objects.schema.schema import Schema

from tests.objects.schema.mock_responses import (
    ONTOLOGY_RESPONSE,
    TERMINSTANCES_RESPONSE,
)


@pytest.fixture
def mock_schema_data():
    return {
        "fields": [
            {
                "name": "field1",
                "type": "string",
            },
            {
                "name": "field2",
                "type": "string",
                "ontology": "test_ontology::test_ontology_class",
            },
            {
                "name": "field3",
                "type": "string",
                "ontology": "test_ontology::test_ontology_class",
            },
        ],
    }


@pytest.fixture(autouse=True)
def mock_api_endpoints():
    with requests_mock.mock() as m:
        # Mock the API endpoints you need for your tests
        m.get(
            ONTOLOGY_RESPONSE["mock_endpoint"],
            json=ONTOLOGY_RESPONSE["mock_response"],
            status_code=200,
        )

        m.get(
            TERMINSTANCES_RESPONSE["mock_endpoint"],
            json=TERMINSTANCES_RESPONSE["mock_response"],
            status_code=200,
        )

        yield m


@pytest.fixture(autouse=True)
def set_api_url():
    OntologySchemaEnrichment.API_URL = "http://example.com/api"
    yield


def test_init():
    # Test that we can initialize a OntologySchemaEnrichment
    result = OntologySchemaEnrichment()

    # Check that the OntologySchemaEnrichment was created correctly
    assert isinstance(result, OntologySchemaEnrichment)


def test_get_ontology_data():
    result = OntologySchemaEnrichment.get_ontology_data(ontology="test_ontology")

    assert isinstance(result, dict)

    assert result["ontologyId"] == "test_ontology"
    assert result["loaded"] == datetime.datetime(
        2020, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
    )
    assert result["updated"] == datetime.datetime(
        2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
    )
    assert result["title"] == "Test Ontology"
    assert result["description"] == "This is a test ontology"
    assert result["namespace"] == "test"
    assert result["preferredPrefix"] == "TEST"
    assert result["config_id"] == "http://my_onto.test.com/test_ontology/terms/"
    assert result["homepage"] == "http://my_onto.test.com"
    assert result["version"] == "1.0.0"
    assert result["license"] == "https://creativecommons.org/licenses/by/4.0/"


def test_get_ontology_term_instances():
    result = OntologySchemaEnrichment.get_ontology_term_instances(
        ontology="test_ontology", term="test_ontology_class"
    )

    assert isinstance(result, list)
    assert all(isinstance(value, dict) for value in result)

    term1 = result[0]
    assert term1["iri"] == "http://my_onto.test.com/test_ontology/terms/foo"
    assert term1["label"] == "foo"
    assert term1["description"] == "This is a foo"
    assert term1["type_description"] == "This is a test type description"

    term2 = result[1]
    assert term2["iri"] == "http://my_onto.test.com/test_ontology/terms/bar"
    assert term2["label"] == "bar"
    assert term2["description"] == "This is a bar"
    assert term2["type_description"] == "This is a test type description"


def test_enrich_schema_field(mock_schema_data):
    field_to_enrich = mock_schema_data["fields"][1]

    result = OntologySchemaEnrichment.enrich_schema_field(field=field_to_enrich)

    assert result == {
        "name": "field2",
        "type": "string",
        "ontology": "test_ontology::test_ontology_class",
        "title": "test_ontology_class",
        "description": "This is a test type description",
        "ontology_name": "Test Ontology",
        "date_issued": datetime.datetime(
            2020, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
        ),
        "date_modified": datetime.datetime(
            2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
        ),
        "constraints": {"enum": ["foo", "bar"]},
    }


def test_enrich_schema(mock_schema_data):
    # Test that we can enrich a Schema with the information from the TiB Ontology service
    result = OntologySchemaEnrichment().get_enriched_schema(raw_schema=mock_schema_data)

    assert result == {
        "fields": [
            {
                "name": "field1",
                "type": "string",
            },
            {
                "name": "field2",
                "type": "string",
                "ontology": "test_ontology::test_ontology_class",
                "title": "test_ontology_class",
                "description": "This is a test type description",
                "ontology_name": "Test Ontology",
                "date_issued": datetime.datetime(
                    2020, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
                ),
                "date_modified": datetime.datetime(
                    2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
                ),
                "constraints": {"enum": ["foo", "bar"]},
            },
            {
                "name": "field3",
                "type": "string",
                "ontology": "test_ontology::test_ontology_class",
                "title": "test_ontology_class",
                "description": "This is a test type description",
                "ontology_name": "Test Ontology",
                "date_issued": datetime.datetime(
                    2020, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
                ),
                "date_modified": datetime.datetime(
                    2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
                ),
                "constraints": {"enum": ["foo", "bar"]},
            },
        ],
    }


def test_enrich_schema_caching(mock_schema_data, mock_api_endpoints):
    # Test that when we enrich a Schema with the information from the TiB Ontology service,
    # we cache the results so that we don't make multiple requests to the same endpoint

    # Clear the cache
    OntologySchemaEnrichment._cache = {}

    # Enrich the schema twice
    result1 = OntologySchemaEnrichment().get_enriched_schema(
        raw_schema=mock_schema_data
    )

    result2 = OntologySchemaEnrichment().get_enriched_schema(
        raw_schema=mock_schema_data
    )

    # Check that the results are the same
    assert result1 == result2

    # Check that we only make a single request to each endpoint
    assert mock_api_endpoints.call_count == 2


def test_automatic_schema_enrichment(mock_schema_data):
    # When we create a schema file, we should automatically enrich it with the information
    # from the TiB Ontology service

    my_schema = Schema.from_raw_schema(mock_schema_data)

    schema_fields = my_schema.get_schema_data()["fields"]

    assert schema_fields[0] == {
        "name": "field1",
        "type": "string",
    }

    assert schema_fields[1] == {
        "name": "field2",
        "type": "string",
        "ontology": "test_ontology::test_ontology_class",
        "title": "test_ontology_class",
        "description": "This is a test type description",
        "ontology_name": "Test Ontology",
        "date_issued": datetime.datetime(
            2020, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
        ),
        "date_modified": datetime.datetime(
            2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
        ),
        "constraints": {"enum": ["foo", "bar"]},
    }


def test_enrich_existing_schema_data():
    # It may be the case that the user provides some constriants in additionl to the
    # ontology. In this case we want to ensure that we don't overwrite the user's
    # constraints

    mock_field_data = {
        "name": "field2",
        "type": "string",
        "ontology": "test_ontology::test_ontology_class",
        "constraints": {
            "required": True,
        },
    }

    result = OntologySchemaEnrichment.enrich_schema_field(field=mock_field_data)

    # Assert that we have enriched the schema, but that the user constraints have not
    # been overwritten

    assert result["name"] == "field2"
    assert result["type"] == "string"
    assert result["ontology"] == "test_ontology::test_ontology_class"
    assert result["title"] == "test_ontology_class"
    assert result["description"] == "This is a test type description"
    assert result["ontology_name"] == "Test Ontology"

    field_constraints = result["constraints"]
    assert field_constraints["required"] is True
    assert field_constraints["enum"] == ["foo", "bar"]
