import pytest
from unittest.mock import MagicMock

import datetime

import pandas as pd

from fair_ds_ap42.objects import DataFile
from fair_ds_ap42.objects.schema import Schema
from fair_ds_ap42.source.isource import ISource


@pytest.fixture
def mock_datafile(request):
    mock = MagicMock(spec=DataFile)
    mock.filepath = "mock_filepath"
    return mock


def test_validate_datafile_clean_booleans(mock_datafile):
    # A dataframe with a column of boolean values and a column of integers
    df = pd.DataFrame(
        {
            "bool_col": [True, False, True, False, True],
            "int_col": [1, 2, 3, 4, 5],
        }
    )
    mock_datafile.get_dataframe.return_value = df

    my_schema = Schema.from_raw_schema(
        {
            "fields": [
                {"name": "bool_col", "type": "boolean"},
                {"name": "int_col", "type": "integer"},
            ]
        }
    )

    violations = my_schema.validate_datafile(mock_datafile)

    # We should have one "violation", which is just a message saying everything is fine
    assert len(violations) == 1


def test_init_schema_from_dict():
    # Test that we can initialize a Schema from a dictionary
    my_dict = {
        "filepath": "schema1.json",
        "source": {"type": "local_directory", "location": "./my_dir"},
        "size_in_bytes": 100,
        "last_modified": "2020-01-01T00:00:00Z",
    }

    # Create a Schema from the dictionary
    result = Schema.from_dict(my_dict)

    # Check that the Schema was created correctly
    assert result.filepath == "schema1.json"
    assert isinstance(result.source, ISource)
    assert result.source.name == "local"
    assert result.source.location == "./my_dir"
    assert result.size_in_bytes == 100
    assert result.last_modified == datetime.datetime(2020, 1, 1)
