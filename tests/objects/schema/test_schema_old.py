import datetime
import json
import os
from pathlib import Path

import frictionless
import pandas as pd
import pytest

from fair_ds_ap42.objects.datafile.datafile_factory import DataFileFactory
from fair_ds_ap42.objects.schema import Schema
from fair_ds_ap42.settings import settings

from tests.util import gen_dummy_data


class Test_LocalDirectory:
    @pytest.mark.parametrize(
        "filepath", ["schemas/schema_1.json", Path("schemas/schema_1.json")]
    )
    def test_initialize(self, filepath, s3):
        """
        We should be able to initialize this object with either a string or
        Path
        """
        settings.storage.s3.bucket = "testbucket"

        schema_data = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        with open(filepath, "w") as f:
            json.dump(schema_data, f)

        my_schema = Schema.from_file(filepath)

        assert isinstance(my_schema.last_modified, datetime.datetime)
        assert isinstance(my_schema.size_in_bytes, int)
        assert my_schema.source.name == "local"
        assert Path(my_schema.filepath).as_posix() == Path(filepath).as_posix()

        assert isinstance(my_schema.frictionless_schema, frictionless.Schema)
        assert isinstance(my_schema.fields, list)
        for field in my_schema.fields:
            assert isinstance(field, frictionless.schema.field.Field)
        assert isinstance(my_schema.field_names, list)


class Test_S3Source:
    def test_initialize(self, s3):
        """
        We should be able to initialize this object with either a string or
        Path
        """
        settings.storage.s3.bucket = "testbucket"

        schema_data = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        filepath = "schemas/schema_1.json"
        with open(filepath, "w") as f:
            json.dump(schema_data, f)
        s3.create_bucket(Bucket="testbucket")
        s3.upload_file(filepath, "testbucket", filepath)
        os.remove(filepath)

        my_schema = Schema.from_file(filepath)

        assert isinstance(my_schema.last_modified, datetime.datetime)
        assert isinstance(my_schema.size_in_bytes, int)
        assert my_schema.source.name == "s3"
        assert my_schema.filepath == filepath

        assert isinstance(my_schema.frictionless_schema, frictionless.Schema)
        assert isinstance(my_schema.fields, list)
        for field in my_schema.fields:
            assert isinstance(field, frictionless.schema.field.Field)
        assert isinstance(my_schema.field_names, list)

    def test_infer(self, s3):
        """
        Replicating Issue #32
        We should be able to infer a schema from a datafile on an s3 bucket.
        """
        s3.create_bucket(Bucket="testbucket")
        settings.storage.s3.bucket = "testbucket"
        gen_dummy_data().to_csv("data/file_1.csv")
        s3.upload_file("data/file_1.csv", "testbucket", "data/file_1.csv")
        os.remove("data/file_1.csv")

        my_datafile = DataFileFactory.from_filepath("data/file_1.csv")
        assert my_datafile.source.name == "s3"
        Schema.infer(my_datafile)


def test_missing_file():
    """
    We should be able to create a schema file with a missing file, but all
    attempts to perform an operation on the resulting object should fail
    with a FileNotFoundError
    """
    assert not Path("schemas/schema_1.json").exists()

    my_schema = Schema.from_file("schemas/schema_1.json")

    with pytest.raises(FileNotFoundError):
        my_schema.frictionless_schema

    with pytest.raises(FileNotFoundError):
        my_schema.fields

    with pytest.raises(FileNotFoundError):
        my_schema.field_names

    assert not my_schema.is_valid


def test_invalid_file():
    """
    We should get errors depending on what is wrong with a given file:
    - a JSONDecodeError if the schema file we are provided is not valid JSON
    """
    filepath = "schemas/schema_1.json"

    # Check bad JSON data
    with open(filepath, "w") as f:
        f.write("bad text data\nthat isn't a schema")

    with pytest.raises(json.JSONDecodeError):
        Schema.from_file(filepath)


class Test_ValidateSchema:
    """
    Collection of tests related to the Schema.validate and
    Schema.validation_errors methods
    """

    def test_valid_schema(self):
        """If we have a valid schema, the method should return True and the
        Schema.validation_errors method should return an empty list
        """
        filepath = "schemas/schema_1.json"
        schema_data = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        with open(filepath, "w") as f:
            json.dump(schema_data, f)

        my_schema = Schema.from_file(filepath)

        assert my_schema.is_valid
        assert len(my_schema.test_cases) == 1

        my_error = my_schema.test_cases[0]
        assert my_error.testcase_id == ("schema_file_located_schemas/schema_1.json")
        assert my_error.name == "Located Schema File: schemas/schema_1.json"

    def test_invalid_schema(self):
        """If we provide a schema file that is incorrectly constructed, we
        should get back an error message indicating as clearly as possible what
        is wrong"""

        filepath = "schemas/schema_1.json"
        schema_data = {
            "my_fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        with open(filepath, "w") as f:
            json.dump(schema_data, f)

        my_schema = Schema.from_file(filepath)

        assert not my_schema.is_valid
        assert len(my_schema.test_cases) > 0

        my_error = my_schema.test_cases[0]
        assert my_error.testcase_id == ("schema_file_invalid_schemas/schema_1.json")
        assert my_error.name == "Invalid Schema File: schemas/schema_1.json"
        assert "Validation Error" in my_error.msg
        assert "Schema file does not contain a 'fields' key" in my_error.msg


class Test_ValidateDatafile:
    def test_string_with_empty(self):
        """
        If we have a column with type string and empty values,
        we should not be raising type-errors for each and every
        empty value.
        """
        schema_data = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
                {"name": "notints", "type": "integer"},
            ]
        }
        my_schema = Schema.from_raw_schema(schema_data)
        pd.DataFrame(
            {
                "id": [1, 2, 3, 4, 5],
                "name": ["foo", "bar", None, None, "fuzz"],
                "notints": [5, 4, "three", 2, 1],
            }
        ).to_csv("file_1.csv", index=None)
        my_datafile = DataFileFactory.from_filepath("file_1.csv")
        my_datafile.assign_schema(my_schema)

        violations = my_schema.validate_datafile(my_datafile)

        assert len(violations) == 1
        assert violations[0].msg == (
            'Type error in the cell "three" in row "4" and '
            'field "notints" at position "3": type is "integer/default"'
        )


class Test_InferSchema:
    """Collection of tests related to the Schema.infer method"""

    def test_infer_simple(self):
        data_filepath = Path("data/test_file.csv")
        pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]}).to_csv(
            data_filepath, index=None
        )
        my_datafile = DataFileFactory.from_filepath(data_filepath)

        my_schema = Schema.infer(my_datafile)

        my_fields = [
            {"name": "id", "type": "integer"},
            {"name": "name", "type": "string"},
        ]

        assert len(my_schema.fields) == len(my_fields)
        assert my_schema.field_names == [x["name"] for x in my_fields]
        for schema_field in my_schema.fields:
            same_type = False
            for test_field in my_fields:
                if schema_field.name == test_field["name"]:
                    same_type = schema_field.type == test_field["type"]
                    assert same_type

    def test_infer_complex(self):
        """
        Test schema inference on more complex data - in this instance we're
        using a helper function to generate 10 records of data with several
        different data types.
        """

        data_filepath = Path("data/test_file.csv")
        gen_dummy_data().to_csv(data_filepath, index=None)
        my_datafile = DataFileFactory.from_filepath(data_filepath)

        my_schema = Schema.infer(my_datafile)

        my_fields = [
            {"name": "char_1", "type": "string"},
            {"name": "str_1", "type": "string"},
            {"name": "bool_1", "type": "boolean"},
            {"name": "date_1", "type": "date"},
            {"name": "float_1", "type": "number"},
            {"name": "int_1", "type": "integer"},
            {"name": "geopoint_1", "type": "geopoint"},
        ]
        assert len(my_schema.fields) == len(my_fields)
        assert my_schema.field_names == [x["name"] for x in my_fields]
        for schema_field in my_schema.fields:
            for test_field in my_fields:
                if schema_field.name == test_field["name"]:
                    assert schema_field.type == test_field["type"], (
                        f"{schema_field.name}: {schema_field} from "
                        f"my_schema is not the same as "
                        f"{test_field['name']}: {test_field['type']})"
                    )
