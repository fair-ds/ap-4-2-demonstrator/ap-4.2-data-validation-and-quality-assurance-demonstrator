import pytest
from unittest.mock import Mock

from fair_ds_ap42.objects.quality_issues import SingularValue
from fair_ds_ap42.objects.data_column import DataColumn


@pytest.fixture(
    params=[
        {"desc": {"True/False Ratio": 0}, "data_type": "boolean"},
        {"desc": {"True/False Ratio": 1}, "data_type": "boolean"},
        {"desc": {"True/False Ratio": 0.5}, "data_type": "boolean"},
    ]
)
def singular_value_boolean(request):
    mock_column = Mock(spec=DataColumn)
    mock_column.desc = request.param["desc"]
    mock_column.data_type = request.param["data_type"]
    mock_column.name = "mock_column"
    mock_column.parent = Mock(filepath="mock_filepath")

    return SingularValue(mock_column)


@pytest.fixture(
    params=[
        {"desc": {"Unique": 1}, "data_type": "int"},
        {"desc": {"Unique": 2}, "data_type": "int"},
    ]
)
def singular_value_non_boolean(request):
    mock_column = Mock(spec=DataColumn)
    mock_column.desc = request.param["desc"]
    mock_column.data_type = request.param["data_type"]
    mock_column.name = "mock_column"
    mock_column.parent = Mock(filepath="mock_filepath")

    return SingularValue(mock_column)


def test_singular_value_boolean(singular_value_boolean):
    # Test the `code` property
    assert singular_value_boolean.code == "single-value"

    # Test the `violation` property
    expected_violation = singular_value_boolean.column.desc.get("True/False Ratio") in [
        0,
        1,
    ]
    assert singular_value_boolean.violation == expected_violation

    # Test the `error_message` property
    if expected_violation:
        assert (
            singular_value_boolean.error_message.msg
            == 'Column "mock_column" has only one value'
        )
    else:
        with pytest.raises(ValueError):
            singular_value_boolean.error_message


def test_singular_value_non_boolean(singular_value_non_boolean):
    # Test the `code` property
    assert singular_value_non_boolean.code == "single-value"

    # Test the `violation` property
    expected_violation = singular_value_non_boolean.column.desc.get("Unique") == 1
    assert singular_value_non_boolean.violation == expected_violation

    # Test the `error_message` property
    if expected_violation:
        assert (
            singular_value_non_boolean.error_message.msg
            == 'Column "mock_column" has only one value'
        )
    else:
        with pytest.raises(ValueError):
            singular_value_non_boolean.error_message
