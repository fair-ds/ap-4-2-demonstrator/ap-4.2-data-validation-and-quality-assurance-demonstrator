import datetime

import pytest
from unittest.mock import Mock
import pandas as pd

from fair_ds_ap42.objects.quality_issues import PlaceholderValue
from fair_ds_ap42.objects.data_column import DataColumn

placeholder_data = [
    {"data": pd.Series([0, 1, 2, 3, -999, 5, 6, 7, 8, 9]), "data_type": "numeric"},
    {"data": pd.Series(["foo", "bar", "buzz", "TBD"]), "data_type": "string"},
    {
        "data": pd.Series(
            [
                datetime.date(2021, 5, 18),
                datetime.date(2021, 4, 5),
                datetime.date(1970, 1, 1),
                datetime.date(2024, 1, 1),
            ]
        ),
        "data_type": "date",
    },
    {"data": pd.Series(["45,67", "12,34", "0,0", "56,78"]), "data_type": "geodata"},
]

non_placeholder_data = [
    {"data": pd.Series([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]), "data_type": "numeric"},
    {"data": pd.Series(["foo", "bar", "buzz", "fuzz"]), "data_type": "string"},
    {
        "data": pd.Series(
            [
                datetime.date(2021, 5, 18),
                datetime.date(2021, 4, 5),
                datetime.date(2020, 1, 1),
                datetime.date(2024, 1, 1),
            ]
        ),
        "data_type": "date",
    },
    {"data": pd.Series(["45,67", "12,34", "12,45", "56,78"]), "data_type": "geodata"},
]


@pytest.fixture(params=placeholder_data)
def mock_column_with_placeholders(request):
    mock = Mock(spec=DataColumn)
    mock.name = "mock_column"
    mock.data_type = request.param["data_type"]
    mock.data = request.param["data"]
    mock.parent = Mock(filepath="mock_filepath")
    return mock


@pytest.fixture(params=non_placeholder_data)
def mock_column_without_placeholders(request):
    mock = Mock(spec=DataColumn)
    mock.name = "mock_column"
    mock.data_type = request.param["data_type"]
    mock.data = request.param["data"]
    mock.parent = Mock(filepath="mock_filepath")
    return mock


def test_placeholder_values(mock_column_with_placeholders):
    result = PlaceholderValue(mock_column_with_placeholders)
    assert result.violation is True
    assert "contains potential placeholder values" in result.error_message.msg


def test_non_placeholder_values(mock_column_without_placeholders):
    result = PlaceholderValue(mock_column_without_placeholders)
    assert result.violation is False
    with pytest.raises(ValueError):
        result.error_message
