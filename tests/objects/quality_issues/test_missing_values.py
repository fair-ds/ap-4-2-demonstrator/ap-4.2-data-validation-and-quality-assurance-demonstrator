import pytest
from unittest.mock import Mock

from fair_ds_ap42.objects.quality_issues import MissingValues
from fair_ds_ap42.objects.data_column import DataColumn


@pytest.fixture(params=[0.96, 0.95, 0.94, 0.04, 1.0])
def mock_column(request):
    mock = Mock(spec=DataColumn)
    mock.name = "mock_column"
    mock.desc = {"Percent Missing": request.param}
    mock.parent = Mock(filepath="mock_filepath")
    return mock


def test_missing_values(mock_column):
    result = MissingValues(mock_column)

    expected_violation = mock_column.desc["Percent Missing"] > 0.95
    assert result.violation == expected_violation

    assert result.code == "highly-missing"

    if expected_violation:
        assert (
            result.error_message.msg
            == 'Column "mock_column" is missing > 95% of values'
        )
    else:
        with pytest.raises(ValueError):
            result.error_message
