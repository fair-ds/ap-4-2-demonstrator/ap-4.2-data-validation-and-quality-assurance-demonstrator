import datetime
import json

import pytest
from unittest.mock import patch, Mock

from fair_ds_ap42.objects.schema_assignment import SchemaAssignment
from fair_ds_ap42.objects import Schema, DataFile
from fair_ds_ap42.source.isource import ISource
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader


def get_schema_reader(filepath):
    class MockReader(IDataReader, IJSONDataReader):
        pass

    mock = Mock(spec=MockReader)
    mock.exists.return_value = True
    mock.get_size_in_bytes.return_value = 100
    mock.get_date_last_modified.return_value = datetime.datetime(2020, 1, 1)
    my_dict = {}
    if filepath == "schemas/schema_1":
        my_dict = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
    elif filepath == "schemas/schema_2":
        my_dict = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
                {"name": "value", "type": "integer"},
            ]
        }
    mock.read_dict.return_value = my_dict
    return mock


# Function to create a mock reader for a given file
def get_mock_reader(filepath):
    if filepath.endswith(".json"):
        return get_schema_reader(filepath)

    mock = Mock(spec=IDataReader)
    return mock


@pytest.fixture
def mock_local_source():
    mock = Mock(spec=ISource)
    mock.source_type = "local_directory"
    mock.name = "local"
    mock.location = "./my_dir"
    mock.list_files.return_value = ["file1.csv", "file2.csv"]
    mock.get_reader.side_effect = lambda filepath: get_mock_reader(filepath)
    mock.to_json.return_value = {
        "type": "local_directory",
        "location": "./my_dir",
    }

    # Side effect - if we call write_json, we'll write the json to a file in the CWD
    def write_json(filepath, data):
        with open(filepath, "w") as f:
            json.dump(data, f)

    mock.write_json.side_effect = write_json
    return mock


# Autouse fixture that mocks the return value of the SchemaAssignmentLoader.from_reader() method.
@pytest.fixture
def mock_schema_assignment_loader(mock_local_source):
    with patch(
        (
            "fair_ds_ap42.objects.schema_assignment.schema_assignment_loader."
            "SchemaAssignmentLoader.from_reader"
        )
    ) as mock_loader:
        mock_loader.return_value = SchemaAssignment(
            assignment={
                Schema(filepath="schemas/schema_1.json", source=mock_local_source): [
                    DataFile(filepath="data/file1.csv", source=mock_local_source),
                    DataFile(filepath="data/file2.csv", source=mock_local_source),
                ],
                Schema(filepath="schemas/schema_2.json", source=mock_local_source): [
                    DataFile(filepath="data/file3.csv", source=mock_local_source),
                ],
            }
        )
        yield mock_loader
