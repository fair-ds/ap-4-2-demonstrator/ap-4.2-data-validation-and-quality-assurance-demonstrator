import json
from pathlib import Path
import time

import pandas as pd
import pytest

from fair_ds_ap42.state.current_state import CurrentState
from fair_ds_ap42.objects.errors import CredentialsTestCase, DataQualityTestCase
from fair_ds_ap42.objects.datafile import DataFile
from fair_ds_ap42.objects.datafile.datafile_factory import DataFileFactory
from fair_ds_ap42.objects.schema_assignment import (
    SchemaAssignmentLoader,
)


def test_initialize():
    """No file exists, so we should get a totally empty object"""
    my_state = CurrentState()

    assert my_state.xml.total_tests == 0

    # Since we've created one object already, any future constructors
    # should return the exact same object

    my_state2 = CurrentState()

    assert my_state == my_state2


def test_io():
    """If we write this object to disk, we should be able to load
    it back and have it be exactly the same"""
    my_state = CurrentState()
    my_state.save()

    CurrentState.clear()

    my_state2 = CurrentState.load()

    assert my_state == my_state2


def test_load():
    """The CurrentState of a given process should be a singleton, but if we
    load in a previously saved state, we should still get a different
    object"""
    my_state = CurrentState()
    my_state.add_testcase(CredentialsTestCase(is_valid=True))
    my_state.save("old.state")

    CurrentState.clear()
    my_new_state = CurrentState()
    my_old_state = CurrentState.load("old.state")

    assert my_new_state != my_old_state
    assert my_old_state.xml.get_testsuite("credentials").get_testcase(
        "valid_credentials"
    )
    with pytest.raises(Exception):
        assert my_new_state.xml.get_testsuite("locate_data").get_testcase(
            "valid_credentials"
        )


def test_add():
    """We should be able to add multiple objects of CurrentState together.
    This will facilitate splitting the jobs up into multiple concurrent
    tasks, each of which can create it's own state, and combine these
    at a later step."""

    my_state = CurrentState()
    my_state.add_testcase(CredentialsTestCase(is_valid=True))
    my_state.save("old.state")

    CurrentState.clear()
    my_new_state = CurrentState()
    # If we add together two objects which have identical testcases, we
    # should only end up with a single instance of that testcase in the
    # final object
    my_new_state.add_testcase(CredentialsTestCase(is_valid=True))
    my_new_state.add_testcase(DataQualityTestCase("data.csv"))

    my_old_state = CurrentState.load("old.state")

    combined_state = my_new_state + my_old_state

    assert combined_state.xml.get_testsuite("credentials")
    testsuite = combined_state.xml.get_testsuite("credentials")
    # This will also fail if two testcases called "valid_credentials" exist
    assert testsuite.get_testcase("valid_credentials")

    assert combined_state.xml.get_testsuite("data_quality")
    testsuite = combined_state.xml.get_testsuite("data_quality")
    assert testsuite.get_testcase("data_quality_passed_data.csv")


def test_reset():
    """We should be able to call reset() on a state and clear the log and
    files_to_process list. This would be done when first loading in a state at
    the beginning of a script run."""

    # Setup
    my_state = CurrentState()
    my_state.add_testcase(CredentialsTestCase(is_valid=True))
    df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
    df.to_csv("data/file1.csv")
    my_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))

    my_ts = my_state.xml.get_testsuite("credentials")
    assert my_ts.get_testcase("valid_credentials")
    assert len(my_state.data_files) == 1
    assert len(my_state.files_to_process) == 1

    # Test
    my_state.reset()
    assert len(my_state.xml.testsuites) == 0
    assert len(my_state.data_files) == 1
    assert len(my_state.files_to_process) == 0


def test_add_testcase():
    """Test that we can add a testcase with the add_testcase method"""
    my_state = CurrentState()

    my_state.add_testcase(CredentialsTestCase(is_valid=True))
    assert my_state.xml.total_tests == 1
    my_ts = my_state.xml.get_testsuite("credentials")
    assert my_ts.num_tests == 1
    my_tc = my_ts.get_testcase("valid_credentials")
    assert my_tc.name == "Valid Credentials"
    assert str(my_tc.fail_type) == "SUCCESS"
    assert my_tc.fail_msg == "Sucessfully Validated Credentials"


class Test_DataFile:
    """Tests related to adding or manipulating DataFile objects within the
    CurrentState object"""

    def test_add(self):
        """Test that we can add a data file with the add_datafile method. We
        should see both that there was a datafile added to the list of
        datafiles as well as that this file is in the files_to_process list.
        """

        # Setup
        my_state = CurrentState()
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})

        df.to_csv("data/file1.csv")
        my_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))

        # Test
        assert len(my_state.data_files) == 1
        assert DataFileFactory.from_filepath("data/file1.csv") in my_state.data_files

        assert len(my_state.files_to_process) == 1
        assert (
            DataFileFactory.from_filepath("data/file1.csv") in my_state.files_to_process
        )

    # def test_add_already_exists(self):
    #     """Test that if we add a data file which already existed in the state,
    #     that we will not add it to the files to process."""
    #     # Setup
    #     my_state = CurrentState()
    #     df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})

    #     df.to_csv("data/file1.csv")
    #     my_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))
    #     # first call, we should see the file in both lists
    #     assert len(my_state.data_files) == 1
    #     assert len(my_state.files_to_process) == 1
    #     my_state.save()
    #     CurrentState.clear()

    #     # Test
    #     my_new_state = CurrentState.load()
    #     my_new_state.reset()
    #     assert len(my_new_state.data_files) == 1
    #     assert (
    #         DataFileFactory.from_filepath("data/file1.csv") in my_new_state.data_files
    #     )
    #     # files to process should have been cleared
    #     assert len(my_new_state.files_to_process) == 0

    #     my_new_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))

    #     assert len(my_new_state.data_files) == 1
    #     assert (
    #         DataFileFactory.from_filepath("data/file1.csv") in my_new_state.data_files
    #     )
    #     assert len(my_new_state.files_to_process) == 0

    @pytest.mark.skip(
        "We aren't finding the updated file to process, but this code is due for a rewrite"
    )
    def test_updated(self):
        """Test that if a data file which was already in the state has
        now been updated, it will be included in the files_to_process list"""

        # Setup
        my_state = CurrentState()
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})

        df.to_csv("data/file1.csv")
        df.to_csv("data/file2.csv")
        df.to_csv("data/file3.csv")

        my_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))
        my_state.add_datafile(DataFileFactory.from_filepath("data/file2.csv"))
        my_state.add_datafile(DataFileFactory.from_filepath("data/file3.csv"))

        assert len(my_state.data_files) == 3
        assert len(my_state.files_to_process) == 3
        my_state.save()
        CurrentState.clear()

        time.sleep(1)
        df2 = pd.DataFrame({"id": [1, 2, 3], "name": ["foo", "bar", "boo"]})
        df2.to_csv("data/file2.csv")  # Overwrite the file with different data

        my_new_state = CurrentState.load()
        my_new_state.reset()
        assert len(my_new_state.data_files) == 3
        assert (
            DataFileFactory.from_filepath("data/file1.csv") in my_new_state.data_files
        )
        assert DataFileFactory.from_filepath("data/file2.csv").is_similar_to(
            my_new_state.data_files[1]
        )
        assert (
            DataFileFactory.from_filepath("data/file3.csv") in my_new_state.data_files
        )
        # files to process should have been cleared
        assert len(my_new_state.files_to_process) == 0

        my_new_state.add_datafile(DataFileFactory.from_filepath("data/file1.csv"))
        my_new_state.add_datafile(DataFileFactory.from_filepath("data/file2.csv"))
        my_new_state.add_datafile(DataFileFactory.from_filepath("data/file3.csv"))

        assert len(my_new_state.data_files) == 3
        # file 2 should have been flagged in the files to process list
        assert len(my_new_state.files_to_process) == 1
        assert DataFileFactory.from_filepath("data/file2.csv").is_similar_to(
            my_new_state.files_to_process[0]
        )


class Test_SchemaAssignemnt:
    """When we add a schema assignment to a current state object, we should
    collect TestCases from all objects involved based off of if they were
    found or not, and if they were, if they were invalid"""

    @pytest.mark.skip("This Object is due for a rewrite")
    def test_valid(self, mock_schema_assignment_loader):
        """When adding a schema assignment, we should run through the simple
        validation checks. In the case that there is nothing wrong, we should
        have success test cases for each file"""

        # Setup
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
        df.to_csv("data/file1.csv")
        df.to_csv("data/file2.csv")

        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"], "value": [45, 42]})
        df.to_csv("data/file3.csv")

        schema_json = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        with open("schemas/schema_1.json", "w") as f:
            json.dump(schema_json, f)

        schema_json = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
                {"name": "value", "type": "integer"},
            ]
        }
        with open("schemas/schema_2.json", "w") as f:
            json.dump(schema_json, f)

        my_sa = SchemaAssignmentLoader.from_reader("schemas/schemas.json")

        my_state = CurrentState()
        my_state.schema_assignment = my_sa

        # Test

        # We should have 6 test cases:
        # 1x Sucessfully validated the SchemaAssignemnt
        # 2x Found Schema Files
        # 3x Found Data Files

        testsuite = my_state.xml.get_testsuite("locate_data")
        assert len(testsuite.testcases) == 6
        assert testsuite.get_testcase("schema_assignment_file_found")
        assert testsuite.get_testcase("schema_file_located_schemas/schema_1.json")
        assert testsuite.get_testcase("schema_file_located_schemas/schema_2.json")
        assert testsuite.get_testcase("data_file_located_data/file1.csv")
        assert testsuite.get_testcase("data_file_located_data/file2.csv")
        assert testsuite.get_testcase("data_file_located_data/file2.csv")

        # We should also see that the state has been updated to show three
        # data files, all of which need to be processed
        assert len(my_state.data_files) == 3
        for data_file in my_state.data_files:
            assert isinstance(data_file, DataFile)
            assert Path(data_file.filepath).exists()

        assert len(my_state.files_to_process) == 3
        for file_to_process in my_state.files_to_process:
            assert file_to_process in my_state.data_files

    @pytest.mark.skip("This Object is due for a rewrite")
    def test_missing_file(self, mock_schema_assignment_loader):
        """If the schema assignment contains files there are missing, we
        should recieve an error about them"""
        # Setup
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
        df.to_csv("data/file1.csv")
        # Deliberatly missing data/file2.csv

        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"], "value": [45, 42]})
        df.to_csv("data/file3.csv")

        schema_json = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
            ]
        }
        with open("schemas/schema_1.json", "w") as f:
            json.dump(schema_json, f)
        # Deliberatly missing schemas/schema_2.json

        my_sa = SchemaAssignmentLoader.from_reader("schemas/schemas.json")

        my_state = CurrentState()
        my_state.schema_assignment = my_sa

        # Test

        # We should have 6 test cases:
        # 1x Sucessfully validated the SchemaAssignemnt
        # 1x Found Schema File (schema_1)
        # 1x Missing Schema file (schema_2)
        # 2x Found Data Files (file1, file3)
        # 1x Missing Data Files (file2)

        testsuite = my_state.xml.get_testsuite("locate_data")
        assert len(testsuite.testcases) == 6
        assert testsuite.get_testcase("schema_assignment_file_found")
        assert testsuite.get_testcase("schema_file_located_schemas/schema_1.json")
        assert testsuite.get_testcase("schema_file_missing_schemas/schema_2.json")
        assert testsuite.get_testcase("data_file_located_data/file1.csv")
        assert testsuite.get_testcase("data_file_missing_data/file2.csv")
        assert testsuite.get_testcase("data_file_located_data/file3.csv")

    @pytest.mark.skip("This Object is due for a rewrite")
    def test_invalid_files(self, mock_schema_assignment_loader):
        """If the schema assignment files contains files which are invalid,
        we should recieve an error about them"""

        # Setup
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
        df.to_csv("data/file1.csv")
        df.to_csv("data/file2.arrow", sep="\t")

        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"], "value": [45, 42]})
        df.to_csv("data/file3.csv")

        bad_schema_json = {
            "fields": {"id": {"type": "integer"}, "name": {"type": "string"}}
        }
        with open("schemas/schema_1.json", "w") as f:
            json.dump(bad_schema_json, f)

        schema_json = {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "name", "type": "string"},
                {"name": "value", "type": "integer"},
            ]
        }
        with open("schemas/schema_2.json", "w") as f:
            json.dump(schema_json, f)

        my_sa = SchemaAssignmentLoader.from_reader("schemas/schemas.json")

        my_state = CurrentState()
        my_state.schema_assignment = my_sa

        # Test

        # We should have 6 test cases:
        # 1x Sucessfully validated the SchemaAssignemnt
        # 1x Found Schema File
        # 3x Invalid Schema file (3 issues with this schema)
        # 2x Found Data Files
        # 1x Invalid Data Files

        testsuite = my_state.xml.get_testsuite("locate_data")
        assert len(testsuite.testcases) == 8
        assert testsuite.get_testcase("schema_assignment_file_found")
        assert (
            len(testsuite.filter("schema_file_invalid_schemas/schema_1.json").testcases)
            == 3
        )
        assert testsuite.get_testcase("schema_file_located_schemas/schema_2.json")
        assert testsuite.get_testcase("data_file_located_data/file1.csv")
        # assert testsuite.get_testcase("data_file_invalid_data/file2.arrow")
        assert testsuite.get_testcase("data_file_located_data/file3.csv")
