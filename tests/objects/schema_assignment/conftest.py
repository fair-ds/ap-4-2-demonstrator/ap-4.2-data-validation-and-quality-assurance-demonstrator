"""
Fixtures that are relevant for testing the SchemaAssignment class.
"""

import pytest
from unittest.mock import Mock, patch

from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.source import ISource, S3Bucket
from fair_ds_ap42.objects.schema_assignment import SchemaAssignmentLoader
from fair_ds_ap42.file_auditor import FileAuditor
from tests.objects.schema_assignment.schema_assignment_helpers import (
    gen_resource,
    get_mock_reader,
    get_mock_datafile,
)
from tests.test_utils.schema_mocks import (
    SchemaMock,
    get_mock_schema,
)
from tests.test_utils.source_mocks import VirtualSourceMock


@pytest.fixture(autouse=True, scope="function")
def patch_s3_bucket(s3):
    print("Patching S3Bucket")
    # Patch the client property of fair_ds_ap42.source.S3Bucket so that we don't actually make any
    # calls to S3
    with patch.object(S3Bucket, "client", s3):
        yield


@pytest.fixture
def mock_source():
    mock = Mock(spec=ISource)
    mock.name = "mock"
    mock.to_json.return_value = {"type": "mock"}
    mock.get_reader.side_effect = lambda filepath: get_mock_reader(filepath)
    return mock


@pytest.fixture(autouse=True, scope="function")
def patch_source_from_dict(mock_source):
    # Patch the SourceFactory.from_dict method so that it returns our mock source
    with patch("fair_ds_ap42.source.source_factory.SourceFactory.from_dict") as mock:
        mock.return_value = mock_source
        yield


@pytest.fixture
def mock_json_reader(mock_source):
    mock = Mock(spec=IJSONDataReader)
    mock.read_dict.return_value = [
        {
            "data_files": [
                {
                    "filepath": "file1.csv",
                    "source": mock_source.to_json(),
                    "size_in_bytes": 100,
                    "last_modified": "2020-01-01T00:00:00Z",
                },
                {
                    "filepath": "file2.csv",
                    "source": mock_source.to_json(),
                    "size_in_bytes": 100,
                    "last_modified": "2020-01-01T00:00:00Z",
                },
            ],
            "schema": {
                "filepath": "schema1.json",
                "source": mock_source.to_json(),
                "size_in_bytes": 100,
                "last_modified": "2020-01-01T00:00:00Z",
            },
        },
        {
            "data_files": [
                {
                    "filepath": "file3.csv",
                    "source": mock_source.to_json(),
                    "size_in_bytes": 100,
                    "last_modified": "2020-01-01T00:00:00Z",
                }
            ],
            "schema": {
                "filepath": "schema2.json",
                "source": mock_source.to_json(),
                "size_in_bytes": 100,
                "last_modified": "2020-01-01T00:00:00Z",
            },
        },
    ]
    return mock


@pytest.fixture
def mock_sa_loader(mock_json_reader):
    """
    Returns a mock SchemaAssignmentLoader.
    """
    mock = Mock(spec=SchemaAssignmentLoader)
    mock.from_reader.return_value = SchemaAssignmentLoader.from_reader(mock_json_reader)
    return mock


@pytest.fixture
def mock_schema_assignment(mock_sa_loader):
    return mock_sa_loader.from_reader()


@pytest.fixture
def mock_auditor():
    mock = Mock(spec=FileAuditor)
    mock.unchanged_files = [
        gen_resource("schema1.json"),
        gen_resource("schema2.json"),
        gen_resource("file1.csv"),
        gen_resource("file2.csv"),
        gen_resource("file3.csv"),
    ]
    mock.all_existing_files = mock.unchanged_files
    mock.all_files = mock.unchanged_files
    return mock


@pytest.fixture(autouse=True, scope="function")
def patched_create_object():
    # We don't really care how these objects are created, but we want to compare them. This will
    # be easier if we monkeypatch all of the Datafile and Schema methods that create new objects
    # so that they return the same objects every time.

    # Create the patch objects
    schema_from_resource_patch = patch(
        "fair_ds_ap42.objects.Schema.from_resource",
        side_effect=lambda resource: get_mock_schema(resource.filepath),
    )
    schema_from_dict_patch = patch(
        "fair_ds_ap42.objects.Schema.from_dict",
        side_effect=lambda resource: get_mock_schema(resource["filepath"]),
    )

    datafile_from_resource_patch = patch(
        "fair_ds_ap42.objects.datafile.datafile_factory.DataFileFactory.from_filepath",
        side_effect=lambda filepath: get_mock_datafile(filepath),
    )
    datafile_from_dict_patch = patch(
        "fair_ds_ap42.objects.DataFile.from_dict",
        side_effect=lambda resource: get_mock_datafile(resource["filepath"]),
    )

    with (
        schema_from_resource_patch,
        schema_from_dict_patch,
        datafile_from_resource_patch,
        datafile_from_dict_patch,
    ):
        yield


@pytest.fixture(autouse=True, scope="function")
def patch_infer_schema():
    # Patch the infer schema method so that it returns a mock schema based on the filepath of the
    # datafile

    def mock_infer_schema(datafile):
        if datafile.filepath in ["file1.csv", "file2.csv", "file4.csv"]:
            return get_mock_schema("schema_1.json")
        elif datafile.filepath in ["file3.csv", "file5.csv"]:
            return get_mock_schema("schema_2.json")
        elif datafile.filepath in ["fileX.csv"]:
            mock = SchemaMock()
            mock.source = VirtualSourceMock()
            return mock
        raise Exception(f"unexpected datafile name in test: {datafile.filepath}")

    with patch("fair_ds_ap42.objects.Schema.infer", side_effect=mock_infer_schema):
        yield


@pytest.fixture(autouse=True, scope="function")
def patch_frictionless_describe():
    mock = Mock()
    frictionless_schema_mock = Mock()
    frictionless_schema_mock.to_dict.return_value = {}
    mock.schema = frictionless_schema_mock

    describe_patch = patch("frictionless.describe", return_value=mock)

    with describe_patch:
        yield
