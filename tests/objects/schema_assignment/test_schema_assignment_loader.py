from unittest.mock import Mock

from fair_ds_ap42.objects.schema_assignment import SchemaAssignmentLoader
from fair_ds_ap42.objects.schema_assignment import SchemaAssignment
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.source.isource import ISource
from fair_ds_ap42.source.available_sources import register_source
from fair_ds_ap42.objects import DataFile, Schema


def test_init_no_file():
    # We should be able to initialize a SchemaAssignment without providing a reader

    # Initialize a SchemaAssignment
    result = SchemaAssignmentLoader.from_reader()

    # Check the result
    assert isinstance(result, SchemaAssignment)
    assert result.schemas == []


def test_init_with_file(mock_json_reader):
    # We should be able to initialize a SchemaAssignment with a schemas.json file. This will
    # result in an assignment with the schemas from the file.

    # Initialize a SchemaAssignment
    result = SchemaAssignmentLoader.from_reader(reader=mock_json_reader)

    # Check the result
    assert isinstance(result, SchemaAssignment)

    # Check our Schemas
    assert isinstance(result.schemas, list)
    assert len(result.schemas) == 2
    assert all([isinstance(schema, Schema) for schema in result.schemas])
    schema_filepaths = [schema.filepath for schema in result.schemas]
    assert "schema1.json" in schema_filepaths
    assert "schema2.json" in schema_filepaths

    # Check out DataFiles
    assert isinstance(result.data_files, list)
    assert len(result.data_files) == 3
    assert all([isinstance(data_file, DataFile) for data_file in result.data_files])
    data_file_filepaths = [data_file.filepath for data_file in result.data_files]
    assert "file1.csv" in data_file_filepaths
    assert "file2.csv" in data_file_filepaths
    assert "file3.csv" in data_file_filepaths


def test_low_information_file(mock_json_reader):
    # The dict returned by the reader should be far more sparse
    mock_json_reader.read_dict.return_value = {
        "schema1.json": ["file1.csv", "file2.csv"],
        "schema2.json": ["file3.csv"],
    }

    # Create a mock source that looks like it contains the files
    def get_mock_reader(filepath):
        class MockReader(IDataReader, IJSONDataReader):
            pass

        mock = Mock(spec=MockReader)
        mock.filepath = filepath
        mock.read_dict.return_value = {"filepath": filepath}
        mock.exists.return_value = True
        return mock

    mock_source = Mock(spec=ISource)
    mock_source.to_json.return_value = {"type": "mock"}
    mock_source.get_reader.side_effect = get_mock_reader
    mock_source.sort_rank = -1

    register_source(mock_source)

    # Initialize a SchemaAssignment
    result = SchemaAssignmentLoader.from_reader(
        reader=mock_json_reader, sources=[mock_source]
    )

    # Check the result
    assert isinstance(result, SchemaAssignment)

    # Check our Schemas
    assert isinstance(result.schemas, list)
    assert len(result.schemas) == 2
    assert all([isinstance(schema, Schema) for schema in result.schemas])
    schema_filepaths = [schema.filepath for schema in result.schemas]
    assert "schema1.json" in schema_filepaths
    assert "schema2.json" in schema_filepaths

    # Check out DataFiles
    assert isinstance(result.data_files, list)
    assert len(result.data_files) == 3
    assert all([isinstance(data_file, DataFile) for data_file in result.data_files])
    data_file_filepaths = [data_file.filepath for data_file in result.data_files]
    assert "file1.csv" in data_file_filepaths
    assert "file2.csv" in data_file_filepaths
    assert "file3.csv" in data_file_filepaths
