from unittest.mock import Mock, MagicMock

import datetime


from fair_ds_ap42.source import ISource
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.source.reader.itabular_data_reader import ITabularDataReader
from fair_ds_ap42.objects import DataFile, Schema
from fair_ds_ap42.objects.resource.resource import Resource


def get_mock_reader(filepath):
    # Create a mock DataReader for the Source
    class MockDataReader(IDataReader, IJSONDataReader, ITabularDataReader):
        pass

    mock = Mock(spec=MockDataReader)
    mock.filepath = filepath
    mock.read_dict.return_value = {}
    mock.get_size_in_bytes.return_value = 100
    mock.get_date_last_modified.return_value = datetime.datetime(2020, 1, 1)

    if filepath in ["file1.csv", "file2.csv", "file4.csv"]:
        mock.get_fields.return_value = ["foo", "bar"]
    elif filepath in ["file3.csv"]:
        mock.get_fields.return_value = ["foo", "bar", "baz"]
    elif filepath in ["schema1.json", "schema2.json"]:
        # mock.get_fields.side_effect = Exception("This is a schema file")
        pass
    else:
        mock.get_fields.side_effect = Exception("This is not a valid filename")

    return mock


def gen_mock_source(filepath):
    mock = Mock(spec=ISource)
    mock.name = "mock"
    mock.to_json.return_value = {"type": "mock"}
    mock.get_reader.return_value = get_mock_reader(filepath)
    return mock


def gen_resource(filepath):
    mock = MagicMock(spec=Resource)
    mock.filepath = filepath
    mock.source = gen_mock_source(filepath)
    mock.size_in_bytes = 100
    mock.last_modified = datetime.datetime(2020, 1, 1)
    mock.__eq__.side_effect = lambda other: other.filepath == filepath

    return mock


def get_mock_schema(filepath: str) -> Schema:
    mock = MagicMock(spec=Schema)
    mock.filepath = filepath
    mock.fields = ["foo", "bar"]
    mock.__eq__.side_effect = lambda other: other.filepath == filepath
    mock.__ne__.side_effect = lambda other: other.filepath != filepath
    return mock


def get_mock_datafile(filepath: str) -> DataFile:
    mock = MagicMock(spec=DataFile)
    mock.filepath = filepath
    mock.fields = ["foo", "bar"]
    # if the schema filepath is "schema1.json", then return True if teh datafile filepath is
    # "file1.csv" or "file2.csv". If the schema filepath is "schema2.json", then return True if the
    # datafile filepath is "file3.csv"
    mock.source = gen_mock_source(filepath)
    mock.compatible_schema.side_effect = lambda schema: (
        schema.filepath == "schema1.json"
        and filepath in ["file1.csv", "file2.csv", "file4.csv"]
    ) or (schema.filepath == "schema2.json" and filepath == "file3.csv")
    mock.__eq__.side_effect = lambda other: other.filepath == filepath
    return mock
