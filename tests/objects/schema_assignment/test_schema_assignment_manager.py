import pytest

from fair_ds_ap42.objects.schema_assignment import SchemaAssignmentManager
from fair_ds_ap42.objects import DataFile, Schema

from tests.objects.schema_assignment.schema_assignment_helpers import (
    gen_mock_source,
    get_mock_datafile,
    get_mock_schema,
    gen_resource,
)


def test_init(mock_schema_assignment):
    # Test that we can initialize a SchemaAssignmentManager.

    # Initialize a SchemaAssignmentManager
    result = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Check that the SchemaAssignmentManager has the correct attributes
    assert result.assignment == mock_schema_assignment


def test_add_schema(mock_schema_assignment):
    # Test that we can add a Schema to the SchemaAssignment.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a Schema
    schema = get_mock_schema("schema3.json")

    # Add the Schema to the SchemaAssignment
    manager.add_schema(schema)

    # Check that the SchemaAssignment now contains the Schema
    assert schema in manager.assignment.schemas


def test_add_datafile_matches_schema(mock_schema_assignment):
    # Test that we can add a DataFile to the SchemaAssignment when it matches an existing Schema.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a DataFile
    datafile = get_mock_datafile("file4.csv")

    # Add the DataFile to the SchemaAssignment
    manager.add_datafile(datafile)

    # Check that the SchemaAssignment now contains the DataFile
    assert datafile in manager.assignment.data_files


def test_add_datafile_no_matching_schema(mock_schema_assignment):
    # Test that we can add a DataFile to the SchemaAssignment when it does not match an existing
    # Schema. In this case, we should create a new Schema as well to accomodate the DataFile.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a DataFile
    datafile = get_mock_datafile("file5.csv")

    # Add the DataFile to the SchemaAssignment
    manager.add_datafile(datafile)

    # Check that the SchemaAssignment now contains the DataFile
    assert datafile in manager.assignment.data_files

    # Check that the SchemaAssignment now contains the new Schema
    assert len(manager.assignment.schemas) == 3


def test_create_new_schema(mock_schema_assignment):
    # Test that we can create a new Schema from a DataFile.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a DataFile
    datafile = DataFile(filepath="file4.csv", source=gen_mock_source("file4.csv"))

    # Create a new Schema from the DataFile
    schema = manager.create_new_schema(datafile)

    assert isinstance(schema, Schema)


def test_remove_datafile(mock_schema_assignment):
    # Test that we can remove a specific DataFile from the SchemaAssignment.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a DataFile
    datafile = get_mock_datafile("file1.csv")

    # Remove the DataFile from the SchemaAssignment
    manager.remove_datafile(datafile)

    # Check that the SchemaAssignment no longer contains the DataFile
    assert datafile not in manager.assignment.data_files


def test_remove_last_datafile(mock_schema_assignment):
    # Test that if we remove the last DataFile from a Schema, we do not remove the schema.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a DataFile
    datafile = get_mock_datafile("file3.csv")

    # Remove the DataFile from the SchemaAssignment
    manager.remove_datafile(datafile)

    # Check that the SchemaAssignment no longer contains the DataFile
    assert datafile not in manager.assignment.data_files

    # Check that the SchemaAssignment still contains the Schema
    assert len(manager.assignment.schemas) == 2


def test_remove_schema(mock_schema_assignment):
    # Test that we can remove a schema from the SchemaAssignment.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Create a Schema
    schema = get_mock_schema("schema2.json")

    # Remove the Schema from the SchemaAssignment
    manager.remove_schema(schema)

    # Check that the SchemaAssignment no longer contains the Schema
    assert schema not in manager.assignment.schemas


@pytest.mark.skip("Need to work out mocking the Resource __eq__ method")
def test_get_all_unassigned_datafiles(mock_schema_assignment, mock_auditor):
    # Test that we can identify all Datafiles in a FileAuditor that have not been assigned to a
    # Schema.

    # Add a new Resource to the mock FileAuditor
    mock_auditor.unchanged_files.append(gen_resource("file4.csv"))

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Get all unassigned DataFiles
    unassigned_datafiles = manager.get_all_unassigned_datafiles(
        data_file_patterns=["*.csv"], file_auditor=mock_auditor
    )

    # Check that the unassigned DataFiles are correct
    assert isinstance(unassigned_datafiles, list)
    assert len(unassigned_datafiles) == 1
    assert unassigned_datafiles[0].filepath == "file4.csv"


@pytest.mark.skip("Need to work out mocking the Resource __eq__ method")
def test_update_schema_assignment_new_file(mock_schema_assignment, mock_auditor):
    # We should be able to provide a SchemaAssignmentManager with a FileAuditor and it will update
    # the SchemaAssignment accordingly.

    # Add a new Datafile to the mock FileAuditor
    mock_auditor.unchanged_files.append(gen_resource("file4.csv"))

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Update the SchemaAssignment
    manager.update_schema_assignment(mock_auditor, ["*.json"], ["*.csv"])

    # Check that the SchemaAssignment now contains the new DataFile
    result = manager.assignment

    assert len(result.data_files) == 4
    datafile_paths = [datafile.filepath for datafile in result.data_files]
    assert "file4.csv" in datafile_paths

    # The new file should be present under the schema that it matches - schema1.json
    assert len(result.assignment[result.schemas[0]]) == 3
    assert result.assignment[result.schemas[0]][-1].filepath == "file4.csv"


def test_update_schema_assignment_removed_file(mock_schema_assignment, mock_auditor):
    # We should be able to provide a SchmeaAssingmentManager with a FileAuditor and it will update
    # the SchemaAssignment accordingly. In this case, we have removed a datafile which was present
    # in the SchemaAssignment.

    # Remove a Datafile from the mock FileAuditor
    mock_auditor.all_files = [
        resource
        for resource in mock_auditor.unchanged_files
        if resource.filepath != "file3.csv"
    ]

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Update the SchemaAssignment
    manager.update_schema_assignment(mock_auditor, ["*.json"], ["*.csv"])

    # Check that the SchemaAssignment no longer contains the removed DataFile
    result = manager.assignment

    assert len(result.data_files) == 2
    assert result.data_files[-1].filepath == "file2.csv"

    # There should no longer be any files associated with schema2.json
    assert len(result.assignment[result.schemas[1]]) == 0


@pytest.mark.skip("Need to work out mocking the Resource __eq__ method")
def test_update_schmea_assignment_removed_schema(mock_schema_assignment, mock_auditor):
    # In the case when we find that a schema was removed, we should remove all datafiles associated
    # with that schema, then create a new schema that will accomodate those datafiles.

    # Remove a Schema from the mock FileAuditor
    mock_auditor.all_existing_files = [
        resource
        for resource in mock_auditor.unchanged_files
        if resource.filepath != "schema2.json"
    ]

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Update the SchemaAssignment
    manager.update_schema_assignment(mock_auditor, ["*.json"], ["*.csv"])

    # Check that the SchemaAssignment no longer contains the removed Schema
    result = manager.assignment

    # We will have two schemas, because the removed schema will be replaced by a new one
    assert len(result.schemas) == 2

    # There should no longer be any files associated with schema2.json
    assert len(result.assignment[result.schemas[0]]) == 2


def test_get_all_virtual_schema(mock_schema_assignment):
    # We should be able to get a list of all virtual schemas in the SchemaAssignment.

    # Initialize a SchemaAssignmentManager
    manager = SchemaAssignmentManager(assignment=mock_schema_assignment)

    # Add a new DataFile to the SchemaAssignment
    datafile = get_mock_datafile("fileX.csv")
    manager.add_datafile(
        datafile
    )  # This will trigger the creation of a new virtual schema

    # We should have three schemas
    assert len(manager.assignment.schemas) == 3

    # Get all virtual schemas
    virtual_schemas = manager.get_all_virtual_schemas()

    # Check that we have exactly one virtual schema
    assert len(virtual_schemas) == 1
    assert virtual_schemas[0].filepath == "test_schema.json"


# Tests to add
# If we add a schema file, but that schema is already in the assignment, we should not add it
