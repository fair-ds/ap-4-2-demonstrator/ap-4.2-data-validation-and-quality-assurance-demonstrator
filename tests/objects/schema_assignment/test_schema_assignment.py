from fair_ds_ap42.objects.schema_assignment import SchemaAssignment
from fair_ds_ap42.objects import DataFile, Schema

from tests.objects.schema_assignment.schema_assignment_helpers import (
    get_mock_datafile,
    get_mock_schema,
)


def test_init_no_file():
    # We should be able to initialize a SchemaAssignment without providing any files. This will
    # result in an empty assignment.

    # Initialize a SchemaAssignment
    result = SchemaAssignment()

    # Check that the assignment is empty
    assert result.assignment == {}

    # Check that all our properties and methods still work
    assert result.schemas == []
    assert result.data_files == []
    assert result.get_datafiles_by_schema(get_mock_schema("schema1.json")) == []
    assert result.get_schema_by_datafile(get_mock_datafile("file1.csv")) is None


def test_init_with_assignment():
    # We should be able to initialize a SchemaAssignment with a dictionary of schemas and data
    # files.

    # Create a Assignment dictionary
    schema1, schema2 = get_mock_schema("schema1.json"), get_mock_schema("schema2.json")
    file1, file2, file3 = (
        get_mock_datafile("file1.csv"),
        get_mock_datafile("file2.csv"),
        get_mock_datafile("file3.csv"),
    )

    assignment_dict = {
        schema1: [file1, file2],
        schema2: [file3],
    }

    # Initialize a SchemaAssignment
    result = SchemaAssignment(assignment_dict)

    # Check that the assignment is correct
    assert result.assignment == assignment_dict

    # Check that all our properties and methods still work
    assert result.schemas == list(assignment_dict.keys())
    assert result.data_files == [
        data_file for data_files in assignment_dict.values() for data_file in data_files
    ]

    assert result.get_datafiles_by_schema(schema1) == assignment_dict[schema1]
    assert result.get_datafiles_by_schema(schema2) == assignment_dict[schema2]

    assert result.get_schema_by_datafile(file1) == schema1
    assert result.get_schema_by_datafile(file2) == schema1
    assert result.get_schema_by_datafile(file3) == schema2


def test_schemas_property(mock_sa_loader):
    # We should be able to get a list of Schemas from the SchemaAssignment by accessing the schemas
    # property.

    # Initialize a SchemaAssignment
    assignment = mock_sa_loader.from_reader()

    # Get the schemas
    result = assignment.schemas

    # Check the result
    assert isinstance(result, list)
    assert len(result) == 2
    assert all([isinstance(schema, Schema) for schema in result])
    schema_filepaths = [schema.filepath for schema in result]
    assert "schema1.json" in schema_filepaths
    assert "schema2.json" in schema_filepaths


def test_data_files_property(mock_sa_loader):
    # We should be able to get a list of DataFiles from the SchemaAssignment by accessing the data
    # files property.

    # Initialize a SchemaAssignment
    assignment = mock_sa_loader.from_reader()

    # Get the data files
    result = assignment.data_files

    # Check the result
    assert isinstance(result, list)
    assert len(result) == 3
    assert all([isinstance(data_file, DataFile) for data_file in result])
    data_file_filepaths = [data_file.filepath for data_file in result]
    assert "file1.csv" in data_file_filepaths
    assert "file2.csv" in data_file_filepaths
    assert "file3.csv" in data_file_filepaths


def test_get_datafiles_by_schema(mock_sa_loader):
    # We should be able to call get_datafiles_by_schema and get a list of DataFiles for a given
    # schema.

    # Initialize a SchemaAssignment
    assignment = mock_sa_loader.from_reader()

    schema_to_test = assignment.schemas[0]

    # Get the DataFiles for schema1.json
    result = assignment.get_datafiles_by_schema(schema_to_test)

    # Check the result
    assert isinstance(result, list)
    assert len(result) == 2
    assert all([isinstance(data_file, DataFile) for data_file in result])
    data_file_filepaths = [data_file.filepath for data_file in result]
    assert "file1.csv" in data_file_filepaths
    assert "file2.csv" in data_file_filepaths
    assert "file3.csv" not in data_file_filepaths


def test_get_schema_by_datafile(mock_sa_loader):
    # We should be able to call get_schema_by_datafile and get a Schema for a given DataFile.

    # Initialize a SchemaAssignment
    assignment = mock_sa_loader.from_reader()

    data_file_to_test = assignment.data_files[0]

    # Get the Schema for file1.csv
    result = assignment.get_schema_by_datafile(data_file_to_test)

    # Check the result
    assert isinstance(result, Schema)
    assert result.filepath == "schema1.json"
