import pytest

import pandas as pd
import geopandas as gpd

from shapely.geometry import Point

from fair_ds_ap42.objects.data_column import GeodataColumn
from fair_ds_ap42.objects.data_column.geodata_column import parse_value, parse_geodata

# The geo_data fixture is defined in tests\fixtures\geodata_fixtures.py
# It returns a series of 10 geodata points, and is parametrized to return
# the series in each of the three formats used in frictionless


def test_geodata_column(geo_data):
    # Create a geodata column from the series of geodata points
    my_format = geo_data.name
    col = GeodataColumn(
        parent=None, name="test_column", data=geo_data, format=my_format
    )

    assert col.format == my_format
    assert col.data_type == "geodata"
    assert isinstance(col.description, dict)
    assert col.description["Data Type"] == "geodata"
    assert col.description["Percent Unique"] == 1
    assert col.description["Percent Missing"] == 0
    assert col.description["Count"] == 10
    assert col.description["Unique"] == 10
    assert col.description["Missing"] == 0


@pytest.mark.parametrize(
    "record, my_format",
    [("1,2", "default"), ([1, 2], "array"), ({"lon": 1, "lat": 2}, "object")],
)
def test_parse_value(record, my_format):
    lon, lat = parse_value(record, my_format)
    assert lon == 1
    assert lat == 2


def test_parse_value_coerce():
    # If we provide an unparsable record and errors is set to coerce, we should
    # get back None, None. If errors is set to strict, we should get a TypeError
    record = "lon:1 lat:2"
    my_format = "default"
    lon, lat = parse_value(record, my_format, errors="coerce")
    assert lon is None
    assert lat is None

    with pytest.raises(TypeError):
        parse_value(record, my_format, errors="strict")


def test_parse_value_invalid_values():
    # If we provide values outside of the valid range for geographical coordinates, we should
    # get back None, None if coerce is set to True, and raise a ValueError if errors is set to
    # "strict"
    record = "181,2"
    my_format = "default"
    lon, lat = parse_value(record, my_format, errors="coerce")
    assert lon is None
    assert lat is None

    with pytest.raises(ValueError):
        parse_value(record, my_format, errors="strict")


def test_parse_geodata(geo_data):
    # If we provide a series of geodata points, we should get back a series of
    # tuples of (lon, lat)
    my_format = geo_data.name
    parsed = parse_geodata(geo_data, my_format)
    assert isinstance(parsed, pd.DataFrame)

    # each element of the dataframe should be a float
    assert parsed.map(lambda x: isinstance(x, float)).all().all()


def test_get_geometry(geo_data):
    my_format = geo_data.name
    col = GeodataColumn(
        parent=None, name="test_column", data=geo_data, format=my_format
    )

    geometry = col.get_geometry()
    assert isinstance(geometry, gpd.GeoSeries)
    assert geometry.apply(lambda x: isinstance(x, Point)).all()
