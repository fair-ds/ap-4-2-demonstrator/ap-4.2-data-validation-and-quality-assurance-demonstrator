import pandas as pd

from fair_ds_ap42.objects.data_column import DateColumn


def test_date_column():
    # Create test dates as a pandas series of 10 strings with the format %Y-%m-%d
    dates = pd.Series(
        [
            "2020-01-01",
            "2020-01-02",
            "2020-01-03",
            "2020-01-04",
            "2020-01-05",
            "2020-01-06",
            "2020-01-07",
            "2020-01-08",
            "2020-01-09",
            "2020-01-10",
        ]
    )
    col = DateColumn(parent=None, name="test_column", data=dates, format="%Y-%m-%d")

    assert col.data.equals(pd.to_datetime(dates, errors="coerce"))
    assert isinstance(col.description, dict)
    assert col.description["Data Type"] == "date"
