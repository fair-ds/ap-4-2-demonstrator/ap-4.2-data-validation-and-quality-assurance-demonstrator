import pytest
from unittest.mock import Mock

import frictionless
import pandas as pd

from fair_ds_ap42.objects import DataFile, Schema

from fair_ds_ap42.objects.data_column.boolean_column import parse_boolean_from_strings
from fair_ds_ap42.objects.data_column import BooleanColumn


@pytest.fixture
def mock_schema_default():
    mock = Mock(spec=Schema)
    mock.raw_data = {"fields": [{"name": "test_column", "type": "boolean"}]}
    mock.frictionless_schema = frictionless.Schema.from_descriptor(mock.raw_data)
    return mock


@pytest.fixture
def mock_schema_user_defined():
    mock = Mock(spec=Schema)
    mock.raw_data = {
        "fields": [
            {
                "name": "test_column",
                "type": "boolean",
                "trueValues": ["T"],
                "falseValues": ["Not Present in this Instance"],
            }
        ]
    }
    mock.frictionless_schema = frictionless.Schema.from_descriptor(mock.raw_data)
    return mock


@pytest.fixture
def mock_datafile():
    mock = Mock(spec=DataFile)
    return mock


def test_parse_boolean_from_strings():
    # Check that the parse method correctly converts strings to booleans with defaults
    data = pd.Series(["True", "False", "T", "F", "True", "True", "False"])
    result = parse_boolean_from_strings(data)
    assert result.equals(pd.Series([True, False, True, False, True, True, False]))

    # Check that the parse method correctly converts strings to booleans with user defined values
    data = pd.Series(
        [
            "Yes",
            "Not Present in this Instance",
            "Yes",
            "Not Present in this Instance",
            "Yes",
            "Yes",
            "Not Present in this Instance",
        ]
    )
    result = parse_boolean_from_strings(
        data, true_values=["Yes"], false_values=["Not Present in this Instance"]
    )
    assert result.equals(pd.Series([True, False, True, False, True, True, False]))

    # Valuse that are not in the true/false values should be converted to NaN
    data = pd.Series(["True", "False", "T", "F", "True", "True", "False", "Maybe"])
    result = parse_boolean_from_strings(data)
    assert result.equals(pd.Series([True, False, True, False, True, True, False, None]))


def test_string_data(mock_datafile, mock_schema_user_defined):
    # We may recieve data as a pandas series of strings. The schema in the parent should
    # indicate which values are True or False.

    # Create test data as a pandas series of 10 strings
    data = pd.Series(
        [
            "T",
            "Not Present in this Instance",
            "T",
            "Not Present in this Instance",
            "T",
            "T",
            "Not Present in this Instance",
            "T",
            "Not Present in this Instance",
            "T",
        ]
    )
    mock_datafile.schema = mock_schema_user_defined
    col = BooleanColumn(parent=mock_datafile, name="test_column", data=data)

    # Check that the data was correctly converted to booleans
    assert col.data.equals(
        pd.Series([True, False, True, False, True, True, False, True, False, True])
    )
