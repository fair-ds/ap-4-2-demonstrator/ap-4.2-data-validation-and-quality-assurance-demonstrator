import pytest

import json
import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    CategoricalColumnDescriber,
)
from fair_ds_ap42.utils.file_io import find_non_serializable_values


def test_describe():
    # Create some categorical data for testing
    test_data = pd.Series(["foo", "bar", "fuzz", "buzz", "foo", "bar", "fuzz", "foo"])
    result = CategoricalColumnDescriber().describe(test_data)

    assert result["Data Type"] == "categorical"
    assert result["Count"] == 8
    assert result["Most Common Value"] == "foo"
    assert result["Most Common Value Count"] == 3
    assert result["Most Common Value Ratio"] == 0.375
    assert result["Least Common Value"] == "buzz"
    assert result["Least Common Value Count"] == 1
    assert result["Least Common Value Ratio"] == 0.125
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0.0
    assert result["Unique"] == 4
    assert result["Unique Ratio"] == 0.5

    try:
        json.dumps(result)
    except TypeError:
        pytest.fail(
            f"Result of CategoricalColumnDescriber.describe() is not serializable: "
            f"{find_non_serializable_values(result)}"
        )


def test_describe_numeric_values():
    # If the catalogical data is numeric, it should still be treated as categorical, and
    # the numeric values should be treated as categories

    # Create some categorical data for testing
    test_data = pd.Series([1, 2, 3, 4, 1, 2, 3, 1])
    result = CategoricalColumnDescriber().describe(test_data)

    assert result["Data Type"] == "categorical"
    assert result["Count"] == 8
    assert result["Most Common Value"] == "1"
    assert result["Most Common Value Count"] == 3
    assert result["Most Common Value Ratio"] == 0.375
    assert result["Least Common Value"] == "4"
    assert result["Least Common Value Count"] == 1
    assert result["Least Common Value Ratio"] == 0.125

    try:
        json.dumps(result)
    except TypeError:
        pytest.fail(
            f"Result of CategoricalColumnDescriber.describe() is not serializable: "
            f"{find_non_serializable_values(result)}"
        )


def test_describe_some_missing_values():
    # Create some categorical data for testing
    test_data = pd.Series(["foo", "bar", "fuzz", "buzz", "foo", "bar", "fuzz", None])
    result = CategoricalColumnDescriber().describe(test_data)

    assert result["Count"] == 7
    assert result["Missing"] == 1
    assert result["Percent Missing"] == 1 / 8
    assert result["Unique"] == 4
    assert result["Unique Ratio"] == 4 / 7


def test_describe_all_missing_values():
    # Create some categorical data for testing
    test_data = pd.Series([None, None, None, None, None, None, None, None])
    result = CategoricalColumnDescriber().describe(test_data)

    assert result["Count"] == 0
    assert result["Missing"] == 8
    assert result["Percent Missing"] == 1
    assert result["Unique"] == 0
    assert result["Unique Ratio"] is None
    assert result["Most Common Value"] is None
    assert result["Most Common Value Count"] is None
    assert result["Most Common Value Ratio"] is None
    assert result["Least Common Value"] is None
    assert result["Least Common Value Count"] is None
    assert result["Least Common Value Ratio"] is None
