import datetime

import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    DateColumnDescriber,
)


def test_describe():
    # Create some test data - 10 dates, 5 of which are the same
    test_data = pd.Series(
        [
            datetime.datetime(2020, 1, 1),
            datetime.datetime(2020, 1, 2),
            datetime.datetime(2020, 1, 3),
            datetime.datetime(2020, 1, 4),
            datetime.datetime(2020, 1, 5),
            datetime.datetime(2020, 2, 6),
            datetime.datetime(2020, 2, 6),
            datetime.datetime(2020, 2, 6),
            datetime.datetime(2020, 2, 6),
            datetime.datetime(2020, 2, 6),
        ]
    )
    result = DateColumnDescriber().describe(test_data)

    assert isinstance(result, dict)
    assert result["Data Type"] == "date"
    assert result["Count"] == 10
    assert result["Unique"] == 6
    assert result["Most Frequent Value"] == datetime.datetime(2020, 2, 6).isoformat()
    assert result["Mean"] == datetime.datetime(2020, 1, 20).isoformat()
    assert result["Standard Deviation"] == test_data.std().isoformat()
    assert result["25th Percentile"] == test_data.quantile(0.25).isoformat()
    assert result["Median"] == test_data.median().isoformat()
    assert result["75th Percentile"] == test_data.quantile(0.75).isoformat()
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0

    assert result["Earliest"] == datetime.datetime(2020, 1, 1).isoformat()
    assert result["Latest"] == datetime.datetime(2020, 2, 6).isoformat()
    assert result["Range"] == "P36DT0H0M0S"
