import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    IntegerColumnDescriber,
)


def test_describe():
    test_data = pd.Series([1, 2, 3, 4, 5, 6, 7])
    result = IntegerColumnDescriber().describe(test_data)

    assert result["Data Type"] == "integer"
    assert result["Count"] == 7
    assert result["Mean"] == 4.0
    assert result["Standard Deviation"] == 2.160246899469287
    assert result["Minimum"] == 1
    assert result["25th Percentile"] == 2.5
    assert result["Median"] == 4.0
    assert result["75th Percentile"] == 5.5
    assert result["Maximum"] == 7
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0.0
    assert result["Unique"] == 7
    assert result["Percent Unique"] == 1.0


def test_some_missing():
    test_data = pd.Series([1, 2, 3, 4, pd.NA])
    result = IntegerColumnDescriber().describe(test_data)

    assert result["Count"] == 5
    assert result["Missing"] == 1
    assert result["Percent Missing"] == 0.2


def test_all_missing():
    test_data = pd.Series([pd.NA, pd.NA, pd.NA])
    result = IntegerColumnDescriber().describe(test_data)

    assert result["Count"] == 3
    assert result["Mean"] is None
    assert result["Standard Deviation"] is None
    assert result["Minimum"] is None
    assert result["25th Percentile"] is None
    assert result["Median"] is None
    assert result["75th Percentile"] is None
    assert result["Maximum"] is None
    assert result["Missing"] == 3
    assert result["Percent Missing"] == 1.0
    assert result["Unique"] == 1
    assert result["Percent Unique"] is None


def test_invalid_data():
    # If there are non numeric values in the column, the we should exclude these values when
    # describing the column.
    test_data = pd.Series([1, 2, 3, 4, "a"])
    result = IntegerColumnDescriber().describe(test_data)

    assert result["Count"] == 5
    assert result["Missing"] == 1
    assert result["Percent Missing"] == 0.2
    assert result["Unique"] == 5
    assert result["Percent Unique"] == 1.0
    assert result["Mean"] == 2.5
    assert result["Minimum"] == 1
    assert result["Maximum"] == 4
