import json

import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    GeodataColumnDescriber,
)
from fair_ds_ap42.utils.file_io import find_non_serializable_values


def test_describe():
    # Some sample geodata to test
    test_data = pd.Series([(30, 10), (30, 10), (45, 60), (-10, 40), (-45, -60)])
    result = GeodataColumnDescriber().describe(test_data)

    assert isinstance(result, dict)
    assert result["Data Type"] == "geodata"
    assert result["Count"] == 5
    assert result["Unique"] == 4
    assert result["Percent Unique"] == 4 / 5
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0
    assert result["Minimum Latitude"] == -45
    assert result["Maximum Latitude"] == 45
    assert result["Minimum Longitude"] == -60
    assert result["Maximum Longitude"] == 60
    assert result["Range Latitude"] == 90
    assert result["Range Longitude"] == 120
    assert result["Central Point Latitude"] == 10
    assert result["Central Point Longitude"] == 12

    try:
        json.dumps(result)
    except TypeError as exc:
        raise Exception(
            "Result should be serializable: {}".format(
                find_non_serializable_values(result)
            )
        ) from exc
