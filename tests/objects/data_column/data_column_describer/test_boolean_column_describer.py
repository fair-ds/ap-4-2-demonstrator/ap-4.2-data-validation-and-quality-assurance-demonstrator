import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    BooleanColumnDescriber,
)


def test_describe():
    test_data = pd.Series([True, False, True, False, True, True, False])
    result = BooleanColumnDescriber().describe(test_data)

    assert result["Data Type"] == "boolean"
    assert result["Count"] == 7
    assert result["Most Frequent"] == "True"
    assert result["True/False Ratio"] == 4 / 7
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0.0


def test_no_mode():
    test_data = pd.Series([True, True, False, False])
    result = BooleanColumnDescriber().describe(test_data)

    assert result["Most Frequent"] is None
    assert result["True/False Ratio"] == 0.5


def test_some_missing():
    test_data = pd.Series([True, True, False, False, pd.NA])
    result = BooleanColumnDescriber().describe(test_data)

    assert result["Missing"] == 1
    assert result["Percent Missing"] == 0.2


def test_all_missing():
    test_data = pd.Series([pd.NA, pd.NA, pd.NA])
    result = BooleanColumnDescriber().describe(test_data)

    assert result["Most Frequent"] is None
    assert result["True/False Ratio"] is None
    assert result["Missing"] == 3
    assert result["Percent Missing"] == 1.0
