import json

import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    FloatColumnDescriber,
)


def test_describe():
    test_data = pd.Series([1.23, 4.56789, 8.9101, 2.000, 3.14159, 7.0])
    result = FloatColumnDescriber().describe(test_data)

    # Since we inherit from IntegerColumnDescriber, we only want to test the precision
    # calculations

    assert result["Highest Precision"] == 5
    assert result["Lowest Precision"] == 1
    assert result["Average Precision"] == 3

    # Check that we can write result to a json file
    json.dumps(result)
