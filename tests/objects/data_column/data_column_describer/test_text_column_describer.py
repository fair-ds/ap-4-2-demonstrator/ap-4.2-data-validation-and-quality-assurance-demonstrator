import json

import pandas as pd

from fair_ds_ap42.objects.data_column.data_column_describer import (
    TextColumnDescriber,
)
from fair_ds_ap42.utils.file_io import find_non_serializable_values


def test_describe():
    # Some text data to test
    test_data = pd.Series(
        [
            "This is a test text",
            "This is a test text",
            "This is a test text",
            "slightly different text",
            "text with punctuation!",
            "numbers in text 123",
        ]
    )
    result = TextColumnDescriber().describe(test_data)

    assert isinstance(result, dict)
    assert result["Data Type"] == "text"
    assert result["Count"] == 6
    assert result["Unique"] == 4
    assert result["Percent Unique"] == 4 / 6
    assert result["Missing"] == 0
    assert result["Percent Missing"] == 0
    assert result["Most Frequent Characters"] == [
        ("t", 23),
        ("e", 12),
        ("i", 11),
        ("s", 11),
        ("x", 6),
    ]
    assert result["Most Frequent Numbers"] == [("1", 1), ("2", 1), ("3", 1)]
    assert result["Most Frequent Punctuation"] == [("!", 1)]
    assert result["Most Frequent Words"] == [
        ("text", 6),
        ("This", 3),
        ("is", 3),
        ("a", 3),
        ("test", 3),
    ]
    assert result["Average Word Length"] == 4.04
    assert result["Standard Deviation Word Length"] == 2.3913176284216195
    assert result["Average Sentence Length"] == 20.166666666666668
    assert result["Standard Deviation Sentence Length"] == 1.834847859269718

    try:
        json.dumps(result)
    except TypeError as exc:
        raise Exception(
            "Result should be serializable: {}".format(
                find_non_serializable_values(result)
            )
        ) from exc
