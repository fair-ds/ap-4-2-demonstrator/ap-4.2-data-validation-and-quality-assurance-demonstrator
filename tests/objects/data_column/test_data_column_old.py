import json
import numpy as np
import pandas as pd
import pytest
import string

from fair_ds_ap42.objects.data_column.exceptions import (
    InvalidItemInColumnError,
)
from fair_ds_ap42.objects.data_column.numeric_column import NumericColumn
from fair_ds_ap42.objects.data_column.text_column import TextColumn
from fair_ds_ap42.objects.data_column.boolean_column import BooleanColumn
from fair_ds_ap42.objects.data_column.date_column import DateColumn
from fair_ds_ap42.objects.data_column.geodata_column import (
    GeodataColumn,
    parse_value,
    parse_geodata,
)
from fair_ds_ap42.objects.data_column.util import create_data_column

from tests.util import gen_geo_data

np.random.seed(42)
col_len = 50
TEST_DATA = [
    (
        "integer",
        NumericColumn,
        pd.Series([np.random.randint(10) for x in range(col_len)]),
        None,
    ),
    (
        "number",
        NumericColumn,
        pd.Series([round(np.random.random(), 3) for x in range(col_len)]),
        None,
    ),
    (
        "string",
        TextColumn,
        pd.Series(
            [
                "".join(np.random.choice(list(string.ascii_letters), 5))
                for x in range(col_len)
            ]
        ),
        None,
    ),
    (
        "string",
        TextColumn,
        pd.Series(["".join(np.random.choice(["foo", "bar"])) for x in range(col_len)]),
        None,
    ),
    (
        "boolean",
        BooleanColumn,
        pd.Series([np.random.choice([True, False]) for x in range(col_len)]),
        None,
    ),
    (
        "boolean",
        BooleanColumn,
        pd.Series([np.random.choice([0, 1]) for x in range(col_len)]),
        None,
    ),
    (
        "datetime",
        DateColumn,
        pd.Series(
            [
                np.datetime64("2020-01-01") + np.random.choice(365 * 5)
                for x in range(col_len)
            ]
        ),
        None,
    ),
    (
        "geopoint",
        GeodataColumn,
        pd.Series([gen_geo_data() for x in range(col_len)]),
        None,
    ),
    (
        "geopoint",
        GeodataColumn,
        pd.Series([gen_geo_data("array") for x in range(col_len)]),
        "array",
    ),
    (
        "geopoint",
        GeodataColumn,
        pd.Series([gen_geo_data("object") for x in range(col_len)]),
        "object",
    ),
]


@pytest.mark.parametrize("dtype,object_type,col_data,my_format", TEST_DATA)
def test_create_column(dtype, object_type, col_data, my_format):
    my_col = create_data_column(
        parent=None, name="my_col", col_data=col_data, dtype=dtype, my_format=my_format
    )

    assert isinstance(my_col, object_type)
    assert isinstance(my_col.description, dict)
    assert isinstance(my_col.sample_values(), list)

    # These keys should be present in all column descriptions:
    keys = ["Count", "Unique", "Percent Unique", "Missing", "Percent Missing"]

    # These additional keys should be present in numeric or datetime columns:
    if isinstance(object_type, NumericColumn) or isinstance(object_type, DateColumn):
        keys = keys + [
            "Mean",
            "Standard Deviation",
            "Minimum Value",
            "Maximum Value",
            "25th Percentile",
            "Median",
            "75th Percentile",
            "Outliers",
        ]

    # If it is a boolean column, we don't have a "Unique" value or a "Percent Unique" value
    if dtype == "boolean":
        keys.remove("Unique")
        keys.remove("Percent Unique")

    # These additional keys should be present in text columns:
    if isinstance(object_type, TextColumn):
        keys = keys + ["Minimum Length", "Average Length", "Maximum Length"]

    for key in keys:
        assert key in my_col.description


GEOPOINT_DATA = [
    ("-143.0571,-18.7102", "default"),
    ([-143.0571, -18.7102], "array"),
    ({"lon": -143.0571, "lat": -18.7102}, "object"),
]


@pytest.mark.parametrize("dtype,object_type,col_data,my_format", TEST_DATA)
def test_IO(dtype, object_type, col_data, my_format):
    """We should always be able to write the description we calculate to a
    JSON file"""
    my_col = create_data_column(
        parent=None, name="my_col", col_data=col_data, dtype=dtype, my_format=my_format
    )

    with open("test.json", "w") as f:
        json.dump(my_col.desc, f)


@pytest.mark.parametrize("record,my_format", GEOPOINT_DATA)
def test_parse_value(record, my_format):
    """
    This helper function should parse any one of the three geopoint formats
    """

    assert parse_value(record, my_format) == (-143.0571, -18.7102)


GEOPOINT_DATA = [
    (pd.Series([gen_geo_data() for x in range(col_len)]), "default"),
    (pd.Series([gen_geo_data() for x in range(col_len)]), "array"),
    (pd.Series([gen_geo_data() for x in range(col_len)]), "object"),
]


@pytest.mark.parametrize("data,my_format", GEOPOINT_DATA)
def test_parse_geodata(data, my_format):
    """
    This helper function should parse the geodata into a pair of columns, one
    for longitude and one for latitude
    """

    my_data = parse_geodata(data, my_format)

    assert isinstance(my_data, pd.DataFrame)
    assert set(my_data.columns) == set(["lon", "lat"])


def test_bad_format():
    """
    We can't always trust the schema to match the data type of every item in
    the column data. We should be able to handle the situation where the
    schema says the data is, for example, numeric, but it contains a string
    value
    """

    my_data = pd.Series([str(x) for x in range(20)])
    my_data[10] = "foo"

    with pytest.raises(InvalidItemInColumnError):
        NumericColumn(parent=None, name="my_col", data=my_data)


def test_text_data():
    """
    We have some particular descriptions and plots we want to make for text columns
    """
    my_data = pd.Series(
        [
            "This is a demo sentence and series.",
            "This is a series with sentence with numbers like 7 8 and 7",
            "This is a sentence that has no punctuation",
            "this this this this a a a sentence and",
        ]
    )

    my_col = create_data_column(
        parent=None, name="my_col", col_data=my_data, dtype="string"
    )

    assert "Most Frequent Characters" in my_col.description
    assert set(my_col.description["Most Frequent Characters"]) == set(
        [("n", 15), ("t", 14), ("e", 19), ("s", 20), ("i", 16)]
    )

    assert "Most Frequent Numbers" in my_col.description
    assert set(my_col.description["Most Frequent Numbers"]) == set([("7", 2), ("8", 1)])
    assert "Most Frequent Punctuation" in my_col.description
    assert set(my_col.description["Most Frequent Punctuation"]) == set([(".", 1)])
    assert "Most Frequent Words" in my_col.description
    assert set(my_col.description["Most Frequent Words"]) == set(
        [("sentence", 4), ("this", 4), ("a", 6), ("is", 3), ("This", 3)]
    )
    assert "Average Sentence Length" in my_col.description
    assert my_col.description["Average Sentence Length"] == 43.25
    assert "Average Word Length" in my_col.description
    assert my_col.description["Average Word Length"] == 3.7567567567567566


def test_text_bad_data():
    """
    What happens when we create a text column with bad data
    """
    my_data = pd.Series([None, None, None])
    TextColumn(parent=None, name="bad", data=my_data)

    my_data = pd.Series([5, 2, 78])
    TextColumn(parent=None, name="bad", data=my_data)
