import copy
import datetime
import json

import pytest
from unittest.mock import Mock

from fair_ds_ap42.file_auditor import FileAuditor
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source import ISource
from fair_ds_ap42.objects.resource import Resource

test_metadata_file_contents = [
    {
        "filepath": "file1.csv",
        "source": {"type": "local_directory", "location": "./my_dir"},
        "size_in_bytes": 100,
        "last_modified": "2022-01-01T00:00:00Z",
    },
    {
        "filepath": "file2.csv",
        "source": {"type": "local_directory", "location": "./my_dir"},
        "size_in_bytes": 200,
        "last_modified": "2022-01-02T00:00:00Z",
    },
    {
        "filepath": "file2.csv",
        "source": {
            "type": "s3_bucket",
            "bucket_name": "my_bucket",
        },
        "size_in_bytes": 200,
        "last_modified": "2022-01-02T00:00:00Z",
    },
]


@pytest.fixture
def mock_file_auditor_reader():
    mock = Mock(spec=IJSONDataReader)
    mock.format = "json"
    mock.read_dict.return_value = test_metadata_file_contents
    return mock


# Function to create a mock reader for a given file
def get_mock_reader(filepath):
    mock = Mock(spec=IDataReader)
    mock.exists.side_effect = filepath in [
        "file1.csv",
        "file2.csv",
        "file3.csv",
        "child_dir/file3.csv",
    ]
    mock.get_size_in_bytes.return_value = {
        "file1.csv": 100,
        "file2.csv": 200,
        "file3.csv": 300,
        "child_dir/file3.csv": 300,
    }.get(filepath)
    mock.get_date_last_modified.return_value = {
        "file1.csv": datetime.datetime(2022, 1, 1),
        "file2.csv": datetime.datetime(2022, 1, 2),
        "file3.csv": datetime.datetime(2022, 1, 3),
        "child_dir/file3.csv": datetime.datetime(2022, 1, 3),
    }.get(filepath)
    return mock


@pytest.fixture
def mock_resource(mock_local_source):
    def _gen_resource(filepath, source=mock_local_source):
        mock = Mock(spec=Resource)
        mock.filepath = filepath
        mock.source = source
        mock.size_in_bytes = 0
        mock.last_modified = datetime.datetime(2022, 1, 1)
        return mock

    return _gen_resource


@pytest.fixture
def mock_s3_source():
    mock = Mock(spec=ISource)
    mock.source_type = "s3"
    mock.name = "s3"
    mock.bucket_name = "my_bucket"
    mock.list_files.return_value = ["file2.csv"]
    mock.get_reader.side_effect = lambda filepath: get_mock_reader(filepath)
    mock.exists.side_effect = lambda filepath: filepath in [
        "file2.csv",
    ]
    mock.get_size_in_bytes.side_effect = lambda filepath: {
        "file2.csv": 200,
    }.get(filepath)
    mock.get_date_last_modified.side_effect = lambda filepath: {
        "file2.csv": datetime.datetime(2022, 1, 2),
    }.get(filepath)
    mock.to_json.return_value = {
        "type": "s3_bucket",
        "bucket_name": "my_bucket",
    }
    return mock


# I would have preferred to mock equivilence and similarity methods on the Source class, but
# I couldn't get it to work. Instead, I mocked the methods on the Resource class, using the
# moch.to_json() method to get comparable dictionaries for the Source objects.
@pytest.fixture(autouse=True)
def mock_resource_equivilance(monkeypatch):
    # Define the custom __eq__ method for Resource
    def custom_resource_eq(self, other):
        return (
            self.filepath == other.filepath
            and self.source.to_json() == other.source.to_json()
            and self.size_in_bytes == other.size_in_bytes
            and self.last_modified == other.last_modified
        )

    # Apply the monkeypatch to replace the __eq__ method with the custom implementation
    monkeypatch.setattr(Resource, "__eq__", custom_resource_eq)

    # Define the custom "is_similar_to" method for Resource
    def custom_resource_is_similar_to(self, other):
        result = (
            self.filepath == other.filepath
            and self.source.to_json() == other.source.to_json()
            and (
                self.size_in_bytes != other.size_in_bytes
                or self.last_modified != other.last_modified
            )
        )
        return result

    # Apply the monkeypatch to replace the "is_similar_to" method with the custom implementation
    monkeypatch.setattr(Resource, "is_similar_to", custom_resource_is_similar_to)


def test_initialization(mock_file_auditor_reader):
    # Test that the file_auditor class can be instantiated
    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    assert file_auditor.filepath == "file_metadata.json"
    assert file_auditor.reader == mock_file_auditor_reader


def test_init_no_file():
    # Test that we can initialize a file auditor without a file
    file_auditor = FileAuditor()

    assert file_auditor.reader is None


def test_read_json(mock_file_auditor_reader):
    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    assert file_auditor.file_metadata == test_metadata_file_contents


def test_new_file(mock_file_auditor_reader, mock_local_source, mock_s3_source):
    # Test that we can identify a new file that has been added to the source

    # Update the mock reader to show a third file in the local source
    mock_local_source.list_files.return_value = ["file1.csv", "file2.csv", "file3.csv"]

    # Test
    file_auditor = FileAuditor(reader=mock_file_auditor_reader)
    file_auditor.search_all([mock_local_source, mock_s3_source])

    # We should have three unchanged files - the three original files
    unchanged_files = sorted(
        file_auditor.file_changes.unchanged_files,
        key=lambda x: (x.filepath, x.source.name),
    )

    assert len(unchanged_files) == 3
    assert unchanged_files[0].filepath == "file1.csv"
    assert unchanged_files[0].source.name == "local"
    assert unchanged_files[1].filepath == "file2.csv"
    assert unchanged_files[1].source.name == "local"
    assert unchanged_files[2].filepath == "file2.csv"
    assert unchanged_files[2].source.name == "s3"

    # We should have one new file - file3.csv, in the local directory
    new_files = sorted(file_auditor.file_changes.new_files)
    assert len(new_files) == 1
    assert isinstance(new_files[0], Resource)
    assert new_files[0].filepath == "file3.csv"
    assert new_files[0].source.name == "local"

    # We should have no removed or modified files
    assert file_auditor.file_changes.removed_files == []
    assert file_auditor.file_changes.modified_files == []


def test_removed_file(mock_file_auditor_reader, mock_local_source, mock_s3_source):
    # Test that we can identify a file that has been removed from the source

    # Alter the local source to only have one file
    mock_local_source.list_files.return_value = ["file2.csv"]

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)
    file_auditor.search_all([mock_local_source, mock_s3_source])

    assert len(file_auditor.file_changes.unchanged_files) == 2
    assert file_auditor.file_changes.new_files == []
    assert len(file_auditor.file_changes.removed_files) == 1
    assert file_auditor.file_changes.removed_files[0].filepath == "file1.csv"
    assert file_auditor.file_changes.removed_files[0].source.name == "local"
    assert file_auditor.file_changes.modified_files == []


def test_modified_file(mock_file_auditor_reader, mock_local_source, mock_s3_source):
    # Test that we can identify a file that has been modified in the source

    # Mock the source to show that the file has been modified. In this case, file1.csv has been
    # modified to show 150 bytes and a date last modified of 2022-02-01
    def modified_file_mock_reader(filepath):
        if filepath == "file1.csv":
            mock = Mock(spec=IDataReader)
            mock.exists.result = True
            mock.get_size_in_bytes.return_value = 150
            mock.get_date_last_modified.return_value = datetime.datetime(2022, 2, 1)
            return mock

        return get_mock_reader(filepath)

    mock_local_source.get_reader.side_effect = modified_file_mock_reader

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)
    file_auditor.search_all([mock_local_source, mock_s3_source])

    assert len(file_auditor.file_changes.modified_files) == 1
    assert file_auditor.file_changes.modified_files[0].filepath == "file1.csv"
    assert file_auditor.file_changes.modified_files[0].source.name == "local"

    assert len(file_auditor.file_changes.unchanged_files) == 2
    assert file_auditor.file_changes.new_files == []
    assert file_auditor.file_changes.removed_files == []


def test_write_file(mock_file_auditor_reader, mock_local_source, mock_s3_source):
    # Test that we can write the metadata for a file that has been modified in the source

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)
    file_auditor.search_all([mock_local_source, mock_s3_source])

    # Write the metadata file
    file_auditor.write_json(mock_local_source)

    # Check that the metadata file was written
    assert mock_local_source.write_json.called

    # Check that the metadata file contents are correct
    with open("file_metadata.json", "r") as f:
        metadata_file_contents = json.load(f)

    # Sort the metadata file contents by filepath and source type so that we can compare them
    metadata_file_contents = sorted(
        metadata_file_contents, key=lambda x: (x["filepath"], x["source"]["type"])
    )

    expected_metadata_file_contents = [
        {
            "filepath": "file1.csv",
            "source": {"type": "local_directory", "location": "./my_dir"},
            "size_in_bytes": 100,
            "last_modified": "2022-01-01T00:00:00",
        },
        {
            "filepath": "file2.csv",
            "source": {"type": "local_directory", "location": "./my_dir"},
            "size_in_bytes": 200,
            "last_modified": "2022-01-02T00:00:00",
        },
        {
            "filepath": "file2.csv",
            "source": {"type": "s3_bucket", "bucket_name": "my_bucket"},
            "size_in_bytes": 200,
            "last_modified": "2022-01-02T00:00:00",
        },
    ]

    # Sort the result by filepath so we can compare them
    result = sorted(metadata_file_contents, key=lambda x: x["filepath"])

    for result, expected in zip(
        metadata_file_contents, expected_metadata_file_contents
    ):
        for key in expected.keys():
            assert result[key] == expected[key]

    assert metadata_file_contents == expected_metadata_file_contents


def test_edge_case_empty_json(mock_file_auditor_reader, mock_local_source):
    # Test that we can handle an empty json file when creating a file auditor
    mock_file_auditor_reader.read_dict.return_value = []

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    file_auditor.search_all([mock_local_source])

    assert len(file_auditor.file_changes.unchanged_files) == 0
    assert len(file_auditor.file_changes.new_files) == 2
    assert len(file_auditor.file_changes.removed_files) == 0
    assert len(file_auditor.file_changes.modified_files) == 0


def test_edge_case_no_metadata_json(mock_file_auditor_reader, mock_local_source):
    # Test that we can instantiate a FileAuditor even if the specified JSON file doesn't exist
    mock_file_auditor_reader.read_dict.side_effect = FileNotFoundError

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    assert file_auditor.file_metadata == []


def test_edge_case_empty_source(mock_file_auditor_reader):
    # Test that we can handle a source with no files
    empty_source = Mock(spec=ISource)
    empty_source.list_files.return_value = []
    empty_source.to_json.return_value = {
        "type": "local_directory",
        "location": "./my_dir",
    }

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    file_auditor.search_all([empty_source])

    assert len(file_auditor.file_changes.unchanged_files) == 0
    assert len(file_auditor.file_changes.new_files) == 0
    assert len(file_auditor.file_changes.removed_files) == 2
    assert len(file_auditor.file_changes.modified_files) == 0


def test_search_with_filepatterns(mock_file_auditor_reader, mock_local_source):
    # Test that we can search for files using file patterns
    test_file_contents = test_metadata_file_contents + [
        {
            "filepath": "file3.tsv",
            "source": {"type": "local_directory", "location": "./my_dir"},
            "size_in_bytes": 300,
            "last_modified": "2022-01-03T00:00:00Z",
        }
    ]
    mock_file_auditor_reader.read_dict.return_value = test_file_contents

    file_auditor = FileAuditor(reader=mock_file_auditor_reader, file_patterns="*.csv")

    # Search for files using a single file pattern
    file_auditor.search(source=mock_local_source)

    assert len(file_auditor.file_changes.unchanged_files) == 2
    assert len(file_auditor.file_changes.new_files) == 0
    assert len(file_auditor.file_changes.removed_files) == 0
    assert len(file_auditor.file_changes.modified_files) == 0

    file_auditor = FileAuditor(
        reader=mock_file_auditor_reader, file_patterns=["*.csv", "*.tsv"]
    )

    # Search for files using a list of file patterns
    file_auditor.search(source=mock_local_source)

    assert len(file_auditor.file_changes.unchanged_files) == 2
    assert len(file_auditor.file_changes.new_files) == 0
    assert len(file_auditor.file_changes.removed_files) == 1
    assert len(file_auditor.file_changes.modified_files) == 0


# def test_source_missing_credentials(mock_file_auditor_reader):
#     # Test that we can handle a source that is missing credentials
#     pass


def test_all_existing_files(mock_file_auditor_reader):
    # Test that the all_files property returns all files in the file auditor

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    assert len(file_auditor.all_existing_files) == 3

    # Sort the files by filepath and source type so that we can compare them
    all_files = sorted(
        file_auditor.all_existing_files, key=lambda x: (x.filepath, x.source.name)
    )

    assert all_files[0].filepath == "file1.csv"
    assert all_files[0].source.name == "local"
    assert all_files[1].filepath == "file2.csv"
    assert all_files[1].source.name == "local"
    assert all_files[2].filepath == "file2.csv"
    assert all_files[2].source.name == "s3"


def test_get_existing_files_by_source(mock_file_auditor_reader, mock_local_source):
    # Test that we can request a list of files filtered by source

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    # Get the files from the local source
    local_files = file_auditor.get_existing_files_as_resources(mock_local_source)

    # Sort the files by filepath and source type so that we can compare them
    local_files = sorted(local_files, key=lambda x: (x.filepath, x.source.name))

    assert len(local_files) == 2
    assert local_files[0].filepath == "file1.csv"
    assert local_files[0].source.name == "local"
    assert local_files[1].filepath == "file2.csv"
    assert local_files[1].source.name == "local"


def test_all_files_before_search(mock_file_auditor_reader):
    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    # Get the files before searching
    # This should return an empty list
    assert file_auditor.all_files == []


def test_all_files_after_search(
    mock_file_auditor_reader, mock_local_source, mock_s3_source
):
    # Update the mock reader to show a third file in the local source
    mock_local_source.list_files.return_value = ["file1.csv", "file2.csv", "file3.csv"]

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    # Search the source
    file_auditor.search_all([mock_local_source, mock_s3_source])

    # Get the files after searching
    # This should return a list of all files in the source
    assert len(file_auditor.all_files) == 4

    # Sort the files by filepath and source type so that we can compare them
    all_files = sorted(
        file_auditor.all_files, key=lambda x: (x.filepath, x.source.name)
    )

    assert all_files[0].filepath == "file1.csv"
    assert all_files[0].source.name == "local"
    assert all_files[1].filepath == "file2.csv"
    assert all_files[1].source.name == "local"
    assert all_files[3].filepath == "file3.csv"
    assert all_files[3].source.name == "local"
    assert all_files[2].filepath == "file2.csv"
    assert all_files[2].source.name == "s3"


def test_search_deduplicate(mock_file_auditor_reader, mock_local_source):
    # Test that when we have search results that overlap, we correctly identify duplicate files
    # in our search results and remove the "deeper" file.

    # Modify the local source to contain an additional file in a child directory
    mock_local_source.list_files.return_value = [
        "file1.csv",
        "file2.csv",
        "child_dir/file3.csv",
    ]

    # Create a copy of the local source, but the location is the child directory
    mock_local_source2 = copy.copy(mock_local_source)
    mock_local_source2.location = "./my_dir/child_dir"

    file_auditor = FileAuditor(reader=mock_file_auditor_reader)

    # Search the sources
    file_auditor.search_all([mock_local_source, mock_local_source2])

    # We should only see the new file once - as it is in the first source. We shoud not see the
    # file from the child source
    assert len(file_auditor.file_changes.unchanged_files) == 2
    assert len(file_auditor.file_changes.new_files) == 1
    assert len(file_auditor.file_changes.removed_files) == 0
    assert len(file_auditor.file_changes.modified_files) == 0

    assert file_auditor.file_changes.new_files[0].filepath == "child_dir/file3.csv"
