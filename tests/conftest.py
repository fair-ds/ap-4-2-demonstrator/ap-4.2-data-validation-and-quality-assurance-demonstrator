import datetime
import logging
import os
from pathlib import Path

from moto import mock_s3
import boto3
import pytest
from unittest.mock import Mock

from fair_ds_ap42.state.current_state import CurrentState
from fair_ds_ap42.objects import DataFile, Schema
from fair_ds_ap42.objects.schema_assignment import SchemaAssignment
from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source.reader.ijson_data_reader import IJSONDataReader
from fair_ds_ap42.settings import settings, SettingsManager
from fair_ds_ap42.utils import s3_utils
from fair_ds_ap42.source.available_sources import source_registry

from .fixtures.geodata_fixtures import *  # noqa F401, F403
from .fixtures.local_io_fixtures import *  # noqa F401, F403

from .test_utils.source_mocks import LocalDirectoryMock
from .test_utils.schema_mocks import SchemaMock

from .util import gen_dummy_data, gen_schema, create_setup_from_schemas_file

loggers_to_disable = ["botocore", "urllib3"]


def pytest_configure():
    for logger_name in loggers_to_disable:
        logger = logging.getLogger(logger_name)
        logger.disabled = True


@pytest.fixture(autouse=True)
def t(tmpdir, request):
    os.chdir(str(tmpdir))
    CurrentState.clear()

    if request.node.get_closest_marker("no_dir_setup"):
        logging.error("Skipping directory setup")
        # Skip the fixture logic if the marker is present
        return

    Path(settings.directories.data_directory).mkdir(parents=True, exist_ok=True)
    Path(settings.directories.schema_directory).mkdir(parents=True, exist_ok=True)
    Path(settings.directories.output_directory).mkdir(parents=True, exist_ok=True)
    Path(settings.directories.log_directory).mkdir(parents=True, exist_ok=True)

    # clear all caches
    s3_utils.file_exists_s3.cache_clear()


@pytest.fixture(autouse=True)
def reset_settings():
    settings = SettingsManager()  # noqa F841


@pytest.fixture(scope="session")
def get_data():
    yield gen_dummy_data


@pytest.fixture(scope="session")
def get_schema():
    yield gen_schema


@pytest.fixture(scope="session")
def setup_from_schema():
    yield create_setup_from_schemas_file


@pytest.fixture(scope="function")
def aws_credentials():
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    yield
    os.environ.pop("AWS_ACCESS_KEY_ID", None)
    os.environ.pop("AWS_SECRET_ACCESS_KEY", None)


@pytest.fixture(scope="function")
def s3(aws_credentials):
    with mock_s3():
        client = boto3.client("s3")
        client.create_bucket(Bucket="testbucket")
        yield client


# Specific Object Mocks
mock_json_reader_contents = {
    "schemas/schema1.json": {"type": "string"},
    "schemas/schema2.json": {"type": "number"},
}


@pytest.fixture
def mock_json_reader():
    def _mock_json_reader(filepath: str) -> Mock:
        class MockReader(IDataReader, IJSONDataReader):
            pass

        mock = Mock(spec=MockReader)
        mock.read_dict.return_value = mock_json_reader_contents.get(filepath, {})
        return mock

    return _mock_json_reader


@pytest.fixture
def mock_local_source():
    return LocalDirectoryMock()


@pytest.fixture
def mock_schema():
    return SchemaMock()


@pytest.fixture
def mock_datafile(mock_local_source) -> Mock:
    def _mock_datafile(filepath: str) -> Mock:
        mock = Mock(spec=DataFile)
        mock.filepath = filepath
        mock.source = mock_local_source
        mock.size_in_bytes = {
            "data/file1.csv": 100,
            "data/file2.csv": 200,
            "data/file3.csv": 300,
        }.get(filepath, 0)
        mock.last_modified = {
            "data/file1.csv": datetime.datetime(2020, 1, 1),
            "data/file2.csv": datetime.datetime(2020, 1, 2),
            "data/file3.csv": datetime.datetime(2020, 1, 3),
        }.get(filepath, datetime.datetime(2020, 1, 1))
        return mock

    return _mock_datafile


@pytest.fixture
def mock_schemafile(mock_local_source) -> Mock:
    def _mock_schemafile(filepath: str) -> Mock:
        mock = Mock(spec=Schema)
        mock.filepath = filepath
        mock.source = mock_local_source
        mock.size_in_bytes = {
            "schemas/schema1.json": 100,
            "schemas/schema2.json": 200,
        }.get(filepath, 0)
        mock.last_modified = {
            "schemas/schema1.json": datetime.datetime(2020, 1, 1),
            "schemas/schema2.json": datetime.datetime(2020, 1, 2),
        }.get(filepath, datetime.datetime(2020, 1, 1))
        return mock

    return _mock_schemafile


@pytest.fixture
def mock_schema_assignment(mock_datafile, mock_schemafile):
    mock = Mock(spec=SchemaAssignment)
    mock.assignment = {
        mock_schemafile("schemas/schema1.json"): [
            mock_datafile("data/file1.csv"),
            mock_datafile("data/file2.csv"),
        ],
        mock_schemafile("schemas/schema2.json"): [mock_datafile("data/file3.csv")],
    }
    return mock


@pytest.fixture(autouse=True)
def reset_source_registry():
    # Reset the source_registry before each test
    source_registry.available_sources = []
    yield
