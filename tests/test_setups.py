import json
import os

from tests.util import gen_dummy_data, gen_schema


def simple_full(s3=None, bucket="testbucket", constraints=["char_1"]):
    """
    The most basic setup. Schema assignment file is provided, two schemas, and
    three data files. Two data files belong to schema 1, the other to schema 2.

    If s3 is provided, will instead upload the files over that s3 client to a
    bucket  and remove them from the local folder. This is
    intended only to be used with a moto mock.
    """
    schemas_data = {
        "schemas/schema_1.json": ["data/file_1.csv", "data/file_2.csv"],
        "schemas/schema_2.json": ["data/file_3.csv"],
    }
    with open("schemas/schemas.json", "w") as f:
        json.dump(schemas_data, f)
    with open("schemas/schema_1.json", "w") as f:
        json.dump(gen_schema(constraints=constraints), f)
    with open("schemas/schema_2.json", "w") as f:
        json.dump(gen_schema(schema_type=42, constraints=constraints), f)
    gen_dummy_data().to_csv("data/file_1.csv")
    gen_dummy_data(seed=1).to_csv("data/file_2.csv")
    gen_dummy_data(schema_type=42).to_csv("data/file_3.csv")

    if s3 is not None:
        s3.create_bucket(Bucket="testbucket")
        schemas_file = "schemas/schemas.json"
        s3.upload_file(schemas_file, Bucket=bucket, Key=schemas_file)
        os.remove(schemas_file)

        for schema_file, data_files in schemas_data.items():
            s3.upload_file(schema_file, Bucket=bucket, Key=schema_file)
            os.remove(schema_file)
            for data_file in data_files:
                s3.upload_file(data_file, Bucket=bucket, Key=data_file)
                os.remove(data_file)
