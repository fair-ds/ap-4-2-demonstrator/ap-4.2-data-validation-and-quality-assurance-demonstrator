import pytest

from fair_ds_ap42.junit_xml_gen.util import find_exactly_one, _are_lists_equal
from fair_ds_ap42.junit_xml_gen.objects.junit_baseobject import JUnitBaseObject


class TestFindExactlyOne:
    @pytest.fixture
    def objs(self):
        return [
            JUnitBaseObject(obj_id="test1", name="testcase1"),
            JUnitBaseObject(obj_id="test2", name="testcase2"),
            JUnitBaseObject(obj_id="test3", name="testcase3"),
        ]

    def test_returns_matching_object(self, objs):
        obj = find_exactly_one(objs, pattern="testcase1")
        assert obj.obj_id == "test1"
        assert obj.name == "testcase1"

    def test_raises_error_with_no_matches(self, objs):
        with pytest.raises(ValueError) as exc_info:
            find_exactly_one(objs, pattern="invalid")
        assert 'No objects found matching pattern "invalid".' in str(exc_info.value)

    def test_raises_error_with_multiple_matches(self, objs):
        with pytest.raises(ValueError) as exc_info:
            find_exactly_one(objs, pattern="testcase")
        assert 'Multiple objects found matching pattern "testcase".' in str(
            exc_info.value
        )

    def test_raises_error_with_no_match_options(self, objs):
        with pytest.raises(ValueError) as exc_info:
            find_exactly_one(objs, pattern="invalid", match_id=False, match_name=False)
        assert 'No objects found matching pattern "invalid".' in str(exc_info.value)


class TestAreListsEqual:
    def test_returns_true_for_equal_lists(self):
        assert _are_lists_equal([1, 2, 3], [1, 2, 3])

    def test_returns_false_for_unequal_lists(self):
        assert not _are_lists_equal([1, 2, 3], [1, 2, 4])

    def test_returns_false_for_unequal_lengths(self):
        assert not _are_lists_equal([1, 2, 3], [1, 2, 3, 4])
