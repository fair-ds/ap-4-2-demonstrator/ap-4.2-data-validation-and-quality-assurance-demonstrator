import pytest

from pathlib import Path


"""
The following fixture allows us to create temporary files in a specified location.

Usage:

temp_file_setup = [
    {"filepaths": ["my_dir/file1.txt", "my_dir/file2.txt"]},
]


@pytest.mark.parametrize('temporary_files', temp_file_setup, indirect=True)
def test_something(temporary_files):
    ...
"""


@pytest.fixture
def temporary_files(request):
    filepaths = request.param.get("filepaths", [])

    # Create directories if they don't exist
    for filepath in filepaths:
        dirname = Path(filepath).parent
        if dirname:
            dirname.mkdir(parents=True, exist_ok=True)

    # Create temporary files
    for filepath in filepaths:
        with open(filepath, "w") as file:
            file.write("abcd")

    # Yield the filepaths to the test
    yield filepaths

    # Teardown: Delete the temporary files
    for filepath in filepaths:
        Path(filepath).unlink()
