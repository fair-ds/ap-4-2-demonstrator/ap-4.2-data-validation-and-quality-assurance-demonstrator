import pytest

import pandas as pd

from tests.util import gen_geo_data


@pytest.fixture(
    params=["default", "array", "object"], ids=["default", "array", "object"]
)
def geo_data(request):
    # Set the format of the geodata in this column. One of "default", "array",
    type_ = request.param

    # Return a series of 10 geodata points in the specified format
    # The name of the series is the format, so that we can check that the
    # format is being set correctly
    return pd.Series([gen_geo_data(type_) for x in range(10)], name=type_)
