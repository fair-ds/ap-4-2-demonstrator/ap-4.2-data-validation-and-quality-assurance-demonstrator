import datetime
import json

from unittest.mock import Mock

from fair_ds_ap42.source.reader.idata_reader import IDataReader
from fair_ds_ap42.source import VirtualSource


# Function to create a mock reader for a given file
def get_mock_reader(filepath):
    mock = Mock(spec=IDataReader)
    mock.exists.side_effect = filepath in [
        "file1.csv",
        "file2.csv",
        "file3.csv",
        "child_dir/file3.csv",
    ]
    mock.get_size_in_bytes.return_value = {
        "file1.csv": 100,
        "file2.csv": 200,
        "file3.csv": 300,
        "child_dir/file3.csv": 300,
    }.get(filepath)
    mock.get_date_last_modified.return_value = {
        "file1.csv": datetime.datetime(2022, 1, 1),
        "file2.csv": datetime.datetime(2022, 1, 2),
        "file3.csv": datetime.datetime(2022, 1, 3),
        "child_dir/file3.csv": datetime.datetime(2022, 1, 3),
    }.get(filepath)
    mock.format = "csv"
    return mock


class LocalDirectoryMock(Mock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source_type = "local_directory"
        self.name = "local"
        self.location = "./my_dir"
        self.list_files = Mock()
        self.list_files.return_value = ["file1.csv", "file2.csv"]
        self.get_reader = Mock(side_effect=lambda filepath: get_mock_reader(filepath))
        self.to_json = Mock()
        self.to_json.return_value = {
            "type": "local_directory",
            "location": "./my_dir",
        }

        # Side effect - if we call write_json, we'll write the json to a file in the CWD
        def write_json(filepath, data):
            with open(filepath, "w") as f:
                json.dump(data, f)

        self.write_json = Mock()
        self.write_json.side_effect = write_json


class VirtualSourceMock(Mock):
    def __init__(self, *args, **kwargs):
        super().__init__(spec=VirtualSource, *args, **kwargs)
        self.name = "Virtual"
        self.exists = False
