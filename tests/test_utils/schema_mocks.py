import datetime

from unittest.mock import Mock, MagicMock

from fair_ds_ap42.objects import Schema

RAW_SCHEMA = {
    "fields": [
        {"name": "foo", "type": "integer", "constraints": {"required": True}},
        {"name": "bar", "type": "string"},
    ]
}

SCHEMA1 = {
    "fields": [
        {"name": "index", "type": "integer"},
        {
            "name": "char_1",
            "type": "string",
            "constraints": {
                "required": True,
                "enum": ["X", "Y", "Z"],
                "minLength": 1,
                "maxLength": 1,
            },
        },
        {"name": "str_1", "type": "string"},
        {"name": "bool_1", "type": "boolean"},
        {"name": "date_1", "type": "date"},
        {"name": "float_1", "type": "number"},
        {"name": "int_1", "type": "integer"},
        {"name": "geopoint_1", "type": "geopoint", "format": "default"},
    ],
    "missingValues": ["NaN", "-", "", " "],
    "primaryKey": "index",
}

SCHEMA2 = {
    "fields": [
        {"name": "index", "type": "integer"},
        {
            "name": "char_1",
            "type": "string",
            "constraints": {
                "required": True,
                "enum": ["X", "Y", "Z"],
                "minLength": 1,
                "maxLength": 1,
            },
        },
        {"name": "float_1", "type": "number"},
        {"name": "geopoint_1", "type": "geopoint", "format": "default"},
    ],
    "missingValues": ["NaN", "-", "", " "],
    "primaryKey": "index",
}


def get_mock_schema(filepath):
    """
    We have some standard schemas that we use for testing. This function returns a mock schema
    based on the filepath of the schema.
    """
    mock = SchemaMock()
    if "schema1.json" in filepath:
        mock.raw_schema = SCHEMA1
    elif "schema2.json" in filepath:
        mock.raw_schema = SCHEMA2

    mock.filepath = filepath
    mock.field_names = [field["name"] for field in mock.raw_schema["fields"]]
    mock.required_fields = ["char1"]
    return mock


def get_schema_by_datafile(filepath):
    """
    As with the get_mock_schema function, we have some standard datafiles that we use for testing.
    These are expected to pair with specific schemas. This function returns the schema that is
    expected to pair with the given datafile.
    """

    if "file1.csv" in filepath or "file2.csv" in filepath:
        return get_mock_schema("schema1.json")
    if "file3.csv" in filepath:
        return get_mock_schema("schema2.json")

    return SchemaMock()


class SchemaMock(MagicMock):
    def __init__(self, *args, **kwargs):
        super().__init__(spec=Schema, *args, **kwargs)
        # Properties
        self.filepath = "test_schema.json"
        self.source = Mock()
        self.size_in_bytes = 100
        self.last_modified = datetime.datetime(2020, 1, 1)
        self.field_names = ["foo", "bar"]
        self.required_fields = ["foo"]
        self.raw_schema = RAW_SCHEMA
        self.is_valid = Mock(return_value=True)
        # Methods

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SchemaMock):
            return False
        return True
