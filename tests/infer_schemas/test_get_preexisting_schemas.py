# import json
# from pathlib import Path

# from fair_ds_ap42.infer_schemas import (
#     get_preexisting_schemas, get_testsuite_xml)


# class Test_GetPreexistingSchemas:
#     def test_find_all_schemas_in_folder(self, get_schema):
#         # Setup
#         with open("schemas/schema_1.json", 'w') as f:
#             json.dump(get_schema(), f)

#         with open("schemas/schema_2.json", 'w') as f:
#             json.dump(get_schema(ignore_fields=["bool", "date"]), f)

#         with open("schemas/schema_3.txt", 'w') as f:
#             f.write("NOT A REAL SCHEMA")

#         assert Path("schemas/schema_1.json").exists()
#         assert Path("schemas/schema_2.json").exists()
#         assert Path("schemas/schema_3.txt").exists()

#         # Test
#         existing_files = get_preexisting_schemas()
#         assert isinstance(existing_files, list)
#         assert existing_files == [
#             "schemas/schema_1.json", "schemas/schema_2.json"], (
#                 "We should only return schema files that end in .json")

#         # we should have two testcases, one for each schema
#         testsuite = get_testsuite_xml().get_testsuite("get_preexisting_schemas")
#         assert testsuite.worst_message == "SUCCESS"
#         assert testsuite.num_tests == 2
#         for testcase in testsuite.testcases:
#             assert testcase.name.startswith("Found Schema File")

#     def test_find_all_schemas_check_invalid(self, get_schema):
#         # Setup
#         with open("schemas/schema_1.json", 'w') as f:
#             json.dump(get_schema(), f)

#         with open("schemas/schema_2.json", 'w') as f:
#             json.dump({"bad_schema": []}, f)

#         # Test
#         existing_files = get_preexisting_schemas()
#         assert existing_files == ["schemas/schema_1.json"], (
#                 "We should only return schema files that pass validation")

#         # we should have two testcases, one where we find the schema and an
#         # error that one of them wasn't valid
#         testsuite = get_testsuite_xml().get_testsuite("get_preexisting_schemas")
#         assert testsuite.worst_message == "ERROR"
#         assert testsuite.num_tests == 2

#         tc1, tc2 = testsuite.testcases
#         assert tc1.name.startswith("Found Schema File")
#         assert tc2.name.startswith("Invalid Schema File")
