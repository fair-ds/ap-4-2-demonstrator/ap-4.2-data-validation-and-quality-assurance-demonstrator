import pytest

from fair_ds_ap42.settings.file_pattern_settings import FilePatternSettings


@pytest.fixture
def set_env_vars(monkeypatch):
    """
    Fixture that sets environment variables for a single test.
    """
    monkeypatch.setenv("DATA_FILE_PATTERNS", "[custom_data_pattern]")
    monkeypatch.setenv("SCHEMA_FILE_PATTERNS", "[custom_schema_pattern]")
    monkeypatch.setenv("FILE_IGNORE_PATTERNS", "[custom_ignore_pattern]")


@pytest.fixture
def set_env_vars_comma_sep(monkeypatch):
    """
    Fixture that sets environment variables for a single test.
    """
    monkeypatch.setenv("DATA_FILE_PATTERNS", "pattern1, pattern2")
    monkeypatch.setenv("SCHEMA_FILE_PATTERNS", "pattern3, pattern4")
    monkeypatch.setenv("FILE_IGNORE_PATTERNS", "pattern5")


def test_default_values():
    """
    We would set "data" and "schemas" as default values if nothing else is provided.
    """
    settings = FilePatternSettings()
    assert settings.data_file_patterns == ["*.csv", "*.parquet"]
    assert settings.schema_file_patterns == ["*.json"]
    assert settings.ignore_patterns == ["*schemas.json", "*results.json"]


def test_environment_variable_values(set_env_vars):
    """
    If we find the DATA_FILE_PATTERNS, SCHEMA_FILE_PATTERNS or FILE_IGNORE_PATTERNS environment
    variables, we should use those values.
    """
    settings = FilePatternSettings()
    assert settings.data_file_patterns == ["custom_data_pattern"]
    assert settings.schema_file_patterns == ["custom_schema_pattern"]
    assert settings.ignore_patterns == ["custom_ignore_pattern"]


def test_provided_values():
    """
    If the user provides values for the file patterns and ignore patterns, we should use those
    values.
    """
    settings = FilePatternSettings(
        data_file_patterns=["provided_data"],
        schema_file_patterns=["provided_schemas"],
        ignore_patterns=["provided_ignore"])
    assert settings.data_file_patterns == ["provided_data"]
    assert settings.schema_file_patterns == ["provided_schemas"]
    assert settings.ignore_patterns == ["provided_ignore"]


def test_mixed_values(set_env_vars):
    """
    If the user provides values for file patterns and ignore patterns, we should use those values,
    even if the environment variables are also set.
    """
    settings = FilePatternSettings(
        data_file_patterns=["provided_data_pattern"],
        ignore_patterns=["provided_ignore_pattern"])

    assert settings.data_file_patterns == ["provided_data_pattern"]
    assert settings.schema_file_patterns == ["custom_schema_pattern"]
    assert settings.ignore_patterns == ["provided_ignore_pattern"]


def test_comma_separated_values(set_env_vars_comma_sep):
    """
    If the user provides values for file patterns and ignore patterns, we should use those values,
    even if the environment variables are also set.
    """
    settings = FilePatternSettings()

    assert settings.data_file_patterns == ["pattern1", "pattern2"]
    assert settings.schema_file_patterns == ["pattern3", "pattern4"]
    assert settings.ignore_patterns == ["pattern5"]
