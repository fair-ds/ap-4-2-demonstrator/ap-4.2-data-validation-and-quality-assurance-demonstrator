"""
Test that we can properly parse a settings file that contains information about a URL source.
"""

import pytest

from fair_ds_ap42.settings import SettingsManager

settings_file_contents = """
        [PROJECT]
        name = "Example Project"
        csv_field_delimiter = ","

        [STORAGE]
            [STORAGE.URL]
                url = "http://www.example.com/data.csv"
        """


@pytest.fixture
def settings_file(tmp_path):
    """
    Fixture that creates a temporary settings file.
    """
    settings_file = tmp_path / "settings.toml"
    settings_file.write_text(settings_file_contents)
    return settings_file


def test_load_settings(settings_file):
    """
    Test that the settings file is loaded correctly.
    """
    settings = SettingsManager.load(settings_file)

    assert settings.project.name == "Example Project"
    assert settings.storage.url.url == "http://www.example.com/data.csv"
