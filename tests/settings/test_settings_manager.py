from pathlib import Path

import pytest

from fair_ds_ap42.settings import SettingsManager


@pytest.fixture
def settings_file(tmp_path):
    """
    Fixture that creates a temporary settings file.
    """
    settings_file = tmp_path / "settings.toml"
    settings_file.write_text(
        """
        [PROJECT]
        name = "Example Project"
        csv_field_delimiter = ","

        [DIRECTORIES]
        root = ""
        data = "data"
        schemas = "schemas"

        [FILE_PATTERNS]
        data_file_patterns = ["*.csv", "*.parquet"]
        schema_file_patterns = ["*.json"]

        [STORAGE]
            [STORAGE.S3]
                bucket = "MyBucket"

        [GEODATA]
        crs = "EPSG:4326"
        shape_file = "naturalearth_lowres"

        [FILE_DETAILS]
            [[FILE_DETAILS.FILE]]
                filename = "data/file1.csv"
                latitude = "latitude_column_name"
                longitude = "longitude_column_name"
            [[FILE_DETAILS.FILE]]
                filename = "data/file2.csv"
                delimiter = "\t"
                encoding = "cp1252"
        """
    )
    return settings_file


def test_load_settings(settings_file, s3):
    """
    Test that the settings file is loaded correctly.
    """
    settings = SettingsManager.load(settings_file)

    assert settings.project.name == "Example Project"
    assert settings.directories.data == Path("data")
    assert settings.file_patterns.data_file_patterns == ["*.csv", "*.parquet"]
    assert settings.storage.s3.bucket == "MyBucket"
    assert settings.storage.s3.credentials_provided
    assert settings.per_file.files[0].filename == "data/file1.csv"
    assert settings.per_file.files[0].latitude == "latitude_column_name"
    assert settings.per_file.files[0].longitude == "longitude_column_name"
    assert settings.per_file.files[0].delimiter == ","
    assert settings.per_file.files[0].encoding == "utf-8"

    assert settings.per_file.files[1].filename == "data/file2.csv"
    assert settings.per_file.files[1].latitude is None
    assert settings.per_file.files[1].longitude is None
    assert settings.per_file.files[1].delimiter == "\t"
    assert settings.per_file.files[1].encoding == "cp1252"


def test_settings_missing_bucket(settings_file, s3):
    """
    Test that an error is raised if the bucket is missing.
    """

    settings = SettingsManager.load(settings_file)
    settings.storage.s3.bucket = None

    with pytest.raises(KeyError):
        settings.storage.s3.credentials_provided


def test_load_settings_missing_file():
    """
    Test that an empty SettingsManager object is returned if the settings file is missing.
    """
    settings = SettingsManager.load(Path("non_existent_file.toml"))

    assert isinstance(settings, SettingsManager)
    assert settings.project.name == ""
    assert settings.directories.data == Path("data")
    assert settings.file_patterns.data_file_patterns == ["*.csv", "*.parquet"]
    assert settings.storage.s3.bucket is None
    assert settings.per_file.files == []


def test_load_settings_empty_file(tmp_path):
    """
    Test that an empty SettingsManager object is returned if the settings file is empty.
    """
    settings_file = tmp_path / "empty_settings.toml"
    settings_file.write_text("")

    settings = SettingsManager.load(settings_file)

    assert isinstance(settings, SettingsManager)
    assert settings.project.name == ""
    assert settings.directories.data == Path("data")
    assert settings.file_patterns.data_file_patterns == ["*.csv", "*.parquet"]
    assert settings.storage.s3.bucket is None
    assert settings.per_file.files == []
