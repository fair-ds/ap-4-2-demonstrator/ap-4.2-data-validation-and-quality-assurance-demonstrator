from pathlib import Path

import pytest

from fair_ds_ap42.settings.directory_settings import DirectorySettings


@pytest.fixture
def set_env_vars(monkeypatch):
    """
    Fixture that sets environment variables for a single test.
    """
    monkeypatch.setenv("DATA_DIRECTORY", "env_data")
    monkeypatch.setenv("SCHEMAS_DIRECTORY", "env_schemas")


def test_default_values():
    """
    We would set "data" and "schemas" as default values if nothing else is provided.
    """
    settings = DirectorySettings()
    assert settings.root == Path("")
    assert settings.data == Path("data")
    assert settings.schemas == Path("schemas")


def test_environment_variable_values(set_env_vars):
    """
    If we find the DATA_DIRECTORY and SCHEMAS_DIRECTORY environment variables, we should use
    those values.
    """
    settings = DirectorySettings()
    assert settings.data == Path("env_data")
    assert settings.schemas == Path("env_schemas")


def test_provided_values():
    """
    If the user provides values for "data" and "schemas", we should use those values.
    """
    settings = DirectorySettings(data="provided_data", schemas="provided_schemas")
    assert settings.data == Path("provided_data")
    assert settings.schemas == Path("provided_schemas")


def test_mixed_values(set_env_vars):
    """
    If the user provides values for "data" and "schemas", we should use those values, even if the
    environment variables are set.
    """
    settings = DirectorySettings(data="provided_data")
    assert settings.data == Path("provided_data")
    assert settings.schemas == Path("env_schemas")
