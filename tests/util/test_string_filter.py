from fair_ds_ap42.utils.string_filter import StringFilter


def test_is_regex():
    assert StringFilter._is_regex(".*\\.csv")
    assert not StringFilter._is_regex("*.csv")
    assert StringFilter._is_regex("[a-z]+")


def test_filter_regex():
    my_filter = StringFilter(".*\\.csv")
    strings = ["abc.csv", "def.parquet", "ghi.json", "jkl.xlsx"]
    filtered = my_filter.filter_list(strings)
    assert filtered == ["abc.csv"]


def test_filter_unix():
    my_filter = StringFilter("*.csv")
    strings = ["abc.csv", "def.parquet", "ghi.json", "jkl.xlsx"]
    filtered = my_filter.filter_list(strings)
    assert filtered == ["abc.csv"]


def test_filter_ignore_regex():
    my_filter = StringFilter(".*", ignore_pattern=".*\\.json")
    strings = ["abc.csv", "def.parquet", "ghi.json", "jkl.xlsx"]
    filtered = my_filter.filter_list(strings)

    assert "ghi.json" not in filtered, "Should not contain ghi.json"
    assert "abc.csv" in filtered, "Should contain abc.csv"
    assert "def.parquet" in filtered, "Should contain def.parquet"
    assert "jkl.xlsx" in filtered, "Should contain jkl.xlsx"


def test_filter_ignore_unix():
    my_filter = StringFilter(".*", ignore_pattern="*.json")
    strings = ["abc.csv", "def.parquet", "ghi.json", "jkl.xlsx"]
    filtered = my_filter.filter_list(strings)

    assert "ghi.json" not in filtered, "Should not contain ghi.json"
    assert "abc.csv" in filtered, "Should contain abc.csv"
    assert "def.parquet" in filtered, "Should contain def.parquet"
    assert "jkl.xlsx" in filtered, "Should contain jkl.xlsx"
