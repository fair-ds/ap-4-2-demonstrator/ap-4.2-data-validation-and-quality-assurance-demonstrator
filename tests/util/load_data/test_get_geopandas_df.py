import geopandas as gpd
import pytest

from fair_ds_ap42.utils.load_data import get_geopandas_df
from fair_ds_ap42.settings import settings

from tests.setups import Setups

test_formats = [{"geopoint": "default"}, {"geopoint": "array"}, {"geopoint": "object"}]


@pytest.mark.skip(reason="This section due for rework")
@pytest.mark.parametrize("formats", test_formats)
def test_various_schemas(get_data, get_schema, formats):
    # Setup
    Setups.GenFile.data(get_data, schema=11, formats=formats)
    Setups.GenFile.schema(get_schema, schema=11)

    # Test
    retval = get_geopandas_df("data/file_1.csv", "geopoint_1")

    assert isinstance(retval, gpd.GeoDataFrame)
    assert "geometry" in retval.columns


@pytest.mark.skip(reason="This section due for rework")
def test_two_float_columns(get_data, get_schema):
    """frictionless relies on the data conforming to the geopoint data type,
    but we also want to be sure that we can load a dataset with geocoordinates
    in two columns of floats"""

    # Setup
    # gena geo column and split it into two floats, so we can be sure we're
    # getting valid coordinates
    df = get_data(fields=["str", "geopoint"])
    df["float_1"] = df.geopoint_1.apply(lambda x: x.split(",")[0])
    df["float_2"] = df.geopoint_1.apply(lambda x: x.split(",")[1])
    df.drop(columns=["geopoint_1"], inplace=True)
    df.to_csv("data/file_1.csv")

    Setups.GenFile.schema(get_schema, fields=["str", "float", "float"])

    settings.geodata_file_details = {
        "data/file_1.csv": {"longitude": "float_1", "latitude": "float_2"}
    }

    # Test
    retval = get_geopandas_df("data/file_1.csv")

    assert isinstance(retval, gpd.GeoDataFrame)
    assert "geometry" in retval.columns


which_files = [(False, False), (True, False), (True, True)]


@pytest.mark.skip(reason="Is this test even needed?")
@pytest.mark.parametrize("make_schema, make_schemas", which_files)
def test_missing_file(get_data, get_schema, make_schema, make_schemas):
    """We shouldn't be able to get to this point without:
    - a schema file
    - a schemas file that associates this file with a schema
    - a package file created from the above
    Any of the above missing should generate a FileNotFound Exception
    """
    # Setup
    Setups.GenFile.data(get_data, schema=11)

    if make_schema:
        Setups.GenFile.schema(get_schema, schema=11)

    if make_schemas:
        my_schemas = {"schemas/schema_1.json": ["data/file_1.csv"]}
        Setups.GenFile.schemas_file(my_schemas)

    # Test
    with pytest.raises(FileNotFoundError):
        get_geopandas_df("data/file_1.csv")
