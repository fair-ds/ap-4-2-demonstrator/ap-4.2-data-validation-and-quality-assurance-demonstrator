from frictionless import Schema

from fair_ds_ap42.utils.load_data import get_pandas_dtypes_from_schema


def test_get_pandas_dtypes_from_schema():
    my_schema = Schema.from_descriptor(
        {
            "fields": [
                {"name": "id", "type": "integer"},
                {"name": "field2", "type": "number"},
                {"name": "field3", "type": "string"},
                {"name": "field4", "type": "date"},
                {"name": "field5", "type": "datetime"},
                {"name": "field6", "type": "boolean"},
                {"name": "field7", "type": "geopoint"},
            ]
        }
    )

    my_datatypes = get_pandas_dtypes_from_schema(my_schema)

    assert my_datatypes["id"] == "Int64"
    assert my_datatypes["field2"] == "float64"
    assert my_datatypes["field3"] == "object"
    assert my_datatypes["field4"] == "datetime64"
    assert my_datatypes["field5"] == "datetime64"
    assert my_datatypes["field6"] == "bool"
    assert my_datatypes["field7"] == "object"
