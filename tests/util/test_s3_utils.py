import datetime
import os
from pathlib import Path

import pandas as pd
import pytest

from fair_ds_ap42.settings import settings
from fair_ds_ap42.utils.s3_utils import (
    create_s3_client,
    file_exists_s3,
    get_dataset_columns,
    get_filelist_from_s3,
    get_full_path,
    read_file_from_s3,
    write_dataframe_to_s3,
    move_file_to_s3,
    get_last_modified,
    get_size_in_bytes,
)

from tests.util import gen_dummy_data


@pytest.fixture(autouse=True, scope="module")
def set_credentials():
    settings.storage.s3.bucket = "testbucket"


class Test_create_S3_client:
    def test_create(self, s3):
        create_s3_client()


class Test_get_full_path:
    @pytest.mark.parametrize("my_file", ["test_file.csv", Path("test_file.csv")])
    def test_create(self, my_file):
        ret_val = get_full_path(my_file)

        assert isinstance(ret_val, str)
        assert ret_val == f"s3://{settings.storage.s3.bucket}/test_file.csv"

    def test_s3_path(self):
        ret_val = get_full_path(f"s3://{settings.storage.s3.bucket}/test_file.csv")

        assert isinstance(ret_val, str)
        assert ret_val == f"s3://{settings.storage.s3.bucket}/test_file.csv"


class Test_file_exists_S3:
    @pytest.mark.parametrize("my_file", ["test_file.csv", Path("test_file.csv")])
    def test_file_does_not_exist(self, s3, my_file):
        s3.create_bucket(Bucket="testbucket")
        gen_dummy_data().to_csv(my_file)
        s3.upload_file(Path(my_file).as_posix(), "testbucket", Path(my_file).as_posix())
        os.remove(my_file)

        assert file_exists_s3(my_file)
        assert not file_exists_s3("not_a_file_that_exists.txt")


class Test_get_filelist_from_S3:
    def test_get_filelist(self, s3):
        my_file = "test_file.csv"
        s3.create_bucket(Bucket="testbucket")
        gen_dummy_data().to_csv(my_file)
        s3.upload_file(my_file, "testbucket", my_file)
        os.remove(my_file)

        ret_val = get_filelist_from_s3()

        assert isinstance(ret_val, list)
        assert my_file in ret_val


class Test_read_file_from_S3:
    # TODO: @pytest.mark.parametrize("file_type", ["csv", "parquet", "txt"])
    @pytest.mark.parametrize("file_type", ["csv", "txt"])
    @pytest.mark.parametrize("is_path", [False, True])
    def test_read_file_from_S3(self, s3, file_type, is_path):
        my_file = f"test_file.{file_type}"
        if is_path:
            my_file = Path(my_file)

        if file_type == "txt":
            with open(my_file, "w") as f:
                f.write("I am a text file")
        elif file_type == "csv":
            gen_dummy_data().to_csv(my_file)
        elif file_type == "parquet":
            gen_dummy_data().to_parquet(my_file)

        s3.create_bucket(Bucket="testbucket")
        s3.upload_file(Path(my_file).as_posix(), "testbucket", Path(my_file).as_posix())
        os.remove(my_file)

        if file_type == "txt":
            with pytest.raises(NotImplementedError):
                read_file_from_s3(my_file)
        else:
            ret_val = read_file_from_s3(my_file)
            assert isinstance(ret_val, pd.DataFrame)

    def test_read_file_does_not_exist(self, s3):
        s3.create_bucket(Bucket="testbucket")
        with pytest.raises(FileNotFoundError):
            read_file_from_s3("not_a_file_that_exists.txt")


class Test_write_dataframe_to_S3:
    # TODO: @pytest.mark.parametrize("file_type", ["csv", "parquet"])
    @pytest.mark.parametrize("file_type", ["csv"])
    @pytest.mark.parametrize("is_path", [False, True])
    def test_write_dataframe_to_S3(self, s3, file_type, is_path):
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
        filename = f"test_file.{file_type}"
        if is_path:
            filename = Path(filename)

        s3.create_bucket(Bucket="testbucket")
        ret_val = write_dataframe_to_s3(df, filename, file_type)

        assert isinstance(ret_val, bool)
        assert ret_val
        assert file_exists_s3(filename)

    def test_write_dataframe_overwrite(self, s3):
        my_file = "test_file.csv"
        df = pd.DataFrame({"id": [1, 2], "name": ["foo", "bar"]})
        s3.create_bucket(Bucket="testbucket")
        gen_dummy_data().to_csv(my_file)
        s3.upload_file(my_file, "testbucket", my_file)
        os.remove(my_file)

        with pytest.raises(FileExistsError):
            write_dataframe_to_s3(df, my_file, "csv", overwrite=False)

    def test_bad_file_format(self, s3):
        with pytest.raises(NotImplementedError):
            write_dataframe_to_s3(pd.DataFrame({}), "test_file.tsv", "tsv")


class Test_move_file_to_S3:
    @pytest.mark.parametrize("my_file", ["test_file.txt", Path("test_file.txt")])
    @pytest.mark.parametrize("is_path", [False, True])
    def test_move_file(self, s3, my_file, is_path):
        if is_path:
            my_file = Path(my_file)

        with open(my_file, "w") as f:
            f.write("I am a test text file!")

        s3.create_bucket(Bucket="testbucket")
        ret_val = move_file_to_s3(my_file, my_file)

        assert isinstance(ret_val, bool)
        assert ret_val
        assert file_exists_s3(my_file)

    def test_move_file_do_not_overwrite(self, s3):
        filename = "my_file.txt"
        with open(filename, "w") as f:
            f.write("I am a test text file!")

        s3.create_bucket(Bucket="testbucket")
        assert move_file_to_s3(filename, filename)
        with pytest.raises(FileExistsError):
            move_file_to_s3(filename, filename, overwrite=False)


class Test_get_last_modified:
    @pytest.mark.parametrize("my_file", ["test_file.csv", Path("test_file.csv")])
    def test_get_date(self, s3, my_file):
        s3.create_bucket(Bucket="testbucket")
        gen_dummy_data().to_csv(my_file)
        s3.upload_file(Path(my_file).as_posix(), "testbucket", Path(my_file).as_posix())
        os.remove(my_file)

        ret_val = get_last_modified(my_file)
        assert isinstance(ret_val, datetime.datetime)

    def test_get_date_missingfile(self, s3):
        s3.create_bucket(Bucket="testbucket")
        with pytest.raises(FileNotFoundError):
            get_last_modified("not_a_real_file.txt")


class Test_get_size_in_bytes:
    @pytest.mark.parametrize("my_file", ["test_file.csv", Path("test_file.csv")])
    def test_get_size_in_bytes(self, s3, my_file):
        s3.create_bucket(Bucket="testbucket")
        gen_dummy_data().to_csv(my_file)
        s3.upload_file(Path(my_file).as_posix(), "testbucket", Path(my_file).as_posix())
        os.remove(my_file)

        ret_val = get_size_in_bytes(my_file)
        assert isinstance(ret_val, int)

    def test_get_date_missingfile(self, s3):
        s3.create_bucket(Bucket="testbucket")
        with pytest.raises(FileNotFoundError):
            get_size_in_bytes("not_a_real_file.txt")


class Test_get_dataset_columns:
    def test_csv_columns(self, s3):
        my_file = "test_file.csv"
        s3.create_bucket(Bucket="testbucket")
        df = gen_dummy_data()
        df.to_csv(my_file)
        s3.upload_file(my_file, "testbucket", my_file)
        os.remove(my_file)

        my_cols = get_dataset_columns("test_file.csv")

        assert set(my_cols) == set(list(df.columns) + ["index"])

    @pytest.mark.skip  # TODO
    def test_parquet_columns(self, s3):
        my_file = "test_file.parquet"
        s3.create_bucket(Bucket="testbucket")
        df = gen_dummy_data()
        df.to_parquet(my_file)
        s3.upload_file(my_file, "testbucket", my_file)
        os.remove(my_file)

        my_cols = get_dataset_columns("test_file.parquet")

        assert set(my_cols) == set(list(df.columns) + ["index"])
