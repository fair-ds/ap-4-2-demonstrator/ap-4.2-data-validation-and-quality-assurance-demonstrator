"""
Helper functions related to schema files

These should be incorporated into the schemas objects if they aren't already,
and deleted if not used.
"""

import difflib
import json
import logging
from pathlib import Path
from typing import Union

import frictionless
import pandas as pd

from ..settings import settings

logger = logging.getLogger(__name__)


def get_suggested_constraints(series: pd.Series, data_type: str) -> dict:
    """
    Guess some constraints that might apply to this series

    Args:
        series (pd.Series):
            The series to look at.
        data_type (str):
            The Data Type identified when frictionless looked at the file.

    Returns:
        dict:
            A dictionary of suggested constraints that canbe added into a
            frictionless schema JSON.
    """
    constraints = {}

    # is the column totally unique?
    is_unique = series.nunique() == len(series)
    if is_unique:
        constraints["unique"] = True

    # are there any missing values?
    no_missing = series.isnull().sum()
    if no_missing:
        constraints["required"] = True

    # If it's a numeric column, suggest a max/min value
    if data_type == "number" or (data_type == "integer" and not is_unique):
        constraints["minimum"] = int(series.min())
        constraints["maximum"] = int(series.max())

    # If it's a string column, could it be a categorical? If not, what's the
    # shortest/longest string we see?
    if data_type == "string":
        is_enum = series.nunique() < 10
        if is_enum:
            constraints["enum"] = sorted(list(series.unique()))
        else:
            constraints["minLength"] = int(series.apply(len).min())
            constraints["maxLength"] = int(series.apply(len).max())

    return constraints


def validate_schema(
    filename: Union[Path, str],
    schema: Union[frictionless.Schema, dict, str, Path],
    strict=True,
) -> bool:
    """Validate a given schema on a given file

    Applies the schema to the file and checks to make sure it passes
    frictionless' validation in the report.

    Args:
        filename (Union[Path, str]):
            The Path or filename of the file to be validated
        schema (Union[frictionless.Schema, dict]):
            The schema to chec on the file

    Returns:
        bool
    """
    my_resource = frictionless.Resource(filename, schema=schema)

    if strict:
        skip_errors = []
    else:
        skip_errors = ["constraint-error", "type-error", "blank-row", "unique-error"]

    return frictionless.validate(my_resource, skip_errors=skip_errors).valid


def get_schema_report(
    filename: Union[Path, str], schema: Union[frictionless.Schema, dict, str, Path]
) -> list[str]:
    """
    Get a report from frictionless about the schema.
    """

    # Load in the schema if we need to
    if isinstance(schema, dict):
        schema = frictionless.Schema(schema)
    elif isinstance(schema, (Path, str)):
        with open(schema, "r", encoding="utf-8") as file_obj:
            schema = frictionless.Schema(json.load(file_obj))

    my_report = []

    # Check if the columns match up. If they don't we're going to get a
    # mismatched label, extra label, or missing label, which is fine, but then
    # we might get a shedload of type warkings cluttering things up. We're
    # going to short circut things here and make our own message if that's the
    # case
    data_file_cols = list(pd.read_csv(filename, nrows=0).columns)
    if len(set(data_file_cols) ^ set(schema.field_names)) != 0:
        logger.debug("Identified a schema column mismatch for file %s", filename)
        return [
            (
                "column-mismatch",
                "/n".join(
                    list(difflib.context_diff(data_file_cols, schema.field_names))
                ),
            )
        ]

    my_resource = frictionless.Resource(filename, schema=schema)
    report = frictionless.validate(my_resource)

    if not report.valid:
        for item in report.tasks[0].errors:
            report_item = (item.code, f"{item.description} {item.message}")
            if report_item not in my_report:
                my_report.append(report_item)
                logger.debug("report item: %s", report_item)

    return my_report


def load_schema(name: str) -> frictionless.Schema:
    """Load a given schema file

    Assumes that schema files will be stored in the SCHEMA_FOLDER

    Args:
        name (str):
            The name of the schema file to load

    Returns:
        frictionless.Schema
    """
    filename = Path(name)
    if not filename.exists():
        raise FileNotFoundError(f"{filename} does not exist")
    return frictionless.Schema(filename)


def get_fields_of_type(my_schema, my_type):
    """
    Get all fields with the given type from the schema file.
    """
    fields_by_type = {t: [] for t in {field.type for field in my_schema.fields}}

    for field in my_schema.fields:
        fields_by_type[field.type].append(field.name)

    if my_type == "numeric":
        return fields_by_type.get("integer", []) + fields_by_type.get("number", [])

    if my_type == "text":
        return fields_by_type.get("string", []) + fields_by_type.get("any", [])

    if my_type == "date":
        return fields_by_type.get("date", [])

    if my_type == "bool":
        return fields_by_type.get("boolean", [])

    if my_type == "geo":
        return fields_by_type.get("geopoint", [])

    raise TypeError(f"Unrecognized type: {my_type}")


def get_all_schemas():
    """
    Get all schemas in the schema folder.
    """
    schemas = {}

    for schema_file in settings.directories.schema_directory.glob("**/*.json"):
        schema = frictionless.Schema(schema_file)
        if frictionless.validate(schema).valid:
            schemas[schema_file.as_posix()] = schema

    return schemas
