"""
Provides a class to filter a list of strings using a glob or regex pattern.

Examples:
    >>> from fair_ds_ap42.utils.string_filter import StringFilter
    >>> string_filter = StringFilter(pattern="*.csv")
    >>> string_filter.filter_list(["file1.csv", "file2.csv", "file3.parquet"])
    ["file1.csv", "file2.csv"]

    >>> string_filter = StringFilter(pattern=["*.csv", "*.parquet"])
    >>> string_filter.filter_list(["file1.csv", "file2.csv", "file3.parquet"])
    ["file1.csv", "file2.csv", "file3.parquet"]

    >>> string_filter = StringFilter(pattern="file*.csv")
    >>> string_filter.filter_list(["file1.csv", "file2.csv", "file3.parquet"])
    ["file1.csv", "file2.csv"]
"""

from dataclasses import dataclass
import fnmatch
import logging
import re


logger = logging.getLogger(__name__)


@dataclass
class StringFilter:
    """Class to filter a list of strings using a glob or regex pattern."""

    pattern: str | list[str] = None
    ignore_pattern: str | list[str] = None
    is_regex: bool = False

    @classmethod
    def _is_regex(cls, pattern: str) -> bool:
        try:
            re.compile(pattern)
            return True
        except re.error:
            return False

    @staticmethod
    def _filter_list_with_patterns(
        strings: list[str], patterns: list[str], invert: bool = False
    ) -> list[str]:
        """
        Given a list of strings and a list of patterns, filter the list using the patterns.

        If invert is True, rather than returning the strings that match any of the patterns,
        return the strings that do not match any of the patterns.
        """
        if len(strings) == 0 or patterns is None:
            return strings

        all_matches = set()
        logger.debug("Filtering strings: %s", strings)
        if invert:
            logger.debug("Removing strings that match patterns: %s", patterns)
        else:
            logger.debug("Keeping only strings that match patterns: %s", patterns)

        for pattern in patterns:
            is_regex = StringFilter._is_regex(pattern)

            if is_regex:
                matches = {item for item in strings if re.match(pattern, item)}
            else:
                matches = set(fnmatch.filter(strings, pattern))

            all_matches.update(matches)

        if invert:
            return [item for item in strings if item not in all_matches]

        return list(all_matches)

    def filter_list(self, strings: list[str]) -> list[str]:
        """
        Filter a list of strings using a glob or regex pattern.

        The pattern can be a glob or regex pattern. If the pattern is a glob pattern,
        the list of strings will be filtered using fnmatch.filter. If the pattern is
        a regex pattern, the list of strings will be filtered using re.match.

        Args:
            strings (list[str]): List of strings to filter

        Returns:
            list[str]: List of strings that match the pattern
        """
        if self.pattern is None or len(strings) == 0:
            return strings

        # Ensure that patterns is a list
        patterns = [self.pattern] if isinstance(self.pattern, str) else self.pattern
        ignore_patterns = (
            [self.ignore_pattern]
            if isinstance(self.ignore_pattern, str)
            else self.ignore_pattern
        )

        # Filter the list
        logger.debug("List to filter: %s", strings)
        logger.debug("Filtering list with patterns: %s", patterns)
        logger.debug("Removing items from list with patterns: %s", ignore_patterns)

        postive_filter_results = self._filter_list_with_patterns(strings, patterns)
        logger.debug("Results after positive filter: %s", postive_filter_results)

        negative_filter_results = self._filter_list_with_patterns(
            postive_filter_results, ignore_patterns, invert=True
        )
        logger.debug("Results after negavite filter: %s", negative_filter_results)

        return negative_filter_results
