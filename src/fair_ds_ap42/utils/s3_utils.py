"""
Helper functions related to communicating with S3 storage.
"""

import datetime
from io import StringIO, BytesIO
import functools
import json
from pathlib import Path
import logging

import boto3
from botocore.exceptions import ClientError, NoCredentialsError
import pandas as pd
import urllib3

from .string_filter import StringFilter
from ..settings import settings


# We know the requests are insecure, this is to our own S3 bucket
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger(__name__)


def validate_credentials() -> bool:
    """
    Check that the credentials we have on hand will actually let us access
    an S3 bucket.
    """

    try:
        create_s3_client().list_buckets()
    except ClientError as err:
        logger.warning(
            (
                "Unable to connect to the S3 bucket with the provided "
                "credentials. Please ensure that the Credentials are present "
                "in the environment variables and are correct."
            )
        )
        raise err

    return True


def get_full_path(filepath: Path | str) -> str:
    """
    Get the full s3 path for the given "filepath" (S3 Key)

    Args:
        filepath (str | Path):
            The filepath to convert

    Returns:
        str
    """
    if str(filepath).startswith(f"s3://{settings.storage.s3.bucket}/"):
        return filepath
    return f"s3://{settings.storage.s3.bucket}/{Path(filepath).as_posix()}"


@functools.cache
def file_exists_s3(file_path: Path | str) -> bool:
    """
    Check if the indicated file exists in the S3 Bucket.
    """
    file_path = Path(file_path).as_posix()

    logger.info("checking if %s exists in the S3 bucket", file_path)

    try:
        create_s3_client().head_object(Bucket=settings.storage.s3.bucket, Key=file_path)
        return True
    except ClientError as err:
        # There are multiple ClientErrors that can appear. We only want to
        # ignore the error in the exact instance that the file we called the
        # head_object function on is missing.
        if str(err) == (
            "An error occurred (404) when calling the "
            "HeadObject operation: Not Found"
        ):
            return False
        raise err


def directory_exists_s3(path: Path | str) -> bool:
    """
    Check if the indicated directory exists in the S3 bucket.

    Note: The directory must have at least one file present to exist.

    Args:
        path (Path | str):
            The directory path to search for

    Returns:
        bool
    """
    # Taken from https://stackoverflow.com/a/68910145

    path = Path(path).as_posix()

    client = create_s3_client()
    if not path.endswith("/"):
        path = path + "/"

    response = client.list_objects(
        Bucket=settings.storage.s3.bucket, Prefix=path, Delimiter="/", MaxKeys=1
    )

    return "Contents" in response


def create_s3_client() -> boto3.client:
    """
    Using the credentials stored in settings, create a boto3 client for
    interacting with the S3 bucket. boto3.client provides low level access to
    S3 resources.

    Returns:
        boto3.client
    """
    if settings.storage.coscine.credentials_provided:
        raise NotImplementedError

    if settings.storage.s3.credentials_provided:
        return boto3.client("s3", endpoint_url=settings.storage.s3.endpoint_url)

    raise NoCredentialsError


def get_filelist_from_s3(base_folder="", patterns=None) -> list:
    """
    Get a list of all items in the S3 Bucket that appear in the data folder

    Returns:
        list[str]
    """
    logger.debug("searching for files in S3 bucket with base_folder %s", base_folder)
    logger.debug("filtering results with patterns %s", patterns or "None")

    client = create_s3_client()

    all_objects = client.list_objects(Bucket=settings.storage.s3.bucket)["Contents"]
    logger.debug("found %s objects in S3 bucket", len(all_objects))
    logger.debug("first 5 objects: %s", all_objects[:5])

    files = [
        obj["Key"]
        for obj in all_objects
        if (obj["Key"].startswith(str(base_folder)) and obj["Size"] > 0)
    ]

    if patterns is not None:
        if not isinstance(patterns, list):
            patterns = [patterns]

        my_filter = StringFilter(patterns)
        logger.debug("filtering results with %s", my_filter)
        files = my_filter.filter_list(files)

    logger.debug("located %s files in S3 bucket", len(files))
    return files


def get_line_at_nth_position(filepath: Path | str, line_num: int) -> str:
    """
    Return the Nth line in a given file. (0-based indexing)

    Args:
        filepath (Path | str):
            The filepath (Key) to retrieve from the bucket
        N (int):
            The index of the row to return

    returns:
        str
    """
    filepath = Path(filepath).as_posix()

    client = create_s3_client()
    body = client.get_object(Bucket=settings.storage.s3.bucket, Key=filepath)["Body"]

    for i, line in enumerate(body.iter_lines()):
        if i == line_num:
            return line.decode("utf-8")

    raise IndexError(f"Could not find line {line_num} in {filepath}")


def read_file_from_s3(filename: Path | str, **kwargs) -> pd.DataFrame:
    """
    Read a file out of the S3 bucket.

    Currently restricted to CSV and Parquet files. The file must have either a
    .csv, .csv.gz, .parquet, .parq extension.

    Args:
        filename (str):
            The name of the file to retrieve.
        **kwargs:
            Any valid arguments that could be passed to pd.read_csv or
            pd.read_parquet

    Returns:
        pandas.DataFrame
    """
    logger.error(settings.per_file)

    logger.info("reading %s from S3 bucket with kwargs %s", filename, kwargs)
    filename = Path(filename).as_posix()

    if not file_exists_s3(filename):
        raise FileNotFoundError((f"{filename} could not be found in the S3 bucket. "))

    client = create_s3_client()
    result = client.get_object(Bucket=settings.storage.s3.bucket, Key=filename)

    extension = "".join(Path(filename).suffixes)
    if extension in [".csv", ".csv.gz"]:
        csv_string = StringIO(result["Body"].read().decode("utf-8"))
        return pd.read_csv(csv_string, **kwargs)

    if extension in [".parquet", ".parq"]:
        raise NotImplementedError

    raise NotImplementedError(
        (
            f"files of type {extension} are not yet supported "
            "(valid extensions: .csv, .csv.gz, .parquet, .parq)"
        )
    )


def write_dataframe_to_s3(
    df: pd.DataFrame,
    filepath: Path | str,
    file_format: str,
    overwrite: bool = True,
    **kwargs,
) -> bool:
    """
    Write the given DataFrame to the S3 bucket.

    Args:
        df (pd.DataFrame):
            The DataFrame to write.
        filepath (str):
            The filepath to create in the Bucket. Presumes a root of
            "s3://{bucketname}/", so this does not need to be added.
        file_format (str):
            The format to write this file in. Can be either "csv" or "parquet"
        overwrite (bool, default=True):
            If False, will raise an exception if this file already exists in
            the bucket. Otherwise the file will be overwritten.
        **kwargs:
            Any additional arguments that could be passed to pd.to_csv or
            pd.to_parquet

    Returns:
        bool
            True if the file now exists in the bucket.
    """
    if not isinstance(df, pd.DataFrame):
        raise ValueError("This function only accets pandas dataframes")

    filepath = Path(filepath).as_posix()

    # If we were told not to overwrite the file and it exists on
    # the bucket, then raise an exception
    if not overwrite and file_exists_s3(filepath):
        raise FileExistsError(f"{filepath} alread exists in the S3 Bucket ({filepath})")

    client = create_s3_client()
    if file_format == "csv":
        csv_buffer = BytesIO()
        df.to_csv(csv_buffer, encoding="utf-8", mode="wb", **kwargs)
        client.put_object(
            Body=(csv_buffer.getvalue()),
            Bucket=settings.storage.s3.bucket,
            Key=filepath,
        )

    elif file_format == "parquet":
        raise NotImplementedError
    else:
        raise NotImplementedError(
            (
                f"files of type {file_format} are not yet supported "
                "(valid options: 'csv' or 'parquet')"
            )
        )

    return file_exists_s3(filepath)


def write_json_to_s3(data: dict, filepath: Path | str, overwrite: bool = True) -> bool:
    """
    Write the given dictionary to the s3 bucket as a JSON file.

    Args:
        data (dict):
            The data to write
        filepath (Path | str):
            The Key to use in the S3 bucket
        overwrite (bool, default=True):
            If a file exists with this key, should we overwrite it?

    Returns:
        bool
            True if the file now exists in the Bucket
    """
    client = create_s3_client()
    filepath = Path(filepath).as_posix()

    if not overwrite and file_exists_s3(filepath):
        raise FileExistsError(filepath)

    client.put_object(
        Body=json.dumps(data), Bucket=settings.storage.s3.bucket, Key=filepath
    )

    return file_exists_s3(filepath)


def move_file_to_s3(
    local_filepath: Path | str, target_filepath: Path | str, overwrite: bool = True
) -> bool:
    """
    Move a file from local disk to the S3 bucket.

    Args:
        local_filepath (str):
            The filepath of a local file
        target_filepath (str):
            The filepath to create in the Bucket. Presumes a root of
            "s3://{bucketname}/", so this does not need to be added.
        overwrite (bool, default=True):
            If False, will raise an exception if this file already exists in
            the bucket. Otherwise the file will be overwritten.
    Returns:
        bool
            True if the file now exists in the bucket.
    """
    local_filepath = Path(local_filepath).as_posix()
    target_filepath = Path(target_filepath).as_posix()

    client = create_s3_client()

    if not Path(local_filepath).exists():
        raise FileNotFoundError

    if not overwrite and file_exists_s3(target_filepath):
        raise FileExistsError(
            f"{target_filepath} already exists in the " f"S3 Bucket ({target_filepath})"
        )

    logger.info("moving local file %s to S3 path %s", local_filepath, target_filepath)
    client.upload_file(local_filepath, settings.storage.s3.bucket, target_filepath)

    return file_exists_s3(target_filepath)


def get_last_modified(filename: Path | str) -> datetime.datetime:
    """
    Get the last time an item in the bucket was updated

    Args:
        filename (str):
            The name of the file to query.

    Returns:
        datetime.datetime
    """
    if isinstance(filename, Path):
        filename = filename.as_posix()

    if not file_exists_s3(filename):
        raise FileNotFoundError(f"{filename} was not found in the S3 Bucket")

    response = create_s3_client().get_object(
        Bucket=settings.storage.s3.bucket, Key=filename
    )
    return response["LastModified"]


def get_size_in_bytes(filename: Path | str) -> int:
    """
    Get the last time an item in the bucket was updated

    Args:
        filename (str):
            The name of the file to query.

    Returns:
        datetime.datetime
    """
    if isinstance(filename, Path):
        filename = filename.as_posix()

    if not file_exists_s3(filename):
        raise FileNotFoundError(f"{filename} was not found in the S3 Bucket")

    response = create_s3_client().get_object(
        Bucket=settings.storage.s3.bucket, Key=filename
    )
    return response["ContentLength"]


def get_dataset_columns(filename: Path | str, delimiter: str = ","):
    """
    Get all column names from the indicated file.
    """
    logger.info("getting columns for %s", filename)
    if not file_exists_s3(filename):
        raise FileNotFoundError(filename)

    if ".csv" in Path(filename).suffixes:
        first_row = get_line_at_nth_position(filename, 0)
        logger.info("first row of data: %s", first_row)
        logger.info("splitting row with delimiter %s", delimiter)
        column_names = first_row.split(delimiter)
        return column_names

    if ".parquet" in Path(filename).suffixes:
        raise NotImplementedError

    raise NotImplementedError(Path(filename).suffixes)


def delete_file(filename: Path | str):
    """
    Delete the given file from the bucket.
    """
    filename = Path(filename).as_posix()
    client = create_s3_client()
    client.delete_object(settings.storage.s3.bucket, filename)
