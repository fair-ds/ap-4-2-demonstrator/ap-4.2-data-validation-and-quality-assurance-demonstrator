"""
A collection of odds and ends functions that may or not be applicable in
general cases.
"""


def is_int(my_str: str) -> bool:
    """
    Check if the provided string can be cast to an integer
    """
    try:
        return int(my_str) == float(my_str)
    except ValueError:
        return False


def is_float(my_str: str) -> bool:
    """
    Check if the provided string can be cast to a float
    """
    try:
        float(my_str)
        return not is_int(my_str)
    except ValueError:
        return False
