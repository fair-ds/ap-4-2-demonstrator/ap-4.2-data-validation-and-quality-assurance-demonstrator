"""
Various functions related to loading in data and returning it in various
formats.
"""

import logging

import frictionless
import pandas as pd
import geopandas as gpd

from ..objects.data_column.geodata_column import parse_value

from ..settings import settings

logger = logging.getLogger(__name__)


def get_pandas_dtypes_from_schema(my_schema: frictionless.Schema) -> dict:
    """
    Iterate through a frictionless Schema object and get the corresponding
    pandas data type for each field type
    """
    schema_to_pandas_dtypes = {
        "string": "object",
        "any": "object",
        "number": "float64",
        "integer": "Int64",
        "date": "datetime64",
        "datetime": "datetime64",
        "boolean": "bool",
        "geopoint": "object",
    }
    fields = {}
    for field in my_schema.fields:
        try:
            fields[field.name] = schema_to_pandas_dtypes[field.type]
        except KeyError as exc:
            raise NotImplementedError(
                (
                    f'schema type "{field.type}" is not implemented '
                    f'(field "{field.name}")'
                )
            ) from exc

    return fields


def load_csv_with_schema(
    filename: str, schema_data=None, repair_schema=True
) -> pd.DataFrame:
    """Load in a csv using the schema stored in SCHEMA_FOLDER.

    Args:
        filename (str):
            The filepath to the CSV file to load.

    Returns:
        pd.DataFrame:
            A dataframe created from the csv file.
    """
    if not schema_data:
        # Load the schema
        raise NotImplementedError

    if repair_schema:
        schema_data = fix_schema_types(filename, schema_data)

    # Read in the csv using pandas
    logger.info("loading %s...", filename)

    dtypes = get_pandas_dtypes_from_schema(schema_data)
    dtype_dict = {k: v for k, v in dtypes.items() if v != "datetime64"}
    dates_list = [k for k, v in dtypes.items() if v == "datetime64"]

    df = pd.read_csv(
        filename,
        sep=settings.project.csv_field_delimiter,
        na_values=schema_data.missing_values,
        dtype=dtype_dict,
        parse_dates=dates_list,
        infer_datetime_format=True,
    )

    if len(schema_data.primary_key) > 0:
        df.set_index(schema_data.primary_key, inplace=True)

    # Check if the user indicated that this file has geodata that we need to
    # create a new column out of
    files_with_geo_info = [
        settings.per_file.get_file_settings(filepath)
        for filepath in settings.per_file.filenames_with_geodata_settings
    ]

    def get_geo_info(row, latitude_col, longitude_col):
        return f"{row[latitude_col]}, {row[longitude_col]}"

    for file_geo_info in files_with_geo_info:
        if "geo" in df.columns:
            raise ValueError(
                "data already contains a column named 'geo'. "
                "Please rename this column before loading the data."
            )

        df["geo"] = df.apply(
            get_geo_info, args=(file_geo_info.latitude, file_geo_info.longitude), axis=1
        )

    return df


def parse_geodata(
    col: pd.Series, fmt: str = "default", errors="coerce"
) -> pd.DataFrame:
    """Given a Column of geopoint data, parse it according to the format

    Args:
        col (pd.Series):
            A column of geopoint data

    """
    logger.debug("parsing geodata column %s with format %s", col.name, fmt)
    assert errors in ["raise", "coerce"]

    col = col.apply(lambda x: parse_value(x, fmt, errors))
    ret_val = pd.DataFrame(col.to_list(), columns=["lon", "lat"])
    logger.debug("sucessfully parsed %d records", len(ret_val))
    return gpd.points_from_xy(ret_val.lon, ret_val.lat)


def get_geopandas_df(filename, col="geo", **kwargs) -> gpd.GeoDataFrame:
    """
    Load a file as a GeoDataFrame, with the indicated column as the geo data.
    """
    df = load_csv_with_schema(filename, **kwargs)
    gdf = gpd.GeoDataFrame(
        df, geometry=parse_geodata(df[col], errors="coerce"), crs=settings.geodata.crs
    )
    return gdf


def fix_schema_types(filename, my_schema) -> frictionless.Schema:
    """
    Try to correct a schema manually if frictionless isn't able to infer the
    schema type correctly.
    """
    try:
        load_csv_with_schema(filename, my_schema, repair_schema=False)
    except ValueError:
        logger.debug(
            (
                "Frictionless let us down. We're going to try and "
                "figure out which column is the problem"
            )
        )
        # frictionless isn#t great at inferring schemas sometimes.
        # Lets try to figure out which of these columns isn't actually
        # what it thinks - read them in one at a time as objects and try
        # to convert them to the appropriate data type. If it fails, we
        # know that one should be a string
        cols_to_check = {
            col: value_type
            for col, value_type in get_pandas_dtypes_from_schema(my_schema).items()
            if value_type != "object"
        }
        for col, value_type in cols_to_check.items():
            logger.debug("testing column %s - was a %s", col, value_type)
            try:
                pd.read_csv(filename, usecols=[col], dtype={col: value_type})
            except ValueError:
                logger.debug("col %s was a problem. Changing field type to string", col)
                for i, field in enumerate(my_schema.fields):
                    if field.name == col:
                        my_schema.fields[i].type = "string"
                        break

            except TypeError:
                logger.debug("skipping %s: is a datetime", col)
                # We know this pattern fails on datetime data

        load_csv_with_schema(filename, my_schema, repair_schema=False)

    return my_schema
