"""
Generic logging setup. Creates a file logger if requested.
"""

import logging
from pathlib import Path

from ..settings import settings

LOG_LEVEL = logging.DEBUG
LOG_FORMAT = logging.Formatter(
    "%(asctime)s - %(funcName)s - %(levelname)s - %(message)s"
)


def setup_logging(my_logger=None, file_logger=None):
    """
    Create a generic logging setup that is shared among all scripts.
    """
    if not my_logger:
        my_logger = logging.getLogger("fair_ds_ap42")

    my_logger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(LOG_LEVEL)
    stream_handler.setFormatter(LOG_FORMAT)
    my_logger.addHandler(stream_handler)

    if file_logger:
        # if the logging folder doesn't exist yet, make it here
        Path(settings.directories.log_directory).mkdir(parents=True, exist_ok=True)

        file_handler = logging.FileHandler(
            settings.directories.log_directory / Path(file_logger)
        )
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(LOG_FORMAT)
        my_logger.addHandler(file_handler)

    return my_logger
