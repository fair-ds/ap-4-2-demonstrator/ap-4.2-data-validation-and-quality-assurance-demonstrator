"""
Functions related to searching, reading and writing files.
"""

import logging
import os
from pathlib import Path
import pickle
import json

logger = logging.getLogger(__name__)


def gather_files(root_dir: str) -> list:
    """Search the given directory for all csv files"""
    if not Path(root_dir).is_dir():
        raise FileNotFoundError("Directory is not valid!")
    return list(Path(root_dir).glob("**/*.csv"))


def get_output_filename(
    root_folder: str, data_file: str, filename: str, create_folders=True
) -> Path:
    """Creates a filepath given a folder and a pair of filenames.

    Used to create a standard way of outputting files from these scripts. We
    could just do this in the scripts, but this way if we decide to change the
    structure of the "out" folder we only have to change it here.

    Typically, "root_folder" is "out", and "data_file" is the relative path of
    the csv data file.

    Args:
        root_folder (str):
            The root folder to use. Typically "out"
        data_file (str):
            At the moment, we use the same structure as the files we find in
            the DATA_FOLDER. This is usually a relative path to a data file,
            from which we pull the parent path.
        filename (str):
            The filename to give this item.
        create_folders (bool, default=True):
            If True, will create all folders which do not currently exist in
            the filepath.
    """
    out = Path(root_folder) / Path(data_file) / Path(filename)

    if create_folders:
        # Make these folders in case they don't exist
        os.makedirs(os.path.dirname(out), exist_ok=True)

    return out


def find_non_serializable_values(obj):
    """
    Given a dictionary or list, find all values which are not serializable.

    Args:
        obj (dict or list): The object to search

    Returns:
        list: A list of tuples, where the first item is the key of the
                non-serializable value, and the second item is the value itself.
    """

    non_serializable_values = []

    def check_value(value, key, is_json):
        if isinstance(value, dict):
            check_dict(value, key, is_json)
        elif isinstance(value, list):
            check_list(value, key, is_json)
        elif not is_serializable(value, is_json):
            non_serializable_values.append((key, value))

    def check_dict(dictionary, parent_key, is_json):
        for key, value in dictionary.items():
            current_key = f"{parent_key}.{key}" if parent_key else key
            check_value(value, current_key, is_json)

    def check_list(lst, parent_key, is_json):
        for index, value in enumerate(lst):
            current_key = f"{parent_key}[{index}]" if parent_key else f"[{index}]"
            check_value(value, current_key, is_json)

    def is_serializable(value, is_json):
        try:
            if is_json:
                json.dumps(value)
            else:
                pickle.dumps(value)
            return True
        except Exception:  # pylint: disable=broad-except
            return False

    check_value(obj, "", is_json=True)
    check_value(obj, "", is_json=False)

    return non_serializable_values
