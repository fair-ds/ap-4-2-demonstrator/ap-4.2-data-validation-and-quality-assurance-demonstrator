"""
this file contains common CLI elements to avoid repeating code
"""


def add_log_argument(parser):
    """
    Add the log argument to the parser
    """
    parser.add_argument(
        "--log",
        "-l",
        choices=["DEBUG", "INFO", "WARNING"],
        default="WARNING",
        help="Set the logging level",
    )
    return parser
