"""
validate.py

Variable names in all caps refer to variables set by either the user in a
settings.yml file or set to default values in settings.py.

This script will validate files indicated in SCHEMAS_FILE using the
frictionless standards and provide feedback in the form of an XML report.

Leverages the tableschema (https://pypi.org/project/tableschema/) library to
check that the format of a provided csv data file is in the correct format.
Requires the user to provide a schema file as "schemas/input-table.json" in
the format described in the link above.

Command Line Options:
    --n_processes (int, default=8):
        The maximum number of threads to create. With many files can increase
        the speed of this script.
    --debug: Sets logging to DEBUG and disables multithreading
"""

import argparse
import logging
from pathlib import Path
import time

from .junit_xml_gen import JUnitTestSuites, JUnitTestSuite, JUnitTestCase

from .file_auditor import FileAuditor
from .register_sources import register_all_sources, get_all_sources
from .objects import Schema
from .objects.datafile.datafile_factory import DataFileFactory
from .settings import settings
from .utils.logging import setup_logging
from .utils.schema import get_schema_report
from .state.current_state import CurrentState

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(description="Validate file using TableSchema")

    parser.add_argument("--n_processes", type=int, default=8)
    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def get_testsuite_xml(filename=settings.directories.xml_report) -> JUnitTestSuites:
    """
    Load the testsuite xml
    """
    return JUnitTestSuites(Path(__file__).name, filename)


def validate_file(filename, schema, process_id=None):
    """
    Run validation on the given file.
    """
    # We can't write to a file, but we can at least dump some info to the
    # console about what's happening in a thread
    proc_logger = logging.getLogger(f"PID#{process_id} {__name__}")
    log_file = f"{Path(filename).stem}_validation_{int(time.time())}.log"
    setup_logging(proc_logger, log_file)

    proc_logger.info("validating %s", filename)

    xml = get_testsuite_xml()
    my_testsuite = JUnitTestSuite(
        f"validate_file|{filename}", f"CSV Schema Validation|{filename}"
    )

    # Load the file in with the given schema
    proc_logger.debug("running validation code")

    my_report = get_schema_report(filename, schema)

    # We create a new testcase for each difference code type
    for code_type in {code for code, _ in my_report}:
        proc_logger.debug("adding testcase for code type %s", code_type)
        messages = [msg for code, msg in my_report if code == code_type]
        to_display = messages[:10]
        if len(messages) > 10:
            proc_logger.debug("got %d messages, trimming to first 10", len(messages))
            to_display.append(f"({len(messages)-10} Additional Messages)")

        case_id = f"{filename}|schema violation:{code_type}"
        case_name = f'{filename}: Schema Violation "{code_type}"'

        my_testcase = JUnitTestCase(
            obj_id=case_id,
            name=case_name,
            fail_type="ERROR",
            fail_msg="\n".join(to_display),
            fail_text="",
        )

        my_testsuite.add_testcase(my_testcase)

    # In the event we don't have any schema errors (num_test == 0), create
    # a blank testcase called SUCCESS.
    case_id = f"{filename}|schema_validated"

    if my_testsuite.num_tests == 0:
        proc_logger.info("schema validated!")

        my_testcase = JUnitTestCase(
            obj_id=case_id, name=f"{filename}: File Passed Validation"
        )

        my_testsuite.add_testcase(my_testcase)

    xml.add_testsuite(my_testsuite)

    xml.to_file(settings.directories.xml_report)


def validate_data_with_schema():
    """
    Run validation on the given file with it's associated schema.
    """
    state = CurrentState.load()

    # Audit all of the files we can see in our sources
    my_file_auditor = FileAuditor(
        file_patterns=settings.file_patterns.all_file_patterns
    )
    my_file_auditor.search_all(get_all_sources())

    data_files_to_process = my_file_auditor.get_files_to_process_with_pattern(
        pattern=settings.file_patterns.data_file_patterns
    )

    for data_file in data_files_to_process:
        data_file = DataFileFactory.from_resource(data_file)

        assigned_schema = state.schema_assignment.get_schema_by_datafile(data_file)
        data_file.assign_schema(assigned_schema)

        data_file.validator.validate_self()
        if len(data_file.test_cases) == 0:
            logger.info("No validation issues noted with file %s", data_file.filepath)

        for test_case in data_file.test_cases:
            state.add_testcase(test_case)

    schema_files_to_process = my_file_auditor.get_files_to_process_with_pattern(
        pattern=settings.file_patterns.schema_file_patterns,
    )

    for schema_file in schema_files_to_process:
        schema_file = Schema.from_resource(schema_file)

        for test_case in schema_file.test_cases:
            state.add_testcase(test_case)

    testsuites = state.xml.filter("data_validation")
    testsuites.to_file(settings.directories.xml_report)

    CurrentState().save()


if __name__ == "__main__":
    # Parse any command line arguments
    args = _parse_cli()

    if args.debug:
        args.n_processes = 1

    # set up logging
    setup_logging()

    register_all_sources()

    validate_data_with_schema()
