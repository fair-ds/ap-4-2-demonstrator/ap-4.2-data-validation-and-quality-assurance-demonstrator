# set PYTHONPATH=../../src && python -m fair_ds_ap42.build_report --log INFO

"""
This script is intended to be run only after the data has been processed and saved in a json file.

It will parse JSON as output by `SchemaAssignment.get_rich_file_contents` and genertate a static
collection of html files that can be used to browse the data and the analysis.

This output is designed to be hosted on a static web server, such as GitHub Pages.
"""

import argparse
from importlib import resources
import logging
from pathlib import Path
import shutil

from .cli import add_log_argument
from .state.result_parser import ResultParser
from .output import Index, DatafilePage

logger = logging.getLogger(__name__)


def _parse_cli() -> argparse.Namespace:
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser("Run FAIR-DS-AP42 Report Generation")
    parser.add_argument(
        "--input_file",
        "-i",
        default="results.json",
        help="The file to load the results from (in JSON format)",
    )
    parser.add_argument(
        "--cleanDestinationDir",
        action="store_true",
        help="Delete the output directory if it exists",
    )

    parser = add_log_argument(parser)
    # parser.add_argument(
    #     "--source_prefix",
    #     default="../../",
    #     help="The prefix to add to the source path when loading the static files",
    # )
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_cli()

    logging.basicConfig(level=args.log)

    # Delete the output directory if it exists
    if Path("output").exists() and args.cleanDestinationDir:
        logger.warning("Deleting existing output directory")
        shutil.rmtree("output", ignore_errors=True)
        Path("output").mkdir()

    # Load the results
    results = ResultParser.load(args.input_file)

    page = Index(results)
    page.generate()

    # Iterate over the schema groups and generate pages for all of the datafiles
    for datafile in results.data_files:
        page = DatafilePage(datafile, results.schema_assignment)
        page.generate()

    # Move the static directory to the output directory
    source_dir = resources.files("fair_ds_ap42") / "output/static"
    try:
        shutil.copytree(source_dir, "output/static")
    except FileExistsError:
        logger.warning("output/static directory already exists")
