"""
Geodata file settings

Structure for ensuring correct settings for geodata files
"""


from dataclasses import dataclass
import logging


@dataclass
class FileSettings:
    """
    Object for storing settings specific to a geodata file
    """

    filename: str
    latitude: str = None
    longitude: str = None
    delimiter: str = ","
    encoding: str = "utf-8"

    def __post_init__(self):
        logging.debug("Created settings for %s: %s", self.filename, self)
