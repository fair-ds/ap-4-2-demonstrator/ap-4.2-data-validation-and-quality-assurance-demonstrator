"""
get_env_or_default.py

We have some settings that we want to be able to set in multiple ways. We want to be able to set
them in a settings file, as an environment variable, or use a default value. This function allows
us to do that. It takes a provided value, an environment variable name, and a default value. If
the provided value is not None, it returns that value. If the environment variable is set, it
returns that value. Otherwise, it returns the default value.

We additionally check the type of the default value and parse the environment variable accordingly.
This allows us to set lists and dictionaries as environment variables and have them parsed
correctly.

Usage:
```python
get_env_or_default(provided_value, "ENV_VAR_NAME", default_value)
```

"""

import json
import os
import re
import typing

T = typing.TypeVar("T")


def get_env_or_default(provided: T, env_var_name: str, default: T) -> T:
    """
    Get the value from the provided value, environment variable, or default.

    We prioritize the provided value, then the environment variable, and finally the default.

    Args:
        provided (T): The provided value
        env_var_name (str): The name of the environment variable
        default (T): The default value

    Returns:
        T: The value to use
    """
    if provided is not None:
        return provided

    if env_var_name not in os.environ:
        return default

    returned_value = os.getenv(env_var_name)

    # If the default is a list, check to see if the environment var looks like a literal list
    # or a comma-separated list and parse it accordingly
    if isinstance(default, list):

        if re.match(r"^\[.*\]$", returned_value):
            # If the value has leading and trailing brackets, remove them
            returned_value = returned_value[1:-1]

        return [value.strip() for value in returned_value.split(",")]

    # If the default is a dictionary, parse the environment variable as JSON
    if isinstance(default, dict):
        return json.loads(returned_value)

    # Otherwise, return the value as is
    return returned_value
