# Ignore too-many-positional-arguments from pylint. We use a number of positional arguments in this
# object to make the settings manager more
# flexible with regard to testing
# pylint: disable=too-many-arguments,too-many-positional-arguments


"""
The Overall Storage Settings Manager

This is the main settings manager that is used to load in all of the settings from the user
provided settings file. It also provides a consistant way to interact with the settings
for the rest of the application.
"""

import logging
from pathlib import Path
import toml


from .project_settings import ProjectSettings
from .directory_settings import DirectorySettings
from .file_pattern_settings import FilePatternSettings
from .geodata_settings import GeodataSettings
from .per_file_settings import PerFileSettings
from .file_settings import FileSettings

from .storage.storage_settings import StorageSettings

logger = logging.getLogger(__name__)


SETTINGS_FILE = Path("settings.toml")


class SettingsManager:
    """
    Provides a consistant way to interact with settings files and provide
    default values
    """

    def __init__(
        self,
        config: dict = None,
        project: ProjectSettings = None,
        directories: DirectorySettings = None,
        file_patterns: FilePatternSettings = None,
        storage: StorageSettings = None,
        geodata: GeodataSettings = None,
        per_file: PerFileSettings = None,
    ):
        if config is None:
            self.project = project if project else ProjectSettings()
            self.directories = directories if directories else DirectorySettings()
            self.file_patterns = (
                file_patterns if file_patterns else FilePatternSettings()
            )
            self.storage = storage if storage else StorageSettings()
            self.geodata = geodata if geodata else GeodataSettings()
            self.per_file = per_file if per_file else PerFileSettings()
        else:
            self.parse_config(config)

    @classmethod
    def load(cls, filepath: Path = SETTINGS_FILE):
        """
        Load a .toml settings file.
        """
        logger.info("Loading settings from %s", filepath)
        if not filepath.exists():
            logging.debug("No settings file found - using defaults")
            return cls({})

        with open(filepath, mode="r", encoding="utf-8") as file_obj:
            config = toml.load(file_obj)

        return cls(config)

    def parse_config(self, config: dict):
        """
        Parse config dictionary into settings objects.
        """
        self.project = ProjectSettings(**config.get("PROJECT", {}))
        self.directories = DirectorySettings(**config.get("DIRECTORIES", {}))
        self.file_patterns = FilePatternSettings(**config.get("FILE_PATTERNS", {}))
        self.storage = StorageSettings(config=config.get("STORAGE", {}))
        self.geodata = GeodataSettings(**config.get("GEODATA", {}))

        per_file_config = config.pop("FILE_DETAILS", {})
        logger.info("Parsing per file settings: %s", per_file_config)
        file_configs = [
            FileSettings(**file) for file in per_file_config.pop("FILE", [])
        ]
        self.per_file = PerFileSettings(files=file_configs)
