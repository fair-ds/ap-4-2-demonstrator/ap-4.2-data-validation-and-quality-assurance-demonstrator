"""
Geodata settings

Provides default values for settings that are not provided in the settings file
"""

from dataclasses import dataclass


@dataclass
class GeodataSettings:
    """Object for storing geodata settings"""

    crs: str = "EPSG:4326"
    shape_file: str = "naturalearth_lowres"
    shape_file_column: str = "name"
