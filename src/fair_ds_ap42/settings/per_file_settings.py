"""
File settings

Provides per file settings for all files
"""

from dataclasses import dataclass, field
import logging
from pathlib import Path

from .file_settings import FileSettings

logger = logging.getLogger(__name__)


@dataclass
class PerFileSettings:
    """Object for storing geodata settings"""

    files: list[FileSettings] = field(default_factory=list)

    @property
    def filenames_with_geodata_settings(self) -> list[str]:
        """List of filenames that have settings"""
        return [
            file.filename
            for file in self.files
            if file.longitude is not None and file.latitude is not None
        ]

    def get_file_settings(self, filename: str) -> FileSettings:
        """
        Get the settings for a specific file

        Args:
            filename (str): The name of the file to get settings for

        Returns:
            GeodataFileSettings: The settings for the file
        """
        if len(self.files) != 0:
            logger.debug("Searching %d files for %s", len(self.files), filename)
            for file in self.files:
                logger.info('Checking "%s" against "%s"', file.filename, filename)
                if Path(file.filename).as_posix() == Path(filename).as_posix():
                    return file

        logger.info("No settings found for %s - using defaults", filename)
        return FileSettings(filename=filename)
