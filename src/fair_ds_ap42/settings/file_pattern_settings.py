"""
File pattern settings

Provides default values for settings that are not provided in the settings file
"""

from dataclasses import dataclass

from .get_env_or_default import get_env_or_default


@dataclass
class FilePatternSettings:
    """Object for storing file pattern settings"""

    data_file_patterns: list[str] = None
    schema_file_patterns: list[str] = None
    ignore_patterns: list[str] = None

    def __post_init__(self):
        self.data_file_patterns = get_env_or_default(
            self.data_file_patterns, "DATA_FILE_PATTERNS", ["*.csv", "*.parquet"]
        )
        self.schema_file_patterns = get_env_or_default(
            self.schema_file_patterns, "SCHEMA_FILE_PATTERNS", ["*.json"]
        )
        self.ignore_patterns = get_env_or_default(
            self.ignore_patterns, "FILE_IGNORE_PATTERNS", ["*schemas.json", "*results.json"]
        )

    @property
    def all_file_patterns(self) -> list[str]:
        """A list of all file patterns"""
        return self.data_file_patterns + self.schema_file_patterns
