"""
Object for storing settings specific to the S3 storage provider

Provides default values for settings that are not provided in the settings file
"""

from dataclasses import dataclass
import os


@dataclass
class S3Settings:
    """Settings specific to the S3 storage provider"""

    bucket: str = None
    endpoint_url: str = None

    @property
    def credentials_provided(self):
        """Whether the credentials are provided"""
        credentials_provided = (
            os.getenv("AWS_ACCESS_KEY_ID") is not None
            and os.getenv("AWS_SECRET_ACCESS_KEY") is not None
        )

        if credentials_provided and self.bucket is None:
            raise KeyError(f"S3 Credentials are provided, but no bucket name ({self})")

        return credentials_provided

    @property
    def create_cache_files(self):
        """Should we create parquet cache files on the S3 bucket"""
        return False
