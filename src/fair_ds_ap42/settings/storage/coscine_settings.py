"""
Object for storing settings specific to the Coscine storage provider

Provides default values for settings that are not provided in the settings file
"""

from dataclasses import dataclass


@dataclass
class CoscineSettings:
    """
    Settings specific to the Coscine storage provider
    """

    key: str = ""

    @property
    def credentials_provided(self):
        """Whether the credentials are provided"""
        return False
