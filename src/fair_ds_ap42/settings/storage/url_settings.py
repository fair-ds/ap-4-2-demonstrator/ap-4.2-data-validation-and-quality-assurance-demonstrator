"""
Object for storing settings related to a data file that exists at a URL
"""

from dataclasses import dataclass


@dataclass
class URLSettings:
    """
    Settings specific to the Coscine storage provider
    """

    url: str = ""
