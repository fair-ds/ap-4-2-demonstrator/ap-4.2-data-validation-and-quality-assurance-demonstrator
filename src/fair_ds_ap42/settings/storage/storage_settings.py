"""
Submodule for storing settings specific to storage providers
"""

from dataclasses import dataclass

from .s3_settings import S3Settings
from .coscine_settings import CoscineSettings
from .url_settings import URLSettings


@dataclass
class StorageSettings:
    """
    Intermediate object for storing settings specific to storage providers
    """

    s3: S3Settings = None  # pylint: disable=invalid-name
    coscine: CoscineSettings = None
    url: URLSettings = None

    def __init__(self, config: dict = None):
        s3_config = config.get("S3") if config else {}
        self.s3 = S3Settings(**s3_config) if s3_config else S3Settings()

        coscine_config = config.get("COSCINE") if config else {}
        self.coscine = (
            CoscineSettings(**coscine_config) if coscine_config else CoscineSettings()
        )

        url_config = config.get("URL") if config else {}
        self.url = URLSettings(**url_config) if url_config else URLSettings()
