"""
Project directory settings

Provides default values for settings that are not provided in the settings file
"""

from dataclasses import dataclass
from pathlib import Path

from .get_env_or_default import get_env_or_default


@dataclass
class DirectorySettings:
    """Object for storing directory settings"""

    root: Path = Path("")
    data: Path = None
    schemas: Path = None

    def __post_init__(self):
        self.root = Path(self.root)
        self.data = Path(get_env_or_default(self.data, "DATA_DIRECTORY", "data"))
        self.schemas = Path(get_env_or_default(self.schemas, "SCHEMAS_DIRECTORY", "schemas"))

    @property
    def output_directory(self) -> str:
        """The directory output files are placed in"""
        return (self.root / Path("out")).as_posix()

    @property
    def log_directory(self) -> str:
        """The directory log files are placed in"""
        return (self.root / Path("log")).as_posix()

    @property
    def data_directory(self) -> str:
        """The project directory that contains data files"""
        return (self.root / Path(self.data)).as_posix()

    @property
    def schema_directory(self) -> str:
        """The project directory that contains schema files"""
        return (self.root / Path(self.schemas)).as_posix()

    @property
    def xml_report(self) -> str:
        """The project directory that contains schema files"""
        return (self.output_directory / Path("xml_report.xml")).as_posix()

    @property
    def schemas_file(self) -> str:
        """Path of the schemas assignment file"""
        return (self.schema_directory / Path("schemas.json")).as_posix()
