"""
Check the settings file that the user has provided to the demonstrator.

We will always add a source for the current working directory, but we will also add any sources
that the user has provided in the settings file.

These will be accessible to the demonstrator via the get_registered_sources() method of
source.available_sources.
"""

import logging

from .settings import settings

from .source import LocalDirectory, S3Bucket, URLSource
from .source.available_sources import register_source

logger = logging.getLogger(__name__)


def get_local_directory_sources() -> list[LocalDirectory]:
    """
    Using the SettingsManager stored in "settings", create a LocalDirectory source for each
    local directory that the user has provided.

    Returns:
        list[LocalDirectory]
    """
    sources = []

    # Add any other local directories that the user has provided in the settings
    sources.append(LocalDirectory(location=settings.directories.data_directory))
    sources.append(LocalDirectory(location=settings.directories.schema_directory))

    return sources


def get_s3_sources() -> list[S3Bucket]:
    """
    Using the SettingsManager stored in "settings", create an S3Source for each
    S3 bucket that the user has provided.

    NOTE: We currently only support a single S3 bucket. This is because we are using environment
    variables to store the AWS credentials, and we can only store a single set of credentials.

    Returns:
        list[S3Bucket]
    """

    sources = []

    # Add any S3 buckets that the user has provided in the settings
    if settings.storage.s3.bucket is not None:
        data_bucket = S3Bucket(
            bucket_name=settings.storage.s3.bucket,
            endpoint_url=settings.storage.s3.endpoint_url,
            prefix=settings.directories.data_directory,
        )
        schema_bucket = S3Bucket(
            bucket_name=settings.storage.s3.bucket,
            endpoint_url=settings.storage.s3.endpoint_url,
            prefix=settings.directories.schema_directory,
        )

        # Check that we have access to the bucket
        if data_bucket.accessible:
            sources.append(data_bucket)

        if schema_bucket.accessible:
            sources.append(schema_bucket)

    return sources


def get_url_sources() -> list[URLSource]:
    """
    Using the SettingsManager stored in "settings", create an URLSource for each
    URL that the user has provided.

    TODO: Unlike the S3 bucket, we can support multiple URLs. We should add this functionality.

    Returns:
        list[URLSource]
    """

    sources = []

    # Add any URLs that the user has provided in the settings
    if settings.storage.url.url != "":
        sources.append(URLSource(url=settings.storage.url.url, location="."))

    return sources


def get_all_sources() -> list[LocalDirectory, S3Bucket, URLSource]:
    """
    Using the SettingsManager stored in "settings", create a source for each
    directory, S3 bucket, and URL that the user has provided.

    Returns:
        list[LocalDirectory, S3Bucket, URLSource]
    """
    sources = []

    sources.extend(get_local_directory_sources())
    sources.extend(get_s3_sources())
    sources.extend(get_url_sources())

    # Print the sources we have to the logger
    logger.info("Available Sources:")
    for source in sources:
        logger.info(" - {%s}", source)

    return sources


def register_all_sources():
    """
    Register all sources that we can locate with the settings provided by the user.
    """
    for source in get_all_sources():
        register_source(source)


if __name__ == "__main__":
    # If invoking this as a script, we should register all of the sources
    register_all_sources()
