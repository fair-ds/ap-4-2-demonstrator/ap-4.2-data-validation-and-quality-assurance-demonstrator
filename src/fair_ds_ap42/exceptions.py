"""
Exceptions Specific to the Operation of the demonstrator
"""


class InvalidSchemasFileException(Exception):
    """
    The Schemas file we have been provided is in an invalid format.
    """

    def __init__(self, message):
        super().__init__(message)
