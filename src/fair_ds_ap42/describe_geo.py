"""Collect descriptive statistics about geographic data in a given file

Checks all of the schemas listed in schemas.json for geospatial data fields.
For each file where such data is located, we load in that field and perform
several checks:
- (TODO) Are all of the data points valid?
- (TODO) Are any of the data points significantly far away from the remainder?
- (TODO) Do any of the points appear to be placeholders?
- (TODO) Is there any unindented pattern in the data (collected in order)
- (TODO) Given a shapefile, where do the points fall in relation to polygons?
  - e.g. In different countries? All on land/water?

Finally, (TODO) create a plot of all of the geodata overlayed on a shapefile.
"""


import argparse
import json
import logging
from pathlib import Path

import pandas as pd
import geopandas as gpd

from .settings import settings
from .utils.file_io import get_output_filename
from .utils.logging import setup_logging
from .utils.load_data import get_geopandas_df

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(
        description="Describe/Visualize geospatial data from a file"
    )

    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def get_files_with_geodata() -> list[tuple[Path, list[str]]]:
    """Search for all files assigned to a schema with geodata

    Scans through the schemas in the package file to find all of those files
    which are indicated as having geo data in them

    Args:
        package_file (Path):
            The path of the package file. Defaults to the settings file.

    Returns:
        list[tuple[Path, list[str]]]:
            A list of tuples, where the first value is the path of the data
            file with geo data, and the second is a list of the columns in
            that file that have geo data.
    """
    ret_value = []
    for file in settings.per_file.files:
        if file.longitude is not None and file.latitude is not None:
            ret_value.append((Path(file.filename), [file.latitude, file.longitude]))
    return ret_value


def validate_geodata(filename, col):
    """
    This function needs to be moved to the DataColumn object
    """
    gdf = get_geopandas_df(filename, col)
    return gdf


def describe_geodata(col: gpd.GeoSeries) -> dict:
    """
    This function needs to be moved to the DataColumn object
    """
    ret_val = {}

    coords = pd.concat([col.x, col.y], axis=1)
    ret_val["Top"] = list(coords.value_counts().idxmax())
    ret_val["Count"] = len(coords)

    ret_val["Missing Values"] = int(coords.isnull().all(axis=1).sum())
    ret_val["Percent Missing"] = ret_val["Missing Values"] / ret_val["Count"]

    ret_val["Unique"] = int(coords.nunique().min())
    ret_val["Percent Unique"] = ret_val["Unique"] / ret_val["Count"]

    sample = coords.sample(min(ret_val["Count"], 10), random_state=42).dropna()
    ret_val["Sample Values"] = sample.values.tolist()

    return ret_val


# def records_outside_shapes(gdf):
#     """
#     Find all of the records that don't intersect with a shape from the
#     shapefile
#     """

#     geo_right_merge = load_base_shapefile().sjoin(
#         gdf, how="right", predicate="intersects"
#     )
#     outside_records = geo_right_merge[
#         (pd.isnull(geo_right_merge.name) & geo_right_merge["geometry"].is_valid)
#     ]
#     return outside_records


def main():
    """
    The main function for this script.
    """
    for filename, columns in get_files_with_geodata():
        description = {"file": filename.as_posix(), "Geographic Variables": {}}
        for col in columns:
            logger.info("loading geography data in column %s from %s...", col, filename)
            logger.info("validating data...")
            gdf = validate_geodata(filename, col)
            logger.info("describing column %s in file %s", col, filename)
            col_desc = describe_geodata(gdf.geometry)
            logger.info("making plots...")
            # fig_files = make_plots(gdf, filename, col)
            # col_desc["figures"] = fig_files

            description["Geographic Variables"][col] = col_desc

        out_file = get_output_filename("out", filename, "desc_geo.json")
        logger.debug("saving descriptions to %s", out_file)

        with open(out_file, "w", encoding="utf-8") as file_obj:
            json.dump(description, file_obj)


if __name__ == "__main__":
    args = _parse_cli()

    # set up logging
    setup_logging(logger)

    main()
