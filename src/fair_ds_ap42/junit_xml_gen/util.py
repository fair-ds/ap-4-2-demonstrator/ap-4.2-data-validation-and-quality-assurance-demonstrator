"""
Utility objects shared by all of the JUnit XML generation scripts.
"""

import logging
import re

from .objects.junit_baseobject import JUnitBaseObject

logger = logging.getLogger(__name__)


def filter_objects(
    objs: JUnitBaseObject, pattern: str, match_id: bool = True, match_name: bool = True
):
    """
    Filter a list of objects to only those whcih match the provided regex
    pattern.

    Returns a list of those objects that match the given pattern. The pattern
    will be checked against either the id or name by default.

    Args:
        testsuites (TestSuites):
            The Testsuites to filter.
        pattern (str):
            The regex pattern to use as a filter.
        match_id (bool, default=True):
            If True, then check the regex pattern against the id of the
            testcases
        match_name (bool, default=True):
            If True, then check the regex pattern against the name of
            the testcases.

    Returns:
        list[JUnitBaseObject]
    """
    ret_val = []
    for obj in objs:
        id_match = match_id and re.search(pattern, obj.obj_id)
        name_match = match_name and re.search(pattern, obj.name)

        logger.debug("%s found in %s: %s", pattern, obj.obj_id, id_match)
        logger.debug("%s found in %s: %s", pattern, obj.name, name_match)

        if id_match or name_match:
            ret_val.append(obj)

    return ret_val


def find_exactly_one(
    objs: list[JUnitBaseObject],
    pattern: str,
    match_id: bool = True,
    match_name: bool = True,
) -> JUnitBaseObject:
    """
    Find exactly one object from the list.

    Similar to the function above, but will raise an exception if the pattern
    matches more than one item in the list.

    Args:
        objs (list[JUnitBaseObject]):
            The list of objects to filter
        pattern (str):
            The regex pattern to use as a filter.
        match_id (bool, default=True):
            If True, then check the regex pattern against the id of the
            testcases
        match_name (bool, default=True):
            If True, then check the regex pattern against the name of
            the testcases.

    Returns:
        list[JUnitBaseObject]
    """
    matching_objs = filter_objects(objs, pattern, match_id, match_name)
    if len(matching_objs) == 1:
        return matching_objs[0]

    if len(matching_objs) > 1:
        msg = f'Multiple objects found matching pattern "{pattern}".'
        logger.error(msg)
        raise ValueError(msg)

    msg = f'No objects found matching pattern "{pattern}".'
    logger.error(msg)
    raise ValueError(msg)


def _are_lists_equal(list1, list2):
    """Helper function for matching unhashable objects

    Can compare two lists and will return True if they contain exactly the
    same elements, even if they are in different orders.
    """
    # make a copy so we don't modify the original value by reference
    list_copy = list1.copy()
    if len(list_copy) != len(list2):
        return False  # If the lengths don't match, just bail now

    for i in list2:
        if i in list_copy:
            # Pop off each matching element we find
            list_copy.remove(i)
        else:
            # As soon as we come across one that's missing, we can bail
            return False

    # If there are any leftover items, they aren't equal
    if len(list_copy) != 0:
        return False

    # If we made it this far, the lists are equal
    return True
