"""
Module defining the JUnitBaseObject class.
"""

from dataclasses import dataclass, field
import time


@dataclass
class JUnitBaseObject:
    """
    Base class for creating JUnit objects.
    """

    name: str
    obj_id: str = field(default=None)

    def __post_init__(self):
        """
        Initialize the object with the current timestamp if id is not set.
        """
        if self.obj_id is None:
            self.obj_id = f"{time.time():0.0f}"
