"""
Module defining the JUnitTestCase class.
"""

from dataclasses import dataclass, field
import logging
from xml.etree.ElementTree import Element, SubElement

from .failure_types import FailureType
from .junit_baseobject import JUnitBaseObject

logger = logging.getLogger(__name__)


@dataclass
class JUnitTestCase(JUnitBaseObject):
    """
    Represents a JUnit test case.
    """

    fail_type: FailureType = field(default=FailureType.SUCCESS)
    fail_msg: str = field(default="")
    fail_text: str = field(default="")

    def __post_init__(self):
        try:
            if isinstance(self.fail_type, str):
                self.fail_type = FailureType[self.fail_type]
        except KeyError as err:
            msg = (
                f'"{self.fail_type}" is not a valid failure_type value. '
                f"Valid values: {', '.join([x.name for x in FailureType])}"
            )
            raise ValueError(msg) from err

        logger.debug(
            "initing testcase (obj_id: %s, name: %s type: %s)",
            self.obj_id,
            self.name,
            self.fail_type,
        )

    def to_xml(self) -> Element:
        """Convert this object to XML"""
        my_xml = Element("testcase", attrib={"id": self.obj_id, "name": self.name})
        if self.fail_type != FailureType.SUCCESS:
            failure = SubElement(
                my_xml,
                "failure",
                attrib={"type": str(self.fail_type), "message": str(self.fail_msg)},
            )
            failure.text = str(self.fail_text)
        return my_xml
