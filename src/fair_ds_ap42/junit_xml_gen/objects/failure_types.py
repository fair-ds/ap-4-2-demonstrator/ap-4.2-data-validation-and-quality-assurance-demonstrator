"""
This module defines an enumeration of FailureType, which contains the possible failure types.

Attributes:
SUCCESS (str): A success message.
WARNING (str): A warning message.
ERROR (str): An error message.

Methods:
str(self): Returns the string representation of the value associated with the instance.

Example:
FailureType.SUCCESS
>>> SUCCESS
FailureType.WARNING
>>> WARNING
FailureType.ERROR
>>> ERROR
"""

from enum import Enum


class FailureType(Enum):
    """
    This enum class defines the different types of test failures.

    The available types are:
    - SUCCESS: The test passed successfully.
    - WARNING: The test passed, but with a warning.
    - ERROR: The test failed.

    Each type is defined as an instance of this class, with a string value indicating the type.

    """

    SUCCESS = "SUCCESS"
    WARNING = "WARNING"
    ERROR = "ERROR"

    def __str__(self):
        return str(self.value)
