"""
This module contains a dataclass JUnitTestSuites that represents a collection of JUnit test
suites. It provides methods to load JUnit XML files, add test suites, and iterate over the test
suites in the object.

The load method takes a file path to an XML file in JUnit format and returns a JUnitTestSuites
object representing the contents of the file. The add_testsuite and add_testcase methods are used
to add test suites and test cases to the object, respectively.

The class also provides properties that return the total number of tests, errors, and warnings in
the test suites, as well as the worst message (either "ERROR", "WARNING", or "SUCCESS", depending
on the number of errors and warnings).
"""

from dataclasses import dataclass, field
import logging
import re
import xml
from xml.etree.ElementTree import Element, ElementTree

from .junit_baseobject import JUnitBaseObject
from .junit_testsuite import JUnitTestSuite
from .junit_testcase import JUnitTestCase
from ..util import find_exactly_one

logger = logging.getLogger(__name__)


@dataclass
class JUnitTestSuites(JUnitBaseObject):
    """
    Represents a collection of JUnit test suites.

    This class provides functionality to load and manipulate JUnit test suites, which are
    represented as a list of `JUnitTestSuite` objects. It also provides several properties to
    summarize the total number of tests, errors, and warnings across all test suites.

    Attributes:
        name (str):
            The name of the test suites.
        obj_id (str):
            The ID of the test suites.
        testsuites (List[JUnitTestSuite]):
            A list of `JUnitTestSuite` objects representing the test suites.
        timestamp (str):
            The timestamp of the test suites.
    """

    testsuites: list[JUnitTestSuite] = field(init=False, repr=False)
    timestamp: str = field(init=False, repr=False, default=None)

    @classmethod
    def load(cls, filename: str) -> "JUnitTestSuites":
        """
        Load a JUnitTestSuites object from an XML file.

        Args:
            filename (str):
                The filepath to an XML file in JUnit format.

        Returns:
            JUnitTestSuites
        """

        with open(filename, encoding="utf-8") as file_obj:
            tree = xml.etree.ElementTree.parse(file_obj)

        root = tree.getroot()
        my_ts = JUnitTestSuites(root.attrib["id"], root.attrib["name"])

        for testsuite in root:
            my_testsuite = JUnitTestSuite(
                testsuite.attrib["id"], testsuite.attrib["name"]
            )

            my_testsuite.max_testcases = int(testsuite.attrib["max_testcases"])

            for testcase in testsuite:
                if sum(1 for x in testcase) == 0:
                    my_testcase = JUnitTestCase(
                        obj_id=testcase.attrib["id"], name=testcase.attrib["name"]
                    )

                    my_testsuite.add_testcase(my_testcase)
                else:
                    for failure in testcase:
                        my_testcase = JUnitTestCase(
                            obj_id=testcase.attrib["id"],
                            name=testcase.attrib["name"],
                            fail_type=failure.attrib["type"],
                            fail_msg=failure.attrib["message"],
                            fail_text=failure.text,
                        )

                        my_testsuite.add_testcase(my_testcase)

            my_testsuite.additional_cases = {
                "SUCCESS": int(testsuite.attrib["additional_success"]),
                "WARNING": int(testsuite.attrib["additional_warning"]),
                "ERROR": int(testsuite.attrib["additional_error"]),
            }

            my_testsuite.add_extra_testcases_notice()

            my_ts.add_testsuite(my_testsuite)

        return my_ts

    def __post_init__(self):
        super().__post_init__()
        self.testsuites = []
        self._index = 0

    def __contains__(self, other):
        return other in self.testsuites

    def __iter__(self):
        return self

    def __next__(self):
        if self._index > len(self.testsuites):
            ret_val = self.testsuites[self._index]
            self._index += 1
            return ret_val
        raise StopIteration

    def __iadd__(self, other):
        for testsuite in other.testsuites:
            if testsuite not in self.testsuites:
                # If we don't see this testsuite already, add the whole thing
                # to this object
                self.add_testsuite(testsuite)
            else:
                # If we do see this testsuite, roll through the testcases
                # and try to add each to this testsuite.
                for testcase in testsuite.testcases:
                    # There is code in this function to avoid duplicates
                    self.add_testcase(testsuite.obj_id, testcase)
        return self

    @property
    def total_tests(self) -> int:
        """
        The total number of tests across all test suites.

        Returns:
            int: The total number of tests.
        """
        return sum(testsuite.num_tests for testsuite in self.testsuites)

    @property
    def total_errors(self) -> int:
        """
        The total number of errors across all test suites.

        Returns:
            int: The total number of errors.
        """
        return sum(testsuite.num_errors for testsuite in self.testsuites)

    @property
    def total_warnings(self) -> int:
        """
        The total number of warnings across all test suites.

        Returns:
            int: The total number of warnings.
        """
        return sum(testsuite.num_warnings for testsuite in self.testsuites)

    @property
    def worst_message(self) -> str | None:
        """
        The worst message among all test suites.

        Returns:
            (str | None): The worst message, which can be "ERROR", "WARNING", or "SUCCESS".
                If no tests are present, returns None.
        """
        if self.total_tests == 0:
            return None

        if self.total_errors > 0:
            return "ERROR"

        if self.total_warnings > 0:
            return "WARNING"

        return "SUCCESS"

    @property
    def as_xml(self) -> Element:
        """Converts this object into an XML element"""
        my_xml = Element("testsuites", attrib={"id": self.obj_id, "name": self.name})
        for testsuite in self.testsuites:
            my_xml.append(testsuite.to_xml())
        return my_xml

    def add_testsuite(self, my_testsuite: JUnitTestSuite):
        """Create a new testsuite

        Args:
            my_testsuite (TestSuite):
                The TestSuite to add to this object.
        """
        # Avoid adding duplicate test suites
        found = False
        for testsuite in self.testsuites:
            if (
                testsuite.obj_id == my_testsuite.obj_id
                and testsuite.name == my_testsuite.name
            ):
                found = True
                target_ts = testsuite

        if not found:
            self.testsuites.append(my_testsuite)
        else:
            logger.debug("%s already exists in %s", my_testsuite, self)
            # If the testsuite already exists, we assume that we wanted to add
            # whatever testcases are here into that one
            for testcase in my_testsuite.testcases:
                target_ts.add_testcase(testcase)

    def filter(self, pattern, prune_empty=True) -> "JUnitTestSuites":
        """
        Filter the JUnitTestSuites object by keeping only the test cases and test suites
        whose names or object IDs match a given pattern. Optionally remove empty test suites.

        Args:
            pattern (str):
                The regular expression pattern to match against the test suite names and object
                IDs.
            prune_empty (bool):
                Whether to remove empty test suites after filtering. Defaults to True.

        Returns:
            JUnitTestSuites:
                A new JUnitTestSuites object that contains only the filtered test cases and test
                suites. The name and obj_id properties of the new object are the same as those of
                the original object.

        """
        return_value = JUnitTestSuites(self.name, self.obj_id)
        for testsuite in self.testsuites:
            returned_testsuite = JUnitTestSuite(testsuite.name, testsuite.obj_id)
            if re.search(testsuite.name, pattern) or re.search(
                testsuite.obj_id, pattern
            ):
                for testcase in testsuite.testcases:
                    returned_testsuite.add_testcase(testcase)
                return_value.add_testsuite(returned_testsuite)
                continue

            for testcase in testsuite.filter(pattern):
                returned_testsuite.add_testcase(testcase)
            if len(returned_testsuite.testcases) > 0 or not prune_empty:
                return_value.add_testsuite(returned_testsuite)
        return return_value

    def get_testsuite(self, pattern, **kwargs) -> JUnitTestSuite:
        """Get a TestSuite object by either id or name"""
        return find_exactly_one(self.testsuites, pattern, **kwargs)

    def add_testcase(self, testsuite_id: str, my_testcase: JUnitTestCase):
        """Create a new testcase

        Args:
            testsuite_id (str):
                The id of the testsuite to nest this testcase under. Must
                exist.
            my_testcase (TestCase):
                The TestCase object to add to the specified TestSuite.
        """

        for i, testsuite in enumerate(self.testsuites):
            if testsuite.obj_id == testsuite_id:
                self.testsuites[i].add_testcase(my_testcase)
                return

        # raise an exception of we got this far, since that means we couldn't
        # find a testsuite with this id
        raise KeyError(f'No testsuite id "{testsuite_id}" was found')

    def to_file(self, filename: str):
        """Saves the XML generated by this class to an XML file

        Args:
            filename (str):
                The filename to be created.
        """
        logger.info("saving textsuite XML to %s", filename)
        ElementTree(self.as_xml).write(filename, encoding="UTF-8", xml_declaration=True)
