"""
Module defining the JUnitTestSuite class.
"""

from dataclasses import dataclass, field
import logging
from xml.etree.ElementTree import Element

from .failure_types import FailureType
from .junit_baseobject import JUnitBaseObject
from .junit_testcase import JUnitTestCase
from ..util import find_exactly_one, filter_objects

logger = logging.getLogger(__name__)


@dataclass
class JUnitTestSuite(JUnitBaseObject):
    """
    Represents a JUnit test suite.
    """

    max_testcases: int = field(init=False, repr=False, default=20)
    additional_cases: dict = field(init=False, repr=False)
    testcases: list[JUnitTestCase] = field(init=False, repr=False)

    def __init__(self, obj_id: str, name: str):
        super().__init__(name=name, obj_id=obj_id)

    def __post_init__(self):
        logger.debug("initing testsuite (obj_id: %s, name: %s)", self.obj_id, self.name)
        super().__post_init__()
        self.additional_cases = {fail_type.name: 0 for fail_type in FailureType}

        self.testcases = []
        self._index = 0

    def __contains__(self, other):
        return other in self.testcases

    def __iter__(self):
        return self

    def __next__(self):
        if self._index < len(self.testcases):
            ret_val = self.testcases[self._index]
            self._index += 1
            return ret_val
        raise StopIteration

    @property
    def num_tests(self):
        """The number of testcases contained in this testsuite"""
        return len(self.testcases) + sum(v for k, v in self.additional_cases.items())

    @property
    def num_errors(self):
        """The number of failing testcases in this testsuite"""
        return (
            sum(1 for i in self.testcases if i.fail_type == FailureType.ERROR)
            + self.additional_cases["ERROR"]
        )

    @property
    def num_warnings(self):
        """The number of warning testcases in this testsuite"""
        return (
            sum(1 for i in self.testcases if i.fail_type == FailureType.WARNING)
            + self.additional_cases["WARNING"]
        )

    @property
    def can_add_testcase(self):
        """Returns True if we haven't hit max_testcases"""
        return len(self.testcases) < int(self.max_testcases)

    @property
    def worst_message(self):
        """
        A property method that returns the worst status of a test suite based on the number of
        errors and warnings it has. If the test suite has not been run yet, it returns None. If
        there are any errors, it returns "ERROR". If there are no errors but there are warnings,
        it returns "WARNING". If there are no errors or warnings, it returns "SUCCESS".

        Returns:
        str or None: The worst status of the test suite, or None if it has not been run yet.
        """
        if self.num_tests == 0:
            return None

        if self.num_errors > 0:
            return "ERROR"

        if self.num_warnings > 0:
            return "WARNING"

        return "SUCCESS"

    def add_extra_testcases_notice(self):
        """
        In the event we exceed max_testcases when adding a new testcase, we
        want to make sure we have an explination in the form of a testcase
        that will be included in the output.
        """
        for tc_type, num_extra in self.additional_cases.items():
            if num_extra > 0:
                extra_tc = JUnitTestCase(
                    obj_id=f"Extra_{tc_type}",
                    name=f"{self.obj_id}: Exceeded TestCase limit",
                    fail_type=tc_type,
                    fail_msg=(f"{num_extra} Additional TestCases"),
                    fail_text=(
                        f"An additional {num_extra} {tc_type}s " "were identified"
                    ),
                )
                self.add_testcase(extra_tc)

    def add_testcase(self, my_testcase: JUnitTestCase):
        """Create a new testcase

        Args:
            my_testcase (TestCase):
                A new TestCase object to try and append to this object.
        """
        # check to make sure we're not over the limit for testcases yet
        if self.can_add_testcase:
            # check all of our testcases alread present to see if an id
            # like this one already exists
            if my_testcase in self.testcases:
                logger.debug("testcase %s already exists in %s", self.obj_id, self)
                return

            self.testcases.append(my_testcase)
        else:
            logger.debug("max_testcases exceeded")
            self.additional_cases[my_testcase.fail_type.name] += 1

    def filter(self, pattern: str, **kwargs) -> "JUnitTestSuite":
        """
        The filter method filters test cases from a JUnitTestSuite instance based on a given
        pattern and keyword arguments. It returns a new JUnitTestSuite instance with the filtered
        test cases.

        Args:

            pattern (str):
                The pattern used to filter the test cases. Only test cases whose name
                matches the pattern will be included in the filtered result.
            **kwargs:
                Additional keyword arguments that are passed to filter_objects function for
                further filtering.

        Returns:
            A new JUnitTestSuite instance that contains the filtered test cases.
        """
        testsuite = JUnitTestSuite(name=self.name, obj_id=self.obj_id)
        for testcase in filter_objects(self.testcases, pattern, **kwargs):
            testsuite.add_testcase(testcase)
        return testsuite

    def get_testcase(self, pattern, **kwargs) -> JUnitTestCase:
        """Get a TestSuite object by either id or name"""
        return find_exactly_one(self.testcases, pattern, **kwargs)

    def to_xml(self) -> Element:
        """Convert this object into an XML element"""
        xml_element = Element(
            "testsuite",
            attrib={
                "id": self.obj_id,
                "name": self.name,
                "tests": str(self.num_tests),
                "failures": str(self.num_errors),
                "additional_success": str(self.additional_cases["SUCCESS"]),
                "additional_warning": str(self.additional_cases["WARNING"]),
                "additional_error": str(self.additional_cases["ERROR"]),
                "max_testcases": str(self.max_testcases),
            },
        )

        if not self.can_add_testcase:
            self.add_extra_testcases_notice()

        for testcase in self.testcases:
            xml_element.append(testcase.to_xml())

        return xml_element
