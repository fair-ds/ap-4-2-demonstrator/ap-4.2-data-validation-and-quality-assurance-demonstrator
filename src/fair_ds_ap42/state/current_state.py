"""
Background Object that keeps track of various aspects of the analysis.

Only a single version of this object can ever exist, and it is expected to be
loaded in as a pickle as it is loaded in and out of the various stages of
analysis.

Tracks the status of:
    - All data files
    - All schema files
    -
"""

import datetime
import logging
from pathlib import Path
import pickle

from ..junit_xml_gen import JUnitTestSuites

from ..objects.errors import BaseTestCase, DataFileTestCase
from ..objects.datafile import DataFile
from ..objects.schema_assignment import SchemaAssignment
from ..singleton_meta import SingletonMeta
from ..objects.validator.schema_assignment_validator import SchemasAssignmentValidator


DEFAULT_FILENAME = Path("state.pickle")
logger = logging.getLogger(__name__)


class CurrentState(metaclass=SingletonMeta):
    """
    Object for tracking the state of all objects processed by this project.
    """

    def __init__(self):
        self.data_files = []
        self.to_process_mask = []
        self.xml = JUnitTestSuites(name="Data Validation and Quality Assurance")

    @property
    def testsuites(self) -> JUnitTestSuites:
        """
        Get the testsuites associated with this object since we started.
        """
        return self.xml.testsuites

    @property
    def files_to_process(self) -> list[DataFile]:
        """
        Returns a list of files that have yet to be processed
        """
        return [r for r, m in zip(self.data_files, self.to_process_mask) if m]

    @property
    def schema_assignment(self) -> SchemaAssignment:
        """
        The Schema Assignment that we have currently.
        """
        return self._schema_assignment

    @schema_assignment.setter
    def schema_assignment(self, schema_assignment: SchemaAssignment):
        """When adding a schema assignment object to the state, we collect all
        of the errors which were noted when validating the schema assignement
        object.
        """
        for sa_data_file in schema_assignment.data_files:
            for my_data_file in self.data_files:
                if sa_data_file == my_data_file and sa_data_file.schema is not None:
                    my_data_file.assign_schema(sa_data_file.schema)
            if sa_data_file not in self.data_files:
                self.add_datafile(sa_data_file)

        for sa_schema_file in schema_assignment.schemas:
            for test_case in sa_schema_file.test_cases:
                self.add_testcase(test_case)

        sa_validator = SchemasAssignmentValidator(reader=schema_assignment.reader)
        sa_validator.validate_self()
        for test_case in sa_validator.test_cases:
            self.add_testcase(test_case)
        self._schema_assignment = schema_assignment

    def __eq__(self, other):
        return isinstance(other, CurrentState) and self.xml == other.xml

    def __add__(self, other):
        self.xml += other.xml
        return self

    def __repr__(self):
        return f"""CurrentState
        - {len(self.testsuites)} testsuites
        - {len(self.data_files)} data files
        - {len(self.files_to_process)} files to process"""

    def as_dict(self, root_url: str = "") -> dict:
        """
        Get all of the data contained in this state object as a dictionary,
        which can be saved to a JSON file.
        """
        return {
            "last_modified": datetime.datetime.now().isoformat(),
            "data_files": [
                datafile.as_dict(root_url)
                for datafile in self.schema_assignment.data_files
            ],
            "schema_files": [
                schema.as_dict(root_url) for schema in self.schema_assignment.schemas
            ],
            "schema_assignment": self.schema_assignment.raw_data,
        }

    def contains_similar_resource(self, resource: DataFile) -> bool:
        """
        Check this object for a resource similar to the provided object

        Args:
            resource (DataFile):
                The datafile resource to check for

        Returns:
            bool
        """
        assert isinstance(resource, DataFile)
        for my_resource in self.data_files:
            if my_resource == resource or my_resource.is_similar_to(resource):
                return True
        return False

    def add_datafile(self, resource: DataFile):
        """
        Add the provided data file to this state object.

        Runs various checks to see if this file has already been processed, if
        it is entirely new, or if it is a modified version of a file that we've
        already seen.

        Args:
            resource (DataFile):
                The data file to add.
        """
        logger.debug("adding %s to data files", resource)
        # iterate over the existing datafiles to check if any are just an
        # old version of this one
        found = False
        if resource in self.data_files:
            found = True
            self.add_testcase(DataFileTestCase(resource.filepath, "no_action"))
            logger.debug("%s was in data_files. taking no action", resource)
        else:
            for i, data_file in enumerate(self.data_files):
                if resource.is_similar_to(data_file):
                    self.to_process_mask[i] = True
                    found = True
                    self.add_testcase(DataFileTestCase(resource.filepath, "updated"))
                    logger.debug("%s was in data_files, but modified.", resource)

        if not found:
            self.data_files.append(resource)
            self.to_process_mask.append(True)
            logger.debug("first time seeing %s. Adding to list.", resource)

        if resource.exists and resource.is_valid:
            self.add_testcase(DataFileTestCase(resource.filepath, "success"))

    def add_testcase(self, case: BaseTestCase):
        """
        Add a testcase to the state object.

        The BaseTestCase object contains all the information needed to assign
        a test case to a testsuite.

        Args:
            case (BaseTestCase):
                The object to add to this state.
        """
        logger.debug("adding test case %s", case.testcase_id)
        case.add_to_xml(self.xml)

    def reset(self):
        """
        Will reset all of the testssuites in this object.

        Used when we first start up a new run of the script to start with a
        blank slate of tests.
        """
        logger.info("resetting all testsuites in CurrentState")
        self.xml = JUnitTestSuites(name="Data Validation and Quality Assurance")
        self.to_process_mask = [False for _ in self.data_files]

    def save(self, filename: Path | str = DEFAULT_FILENAME):
        """
        Save this state object to the provided filepath.

        Args:
            filename (Path | str):
                The location to save this object to. Defaults to "state.pickle"
        """
        logger.info("saving CurrentState to %s", filename)
        with open(filename, "wb") as file_obj:
            pickle.dump(self, file_obj)

    def testsuites_to_xml(self, filename: Path | str):
        """
        Save the testsuites this state object has collected to a local file as
        an XML file in JUnit format.

        Args:
            filename (Path | str):
                The file to save this xml file to
        """
        self.xml.to_file(filename)

    @classmethod
    def load(cls, filename: Path | str = DEFAULT_FILENAME) -> "CurrentState":
        """
        Load a state object from the given filepath.

        Args:
            filename (Path | str):
                The location to save this object to. Defaults to "state.pickle"
        """

        logger.info("loading in CurrentState %s", filename)
        if Path(filename).exists():
            logger.debug(
                "%s exists, replaceing current state with loaded file", filename
            )
            with open(filename, "rb") as file_obj:
                state = pickle.load(file_obj)
                CurrentState._instances[CurrentState] = state

        state = cls()
        logger.debug(repr(state))
        return state
