"""
This file contains the ResultParser class, which is used to parse the results from the JSON data
generated as a result of the validation and quality checks. This object acts as a wrapper around
the JSON data, providing a more convenient interface for accessing the results.
"""

import json
from pathlib import Path


class ResultParser:
    """
    A class used to parse the results from the JSON data generated as a result of the validation
    and quality checks.
    """

    @property
    def full_results(self) -> dict:
        """
        Get the full results dictionary.

        Returns:
            The full results dictionary.
        """
        return self._results

    @classmethod
    def load(cls, file_path: str) -> "ResultParser":
        """
        Load the results from a file.

        Args:
            file_path: The path to the file to load the results from.

        Returns:
            A new ResultParser object containing the results.
        """
        with open(file_path, "r", encoding="utf-8") as file_obj:
            results = json.load(file_obj)
        return cls(results)

    def __init__(self, results: dict):
        self._results = results

    @property
    def get_data_file_list(self) -> list[str]:
        """
        Get the list of data files that are listed.

        Returns:
            The list of data files that are listed in the results.
        """
        data_files = []
        for schema_group in self._results:
            for data_file in schema_group["data_files"]:
                data_files.append(data_file["filepath"])

        return data_files

    @property
    def get_schema_file_list(self) -> list[str]:
        """
        Get the list of schema files that are listed.

        Returns:
            The list of schema files that are listed in the results.
        """
        schema_files = []
        for schema_group in self._results:
            schema_files.append(schema_group["schema"]["filepath"])

        return schema_files

    @property
    def schema_assignment(self) -> dict[str, list[str]]:
        """
        Get the mapping of schema files to data files.

        Returns:
            A dictionary mapping schema files to data files.
        """

        def get_html_path(my_str) -> str:
            return (Path(my_str).with_suffix("") / "index.html").as_posix()

        schema_assignment = {}
        for schema_group in self._results:
            schema_file = schema_group["schema"]["filepath"]
            data_files = []
            for data_file in schema_group["data_files"]:
                data_files.append(
                    (data_file["filepath"], get_html_path(data_file["filepath"]))
                )
            schema_assignment[schema_file] = data_files

        # Sort the schema assignment by schema file name, then order the data files by their file
        # path
        schema_assignment = dict(sorted(schema_assignment.items()))
        for schema_file, data_files in schema_assignment.items():
            schema_assignment[schema_file] = sorted(data_files, key=lambda x: x[0])

        return schema_assignment

    @property
    def data_files(self):
        """
        Get all of the data files that are listed.

        Returns:
            A list of dictionaries, each containing the details of a data file.
        """
        for schema_group in self._results:
            yield from schema_group["data_files"]
