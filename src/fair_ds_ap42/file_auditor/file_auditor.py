"""
Contains the FileAuditor class.

This class is used to read in a json file containing file metadata and compare it to the actual
files currently present in any available locations. It can then be used to return a list of files
that have been added, removed, or modified since the last time the json file was updated. We can
also use it to update the json file with the current file metadata.

Example JSON file format:
[
    {
        "filepath": "file1",
        "source": {
            "type": "local_directory",
            "location": "./my_dir"
        },
        "size_in_bytes": 12345,
        "last_modified": "2020-01-01T00:00:00Z"
    },
    {
        filepath: "file2",
        "source": {
            "type": "s3_bucket",
            "bucket_name": "my_bucket",
        },
        "size_in_bytes": 67890,
        "last_modified": "2020-01-01T00:00:00Z"
    }
]

Example usage:
    file_auditor = FileAuditor()
"""

from dataclasses import dataclass
import logging

from ..source.reader.ijson_data_reader import IJSONDataReader
from ..source import ISource, find_file_location

from ..objects.resource import Resource, remove_duplicate_resources
from ..utils.string_filter import StringFilter
from .file_changes import FileChanges

logger = logging.getLogger(__name__)


@dataclass
class FileAuditor:
    """
    Class for auditing files in one or more sources and comparing them to a json file containing
    file metadata.
    """

    filepath: str = "file_metadata.json"
    file_patterns: list[str] = None
    ignore_patterns: list[str] = None
    reader: IJSONDataReader = None

    file_changes: FileChanges = None

    @property
    def file_metadata(self) -> dict:
        """
        The file metadata dictionary.

        Returns:
            dict:
                The file metadata dictionary.
        """
        if self.reader is None:
            return []

        try:
            return self.reader.read_dict()
        except FileNotFoundError:
            return []

    @property
    def all_existing_files(self) -> list[Resource]:
        """
        A list of all files in the file metadata.

        Returns:
            list:
                A list of all files in the file metadata.
        """
        if self.file_metadata is None:
            return []

        return [Resource.from_dict(metadata) for metadata in self.file_metadata]

    @property
    def files_to_process(self) -> list[Resource]:
        """
        A list of all files that need to be processed.

        Returns:
            list:
                A list of all files that need to be processed.
        """
        return self.file_changes.new_files + self.file_changes.modified_files

    @property
    def all_files(self) -> list[Resource]:
        """
        Gets a list of all files that the Auditor has discovered.

        This is the union of the unchanged, new, removed, and modified files. If we have not
        searched for files yet, this will return an empty list and print a warning.
        """
        return self.file_changes.all_files()

    def __post_init__(self):
        if self.file_changes is None:
            self.file_changes = FileChanges()

        try:
            if self.reader is None:
                self.reader = find_file_location(self.filepath)

            # Check that the reader is a JSON reader
            if self.reader.format != "json":
                raise ValueError(
                    f"FileAuditor reader must be a JSON reader, not {self.reader.format}"
                )
        except FileNotFoundError:
            logger.info("No file metadata found. Initializing empty file metadata.")

        if self.ignore_patterns is None:
            self.ignore_patterns = ["*/schemas.json"]

        self.filter = StringFilter(
            pattern=self.file_patterns, ignore_pattern=self.ignore_patterns
        )

    def get_files_to_process_with_pattern(self, pattern: str) -> list[Resource]:
        """
        Get the files that need to be processed that match the given pattern.

        Args:
            pattern (str): The pattern to match.

        Returns:
            list:
                A list of files that need to be processed that match the given pattern.
        """
        my_filter = StringFilter(pattern=pattern, ignore_pattern=self.ignore_patterns)
        filepaths = my_filter.filter_list(
            [file.filepath for file in self.files_to_process]
        )
        return [file for file in self.files_to_process if file.filepath in filepaths]

    def get_existing_files_as_resources(self, source: ISource) -> list[Resource]:
        """
        Given a source, get a list of all files that are in the source and are also in the file
        metadata.

        All Objects will be returned as Resources.

        Args:
            source (ISource): The source to search for files.

        Returns:
            list[Resource]:
                A list of all files that are in the source and are also in the file metadata.
        """
        existing_filepaths = self.filter.filter_list(
            [metadata["filepath"] for metadata in self.file_metadata]
        )

        existing_files = [
            Resource.from_dict(metadata)
            for metadata in self.file_metadata
            if metadata["source"] == source.to_json()
            and metadata["filepath"] in existing_filepaths
        ]
        logger.debug("Found %d existing files in metadata.", len(existing_files))
        return existing_files

    def get_files_in_source_as_resources(self, source: ISource) -> list[Resource]:
        """
        Given a source, get a list of all files that are in the source.

        All Objects will be returned as Resources.

        Args:
            source (ISource): The source to search for files.

        Returns:
            list[Resource]:
                A list of all files that are in the source.
        """

        filepaths_in_source = self.filter.filter_list(source.list_files())

        files_in_source = [
            Resource(filepath=source_file, source=source)
            for source_file in source.list_files()
            if source_file in filepaths_in_source
        ]
        logger.debug("Found %d files in %s.", len(files_in_source), source)
        return files_in_source

    def search_all(self, sources: list[ISource]):
        """
        Search all given sources for files that have been added, removed, or modified since the last
        time the json file was updated.

        Args:
            sources (list):
                A list of sources to search for files.
        """
        for source in sources:
            self.search(source)

    def search(self, source: ISource) -> list[Resource]:
        """
        Search the given source for files that have been added, removed, or modified since the last
        time the json file was updated.

        Args:
            source (ISource): The source to search for files.

        Returns:
            list:
                A list of files that have been added, removed, or modified since the last time
                the json file was updated.
        """
        existing_files = self.get_existing_files_as_resources(source)
        files_in_source = self.get_files_in_source_as_resources(source)

        self.file_changes.unchanged_files = self._find_unchanged_files(
            existing_files, files_in_source
        )
        self.file_changes.modified_files = self._find_modified_files(
            existing_files, files_in_source
        )
        self.file_changes.new_files = self._find_new_files(
            existing_files, files_in_source
        )
        self.file_changes.removed_files = self._find_removed_files(
            existing_files, files_in_source
        )

    def _find_unchanged_files(
        self, existing_files: list[Resource], files_in_source: list[Resource]
    ) -> list[Resource]:
        """
        Scan through the existing files and find the ones that have not been modified.
        """
        logger.debug("Finding unchanged files...")
        unchanged_files = set(self.file_changes.unchanged_files)
        for file in existing_files:
            if file in files_in_source:
                unchanged_files.add(file)

        return remove_duplicate_resources(list(unchanged_files))

    def _find_modified_files(
        self, existing_files: list[Resource], files_in_source: list[Resource]
    ) -> list[Resource]:
        """
        Scan through the existing files and find the ones that have been modified.
        """
        logger.debug("Finding modified files...")
        modified_files = set(self.file_changes.modified_files)
        for existing_file in existing_files:
            for source_file in files_in_source:
                logger.debug("Comparing %s to %s.", existing_file, source_file)
                if existing_file.is_similar_to(source_file):
                    logger.debug("%s has been modified.", existing_file)
                    modified_files.add(source_file)
        return remove_duplicate_resources(list(modified_files))

    def _find_new_files(
        self, existing_files: list[Resource], files_in_source: list[Resource]
    ) -> list[Resource]:
        """
        Scan through the files in the source and find the ones that have been added.
        """
        logger.debug("Finding new files...")
        new_files = set(self.file_changes.new_files)
        for file in files_in_source:
            if (
                file not in existing_files
                and file not in self.file_changes.modified_files
            ):
                logger.debug("%s is a new file.", file)
                new_files.add(file)
        return remove_duplicate_resources(list(new_files))

    def _find_removed_files(
        self, existing_files: list[Resource], files_in_source: list[Resource]
    ) -> list[Resource]:
        """
        Scan through the existing files and find the ones that have been removed.
        """
        logger.debug("Finding removed files...")
        removed_files = set(self.file_changes.removed_files)
        for file in existing_files:
            # Check if the file is in the modified files list, in which case it's not new
            in_modified = Resource.contains_modified_resource(
                file, self.file_changes.modified_files
            )

            if file not in files_in_source and not in_modified:
                logger.debug("%s has been removed.", file)
                removed_files.add(file)
        return remove_duplicate_resources(list(removed_files))

    def write_json(self, source: ISource) -> None:
        """
        Write the file metadata to a json file.

        Args:
            source (ISource): The source to write the file metadata to.

        Returns:
            None
        """
        # Convert the list of FileEntry objects to a list of dictionaries
        file_metadata = [file.as_dict() for file in self.file_changes.all_files()]

        # Write the file metadata to a json file
        source.write_json(self.filepath, file_metadata)
