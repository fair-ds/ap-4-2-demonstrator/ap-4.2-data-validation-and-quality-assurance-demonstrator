"""
This file contains the FileChanges class, which is used to hold information for the FileAuditor
about which files have been added, removed, modified, or remain unchanged since the last audit.
"""

from dataclasses import dataclass

from ..objects.resource import Resource


@dataclass
class FileChanges:
    """
    This class is used to hold information for the FileAuditor about which files have been added,
    removed, modified, or remain unchanged since the last audit.
    """

    unchanged_files: list[Resource] = None
    new_files: list[Resource] = None
    removed_files: list[Resource] = None
    modified_files: list[Resource] = None

    def __post_init__(self):
        if self.unchanged_files is None:
            self.unchanged_files = []
        if self.new_files is None:
            self.new_files = []
        if self.removed_files is None:
            self.removed_files = []
        if self.modified_files is None:
            self.modified_files = []

    def all_files(self) -> list[Resource]:
        """
        Returns a list of all files that we have information about.
        """
        return (
            self.unchanged_files
            + self.new_files
            + self.removed_files
            + self.modified_files
        )
