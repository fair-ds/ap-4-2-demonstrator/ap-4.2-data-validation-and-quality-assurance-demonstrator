# pylint: skip-file

import requests

# SSL Fix for accessing Coscine
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = "ALL:@SECLEVEL=1"
print("SSL fix for Coscine S3 Buckets in place")
