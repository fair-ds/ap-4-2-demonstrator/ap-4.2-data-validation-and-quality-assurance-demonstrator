"""
run_check.py

This script is used to invoke the FAIR-DS-AP42 quality and validation checks.

Usage:
    python -m fair_ds_ap42.run_check --type <type> --output_file <output_file> --log <log_level>

    type: The type of check to run (quality, validation, both)
    output_file: The file to save the results to (in JSON format)
    log_level: Set the logging level (DEBUG, INFO, WARNING)
"""

import argparse
import logging
import json

from .cli import add_log_argument
from .file_auditor import FileAuditor
from .objects.schema_assignment import (
    SchemaAssignment,
    SchemaAssignmentLoader,
    SchemaAssignmentManager,
)

from .source.available_sources import get_registered_sources
from .register_sources import register_all_sources
from .source import available_sources
from .settings import settings
from .output import StoplightFile, SummaryFile

try:
    from dotenv import load_dotenv

    load_dotenv()
except ModuleNotFoundError:
    pass

# set PYTHONPATH=../../src && python -m fair_ds_ap42.run_check --log DEBUG


def _parse_cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Run FAIR-DS-AP42 Quality/Validation Checks"
    )
    parser.add_argument(
        "--type",
        "-t",
        choices=["quality", "validation", "both"],
        default="both",
        help="The type of check to run",
    )
    parser.add_argument(
        "--output_file",
        "-o",
        default="results.json",
        help="The file to save the results to (in JSON format)",
    )
    parser.add_argument(
        "--summary_file",
        default="qc_summary.json",
        help="The file to save the summary to (in JSON format)",
    )
    parser.add_argument(
        "--upload_stoplight",
        action="store_true",
        help="Upload the stoplight file to any accesible remote sources",
    )
    parser = add_log_argument(parser)
    return parser.parse_args()


def get_schema_assignment(schema_assignment_filepath) -> SchemaAssignment:
    """
    Given a filepath and a list of potential sources, load the schema assignment

    Args:
        schema_assignment_filepath (str): The path to the schema assignment file
        potential_sources (list): A list of sources to search for the file in

    Returns:
        SchemaAssignment: The loaded schema assignment
    """
    try:
        # Locate which source contains this object
        source = available_sources.find_file(schema_assignment_filepath)

        # Get a reader for this file
        my_reader = source.get_reader(schema_assignment_filepath)
    except FileNotFoundError:
        my_reader = None

    # Load the object
    return SchemaAssignmentLoader.from_reader(my_reader, get_registered_sources())


if __name__ == "__main__":
    args = _parse_cli()

    logging.basicConfig(level=args.log)

    # Register all sources that we have access to
    register_all_sources()

    # Create a file auditor and search all sources for files
    my_file_auditor = FileAuditor(
        file_patterns=settings.file_patterns.all_file_patterns,
        ignore_patterns=settings.file_patterns.ignore_patterns,
    )
    my_file_auditor.search_all(get_registered_sources())

    # Load the schema assignment
    my_schema_assignment = get_schema_assignment(settings.directories.schemas_file)

    # Update the schema assignment with the file auditor
    schema_assignment_manager = SchemaAssignmentManager(my_schema_assignment)
    schema_assignment_manager.update_schema_assignment(
        my_file_auditor,
        schema_file_patterns=settings.file_patterns.schema_file_patterns,
        data_file_patterns=settings.file_patterns.data_file_patterns,
    )

    # Iterate over the data files and run the requested checks
    quality_test_cases = {}
    validation_test_cases = {}
    for data_file in my_schema_assignment.data_files:
        if args.type in ["validation", "both"]:
            validation_test_cases[data_file.filepath] = data_file.validation_test_cases
        if args.type in ["quality", "both"]:
            quality_test_cases[data_file.filepath] = data_file.quality_test_cases

    print("*" * 30)
    print("Validation Test Cases")
    print("*" * 30)
    for filepath, test_cases in validation_test_cases.items():
        print("*" * 30)
        print(filepath)
        for test_case in test_cases:
            print("-   ", test_case.msg)

    print("*" * 30)
    print("Quality Test Cases")
    print("*" * 30)
    for filepath, test_cases in quality_test_cases.items():
        print("*" * 30)
        print(filepath)
        for test_case in test_cases:
            print("-   ", test_case.msg)

    all_metadata = my_schema_assignment.get_rich_file_contents(
        plots=True,
        validation_check=args.type in ["validation", "both"],
        quality_check=args.type in ["quality", "both"],
    )

    # Save the metadata to a json file
    with open(args.output_file, "w", encoding="utf-8") as f:
        json.dump(all_metadata, f, indent=4)

    if args.summary_file:
        print("Creating summary file", args.summary_file)
        summary_file = SummaryFile.from_file(args.output_file)
        summary_file.to_file(args.summary_file)

    if args.upload_stoplight:
        print("Creating stoplight file")
        stoplight = StoplightFile.from_file(args.output_file)

        # Upload the stoplight file to any accessible remote sources
        for registered_source in get_registered_sources():
            print("Uploading stoplight file to", registered_source)
            registered_source.write_json("stoplight.json", stoplight.summary)
