"""
Base validator class

Valildators are used to validate a resource against a collection of conditions and store the
results of the validation as a list of test cases.
"""

from copy import deepcopy
from collections import Counter
from dataclasses import dataclass
import logging

from ..errors import BaseTestCase

logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class BaseValidator:
    """
    Base validator class
    """

    # Property that determines the number of identical test cases that are allowed
    # before they are collapsed into one test case
    collapse_threshold: int = 5

    @property
    def test_cases(self) -> list[BaseTestCase]:
        """
        A list of BaseTestCase objects

        Returns:
            list[BaseTestCase]: A list of BaseTestCase objects
        """
        if not self.validation_run:
            logger.warning(
                "Validation has not been run on this object yet. Test cases will be empty."
            )

        # Count how many times a test case with an identical testcase_id appears
        counter = Counter(test_case.testcase_id for test_case in self._test_cases)

        # If the number of times a test case with the same testcase_id appears is greater than
        # the collapse threshold, collapse them into one new test case, and append the text
        # "x{number of times collapsed}" to the end of the test case msg
        ret_val = []
        for test_case in self._test_cases:
            if counter[test_case.testcase_id] > self.collapse_threshold:
                # Make a deep copy of the test case, so we don't modify the original
                test_case = deepcopy(test_case)
                test_case.msg += (
                    f" (error repeated {counter[test_case.testcase_id]} times)"
                )
                ret_val.append(test_case)
                counter[test_case.testcase_id] = 0
            elif counter[test_case.testcase_id] > 0:
                ret_val.append(test_case)
                counter[test_case.testcase_id] = 0

        return ret_val

    @property
    def validation_run(self) -> bool:
        """
        Whether the validation has been run

        Returns:
            bool: Whether the validation has been run
        """
        return self._validation_run

    @property
    def validation_status(self) -> str:
        """
        Get the most severe status of the test cases

        Returns:
            str: The most severe status of the test cases
        """
        statuses = Counter([test_case.fail_type for test_case in self.test_cases])

        if "ERROR" in statuses:
            return "ERROR"
        if "WARNING" in statuses:
            return "WARNING"
        return "SUCCESS"

    def __post_init__(self):
        self._test_cases = []
        self._validation_run = False

    def get_testcases_dict(self) -> dict[str, list[BaseTestCase]]:
        """
        Get a dictionary of test cases

        Returns:
            dict[str, BaseTestCase]: A dictionary of test cases
        """
        return {
            test_case.testsuite_name: [
                tc.as_dict()
                for tc in self.test_cases
                if tc.testsuite_name == test_case.testsuite_name
            ]
            for test_case in self.test_cases
        }

    def get_testsuites_status(self) -> dict:
        """
        Get the most severe status in each of the testsuites
        """
        testcases_dict = self.get_testcases_dict()

        ret_val = {}
        for testsuite, testcases in testcases_dict.items():
            counter = Counter(testcase["fail_type"] for testcase in testcases)
            if "ERROR" in counter:
                ret_val[testsuite] = "ERROR"
            elif "WARNING" in counter:
                ret_val[testsuite] = "WARNING"
            else:
                ret_val[testsuite] = "SUCCESS"
        return ret_val
