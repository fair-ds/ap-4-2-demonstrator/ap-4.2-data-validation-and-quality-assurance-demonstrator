"""
Defines the DataFileValidator class
"""

from dataclasses import dataclass
from collections import Counter
from typing import TYPE_CHECKING

from .base_validator import BaseValidator

from ..errors import (
    BaseTestCase,
    DataFileTestCase,
    SchemaAssignedTestCase,
    DataQualityTestCase,
)

if TYPE_CHECKING:
    from ..datafile.datafile import DataFile


@dataclass(kw_only=True)
class DataFileValidator(BaseValidator):
    """
    Class for validating data files
    """

    datafile: "DataFile"
    _quality_errors: list[DataQualityTestCase] = None
    _validation_errors: list[DataQualityTestCase] = None

    @property
    def validation_errors(self) -> list[DataQualityTestCase]:
        """
        Return a list of DataQualityTestCase objects for the datafile

        Returns:
            list[DataQualityTestCase]: A list of DataQualityTestCase objects
        """
        if self._validation_errors is None:
            self._validation_errors = self.validate_against_schema()

        return self._validation_errors

    @property
    def quality_errors(self) -> list[DataQualityTestCase]:
        """
        Return a list of DataQualityTestCase objects for the datafile

        Returns:
            list[DataQualityTestCase]: A list of DataQualityTestCase objects
        """
        return self.column_quality_check()

    @property
    def quality_status(self) -> str:
        """
        Return the most severe status of the quality test cases.

        Returns:
            str: The most severe status of the quality test cases
        """
        # Check the statuses of the quality test cases
        statuses = Counter(
            [test_case.fail_type for test_case in self.column_quality_check()]
        )

        if "ERROR" in statuses:
            return "ERROR"
        if "WARNING" in statuses:
            return "WARNING"
        return "SUCCESS"

    def validate_self(self):
        """
        Validate this object.

        Check if the object is valid and accessible.
        Stores results in self.test_cases.
        """
        self._test_cases = [
            self.validate_datafile(),
            *self.validate_against_schema(),
        ]
        self._validation_run = True

    def validate_datafile(self) -> DataFileTestCase:
        """
        Check that the datafile is present and has a valid format

        Returns:
            DataFileTestCase: A DataFileTestCase object
        """
        if not self.datafile.exists:
            return DataFileTestCase(self.datafile.filepath, "missing")

        if self.datafile.format == "unknown":
            return DataFileTestCase(self.datafile.filepath, "invalid")

        return DataFileTestCase(self.datafile.filepath, "success")

    def validate_against_schema(self) -> list[BaseTestCase]:
        """
        Validate the datafile against its schema

        Returns:
            list[BaseTestCase]: A list of BaseTestCase objects
        """
        # If the datafile has no schema, return an empty list
        if self.datafile.schema is None:
            return []

        # Check that the schema is compatible with the datafile
        schema_compatible = self.validate_compatible_schema()

        # If the schema is not compatible, there's no point in checking the data
        if schema_compatible is not None:
            return [schema_compatible]

        # Check each column for quality errors
        quality_errors = self.column_quality_check()

        # Check the schema as a whole against the datafile
        schema_check = self.datafile.schema.validate_datafile(self.datafile)

        # Create a "good" test case if there are no errors
        schema_assigned_testcase = SchemaAssignedTestCase(
            schema_file=self.datafile.schema.filepath,
            data_file=self.datafile.filepath,
            case="success",
        )

        return [schema_assigned_testcase] + schema_check + quality_errors

    def validate_compatible_schema(self) -> SchemaAssignedTestCase:
        """
        Check that the schema is compatible with the datafile

        Returns:
            SchemaAssignedTestCase: A SchemaAssignedTestCase object
        """
        # Check if the schema contains the same field names as the datafile
        if not self.datafile.compatible_schema(self.datafile.schema):
            return SchemaAssignedTestCase(
                schema_file=self.datafile.schema.filepath,
                data_file=self.datafile.filepath,
                case="fail",
                msg=("Schema is not compatible with Datafile " "(check field names)"),
            )

        return None

    def column_quality_check(self) -> list[DataFileTestCase]:
        """
        Check the quality of each column in the datafile

        Returns:
            list[DataFileTestCase]: A list of DataFileTestCase objects
        """
        # Check if we cached the quality errors already
        if self._quality_errors is not None:
            return self._quality_errors

        self._quality_errors = []
        for column in self.datafile.columns:
            self._quality_errors.extend(column.quality_issues())
        if len(self._quality_errors) == 0:
            self._quality_errors = [DataQualityTestCase(self.datafile.filepath)]

        return self._quality_errors
