"""
Validates a schemas assignment file.
"""

from dataclasses import dataclass
import json
import logging

from ...source.reader.idata_reader import IDataReader
from ...source.reader.ijson_data_reader import IJSONDataReader

from .base_validator import BaseValidator

from ..errors import (
    SchemasAssignmentFileTestCase,
)

logger = logging.getLogger(__name__)


class IDataJsonReader(IDataReader, IJSONDataReader):
    """
    Interface object that combines the IDataReader and IJSONDataReader interfaces.
    """


@dataclass(kw_only=True)
class SchemasAssignmentValidator(BaseValidator):
    """
    Validates a schemas assignment file.
    """

    reader: IDataJsonReader

    def validate_self(self) -> None:
        """
        Validate this object.

        Check if the object is valid and accessible.
        Stores results in self.test_cases.
        """
        self._validation_run = True

        # Do we even have a reader?
        if self.reader is None:
            self._test_cases = [
                SchemasAssignmentFileTestCase(filename="", case="missing")
            ]
            return

        # Does this file exist in the reader?
        if not self.reader.exists:
            self._test_cases = [
                SchemasAssignmentFileTestCase(self.reader.filepath, "missing")
            ]
            return

        # Is this file valid? Does it have the right structure?
        try:
            schema_assignment_json = self.reader.read_dict()
            if not isinstance(schema_assignment_json, dict):
                self._test_cases = [
                    SchemasAssignmentFileTestCase(self.reader.filepath, "invalid")
                ]
                return
        except (FileNotFoundError, json.JSONDecodeError, UnicodeDecodeError) as err:
            logger.error("Error reading schemas assignment file: %s", err)

        self._test_cases = [
            SchemasAssignmentFileTestCase(self.reader.filepath, "validated")
        ]

        return
