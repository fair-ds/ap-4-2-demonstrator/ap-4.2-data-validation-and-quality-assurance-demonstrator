"""
Validator for schema files
"""

from dataclasses import dataclass
import json
import logging
from typing import TYPE_CHECKING

from .base_validator import BaseValidator

from ..errors import BaseTestCase, SchemaFileTestCase
from ...source import VirtualSource

if TYPE_CHECKING:
    from ..schema import Schema

logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class SchemaFileValidator(BaseValidator):
    """
    Class for validating schema files
    """

    schemafile: "Schema"

    def validate_self(self):
        """
        Validate this object.

        Check if the object is valid and accessible.
        Stores results in self.test_cases.
        """
        logger.info("Validating schema file with SchemaFileValidator")

        self._test_cases = self.validate_schemafile_basic()

        # If no test cases have been added so far, add a success test case
        if len(self._test_cases) == 0:
            self._test_cases.append(
                SchemaFileTestCase(self.schemafile.filepath, "success")
            )

        self._validation_run = True

    def validate_schemafile_basic(self) -> BaseTestCase:
        """
        Perform a quick validation to ensure that
        1. This file exists
        2. This file is proper JSON
        """
        logger.info("Validating schema file %s", self.schemafile.filepath)

        # Check if the schemafile is present
        if not self.schemafile.exists:
            if isinstance(self.schemafile.source, VirtualSource):
                logging.warning(
                    "Schema file %s was generated", self.schemafile.filepath
                )
                return [SchemaFileTestCase(self.schemafile.filepath, "generated")]
            else:
                logging.error("Schema file %s is missing", self.schemafile.filepath)
                return [SchemaFileTestCase(self.schemafile.filepath, "missing")]

        # Check if the schemafile is valid JSON
        try:
            json.loads(self.schemafile.source.get_contents(self.schemafile.filepath))
        except json.JSONDecodeError as err:
            return [
                SchemaFileTestCase(
                    self.schemafile.filepath, "invalid", f"{type(err)} - {err}"
                )
            ]

        # Check that the schema file is in the correct format
        if "fields" not in self.schemafile.raw_file:
            return [
                SchemaFileTestCase(
                    self.schemafile.filepath,
                    "invalid",
                    "Validation Error: Schema file does not contain a 'fields' key",
                )
            ]

        # If it is present and JSON, see if it is valid according to frictionless
        return self.frictionless_schema_validation()

    def frictionless_schema_validation(self) -> list[BaseTestCase]:
        """
        Create testcases from any validation errors found by frictionless
        """
        return [
            SchemaFileTestCase(
                self.schemafile.filepath,
                "invalid",
                f"Validation Error: {name}: {msg}",
            )
            for _, name, msg in self.schemafile.validation_report
        ]
