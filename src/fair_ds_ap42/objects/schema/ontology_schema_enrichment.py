"""
Class that is use to enrich the ontology schema with the information from the
TiB Ontology service
"""

import datetime
import logging
import urllib.parse

import requests

logger = logging.getLogger(__name__)


class OntologySchemaEnrichment:
    """
    Using the TiB Ontology service, enrich the ontology schema with the
    information from the ontology. Requires the ontology to be in the format
    "ontology::term" (e.g. "test_ontology::test_ontology_class") in the schema file
    """

    API_URL = "https://service.tib.eu/ts4tib/api/ontologies"
    DATE_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"
    _cache = {}  # Class level cache for storing the results of the API calls

    @classmethod
    def get_ontology_data(cls, ontology: str) -> dict:
        """
        Get the ontology data from the TiB Ontology service

        Args:
            ontology (str): The ontology to get the data for

        Returns:
            dict: The ontology data

        Raises:
            HTTPError: If the request to the TiB Ontology service fails
        """
        # Check the cache to see if we've already retrieved the ontology data
        if ontology in cls._cache:
            return cls._cache[ontology]

        # Make the request to the TiB Ontology service
        url = f"{cls.API_URL}/{ontology}"
        try:
            response = requests.get(url, timeout=30)
            response.raise_for_status()
        except requests.exceptions.RequestException as err:
            logger.error("Request to TiB Ontology service failed: %s", err)
            raise requests.exceptions.HTTPError(
                f"Request to TiB Ontology service failed: {err}"
            )

        result_json = response.json()

        return_value = {
            "ontologyId": result_json["ontologyId"],
            "loaded": datetime.datetime.strptime(
                result_json["loaded"], cls.DATE_FORMAT
            ),
            "updated": datetime.datetime.strptime(
                result_json["updated"], cls.DATE_FORMAT
            ),
            "title": result_json["config"]["title"],
            "description": result_json["config"]["description"],
            "namespace": result_json["config"]["namespace"],
            "preferredPrefix": result_json["config"]["preferredPrefix"],
            "config_id": result_json["config"]["id"],
            "homepage": result_json["config"]["homepage"],
            "version": result_json["config"]["version"],
            "license": result_json["config"]["license"]["url"],
        }

        # Add the retrieved data to the cache
        cls._cache[ontology] = return_value

        # Log the successful retrieval of ontology term instances
        logger.info("Ontology data retrieved for ontology: %s", ontology)

        return return_value

    @classmethod
    def get_ontology_term_instances(cls, ontology: str, term: str) -> dict:
        """
        Get the ontology term instances from the TiB Ontology service

        Args:
            ontology (str): The ontology to get the term instances for
            term (str): The term to get the instances for

        Returns:
            dict: The ontology term instances

        Raises:
            HTTPError: If the request to the TiB Ontology service fails
        """
        # Check the cache to see if we've already retrieved the term instances
        if (ontology, term) in cls._cache:
            return cls._cache[(ontology, term)]

        # Get the ontology data
        ontology_data = cls.get_ontology_data(ontology=ontology)

        # URL encode the term for the request
        encoded_id_level1 = urllib.parse.quote(
            f"{ontology_data['config_id']}{term}", safe=""
        )
        encoded_id_level2 = urllib.parse.quote(encoded_id_level1, safe="")

        # Make the request to the TiB Ontology service
        url = f"{cls.API_URL}/{ontology}/{encoded_id_level2}/terminstances"
        try:
            response = requests.get(url, timeout=30)
            response.raise_for_status()
        except requests.exceptions.RequestException as err:
            logger.error("Request to TiB Ontology service failed: %s", err)
            raise requests.exceptions.HTTPError(
                f"Request to TiB Ontology service failed: {err}"
            )

        result_json = response.json()

        # Extract relevant information from the response
        return_value = [
            {
                "iri": individual["iri"],
                "label": individual["label"],
                "description": individual["description"][0],
                "type_description": individual["type"][0]["description"][0],
            }
            for individual in result_json["_embedded"]["individuals"]
        ]

        # Add the retrieved data to the cache
        cls._cache[(ontology, term)] = return_value

        # Log the successful retrieval of ontology term instances
        logger.info(
            "Ontology term instances retrieved for ontology: %s, term: %s",
            ontology,
            term,
        )

        return return_value

    @classmethod
    def enrich_schema_field(cls, field: dict) -> dict:
        """
        Given a field from the schema, enrich it with the information from the TiB
        Ontology service

        Args:
            field (dict): The field to enrich

        Returns:
            dict: The field, enriched with the information from the TiB Ontology
        """
        # If the key "ontology" is not in the field, return the field as is
        if "ontology" not in field:
            return field

        ontology, term = field["ontology"].split("::")

        # Get ontology and term data
        ontology_data = OntologySchemaEnrichment.get_ontology_data(ontology=ontology)
        term_data = OntologySchemaEnrichment.get_ontology_term_instances(
            ontology=ontology, term=term
        )

        # Update field constraints with enum from term_data
        field_constraints = field.get("constraints", {})
        field_constraints["enum"] = [individual["label"] for individual in term_data]

        # Create and return the enriched schema field
        return {
            **field,
            "title": term,
            "description": term_data[0][
                "type_description"
            ],  # Should this take into the description of all terms?
            "ontology_name": ontology_data["title"],
            "date_issued": ontology_data["loaded"],
            "date_modified": ontology_data["updated"],
            "constraints": field_constraints,
        }

    def get_enriched_schema(self, raw_schema: dict) -> dict:
        """
        Enrich the schema with the information from the TiB Ontology service

        Args:
            raw_schema (dict): The raw schema to enrich

        Returns:
            dict: The enriched schema
        """
        # Extract the field data from the schema
        field_data = raw_schema.get("fields", {})

        # Enrich the field data
        enriched_fields = [
            OntologySchemaEnrichment.enrich_schema_field(field=field)
            for field in field_data
        ]

        return {
            **raw_schema,
            "fields": enriched_fields,
        }
