"""
schema.py

Class for locating and checking schema files as they relate to Resources
"""

import json
import logging
from pathlib import Path
from typing import TYPE_CHECKING

import frictionless

from ..validator.schemafile_validator import SchemaFileValidator
from ..errors import SchemaFileTestCase, SchemaViolationTestCase
from ..resource import Resource
from ...source import get_source, VirtualSource
from ...settings import settings
from ...utils import s3_utils

from .ontology_schema_enrichment import OntologySchemaEnrichment

if TYPE_CHECKING:
    from ..datafile import DataFile

logger = logging.getLogger(__name__)


def get_generic_schema_filename(directory: Path | str) -> Path:
    """
    Create a generic schema filename

    Just to avoid the situation that appears in tests and potentially in real
    usage where someone has named files "schema_1" etc, just use the next
    available filename

    Args:
        directory (Path | str):
            The directory the schemas are held in.

    Returns:
        Path
    """

    existing_files = list(Path(directory).glob("schema_*.json"))
    return Path(directory) / Path(f"schema_{len(existing_files)+1}.json")


class Schema(Resource):
    """
    Object that represents a Schema in JSON format.
    """

    # Static Methods =========================================================
    @classmethod
    def from_raw_schema(cls, raw_schema: dict) -> "Schema":
        """
        Initialize a Schema object with a dictionary.

        Will set the source to a Virtual Source.

        Args:
            d (dict):
                A dictionary of data in proper frictionless format. See
                https://specs.frictionlessdata.io//table-schema/ for details.

        Returns:
            Schema
        """
        return cls(filepath=None, raw_schema=raw_schema, source=VirtualSource())

    @classmethod
    def from_file(cls, filepath: Path | str) -> "Schema":
        """
        Initialize a Schema object from a file.

        Args:
            filepath (Union[str, Path]):
                A file containing valid json in proper frictionless format. See
                https://specs.frictionlessdata.io//table-schema/ for details.

        Returns:
            Schema
        """
        return cls(filepath=filepath, source=get_source(filepath))

    @classmethod
    def from_resource(cls, obj: Resource) -> "Schema":
        """
        Initialize a Schema object from a Resource.

        Args:
            obj (Resource):
                A Resource object.

        Returns:
            Schema
        """
        return Schema(
            filepath=obj.filepath,
            source=obj.source,
            size_in_bytes=obj.size_in_bytes,
            last_modified=obj.last_modified,
        )

    @classmethod
    def infer(cls, datafile: "DataFile") -> "Schema":
        """
        Infer a schema from a given DataFile.

        Args:
            datafile (DataFile):
                A Datafile object. The underlying file must exist.

        Returns:
            Schema
        """
        if datafile.source.name in ["local", "url"]:
            inferred_schema = frictionless.describe(datafile.filepath)
        elif datafile.source.name == "s3":
            df = datafile.get_dataframe()
            inferred_schema = frictionless.describe(df)
        else:
            raise NotImplementedError(
                f"schema inference not implemented for datafile source '{datafile.source}'"
            )

        inferred_raw_schema = inferred_schema.schema.to_dict()
        logger.debug("inferred schema: %s", inferred_raw_schema)

        return cls.from_raw_schema(raw_schema=inferred_raw_schema)

    # Magic Methods ==========================================================
    def __eq__(self, other):
        if not isinstance(other, Schema):
            return False
        return hash(self) == hash(other)

    def __hash__(self):
        return hash(self.__key())

    # Implement a key for the schema
    def __key(self):
        return (
            self.filepath,
            self.source.name,
            json.dumps(self.raw_file, sort_keys=True),
        )

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f"Schema: {self.filepath} ({self.source})"

    # Constructor ============================================================
    def __init__(
        self,
        raw_schema: dict = None,
        **kwargs,
    ):
        """
        Constructor

        Args:
            filepath (Union[str, Path], optional):
                The filepath of the file underlying this object.
            source:
                The Source of this file (e.g. local, S3)
            raw_schema (dict):
                If no file exists for this object, we can create it by
                directly providing a dictionary.
        """
        super().__init__(**kwargs)

        self._test_cases = None
        self.validator = SchemaFileValidator(schemafile=self)

        if raw_schema:
            if not isinstance(raw_schema, dict):
                raise ValueError(f"unexpected type for raw_schema: {type(raw_schema)}")
            self.raw_file = raw_schema
        else:
            if self.reader is not None and self.reader.exists():
                self.raw_file = self.reader.read_dict()
            else:
                self.raw_file = {}

        if self.raw_file:
            try:
                self._frictionless_schema = frictionless.Schema(self.raw_file)
            except frictionless.exception.FrictionlessException as err:
                logger.debug("schema validation error: %s", err)
                self._frictionless_schema = frictionless.Schema()

    # Properties =============================================================
    @property
    def field_names(self) -> list:
        """A list of field names in this schema."""
        return [k.name for k in self.fields]

    @property
    def fields(self) -> dict:
        """The frictionless fields value."""
        return self.frictionless_schema.fields

    @property
    def frictionless_schema(self) -> frictionless.Schema:
        """Returns a frictionless Schema Object."""
        if self.raw_file != {} and self._frictionless_schema.to_dict() == {}:
            self._frictionless_schema = frictionless.Schema(self.raw_file)

        if self.raw_file:
            return self._frictionless_schema

        raise FileNotFoundError(self.filepath)

    @property
    def is_valid(self) -> bool:
        """Returns True if this Schema passes frictionless validation."""
        # If the schema is just empty, we can't validate it
        if not self.raw_file or not self.raw_file.get("fields"):
            return False

        try:
            return len(self.validation_report) == 0
        except frictionless.exception.FrictionlessException as err:
            logger.debug("schema validation error: %s", err)
            return False

    @property
    def required_fields(self) -> list[str]:
        """Returns a list of fields with the 'required' constraint"""
        req_fields = filter(
            lambda field: "required" in field.constraints,
            self.frictionless_schema.fields,
        )
        return [field.name for field in req_fields]

    @property
    def test_cases(self) -> list[SchemaFileTestCase]:
        """
        Get a list of test cases indicating potential issues with this object.
        """
        if not self.validator.validation_run:
            self.validator.validate_self()
        return self.validator.test_cases

    @property
    def validation_report(self) -> list:
        """Get a list of potential validation errors for this schema."""
        report = frictionless.validate(self.get_schema_data())
        report = report.flatten(["type", "name", "message"])
        return report

    # Methods =================================================================
    def get_schema_data(self):
        """Get the schema data in the format expected by frictionless."""
        schema_enricher = OntologySchemaEnrichment()
        return schema_enricher.get_enriched_schema(raw_schema=self.raw_file)

    def as_dict(self, root_url: str = "", validation_check=True) -> dict:
        """Return a dictionary representation of this object."""

        validation_status = {}
        testsuite_validation_status = {}
        test_cases = []
        if validation_check:
            self.validator.validate_self()
            validation_status = self.validator.validation_status
            testsuite_validation_status = self.validator.get_testsuites_status()
            test_cases = [case.as_dict() for case in self.test_cases]

        return {
            "filepath": self.filepath,
            "htmlpath": self.get_page_path(root_url),
            "source": self.source.name,
            "file_metadata": self.get_file_metadata(),
            "validation_status": validation_status,
            "testsuite_validation_status": testsuite_validation_status,
            "test_cases": test_cases,
            "raw_data": json.dumps(self.raw_file, indent=4),
        }

    def get_page_path(self, root_url: str) -> Path:
        """Get the path to the HTML page for this object."""
        url = root_url + "/" + Path(self.filepath).with_suffix(".html").as_posix()
        logger.debug("schema page url: %s", url)
        return url

    def to_file(self, filepath: Path | str):
        """
        Save this object to a JSON file.

        If S3 keys are provided, will check if the schema file directory
        exists on the S3 Bucket, and if so will save it there. Otherwise will
        save it locally.

        Args:
            filepath (Union[str, Path]):
                The filepath to save this file to.

        Returns:
            None

        """
        if settings.storage.s3.credentials_provided:
            if s3_utils.directory_exists_s3(settings.directories.schema_directory):
                s3_utils.write_json_to_s3(self.raw_file, filepath)
                return

        # Make sure the directory exists, since we often default to a
        # directory that may not be present
        Path(filepath).parent.mkdir(parents=True, exist_ok=True)
        with open(filepath, "w", encoding="utf-8") as file_obj:
            json.dump(self.raw_file, file_obj)

    def validate_datafile(
        self, datafile: Resource, limit_errors=100
    ) -> list[SchemaViolationTestCase]:
        """
        Validate a given Data File against this schema.

        Args:
            data_file (Union[str, Path, Resource]):
                A Resource to validate this schema against

        Returns:
            list[SchemaViolationTestCase]
        """
        report = frictionless.validate(
            datafile.get_dataframe(),
            schema=self.frictionless_schema,
            limit_errors=limit_errors,
        )

        test_cases = []
        for error in report.task.errors:
            test_cases.append(
                SchemaViolationTestCase(
                    case="violation",
                    schema_file=self.filepath,
                    data_file=datafile.filepath,
                    frictionless_error=error,
                )
            )

        if len(test_cases) == 0:
            test_cases.append(
                SchemaViolationTestCase(
                    case="no_error",
                    schema_file=self.filepath,
                    data_file=datafile.filepath,
                )
            )

        return test_cases
