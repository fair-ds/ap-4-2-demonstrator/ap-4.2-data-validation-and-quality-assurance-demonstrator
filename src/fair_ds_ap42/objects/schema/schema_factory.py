"""
This module contains the SchemaFactory class, which is a factory class for creating Schema objects
from various sources.

Attributes:
    None

Methods:
    from_resource: Creates a Schema object from a Resource object.
    from_filepath: Creates a Schema object from a file path.
    from_dict: Creates a Schema object from a dictionary.
    from_schema_json: Creates a Schema object from a JSON schema.
"""
# pylint: disable=R0801

import logging

from ..resource.resource_factory import ResourceFactory

from ...source import ISource, VirtualSource
from ..resource.resource import Resource
from .schema import Schema

logger = logging.getLogger(__name__)


class SchemaFactory(ResourceFactory):
    """A factory class for creating Schema objects from various sources.

    Attributes:
        None

    Methods:
        from_resource: Creates a Schema object from a Resource object.
        from_filepath: Creates a Schema object from a file path.
        from_dict: Creates a Schema object from a dictionary.
        from_schema_json: Creates a Schema object from a JSON schema.
    """

    @classmethod
    def from_resource(cls, resource: Resource) -> Schema:
        """Creates a Schema object from a Resource object.

        Args:
            resource: A Resource object.

        Returns:
            A Schema object.
        """
        return Schema(
            filepath=resource.filepath,
            source=resource.source,
            size_in_bytes=resource.size_in_bytes,
            last_modified=resource.last_modified,
        )

    @classmethod
    def from_filepath(cls, filepath: str, source: ISource = None) -> Schema:
        """Creates a Schema object from a file path.

        Args:
            filepath: A string representing the file path.
            source: An optional ISource object.

        Returns:
            A Schema object.
        """
        my_source = cls.find_source(filepath, source)
        my_schema = Schema(filepath=filepath, source=my_source)
        logger.info("Created Schema Resource %s", my_schema)
        return my_schema

    @classmethod
    def from_dict(cls, resource_dict: dict) -> Schema:
        """Creates a Schema object from a dictionary.

        Args:
            resource_dict: A dictionary representing the schema.

        Returns:
            A Schema object.
        """
        resource = super().from_dict(resource_dict)
        my_schema = cls.from_resource(resource)
        logger.info("Created Schema Resource %s", my_schema)
        return my_schema

    @classmethod
    def from_schema_json(cls, schema_json: dict) -> Schema:
        """Creates a Schema object from a JSON schema.

        Args:
            schema_json: A dictionary representing the JSON schema.

        Returns:
            A Schema object.
        """
        return Schema(raw_schema=schema_json, source=VirtualSource())
