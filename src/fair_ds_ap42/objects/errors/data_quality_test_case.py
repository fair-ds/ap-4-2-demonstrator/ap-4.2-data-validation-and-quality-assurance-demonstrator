"""
Defines the DataQualityTestCase class. This class is used to indicate when a
data quality error was found.

The specific text of a data quality error is defined in the report returned
by frictionless.
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class DataQualityTestCase(BaseTestCase):
    """
    Test case informing the user that the a data quality error was found.
    (WARNING)
    """

    def __init__(
        self,
        data_file: str,
        violation_type: str = None,
        column_name: str = None,
        msg: str = None,
    ):
        data_file = Path(data_file).as_posix()
        if not violation_type:
            super().__init__(
                testcase_id=f"data_quality_passed_{data_file}",
                name=f"{data_file}: No Quality Issues Noted",
                fail_type="SUCCESS",
                msg=f"{data_file}: No Quality Issues Noted",
                testsuite_id="data_quality",
            )
        else:
            my_id = f"data_quality_{violation_type}"
            my_name = f"Data Quality Issue ({violation_type}): "
            if column_name:
                my_id += f"_{column_name}"
                my_name += f"{column_name} in"

            super().__init__(
                testcase_id=f"{my_id}_{data_file}",
                name=(f"{my_name} {data_file}"),
                fail_type="WARNING",
                msg=msg,
                testsuite_id="data_quality",
            )
