"""
Defines the SchemaAssignedTestCase class, which is used to inform the user
that a schema was found that will be applied to a data file.
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class SchemaAssignedTestCase(BaseTestCase):
    """
    Test case informing the user:
        - A schema was found that will be applied to a data file (SUCCESS)
        - A schema was auto assigned, based on broad compatibility to a data
            file (WARNING)
        - A schema was assigned, but did not broadly fit it's assigned data
            file (ERROR)
    """

    def __init__(self, schema_file: str, data_file: str, case: str, msg: str = None):
        schema_file = (
            "(virtual)" if schema_file is None else Path(schema_file).as_posix()
        )
        data_file = Path(data_file).as_posix()

        if case == "success":
            super().__init__(
                testcase_id=f"schema_file_assigned_{schema_file}_{data_file}",
                name=f"Schema File {schema_file} assigned",
                fail_type="SUCCESS",
                msg=f'Schema File "{schema_file}" assigned to "{data_file}"',
                testsuite_id="file_validation",
            )
        elif case == "auto":
            super().__init__(
                testcase_id=f"schema_file_auto_assigned_{schema_file}_{data_file}",
                name=f"Schema File {schema_file} automatically assigned",
                fail_type="WARNING",
                msg=(
                    f'Schema File "{schema_file}" automatically assigned to '
                    f'"{data_file}"'
                ),
                testsuite_id="file_validation",
            )
        elif case == "fail":
            super().__init__(
                testcase_id=f"schema_file_fail_assigned_{schema_file}_{data_file}",
                name=f"Schema File {schema_file} failed to assign",
                fail_type="ERROR",
                msg=msg,
                testsuite_id="file_validation",
            )
        else:
            raise ValueError(
                (
                    'parameter "case" must be one of "success", "auto", '
                    'or "fail" (recieved "{case}"'
                )
            )
