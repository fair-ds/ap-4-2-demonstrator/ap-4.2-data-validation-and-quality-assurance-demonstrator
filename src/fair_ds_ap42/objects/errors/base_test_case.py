"""
Defines the BaseTestCase object. This object is used to define the base
functionality for all other test cases.
"""

import re

from dataclasses import dataclass

from ...junit_xml_gen import JUnitTestSuites, JUnitTestSuite, JUnitTestCase


# apply standard formatting to strings to build the user facing names
def str_fmt(my_str: str) -> str:
    """
    Apply standard formatting to strings to build user facing names.

    Replaces all underscors with spaces and converts the strint to title case.

    Args:
        my_str (str):
            The string to format

    Returns:
        str
    """
    return my_str.replace("_", " ").title()


@dataclass(kw_only=True)
class BaseTestCase:
    """
    Object that forms the base for all other errors.
    """

    testcase_id: str
    name: str
    fail_type: str
    msg: str
    testsuite_id: str

    def __hash__(self) -> int:
        return hash(self.__key())

    def __key(self) -> tuple:
        return (
            self.testcase_id,
            self.name,
            self.fail_type,
            self.msg,
            self.testsuite_id,
        )

    @property
    def testsuite_name(self) -> str:
        """
        Get the human readable format of the testsuite id
        """
        return str_fmt(self.testsuite_id)

    def add_to_xml(self, xml: JUnitTestSuites):
        """
        Given a JUnitTestSuites object, add this object to it.

        If the JUnitTestSuites is missing the TestSuite associated with this
        testcase, create that TestSuite and add it to the JUnitTestSuites
        object as well.

        Args:
            xml (JUnitTestSuites):
                The object to add this testcase to
        """
        my_ts = JUnitTestSuite(name=self.testsuite_id, obj_id=self.testsuite_name)

        my_tc = JUnitTestCase(
            obj_id=self.testcase_id,
            name=self.name,
            fail_type=self.fail_type,
            fail_msg=self.msg,
            fail_text=self.msg,
        )

        my_ts.add_testcase(my_tc)
        xml.add_testsuite(my_ts)

    def as_dict(self):
        """Return the object as a dictionary"""
        return {
            "type": re.findall(r"([A-z]+)", str(type(self)))[-1],
            "id": self.testcase_id,
            "name": self.name,
            "fail_type": self.fail_type,
            "fail_msg": self.msg,
            "testsuite": self.testsuite_name,
        }

    def __eq__(self, other):
        if not isinstance(other, BaseTestCase):
            return False
        return (
            (self.testcase_id == other.testcase_id)
            and (self.name == other.name)
            and (self.fail_type == other.fail_type)
            and (self.msg == other.msg)
            and (self.testsuite_id == other.testsuite_id)
        )
