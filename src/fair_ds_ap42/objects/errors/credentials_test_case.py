"""
Defines the CredentialsTestCase class, which is used to inform the user either:
    - Their AWS/Coscine credentials are valid (SUCCESS)
    - There was an issue with their credentials (ERROR, Fatal)
"""

from .base_test_case import BaseTestCase


class CredentialsTestCase(BaseTestCase):
    """
    Test case informing the user either:
        - Their AWS/Coscine credentials are valid (SUCCESS)
        - There was an issue with their credentials (ERROR, Fatal)
    """

    def __init__(self, is_valid: bool):
        if is_valid:
            super().__init__(
                testcase_id="valid_credentials",
                name="Valid Credentials",
                fail_type="SUCCESS",
                msg="Sucessfully Validated Credentials",
                testsuite_id="credentials",
            )
        else:
            super().__init__(
                testcase_id="invalid_credentials",
                name="Invalid Credentials",
                fail_type="ERROR",
                msg="Credentials are Invalid",
                testsuite_id="credentials",
            )
