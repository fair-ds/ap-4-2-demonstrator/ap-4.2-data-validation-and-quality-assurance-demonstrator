"""
Defines the SchemaAssignmentFileTestCase class.

This class is used to inform the user of the status of a specific schema
assignment file.
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class SchemasAssignmentFileTestCase(BaseTestCase):
    """
    Test case informing the user either:
        - a schema assignment file was found (SUCCESS)
        - a schema assignment was missing (WARNING)
        - a schema assignment was invalid (ERROR)
        - the schema assignment file was valid (SUCCESS)
    """

    def __init__(self, filename: str, case: str, msg: str = None):
        filename = Path(filename).as_posix()

        if case == "success":
            super().__init__(
                testcase_id="schema_assignment_file_found",
                name="Located Schema Assignment File",
                fail_type="SUCCESS",
                msg=f"Located Schema Assignment File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "missing":
            super().__init__(
                testcase_id="schema_assignment_file_missing",
                name="No Schema Assignment File Present",
                fail_type="WARNING",
                msg="No Schema Assignment File Present",
                testsuite_id="locate_data",
            )
        elif case == "invalid":
            super().__init__(
                testcase_id="schema_assignment_file_invalid",
                name="Schema Assignment Invalid",
                fail_type="ERROR",
                msg=f"{filename}: {msg}",
                testsuite_id="locate_data",
            )
        elif case == "validated":
            super().__init__(
                testcase_id="schema_assignment_file_validated",
                name="Schema Assignment Validated",
                fail_type="SUCCESS",
                msg="No issues noted with Schema Assignment File",
                testsuite_id="locate_data",
            )
        else:
            raise ValueError
