"""
Defines the SchemaViolationTestCase class. This class is used to indicate when a
schema was applied to a data file and a validation error was found.
"""

from pathlib import Path

from frictionless.errors.data import DataError

from .base_test_case import BaseTestCase


class SchemaViolationTestCase(BaseTestCase):
    """
    Test case informing the user if there was a validation error found
    when this schema was applied to a data file.
        - case "no_error" (SUCCESS)
        - case "violation" (WARNING)
    """

    def __init__(
        self,
        case: str,
        schema_file: str,
        data_file: str,
        frictionless_error: DataError = None,
    ):
        schema_file = (
            "(virtual)" if schema_file is None else Path(schema_file).as_posix()
        )
        data_file = Path(data_file).as_posix()

        if case == "no_error":
            super().__init__(
                testcase_id=(f"schema_applied_no_error_{schema_file}_{data_file}"),
                name=(f"Schema {schema_file} applied to {data_file} without error"),
                fail_type="SUCCESS",
                msg=(f"Schema {schema_file} applied to {data_file} without error"),
                testsuite_id="data_validation",
            )
        elif case == "violation":
            error_dict = frictionless_error.to_dict()
            violation_type = error_dict["type"]
            column_name = error_dict["fieldName"]
            line_number = error_dict.get("rowPosition")
            msg = error_dict["message"]

            if line_number is not None:
                name = (
                    f"Schema Violation ({violation_type}): {column_name} "
                    f"@ line {line_number} in {data_file}"
                )
            else:
                name = (
                    f"Schema Violation ({violation_type}): {column_name} "
                    f"in {data_file}"
                )

            super().__init__(
                testcase_id=(
                    f"schema_violation_{violation_type}_{column_name}_"
                    f"{schema_file}_{data_file}"
                ),
                name=name,
                fail_type="ERROR",
                msg=msg,
                testsuite_id="data_validation",
            )
