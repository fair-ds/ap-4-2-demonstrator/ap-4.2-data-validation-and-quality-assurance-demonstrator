"""
Defines the DataFileTestCase class, which is used to inform the user of the
status of a specific data file.
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class DataFileTestCase(BaseTestCase):
    """
    Test case informing the user either:
        - "success": a specific data file was found (SUCCESS)
        - "updated": an updated version of a data file was found (SUCCESS)
        - "no_action": no action will be made on this file (SUCCESS)
        - "no_schema": a specific data file has no schema (WARNING)
        - "invalid": a specific data file has a invalid extension (WARNING)
        - "missing": a specific data file was missing (ERROR)
    """

    def __init__(self, filename: str, case: str, msg: str = None):
        filename = Path(filename).as_posix()

        if case == "success":
            super().__init__(
                testcase_id=f"data_file_located_{filename}",
                name=f"Located Data File: {filename}",
                fail_type="SUCCESS",
                msg=f"Sucessfully Located Data File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "updated":
            super().__init__(
                testcase_id=f"updated_data_file_located_{filename}",
                name=f"Located Updated Data File: {filename}",
                fail_type="SUCCESS",
                msg=f"Located Updated Data File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "no_action":
            super().__init__(
                testcase_id=f"data_file_no_action_{filename}",
                name=f"No Action Taken: {filename}",
                fail_type="SUCCESS",
                msg=f"{filename} has not changed. No actions taken.",
                testsuite_id="locate_data",
            )
        elif case == "no_schema":
            super().__init__(
                testcase_id=f"data_file_no_schema_{filename}",
                name=f"Invalid Data File Type: {filename}",
                fail_type="WARNING",
                msg=f"{filename} is not a valid file type for a data file.",
                testsuite_id="locate_data",
            )
        elif case == "invalid":
            super().__init__(
                testcase_id=f"data_file_invalid_{filename}",
                name=f"Invalid Data File Type: {filename}",
                fail_type="WARNING",
                msg=f"{filename} is not a valid data file type",
                testsuite_id="locate_data",
            )
        elif case == "missing":
            super().__init__(
                testcase_id=f"data_file_missing_{filename}",
                name=f"Failed to Locate Data File: {filename}",
                fail_type="ERROR",
                msg=f"Unable to locate data file {filename}",
                testsuite_id="locate_data",
            )
        else:
            raise ValueError(
                (
                    'parameter "case" must be one of "success", "no_schema",'
                    ' or "missing"'
                )
            )
