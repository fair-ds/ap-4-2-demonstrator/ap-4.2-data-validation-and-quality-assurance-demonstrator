"""
Defines the ScriptError class, which is used to indicate that the script
cannot be run due to an error.

Possible Errors:
    - "no_files": No data files could be located
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class ScriptError(BaseTestCase):
    """
    Test case for situations which affect the operation of the script.
        - "no_files": No data files could be located
    """

    def __init__(self, case: str, data_folder: Path):
        if case == "no_files":
            super().__init__(
                testcase_id="no_valid_datafiles_present",
                name=f"No Valid Datafiles Present in {data_folder.as_posix()}",
                fail_type="ERROR",
                msg=f"No Valid Datafiles Present in {data_folder.as_posix()}",
                testsuite_id="locate_data",
            )
        else:
            raise ValueError
