"""
Defines the SchemaFileTestCase class. This class is used to indicate that a
specific schema file was found, generated, missing, or invalid.
"""

from pathlib import Path

from .base_test_case import BaseTestCase


class SchemaFileTestCase(BaseTestCase):
    """
    Test case informing the user either:
        - a specific schema file was found (SUCCESS)
        - a schema file was generated (WARNING)
        - a specific schema file was missing (ERROR)
        - a schema file was invalid (ERROR)
    """

    def __init__(self, filename: str, case: str, msg: str = None):
        filename = Path(filename).as_posix()

        if case == "success":
            super().__init__(
                testcase_id=f"schema_file_located_{filename}",
                name=f"Located Schema File: {filename}",
                fail_type="SUCCESS",
                msg=f"Located Schema File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "generated":
            super().__init__(
                testcase_id=f"schema_file_generated_{filename}",
                name=f"Generated Schema File: {filename}",
                fail_type="WARNING",
                msg=f"Generated Schema File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "missing":
            super().__init__(
                testcase_id=f"schema_file_missing_{filename}",
                name=f"Missing Schema File: {filename}",
                fail_type="ERROR",
                msg=f"Missing Schema File: {filename}",
                testsuite_id="locate_data",
            )
        elif case == "invalid":
            super().__init__(
                testcase_id=f"schema_file_invalid_{filename}",
                name=f"Invalid Schema File: {filename}",
                fail_type="ERROR",
                msg=msg,
                testsuite_id="locate_data",
            )
        else:
            raise ValueError(
                (
                    'parameter "case" must be one of "found", "generated", '
                    '"missing", or "invalid"'
                )
            )
