"""
Defines a generic resource that we want to process in some way.

This could be a local file or a remote file.
"""

from dataclasses import dataclass
import datetime
import logging
from pathlib import Path
import random
import string

from .iresource import IResource
from ...source import ISource, VirtualSource
from ...source.source_factory import SourceFactory

logger = logging.getLogger(__name__)


def generate_random_filename(length=8, extension="file"):
    """
    Generate a random filename.

    Used when we have a VirtualSource.

    Args:
        length (int):
            The length of the random string to generate.
        extension (str):
            The extension to use for the filename.

    Returns:
        str:
            The random filename.
    """
    logger.debug("Generating a random file name for a VirtualSource")
    random_chars = "".join(random.choice(string.ascii_letters) for _ in range(length))
    filename = f"{random_chars}.{extension}"
    return filename


@dataclass(kw_only=True)
class Resource(IResource):
    """
    Represents a generic resource that we want to process in some way-
    """

    filepath: str
    source: ISource
    size_in_bytes: int = None
    last_modified: datetime.datetime = None

    @classmethod
    def from_dict(cls, data: dict) -> "Resource":
        """
        Class method to create a Resource from a dictionary.

        The dictionary should have the following structure:
        {
            "filepath": "file1.csv",
            "source": {
                "type": "local_directory",
                "location": "./my_dir",
            },
            "size_in_bytes": 100,
            "last_modified": "2020-01-01T00:00:00Z",
        }

        Args:
            data (dict):
                The dictionary to create the Resource from.

        Returns:
            Resource:
                The Resource created from the dictionary.
        """
        source = SourceFactory.from_dict(**data["source"])
        date = datetime.datetime.strptime(data["last_modified"], "%Y-%m-%dT%H:%M:%SZ")

        return cls(
            filepath=data["filepath"],
            source=source,
            size_in_bytes=data["size_in_bytes"],
            last_modified=date,
        )

    @classmethod
    def contains_modified_resource(
        cls, resource: "Resource", resources: list["Resource"]
    ) -> bool:
        """
        Class method to check if the provided list of Resources contains a modified version of the
        provided Resource.

        Args:
            resource (Resource):
                The Resource to check for.
            resources (list[Resource]):
                The list of Resources to check in.

        Returns:
            bool:
                True if the list contains a modified version of the provided Resource, False
                otherwise.
        """
        for entry in resources:
            if resource.is_similar_to(entry):
                return True
        return False

    def __post_init__(self):
        # Ensure filepath is a string, not a Path object
        if isinstance(self.filepath, Path):
            self.filepath = self.filepath.as_posix()

        if isinstance(self.source, VirtualSource):
            self.filepath = generate_random_filename()

        self.reader = self.source.get_reader(self.filepath)

        if self.size_in_bytes is None:
            self.size_in_bytes = self.reader.get_size_in_bytes()

        if self.last_modified is None:
            self.last_modified = self.reader.get_date_last_modified()

    def __hash__(self) -> int:
        # Create a hash from the the object attributes.

        # Convert the source dictionary to a hashable tuple
        source = hash(sum(hash(str(value)) for value in self.source.to_json().values()))

        # Create a hash from the filepath, source, size_in_bytes, and last_modified
        return hash((self.filepath, source, self.size_in_bytes, self.last_modified))

    @property
    def exists(self) -> bool:
        if self.reader is None:
            return False
        return self.reader.exists()

    def is_similar_to(self, other: IResource) -> bool:
        """
        Check if this resource is similar to the provided resource.

        Args:
            other (IResource):
                The resource to compare against

        Returns:
            bool
        """
        if not isinstance(other, Resource):
            return False
        return (
            self.filepath == other.filepath
            and self.source == other.source
            and (
                self.size_in_bytes != other.size_in_bytes
                or self.last_modified != other.last_modified
            )
        )

    def as_dict(self) -> dict:
        """
        Get a dictionary representation of the Resource.

        Returns:
            dict:
                A dictionary representation of the Resource.
        """
        return {
            "filepath": self.filepath,
            "source": self.source.to_json(),
            "size_in_bytes": self.size_in_bytes,
            "last_modified": self.last_modified.strftime("%Y-%m-%dT%H:%M:%S"),
        }

    def get_file_metadata(self) -> dict:
        """
        Get the metadata for the file.

        Returns:
            dict:
                The metadata for the file.
        """
        return {
            "last_modified": self.last_modified.isoformat(),
            "last_modified_formatted": self.last_modified.strftime("%B %d, %Y @ %H:%M"),
            "size_in_bytes": self.size_in_bytes,
        }
