"""
This module provides a function to get a resource object based on the given file path and patterns.

Functions:
    get_resource_by_pattern(
        filepath: str,
        source: ISource = None,
        data_file_patterns: list[str] = None,
        schema_file_patterns: list[str] = None
    ) -> IResource:
        Returns a resource object based on the given file path and patterns.
"""
from pathlib import Path

from ...source.isource import ISource
from .iresource import IResource
from .resource_factory import ResourceFactory
from ..datafile.datafile_factory import DataFileFactory
from ..schema.schema_factory import SchemaFactory


def get_resource_by_pattern(
    filepath: str,
    source: ISource = None,
    data_file_patterns: list[str] = None,
    schema_file_patterns: list[str] = None,
) -> IResource:
    """
    Returns a resource object based on the given file path and patterns.

    Args:
        filepath (str): The file path of the resource.
        source (ISource, optional): The source of the resource. Defaults to None.
        data_file_patterns (list[str], optional): List of patterns to match data files.
            Defaults to None.
        schema_file_patterns (list[str], optional): List of patterns to match schema files.
            Defaults to None.

    Returns:
        IResource: The resource object.
    """
    source = ResourceFactory.find_source(filepath, source)

    if data_file_patterns is not None:
        for pattern in data_file_patterns:
            if Path(filepath).match(pattern):
                return DataFileFactory.from_filepath(filepath, source)

    if schema_file_patterns is not None:
        for pattern in schema_file_patterns:
            if Path(filepath).match(pattern):
                return SchemaFactory.from_filepath(filepath, source)

    return ResourceFactory.from_filepath(filepath, source)
