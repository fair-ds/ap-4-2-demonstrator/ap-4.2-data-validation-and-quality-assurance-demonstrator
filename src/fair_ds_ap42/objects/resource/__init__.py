# flake8: noqa
# pylint: skip-file

from .resource import Resource
from .resource_list import ResourceList
from .duplicate_handler import remove_duplicate_resources
