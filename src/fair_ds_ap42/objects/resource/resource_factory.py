"""
Factory for creating Resource objects
"""

import datetime
import logging

from .resource import Resource
from .iresource import IResource
from ...source.isource import ISource
from ...source import get_source
from ...source.source_factory import SourceFactory

from .iresource_factory import IResourceFactory

logger = logging.getLogger(__name__)


class ResourceFactory(IResourceFactory):
    """
    A factory class for creating instances of `IResource`.

    This class provides methods for creating `IResource` instances from filepaths, dictionaries,
    and existing `IResource` instances.
    """

    @classmethod
    def find_source(cls, filepath: str, source: ISource) -> ISource:
        """
        Find and Validate the source for a filepath.

        Args:
            filepath (str):
                The filepath to find the source for.
            source (ISource):
                The source to use. If None, we will check all available sources.

        Returns:
            ISource:
                The source for the filepath.

        Raises:
            FileNotFoundError:
                If the filepath can not be found.
        """
        if source is None:
            source = get_source(filepath)
            if not source.exists(filepath):
                msg = f"File {filepath} does not exist in any source."
                logger.error(msg)
                raise FileNotFoundError(msg)
        else:
            # Check that the filepath exists in the source provided
            if not source.exists(filepath):
                msg = f"File {filepath} does not exist in source {source}."
                logger.error(msg)
                raise FileNotFoundError(msg)

        return source

    @classmethod
    def from_resource(cls, resource: IResource) -> IResource:
        logger.info("Creating resource from existing resource: %s", resource)
        logger.warning(
            (
                "This method is provided for completeness, but is not recommended. "
                "It will simply return the resource provided."
            )
        )
        return resource

    @classmethod
    def from_filepath(cls, filepath: str, source: ISource = None) -> IResource:
        logger.info("Creating resource from filepath: %s", filepath)

        source = cls.find_source(filepath, source)

        my_resource = Resource(
            filepath=filepath,
            source=source,
            size_in_bytes=source.get_size_in_bytes(filepath),
            last_modified=source.get_date_last_modified(filepath),
        )

        logger.info("Resource created: %s", my_resource)
        return my_resource

    @classmethod
    def from_dict(cls, resource_dict: dict) -> IResource:
        logger.info("Creating resource from dictionary: %s", resource_dict)

        source = SourceFactory.from_dict(**resource_dict["source"])
        date = datetime.datetime.strptime(
            resource_dict["last_modified"], "%Y-%m-%dT%H:%M:%SZ"
        )

        my_resource = Resource(
            filepath=resource_dict["filepath"],
            source=source,
            size_in_bytes=resource_dict["size_in_bytes"],
            last_modified=date,
        )

        logger.info("Resource created: %s", my_resource)
        return my_resource
