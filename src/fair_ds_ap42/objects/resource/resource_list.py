"""
A list of resources, with some extra functionality
"""

from pathlib import Path
from typing import Union

from .resource import Resource


class ResourceList(list):
    """
    Acts identically to a standard list object, but lets us do some time
    saving operations when searching
    """

    def __init__(self, objects=None):
        if objects is None:
            objects = []

        super().__init__(objects)

    def __contains__(self, other: Union[Resource, str, Path]):
        if isinstance(other, Resource):
            other_fp = Path(other.filepath).as_posix()
        else:
            other_fp = Path(other).as_posix()

        for obj in self:
            if Path(obj.filepath).as_posix() == other_fp:
                return True
        return False
