"""
Interface for factories desinged to create IResource objects.

"""


from abc import ABC, abstractmethod

from fair_ds_ap42.objects.resource.iresource import IResource
from fair_ds_ap42.source.isource import ISource


class IResourceFactory(ABC):
    """
    Interface for a factory that creates IResource objects from various sources.
    """

    @classmethod
    @abstractmethod
    def from_resource(cls, resource: IResource) -> IResource:
        """
        Create an object from a Resource.

        Args:
            resource (IResource):
                The resource to create the object from

        Returns:
            IResource
        """

    @classmethod
    @abstractmethod
    def from_filepath(cls, filepath: str, source: ISource = None) -> IResource:
        """
        Create an object from a filepath.

        Args:
            filepath (str):
                The filepath of the resource to create
            source (ISource):
                The source to use to create the resource. If None, we will check all
                available sources.

        Returns:
            IResource
        """

    @classmethod
    @abstractmethod
    def from_dict(cls, resource_dict: dict) -> IResource:
        """
        Create an object from a dictionary.

        The dictionary should have the following structure:
        {
            "filepath": "file1.csv",
            "source": {
                "type": "local_directory",
                "location": "./my_dir",
            },
            "size_in_bytes": 100,
            "last_modified": "2020-01-01T00:00:00Z",
        }

        Args:
            resource_dict (dict):
                The dictionary representation of the resource to create

        Returns:
            IResource
        """
