"""
This module contains the functions for handling duplicate resources.
"""

from . import Resource
from ...source import LocalDirectory


def remove_duplicate_resources(resources: list[Resource]) -> list[Resource]:
    """
    Scan through the list of resources and identify which ones are the same file. Remove
    whichever one is lower in the directory tree.

    Args:
        resources (list[Resource]): The list of resources to scan through

    Returns:
        list[Resource]: The list of resources with duplicates removed
    """
    unique_resources = []

    for resource in resources:
        is_duplicate = False

        for existing_resource in unique_resources.copy():
            if not isinstance(resource.source, type(existing_resource.source)):
                continue

            if isinstance(resource.source, LocalDirectory):
                is_relative = resource.source.is_child_or_parent_of(
                    existing_resource.source
                )
                is_file_duplicate = resource.filepath.endswith(
                    existing_resource.filepath
                ) or existing_resource.filepath.endswith(resource.filepath)

                if is_relative and is_file_duplicate:
                    is_duplicate = True
                    break

        if not is_duplicate:
            unique_resources.append(resource)

    return unique_resources
