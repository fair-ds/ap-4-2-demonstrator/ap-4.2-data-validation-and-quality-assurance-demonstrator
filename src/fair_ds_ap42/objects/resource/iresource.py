"""
Defines the IResource interface

This interface represents a generic resource that we want to process in some way.
"""

from abc import ABC, abstractmethod
import datetime


class IResource(ABC):
    """Represents a data resource that can be accessed"""

    @property
    @abstractmethod
    def size_in_bytes(self) -> int:
        """The size of the resource in bytes"""

    @property
    @abstractmethod
    def last_modified(self) -> datetime.datetime:
        """The date this resource was last modified"""

    @property
    @abstractmethod
    def exists(self) -> bool:
        """True if this file exists according to it's source"""
