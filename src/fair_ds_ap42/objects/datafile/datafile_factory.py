"""
This module provides a factory class for creating DataFile objects from various sources.
It includes several class methods for creating DataFile objects from different sources,
including a Resource object, a file path, and a dictionary.

Attributes:
    None

Methods:
    from_resource(cls, resource: Resource) -> DataFile:
        Creates a DataFile object from a Resource object.

    from_filepath(cls, filepath: str, source: ISource = None) -> DataFile:
        Creates a DataFile object from a file path.

    from_dict(cls, datafile_dict: dict) -> DataFile:
        Creates a DataFile object from a dictionary.
"""
# pylint: disable=R0801

import logging

from ..resource.resource_factory import ResourceFactory

from ...source import ISource
from ..resource.resource import Resource
from .datafile import DataFile

logger = logging.getLogger(__name__)


class DataFileFactory(ResourceFactory):
    """Factory class for creating DataFile objects from various sources.

    This class provides several class methods for creating DataFile objects from different sources,
    including a Resource object, a file path, and a dictionary.

    Attributes:
        None

    Methods:
        from_resource(cls, resource: Resource) -> DataFile:
            Creates a DataFile object from a Resource object.

        from_filepath(cls, filepath: str, source: ISource = None) -> DataFile:
            Creates a DataFile object from a file path.

        from_dict(cls, datafile_dict: dict) -> DataFile:
            Creates a DataFile object from a dictionary.

    """

    @classmethod
    def from_resource(cls, resource: Resource) -> DataFile:
        """Creates a DataFile object from a Resource object.

        Args:
            resource (Resource): The Resource object to create the DataFile from.

        Returns:
            DataFile: The created DataFile object.

        """
        return DataFile(
            filepath=resource.filepath,
            source=resource.source,
            size_in_bytes=resource.size_in_bytes,
            last_modified=resource.last_modified,
        )

    @classmethod
    def from_filepath(cls, filepath: str, source: ISource = None) -> DataFile:
        """Creates a DataFile object from a file path.

        Args:
            filepath (str): The file path to create the DataFile from.
            source (ISource, optional): The source of the DataFile. Defaults to None.

        Returns:
            DataFile: The created DataFile object.

        """
        my_source = cls.find_source(filepath, source)
        my_datafile = DataFile(filepath=filepath, source=my_source)
        logger.info("Created DataFile Resource %s", my_datafile)
        return my_datafile

    @classmethod
    def from_dict(cls, resource_dict: dict) -> DataFile:
        """Creates a DataFile object from a dictionary.

        Args:
            resource_dict (dict): The dictionary to create the DataFile from.

        Returns:
            DataFile: The created DataFile object.

        """
        resource = super().from_dict(resource_dict)
        my_datafile = cls.from_resource(resource)
        logger.info("Created DataFile Resource %s", my_datafile)
        return my_datafile
