"""
Represents a file. Can be used to get metadata about that file, as well as
summary information about the columns in that file.

Usage:

```python

from fair_ds_ap42.objects.datafile import DataFile

# From a local file
my_datafile = DataFile.from_file("path/to/my/file.csv")

# Get a list of all fields in this file
my_datafile.fields
```
"""

from collections import Counter
from pathlib import Path
import logging
import time
from typing import Any

import pandas as pd
import geopandas as gpd

from ..validator.datafile_validator import DataFileValidator

from ...calculations.correlation import Correlation

from ..data_column.data_column import DataColumn
from ..data_column.boolean_column import parse_boolean_from_strings
from ..data_column.exceptions import DataColumnFieldDoesntExist
from ..data_column.util import create_data_column

from ..errors import DataFileTestCase
from ..resource import Resource
from ..schema import Schema
from ...source import LocalDirectory
from ...source.reader.itabular_data_reader import ITabularDataReader
from ...visualizations import MissingnessMatrix, Heatmap, CorrelationPlot

from ...settings import settings
from ...settings.file_settings import FileSettings

from .datafile_metadata import DataFileMetadata

logger = logging.getLogger(__name__)


class DataFile(Resource):  # pylint: disable=too-many-public-methods
    """
    Represents a file to be processed.
    """

    # Static Methods =========================================================
    @classmethod
    def from_file(cls, filepath: str) -> "DataFile":
        """
        Create a data file from a local file as a shortcut to creating a Resource first.

        Args:
            filepath (str):
                The path to the file

        Returns:
            DataFile
        """
        source = LocalDirectory(location=Path(filepath).parent.as_posix())
        return DataFile(
            filepath=filepath,
            source=source,
            size_in_bytes=source.get_size_in_bytes(filepath),
            last_modified=source.get_date_last_modified(filepath),
            file_settings=settings.per_file.get_file_settings(filepath),
        )

    @classmethod
    def from_resource(cls, obj: Resource) -> "DataFile":
        """
        Convert a Resource object to a DataFile object

        Args:
            obj (Resource):
                A Resource object to convert to a DataFile

        Returns:
            DataFile
        """
        return DataFile(
            filepath=obj.filepath,
            source=obj.source,
            size_in_bytes=obj.size_in_bytes,
            last_modified=obj.last_modified,
            file_settings=settings.per_file.get_file_settings(obj.filepath),
        )

    # Magic Methods ==========================================================
    def __key__(self):
        return (
            self.filepath,
            self.format,
            self.source.name,
            self.size_in_bytes,
            self.last_modified,
        )

    def __hash__(self):
        return hash(self.__key__())

    def __eq__(self, other):
        if isinstance(other, DataFile):
            return self.__key__() == other.__key__()
        return NotImplemented

    def __repr__(self):
        return str(self)

    def __str__(self):
        my_str = f"DataFile: {self.filepath} ({self.format}, {self.source})"
        if self.schema:
            my_str += f" (Assigned Schema: {self.schema})"
        return my_str

    # Constructor ============================================================
    def __init__(
        self,
        file_settings: FileSettings = None,
        metadata: DataFileMetadata = None,
        **kwargs,
    ):
        """
        Constructor

        Args:
            filepath (str):
                The filepath to this object.
            source (Source):
                The source of this object (where it is located)
            format (Format):
                The format of this object (e.g. csv, parquet)
        """
        super().__init__(**kwargs)

        if file_settings is None:
            file_settings = settings.per_file.get_file_settings(self.filepath)

        if isinstance(self.reader, ITabularDataReader):
            self.reader.delimiter = file_settings.delimiter

        self.schema = None
        self.metadata = metadata

        self._field_descriptions = None
        self._correlations = Correlation(self, "pearson")
        self.validator = DataFileValidator(datafile=self)
        self._test_cases = None
        self._columns = None

    # Properties =============================================================
    @property
    def columns(self) -> list[DataColumn]:
        """Returns a list of all columns in this file"""
        if self._columns is None:
            self._columns = [self.get_column(field) for field in self.fields]

            if self.has_constructed_geodata:
                self._columns.append(self.get_column("_geodata"))

        return self._columns

    @property
    def description(self) -> dict[str, Any]:
        """Returns metadata about this file"""
        if self.metadata is None:
            self.metadata = DataFileMetadata.from_reader(self.reader)

        return {
            "observations": self.metadata.length,
            "num_fields": self.metadata.num_fields,
        }

    @property
    def field_descriptions(self) -> dict[str, dict]:
        """Returns metadata about each column in this file"""
        if not self._field_descriptions:
            self._field_descriptions = {
                field: self.get_column(field).description for field in self.fields
            }
        return self._field_descriptions

    @property
    def fields(self) -> list[str]:
        """The fields that can be found in this resource"""
        my_fields = self.reader.get_fields()

        if self.has_constructed_geodata:
            my_fields.append("_geodata")

        return my_fields

    @property
    def field_types(self) -> list[str]:
        """The types of the files found in this resource"""
        return list(set(c.data_type for c in self.columns))

    @property
    def has_geodata(self) -> bool:
        """
        True if the schema contains a geodata column, or the user has specified
        lat/lon columns in the settings for this file
        """
        if self.has_constructed_geodata:
            return True

        if self.schema is not None:
            return "geodata" in self.field_types

        return False

    @property
    def has_constructed_geodata(self) -> bool:
        """
        True if the schema contains a geodata column, or the user has specified
        lat/lon columns in the settings for this file
        """
        return self.filepath in settings.per_file.filenames_with_geodata_settings

    @property
    def format(self):
        """The file format for this resource"""
        if self.reader is None:
            return "unknown"

        return self.reader.format

    @property
    def is_valid(self) -> bool:
        """Returns True if all of the testcases for this object are SUCCESS"""
        result = len([t for t in self.test_cases if t.fail_type != "SUCCESS"]) == 0
        logger.debug("Am I a valid file? %s", result)
        return result

    @property
    def resource_path(self) -> str:
        """Gets the path to this resource as it will appear in the website"""
        return (Path(self.filepath).parent / Path(self.filepath).stem).as_posix()

    @property
    def test_cases(self) -> list[DataFileTestCase]:
        """Get a list of all testcases associated with this object through validation"""
        if not self.validator.validation_run:
            self.validator.validate_self()
        return self.validator.test_cases

    @property
    def quality_test_cases(self) -> list[DataFileTestCase]:
        """Get a list of all of the Quality TestCases associated with this object"""
        return self.validator.column_quality_check()

    @property
    def validation_test_cases(self) -> list[DataFileTestCase]:
        """Get a list of all of the Validation TestCases associated with this object"""
        return self.validator.validation_errors

    @property
    def validation_status(self) -> str:
        """
        Get the validation status of this object. This is the worst status of
        all of the testcases associated with this object

        Returns:
            str: The validation status of this object
        """
        statuses = Counter([case.fail_type for case in self.test_cases])

        if "ERROR" in statuses:
            return "ERROR"
        if "WARNING" in statuses:
            return "WARNING"
        return "SUCCESS"

    def get_page_path(self, root_url: str) -> str:
        """
        Constructs and returns the URL path to the page for this object on the website.

        Args:
            root_url (str): The root URL of the website.

        Returns:
            str: The URL path to the page for this object.
        """
        # Convert the root URL and file path to a Path object
        root_path = Path(root_url) / self.filepath

        # Construct the URL path by using the file name as the directory and tacking on
        # index.html to the end
        page_path = root_path.parent / root_path.stem / "index.html"

        # Convert the Path object to a POSIX-style string strip any leading slashes to ensure
        # that we get a relative URL path
        url = page_path.as_posix().strip("/")

        logger.debug("datafile page url: %s", url)

        return url

    # Class Methods ==========================================================
    def as_dict(
        self, root_url: str = "", plots=True, validation_check=True, quality_check=True
    ) -> dict:
        """Returns a dictionary representation of this object"""
        my_plots = {}
        field_plots = {}
        if plots:
            missingness_plot = MissingnessMatrix(self)
            if missingness_plot.get() is not None:
                my_plots["missingness"] = missingness_plot.to_dict()

            my_plots["corr_heatmaps"] = [
                Heatmap(self, "pearson").to_dict(),
                Heatmap(self, "spearman").to_dict(),
            ]

            my_plots["correlations"] = [
                plot.to_dict() for plot in self.get_correlation_plots()
            ]

            field_plots = {
                col.name: [plot.to_dict() for plot in col.my_plots]
                for col in self.columns
            }

        validation_status = "Not Run"
        testsuite_validation_status = "Not Run"
        validation_testcases = "Not Run"
        if validation_check:
            validation_status = self.validation_status
            testsuite_validation_status = self.validator.get_testsuites_status()
            validation_testcases = [
                test_case.as_dict() for test_case in self.validator.validation_errors
            ]

        quality_check = "Not Run"
        quality_test_cases = "Not Run"
        if quality_check:
            quality_check = self.validator.quality_status
            quality_test_cases = [
                test_case.as_dict()
                for test_case in self.validator.column_quality_check()
            ]

        return {
            # Metadata
            "filepath": self.filepath,
            "htmlpath": self.get_page_path(root_url),
            "format": self.format,
            "source": self.source.name,
            "schema": self.schema.filepath,
            "schema_htmlpath": self.schema.get_page_path(root_url),
            "file_metadata": self.get_file_metadata(),
            "description": self.description,
            "field_descriptions": self.field_descriptions,
            # Plots
            "plots": my_plots,
            "field_plots": field_plots,
            # File Test (e.g. Does it exist?)
            "file_test_case": self.validator.validate_datafile().as_dict(),
            # Validation Tests
            "validation_status": validation_status,
            "testsuite_validation_status": testsuite_validation_status,
            "validation_testcases": validation_testcases,
            # Quality Tests
            "quality_status": quality_check,
            "quality_testcases": quality_test_cases,
            # Temporary - for backwards compatibility
            "test_cases": self.validator.get_testcases_dict(),
        }

    def assign_schema(self, schema: Schema = None, infer_ok=False) -> None:
        """
        Assign a schema to this datafile

        Args:
            schema (Schema, optional):
                A Schema object to assign to this data file.
            infer_ok (bool, default=False):
                If True, will infer a schema if none is provided

        Returns:
            None
        """
        self.schema = schema
        if self.schema is None:
            logger.warning("A none value was passed to assign_schema")

            if infer_ok:
                self.schema = Schema.infer(self)

            return

        logger.info("assigning schema %s to %s", schema.filepath, self.filepath)
        if not self.compatible_schema(schema):
            my_error = (
                "Schema is not compatible with this data file. Details: \n"
                f"  schema fields: {','.join(schema.field_names)}\n"
                f"  datafile fields: {','.join(self.fields)}"
            )
            logger.error(my_error)

    def compatible_schema(self, schema: Schema) -> bool:
        """
        Check that a given schema is compatible with this datafile.

        Check that the fields of the schema match those of this file. Does not
        check that the types are correct.

        Args:
            schema (Schema):
                A Schema object to assign to this data file.

        Returns:
            bool
        """
        my_fields = set(self.fields)

        if self.has_constructed_geodata:
            my_fields.remove("_geodata")

        is_compatible = my_fields == set(schema.field_names)

        if not is_compatible:
            logger.debug(
                "schema %s not compatible with %s:", schema.filepath, self.filepath
            )

            logger.debug("- %s", ",".join(self.fields))
            logger.debug("- %s", ",".join(schema.field_names))
        else:
            logger.debug(
                "schema %s is compatible with %s", schema.filepath, self.filepath
            )
        return is_compatible

    def get_column(self, column_name: str) -> DataColumn:
        """
        Gets a column of data from this resource.

        Args:
            column_name (str):
                The name of the column of data to retrieve

        Returns:
            pd.Series

        """
        start = time.time()
        logger.info("Retrieving Column %s for file %s", column_name, self.filepath)

        if not self.reader or not self.reader.exists():
            raise FileNotFoundError

        if column_name not in self.fields and column_name != "_geodata":
            raise DataColumnFieldDoesntExist

        # The user can request a column called "geopoint" if they have
        # indicated the latitue and longitude columns in the settings file.
        if column_name == "_geodata":
            if not self.has_constructed_geodata:
                raise DataColumnFieldDoesntExist

            file_geo_data = settings.per_file.get_file_settings(self.filepath)

            col_data = pd.Series(
                zip(
                    self.reader.read_column(file_geo_data.longitude),
                    self.reader.read_column(file_geo_data.latitude),
                )
            )
            dtype = "geopoint"
            my_format = "array"
        else:
            col_data = self.reader.read_column(column_name)
            dtype = self.get_field_type(column_name)
            my_format = self.schema.frictionless_schema.get_field(column_name).format

        my_column = create_data_column(
            parent=self,
            name=column_name,
            col_data=col_data,
            dtype=dtype,
            my_format=my_format,
        )

        logger.debug(
            "Column %s retrieved in %s seconds", column_name, time.time() - start
        )
        return my_column

    def get_correlation_plots(self) -> list:
        """
        Get a list of correlation plots for this datafile.

        Returns:
            list[CorrelationPlot]
        """

        plots = []
        interesting_corrs = self._correlations.get_interesting_correlations()
        for (var1, var2), _ in interesting_corrs.items():
            corr_plot = CorrelationPlot(self.get_column(var1), self.get_column(var2))
            plots.append(corr_plot)
        return plots

    def get_dataframe(self) -> pd.DataFrame:
        """
        Return this datafile as a pandas DataFrame

        Returns:
            pd.DataFrame
        """
        df = self.reader.read_data()

        # If a schema is provided, check for string fields that are not
        # required, and fill them with empty strings. This prevents frictionless from
        # complaining about type errors
        if self.schema is not None:
            fields_to_fill = set(self.get_fields_by_type("string")).difference(
                self.schema.required_fields
            )

            for field in fields_to_fill:
                logger.debug("filling na values for %s", field)
                df[field] = df[field].fillna("")

            # Iterate over the fields, converting them to the proper data type if needed
            for field in self.schema.frictionless_schema.fields:
                if field.type == "boolean":
                    df[field.name] = parse_boolean_from_strings(
                        df[field.name],
                        field.true_values,
                        field.false_values,
                        coerce=False,
                    )

        return df

    def get_geodataframe(self) -> gpd.GeoDataFrame:
        """
        Return this datafile as a geopandas GeoDataFrame

        Will return the datafile, with an additional geometry column, if the
        datafile was indicated as having geodata in the settings file.

        Returns:
            gpd.GeoDataFrame
        """
        if self.has_constructed_geodata:
            geo_column_name = "_geodata"
        elif self.has_geodata:
            # There should never be more than one geodata column in a file
            [geo_column_name] = self.get_fields_by_type("geopoint")

        else:
            raise DataColumnFieldDoesntExist("No geodata column in this datafile")

        # Create a geodataframe from the datafile
        gdf = gpd.GeoDataFrame(
            self.get_dataframe(),
            geometry=self.get_column(geo_column_name).get_geometry(),
        )
        return gdf

    def get_fields_by_type(self, field_type: str) -> list[str]:
        """
        Get a list of fields in this data file by type, as indicated in the
        paired schema. If no schema is assigned to this file, we raise an
        exception

        Args:
            field_type (str):
                A field type. A list of valid field types can be found at
                https://specs.frictionlessdata.io//table-schema/#types-and-formats

        Returns:
            list[str]
        """
        if not self.schema:
            raise AttributeError("No Schema has yet been assigned to this file")

        return [f.name for f in self.schema.fields if f.type == field_type]

    def get_field_type(self, field_name: str) -> str:
        """
        Get the frictionless type of a given field from the assigned schema.
        If a schema has not yet been assigned, will raise an error.

        Args:
            field_name (str):
                The name of the field. Must exist in the data file.

        Returns:
            str
        """
        if not self.schema:
            raise AttributeError("No Schema has yet been assigned to this file")

        [field] = [f for f in self.schema.fields if f.name == field_name]

        if "enum" in field.constraints:
            return "category"

        return field.type
