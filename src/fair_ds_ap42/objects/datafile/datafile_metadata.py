"""
This module contains the DataFileMetadata class, which represents metadata for a data file.
"""

from dataclasses import dataclass

from ...source.reader.itabular_data_reader import ITabularDataReader


@dataclass
class DataFileMetadata:
    """
    Represents metadata for a data file.

    Attributes:
        fields (list[str]): A list of field names.
        length (int): The number of rows in the data file.
    """

    fields: list[str]
    length: int

    @property
    def num_fields(self):
        """
        Returns the number of fields in the data file.
        """
        return len(self.fields)

    @classmethod
    def from_reader(cls, reader: ITabularDataReader):
        """
        Creates a DataFileMetadata instance from a tabular data reader.

        Args:
            reader (ITabularDataReader): A tabular data reader.

        Returns:
            DataFileMetadata: A DataFileMetadata instance.
        """
        fields = reader.get_fields()
        length = reader.read_column(fields[0]).shape[0]

        return cls(fields, length)
