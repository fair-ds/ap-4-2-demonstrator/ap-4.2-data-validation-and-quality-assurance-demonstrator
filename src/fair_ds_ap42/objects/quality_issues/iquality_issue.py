"""
Abstract Base Class for Quality Issues

All quality issues require the following properties:
- code: A string short code that identifies the check
- violation: A boolean, true if the data we are checking fails the check
- error_message: A DataQualityTestCase, with details about what exactly failed.
"""

from abc import ABC, abstractmethod

from ..errors.data_quality_test_case import DataQualityTestCase


class IQualityIssue(ABC):
    """
    Abstract Base Class for Quality Issues
    """

    def __init__(self, column):
        self.column = column

    @property
    @abstractmethod
    def code(self) -> str:
        """
        The short code used to identify the quality issue
        """

    @property
    @abstractmethod
    def violation(self) -> bool:
        """
        Checks if the quality issue is violated in the column
        """

    @property
    @abstractmethod
    def message(self) -> str:
        """
        Return a message explaining the quality issue
        """

    @property
    def error_message(self) -> DataQualityTestCase:
        """
        Return a DataQualityTestCase object with the error message
        """
        self.ensure_violation_exists()

        return DataQualityTestCase(
            data_file=self.column.parent.filepath,
            violation_type=self.code,
            column_name=self.column.name,
            msg=self.message,
        )

    def ensure_violation_exists(self) -> None:
        """
        Ensure that the quality issue is violated before calling the error message.
        This method checks the `violation` property and raises a ValueError if it's False.

        Raises:
            ValueError: If the quality issue is not violated
        """
        if not self.violation:
            raise ValueError(
                f"Error message called for {self.code} when no violation is present"
            )
