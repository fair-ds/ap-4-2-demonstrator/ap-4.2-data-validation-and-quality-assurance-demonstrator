"""
This module contains the PlaceholderValues class, which is used to check if a column appears to
contain placeholder values.
"""
import datetime

from .iquality_issue import IQualityIssue

DATE_PLACEHOLDERS = [datetime.date(1970, 1, 1)]
GEODATA_PLACEHOLDERS = ["0,0", "0.0,0.0"]
NUMERIC_PLACEHOLDERS = [-99, -999]
STRING_PLACEHOLDERS = ["TBD"]


class PlaceholderValue(IQualityIssue):
    """
    Class for checking if a column appears to contain placeholder values.
    """

    @property
    def code(self) -> str:
        return "placeholder-value"

    @property
    def violation(self) -> bool:
        # Depending on the data type, check for placeholder values
        placeholder_map = {
            "date": {"placeholders": DATE_PLACEHOLDERS},
            "geodata": {"placeholders": GEODATA_PLACEHOLDERS},
            "numeric": {"placeholders": NUMERIC_PLACEHOLDERS},
            "string": {"placeholders": STRING_PLACEHOLDERS},
        }

        if self.column.data_type in placeholder_map:
            placeholders = placeholder_map[self.column.data_type]["placeholders"]
            found_placeholders = any(
                value in placeholders for value in self.column.data.unique()
            )
            return found_placeholders

        return False

    @property
    def message(self):
        return (
            f'Column "{self.column.name}" contains potential placeholder values: '
            f"{', '.join(self._get_placeholder_values())}"
        )

    def _get_placeholder_values(self, num_of_results=5) -> list:
        # Depending on the data type, retrieve the first N placeholder values
        placeholder_map = {
            "date": {"placeholders": DATE_PLACEHOLDERS},
            "geodata": {"placeholders": GEODATA_PLACEHOLDERS},
            "numeric": {"placeholders": NUMERIC_PLACEHOLDERS},
            "string": {"placeholders": STRING_PLACEHOLDERS},
        }

        if self.column.data_type in placeholder_map:
            placeholders = placeholder_map[self.column.data_type]["placeholders"]
            placeholder_values = [
                value for value in self.column.data.unique() if value in placeholders
            ]
            return [str(value) for value in placeholder_values[:num_of_results]]

        raise ValueError(f"Unknown data type: {self.column.data_type}")
