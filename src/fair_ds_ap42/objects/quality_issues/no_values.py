"""
This module contains the NoValues class, which is used to check if a column has no values.
"""

from .iquality_issue import IQualityIssue


class NoValues(IQualityIssue):
    """
    Class for checking if a column has no values.
    """

    @property
    def code(self) -> str:
        return "empty-column"

    @property
    def violation(self) -> bool:
        return self.column.desc["Percent Missing"] == 1

    @property
    def message(self):
        return f'Column "{self.column.name}" has no values'
