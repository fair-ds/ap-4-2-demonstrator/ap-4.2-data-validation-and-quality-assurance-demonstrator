"""
This module contains the SingularValue class, which is used to check if a column has only one
value.
"""

from .iquality_issue import IQualityIssue


class SingularValue(IQualityIssue):
    """
    Class for checking if a column has only one value.
    """

    @property
    def code(self) -> str:
        return "single-value"

    @property
    def violation(self) -> bool:
        # Boolean columns can be quickly checked for singular values if the true false ratio is
        # 0 or 1
        if self.column.data_type == "boolean":
            return self.column.desc.get("True/False Ratio") in [0, 1]

        # All other columns can be checked by seeing if the number of unique values is 1
        return self.column.desc.get("Unique") == 1

    @property
    def message(self):
        return f'Column "{self.column.name}" has only one value'
