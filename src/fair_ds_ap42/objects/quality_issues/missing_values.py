"""
This module contains the MissingValues class, which is used to check if a column is missing a
high percentage of values.
"""
from .iquality_issue import IQualityIssue


class MissingValues(IQualityIssue):
    """
    Class for checking if a column is missing a high percentage of values.
    """

    @property
    def code(self) -> str:
        return "highly-missing"

    @property
    def violation(self) -> bool:
        return self.column.desc["Percent Missing"] > 0.95

    @property
    def message(self):
        return f'Column "{self.column.name}" is missing > 95% of values'
