"""
A column of numerical data from a DataFile
"""
from dataclasses import dataclass

import pandas as pd

from .data_column import DataColumn
from ...visualizations.base_plot import BasePlot
from ...visualizations import BoxPlot, Histogram
from .data_column_describer import FloatColumnDescriber
from .exceptions import InvalidItemInColumnError


@dataclass(kw_only=True)
class NumericColumn(DataColumn):
    """
    Represents a column of numerical data
    """

    describer: FloatColumnDescriber = FloatColumnDescriber()

    def __post_init__(self):
        # Check if any values in this column are non-numeric
        try:
            pd.to_numeric(self.data)
        except ValueError as exc:
            if "Unable to parse" in exc.args[0]:
                raise InvalidItemInColumnError(
                    f"Column '{self.name}' contains non-numeric values"
                ) from exc

        super().__post_init__()
        self.desc["Median"] = self.data.median()

    @property
    def data_type(self) -> str:
        return "numeric"

    @property
    def my_plots(self) -> list[BasePlot]:
        return [BoxPlot(self), Histogram(self)]
