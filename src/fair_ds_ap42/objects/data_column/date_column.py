"""
A column of datetime/date data from a DataFile
"""
from dataclasses import dataclass

import pandas as pd

from ...visualizations import Visualization, DateBarPlot, DatePiePlot
from .data_column import DataColumn
from .data_column_describer import DateColumnDescriber


@dataclass(kw_only=True)
class DateColumn(DataColumn):
    """
    Represents a column of datetime/date data
    """

    describer: DateColumnDescriber = DateColumnDescriber()

    def __post_init__(self):
        # Convert all values to datetime
        self.data = pd.to_datetime(
            self.data,
            format=self.format if self.format != "default" else None,
            errors="coerce",
        )

        super().__post_init__()

    @property
    def data_type(self) -> str:
        return "date"

    @property
    def my_plots(self) -> list[Visualization]:
        return [DateBarPlot(self), DatePiePlot(self)]
