"""
A column of geodata from a DataFile
"""
from dataclasses import dataclass

import pandas as pd
import geopandas as gpd

from shapely.geometry import Point

from ...visualizations import BasePlot, GeoScatterPlot
from .data_column import DataColumn
from .data_column_describer import GeodataColumnDescriber


def parse_value(record, my_format, errors="strict"):
    """
    Function for parsing geodata from one of the three formats used in
    frictionless.
    """
    try:
        if my_format == "default":
            if not isinstance(record, str):
                raise ValueError(
                    "Invalid datatype (recieved value: {record}) expected str))"
                )

            lon, lat = record.split(",")
            lon = lon.strip()
            lat = lat.strip()
        elif my_format == "array":
            if not isinstance(record, (list, tuple)):
                raise ValueError(
                    "Invalid datatype (recieved value: {record}) expected list or tuple)"
                )

            lon, lat = record
        elif my_format == "object":
            if not isinstance(record, dict):
                raise ValueError(
                    "Invalid datatype (recieved value: {record}) expected dict))"
                )

            lon, lat = record["lon"], record["lat"]
        else:
            raise ValueError(f"Invalid format: {my_format}")

        lon = float(lon)
        lat = float(lat)
    except ValueError as err:
        if errors == "coerce":
            return None, None

        raise TypeError("Invalid datatype") from err

    # Make sure the coordinates are valid
    lon, lat = validate_geopoint(lon, lat, errors)

    return lon, lat


def validate_geopoint(lon: float, lat: float, errors: str = "coerce") -> None:
    """
    Check that the provided coordinates are valid
    """
    try:
        assert -180 <= lon <= 180, (
            "lontitudinal coordinates must be between -180 and 180 "
            f"(recieved value: {lon})"
        )
        assert -90 <= lat <= 90, (
            "latitudinal coordinates must be between -90 and 90 "
            f"(recieved value: {lat})"
        )
    except AssertionError as err:
        if errors == "coerce":
            return None, None

        raise ValueError("Invalid geopint data") from err

    return lon, lat


def parse_geodata(col, my_format="default", errors="coerce") -> pd.DataFrame:
    """
    Parse this column of geodata into a pandas dataframe with two columns:
    "lon" and "lat"
    """
    col = col.apply(lambda x: parse_value(x, my_format, errors))
    ret_val = pd.DataFrame(col.to_list(), columns=["lon", "lat"])
    return ret_val


@dataclass(kw_only=True)
class GeodataColumn(DataColumn):
    """
    Represents a column of geodata
    """

    describer: GeodataColumnDescriber = GeodataColumnDescriber()

    def __post_init__(self):
        if self.format is None:
            self.format = "default"
        self.set_format(self.format)

        super().__post_init__()

    @property
    def data_type(self) -> str:
        return "geodata"

    @property
    def my_plots(self) -> list[BasePlot]:
        return [GeoScatterPlot(self)]

    def set_format(self, value: str):
        """
        Set the format of the geodata in this column. One of "default", "array",
        or "object". See https://specs.frictionlessdata.io/table-schema/#geojson
        """
        self.format = value
        df = parse_geodata(self.data, self.format)
        self.data = df.apply(lambda row: (row["lon"], row["lat"]), axis=1)

    def get_geometry(self) -> gpd.GeoSeries:
        """
        Return a GeoSeries of the geodata in this column
        """
        point_series = self.data.apply(lambda coord: Point(coord[0], coord[1]))
        return gpd.GeoSeries(point_series)
