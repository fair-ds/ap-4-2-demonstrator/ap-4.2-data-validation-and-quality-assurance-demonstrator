"""
A column of text data from a DataFile.

If we fail to determine an exact type for a data column, we will typically
land on this one.
"""
from dataclasses import dataclass

from .data_column import DataColumn

from ...visualizations import TextBarPlot, TextPiePlot
from .data_column_describer import TextColumnDescriber


@dataclass(kw_only=True)
class TextColumn(DataColumn):
    """
    Represents a column of text data
    """

    describer: TextColumnDescriber = TextColumnDescriber()

    @property
    def data_type(self) -> str:
        return "text"

    @property
    def my_plots(self) -> list:
        return [TextBarPlot(self), TextPiePlot(self)]

    def __post_init__(self):
        self.data = self.data.astype(str).replace("nan", None)
        super().__post_init__()

    def describe(self) -> dict:
        self.desc = self.describer.describe(self.data)
        self.word_counts = self.describer.get_word_counts(
            self.data, include_counts=True
        )
        self.letter_counts = self.describer.get_character_counts(
            self.data, include_counts=True
        )
        return self.desc
