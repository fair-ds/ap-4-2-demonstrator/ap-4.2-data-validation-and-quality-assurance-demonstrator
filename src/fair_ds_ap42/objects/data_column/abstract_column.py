"""
Abstract Base class that dictates how we can interact with any column object
"""

from abc import ABC, abstractmethod

import pandas as pd

from ...visualizations import Visualization
from ..errors import DataQualityTestCase


class AbstractColumn(ABC):
    """Represents an abstract column of data from a resource"""

    @property
    @abstractmethod
    def data_type(self) -> str:
        """Return the simple type of this object"""

    @property
    @abstractmethod
    def my_plots(self) -> list[Visualization]:
        """Get all plots for this column"""

    @abstractmethod
    def describe(self) -> dict:
        """Return a collection of descriptive statistics about this object"""

    @abstractmethod
    def sample_values(self) -> list:
        """Return a list of sample values from this object"""

    @abstractmethod
    def quality_issues(self) -> list[DataQualityTestCase]:
        """Returns a list of Data Quality issues noted in this column"""

    @abstractmethod
    def get_data(self) -> pd.Series:
        """Returns the data from the given column"""
