"""
Exceptions specific to Data Column objects
"""


class DataColumnFieldDoesntExist(Exception):
    """
    The requested field does not exist in this datacolumn.
    """


class InvalidItemInColumnError(Exception):
    """
    The column contains one or more items that do not match the intended type
    """
