"""
Boolean Column Describer
"""

import numpy as np
import pandas as pd

from .idata_column_describer import IDataColumnDescriber


class BooleanColumnDescriber(IDataColumnDescriber):
    """
    Describes boolean columns
    """

    def _calculate_most_frequent(self, column_data: pd.Series) -> bool:
        if column_data.isna().all():
            return None

        if column_data.mean() == 0.5:
            return None

        return (
            np.bincount(column_data.dropna().astype(bool).values).argmax().astype(bool)
        )

    def describe(self, column_data: pd.Series) -> dict:
        num_missing = column_data.isna().sum()
        percent_missing = column_data.isna().mean()

        description = {
            "Data Type": "boolean",
            "Count": int(column_data.count()),
            "Most Frequent": None,
            "True/False Ratio": None,
            "Missing": int(num_missing),
            "Percent Missing": percent_missing,
        }

        if percent_missing < 1:
            most_frequent = self._calculate_most_frequent(column_data)

            description.update(
                {
                    "Most Frequent": str(most_frequent)
                    if most_frequent is not None
                    else None,
                    "True/False Ratio": column_data.mean(),
                }
            )

        return description
