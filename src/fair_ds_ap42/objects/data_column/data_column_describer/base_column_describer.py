"""
This file containers the BaseColumnDescriber class, which is the base class for all column
describers.

It contains all shared methods and attributes that are common to all column describers.
"""

import pandas as pd

from .idata_column_describer import IDataColumnDescriber


class BaseColumnDescriber(IDataColumnDescriber):
    """
    The Base Column Describer class
    """

    def common_metrics(self, column_data: pd.Series) -> dict:
        """
        Calculate metrics which are common to all column types.

        Args:
            column_data (pd.Series): A column of data

        Returns:
            dict: A dictionary of common metrics
        """

        num_missing = column_data.isna().sum()
        percent_missing = num_missing / len(column_data)

        return {
            "Count": len(column_data),
            "Unique": column_data.nunique(),
            "Percent Unique": column_data.nunique() / len(column_data),
            "Missing": int(num_missing),
            "Percent Missing": percent_missing,
        }
