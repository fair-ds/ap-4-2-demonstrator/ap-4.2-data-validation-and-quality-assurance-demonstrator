"""
Integer Column Describer
"""

import pandas as pd

from .idata_column_describer import IDataColumnDescriber


class IntegerColumnDescriber(IDataColumnDescriber):
    """
    Describes integer columns
    """

    def describe(self, column_data: pd.Series) -> dict:
        # Replace any non numeric values with NA
        column_data = pd.to_numeric(column_data, errors="coerce")

        num_missing = column_data.isna().sum()
        percent_missing = column_data.isna().mean()

        description = {
            "Data Type": "integer",
            "Count": int(len(column_data)),
            "Mean": None,
            "Standard Deviation": None,
            "Minimum": None,
            "25th Percentile": None,
            "Median": None,
            "75th Percentile": None,
            "Maximum": None,
            "Missing": int(num_missing),
            "Percent Missing": percent_missing,
            "Unique": column_data.nunique(dropna=False),
            "Percent Unique": None,
        }

        if percent_missing < 1:
            description.update(
                {
                    "Mean": column_data.mean(),
                    "Standard Deviation": column_data.std(),
                    "Minimum": int(column_data.min()),
                    "25th Percentile": column_data.quantile(0.25),
                    "Median": int(column_data.median()),
                    "75th Percentile": column_data.quantile(0.75),
                    "Maximum": int(column_data.max()),
                    "Percent Unique": column_data.nunique() / column_data.count(),
                }
            )

        return description
