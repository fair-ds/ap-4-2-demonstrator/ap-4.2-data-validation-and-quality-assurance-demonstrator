"""
Abstract class for data column describers.
"""

from abc import ABC, abstractmethod

import pandas as pd


class IDataColumnDescriber(ABC):
    """
    Abstract class for data column describers.
    """

    @abstractmethod
    def describe(self, column_data: pd.Series) -> dict:
        """
        Describe a column of data

        Args:
            column_data (pd.Series): A column of data

        Returns:
            dict: A description of the column
        """
