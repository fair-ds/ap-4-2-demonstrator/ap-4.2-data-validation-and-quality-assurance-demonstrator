"""
Geodata Column Describer

Also contains some helper functions for working with coordinates
"""

import numpy as np
import pandas as pd

from .base_column_describer import BaseColumnDescriber


def haversine_distance(coord1: tuple, coord2: tuple) -> float:
    """
    Calculate the great circle distance between two points

    Args:
        coord1 (tuple): First coordinate (latitude, longitude)
        coord2 (tuple): Second coordinate (latitude, longitude)

    Returns:
        float: Distance in kilometers
    """
    if not isinstance(coord1, tuple) or not isinstance(coord2, tuple):
        raise TypeError(f"Invalid datatype (expected tuple, recieved {type(coord1)})")

    if len(coord1) != 2 or len(coord2) != 2:
        raise ValueError(
            f"Invalid coordinate (expected tuple of length 2, "
            f"recieved {len(coord1)} and {len(coord2)})"
        )

    lat1, lon1 = np.radians(coord1[0]), np.radians(coord1[1])
    lat2, lon2 = np.radians(coord2[0]), np.radians(coord2[1])

    delta_lat = lat2 - lat1
    delta_lon = lon2 - lon1

    sin_delta_lat = np.sin(delta_lat / 2)
    sin_delta_lon = np.sin(delta_lon / 2)

    cos_lat_product = np.cos(lat1) * np.cos(lat2)

    haversine_a = sin_delta_lat**2 + cos_lat_product * sin_delta_lon**2
    haversine_c = 2 * np.arcsin(np.sqrt(haversine_a))
    distance = 6371 * haversine_c  # Earth radius in kilometers

    return distance


def get_coordinate_extents(data: pd.Series) -> dict:
    """
    Get the minimum, maximum, and range of latitude and longitude values in a column of coordinates

    Args:
        data (pd.Series): A column of coordinate tuples

    Returns:
        dict: A dictionary containing the minimum, maximum, and range of latitude and longitude
              values
    """
    min_lat = min(data, key=lambda x: x[0])[0]
    max_lat = max(data, key=lambda x: x[0])[0]
    range_lat = max_lat - min_lat

    min_lon = min(data, key=lambda x: x[1])[1]
    max_lon = max(data, key=lambda x: x[1])[1]
    range_lon = max_lon - min_lon

    extents = {
        "Minimum Latitude": min_lat,
        "Maximum Latitude": max_lat,
        "Range Latitude": range_lat,
        "Minimum Longitude": min_lon,
        "Maximum Longitude": max_lon,
        "Range Longitude": range_lon,
    }

    return extents


def get_coordinate_center(data: pd.Series) -> tuple:
    """
    Calculate the center of a set of coordinates

    Args:
        data (pd.Series): A column of coordinate tuples

    Returns:
        tuple: The center of the coordinates (latitude, longitude)
    """

    if not isinstance(data, pd.Series):
        raise ValueError("Data must be a pandas Series")

    total_lat = sum(coord[0] for coord in data)
    total_lon = sum(coord[1] for coord in data)
    num_coords = len(data)

    center_lat = total_lat / num_coords
    center_lon = total_lon / num_coords

    return center_lat, center_lon


class GeodataColumnDescriber(BaseColumnDescriber):
    """
    Geodata Column Describer
    """

    def describe(self, column_data: pd.Series) -> dict:
        description = {
            **{"Data Type": "geodata"},
            **self.common_metrics(column_data),
        }

        if description["Percent Missing"] < 1:
            center_lat, center_lon = get_coordinate_center(column_data)

            description.update(
                {
                    "Central Point Latitude": center_lat,
                    "Central Point Longitude": center_lon,
                }
            )
            description = {**description, **get_coordinate_extents(column_data)}

        return description
