"""
Category Column Describer

Usage:

    describer = CategoricalColumnDescriber()
    description = describer.describe(column_data)

"""
import pandas as pd

from .idata_column_describer import IDataColumnDescriber


class CategoricalColumnDescriber(IDataColumnDescriber):
    """
    Describes categorical columns
    """

    def _get_most_common_value(self, column_data: pd.Series) -> str:
        return column_data.value_counts().idxmax()

    def _get_least_common_value(self, column_data: pd.Series) -> str:
        return column_data.value_counts().idxmin()

    def describe(self, column_data: pd.Series) -> dict:
        num_missing = column_data.isna().sum()
        percent_missing = column_data.isna().mean()

        description = {
            "Data Type": "categorical",
            "Count": int(column_data.count()),
            "Missing": int(num_missing),
            "Percent Missing": percent_missing,
            "Unique": column_data.nunique(),
            "Unique Ratio": None,
            "Most Common Value": None,
            "Most Common Value Count": None,
            "Most Common Value Ratio": None,
            "Least Common Value": None,
            "Least Common Value Count": None,
            "Least Common Value Ratio": None,
        }

        if percent_missing < 1:
            description.update(
                {
                    "Unique Ratio": column_data.nunique() / column_data.count(),
                    "Most Common Value": str(self._get_most_common_value(column_data)),
                    "Most Common Value Count": (
                        most_common_count := int(column_data.value_counts().max())
                    ),
                    "Most Common Value Ratio": most_common_count / column_data.count(),
                    "Least Common Value": str(
                        self._get_least_common_value(column_data)
                    ),
                    "Least Common Value Count": (
                        least_common_count := int(column_data.value_counts().min())
                    ),
                    "Least Common Value Ratio": least_common_count
                    / column_data.count(),
                }
            )

        return description
