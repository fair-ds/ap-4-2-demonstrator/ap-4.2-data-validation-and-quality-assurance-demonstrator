"""
Text Column Describer

Also contains some helper functions for working with text
"""

from collections import Counter

import numpy as np
import pandas as pd

from .base_column_describer import BaseColumnDescriber


def _get_all_words(data: pd.Series, pattern: str):
    return " ".join(data.str.replace(pattern, "", regex=True).dropna()).split()


def _get_avg_word_lengths(data, pattern):
    all_words = _get_all_words(data, pattern)
    if len(all_words) == 0:
        return 0
    return sum(len(word) for word in all_words) / len(all_words)


def _get_std_word_lengths(data, pattern):
    all_words = _get_all_words(data, pattern)
    if len(all_words) == 0:
        return 0
    word_lengths = [len(word) for word in all_words]
    return np.std(word_lengths)


class TextColumnDescriber(BaseColumnDescriber):
    """
    Describe a column of text data
    """

    def get_word_counts(
        self,
        data: pd.Series,
        pattern: str = r"[^\s\w\d]",
        top_n: int = 5,
        include_counts=False,
    ) -> list:
        """
        Get the most common words in a column of text data

        Args:
            data (pd.Series): A column of text data
            pattern (str, optional): A regex pattern to remove from the text data.
            top_n (int, optional): The number of most common words to return.
            include_counts (bool, optional): Whether to include the counts of each word.

        Returns:
            list: A list of the most common words in the column
        """
        counter = Counter(_get_all_words(data, pattern)).most_common(top_n)

        if include_counts:
            return counter

        return [word for word, _ in counter]

    def get_character_counts(
        self,
        data: pd.Series,
        pattern: str = r"[^\w]",
        top_n: int = 5,
        include_counts=False,
    ):
        """
        Get the most common characters in a column of text data

        Args:
            data (pd.Series): A column of text data
            pattern (str, optional): A regex pattern to remove from the text data.
            top_n (int, optional): The number of most common characters to return.
            include_counts (bool, optional): Whether to include the counts of each character.

        Returns:
            list: A list of the most common characters in the columns
        """

        counter = Counter(
            "".join(data.str.replace(pattern, "", regex=True).dropna())
        ).most_common(top_n)

        if include_counts:
            return counter

        return [letter for letter, _ in counter]

    def describe(self, column_data: pd.Series) -> dict:
        description = {
            **{
                "Data Type": "text",
                "Most Frequent Characters": None,
                "Most Frequent Numbers": None,
                "Most Frequent Punctuation": None,
                "Most Frequent Words": None,
                "Average Word Length": None,
                "Standard Deviation Word Length": None,
                "Average Sentence Length": None,
                "Standard Deviation Sentence Length": None,
            },
            **self.common_metrics(column_data),
        }

        if description["Percent Missing"] < 1:
            description.update(
                {
                    "Most Frequent Characters": self.get_character_counts(
                        column_data, r"[^\w]", 5, True
                    ),
                    "Most Frequent Numbers": self.get_character_counts(
                        column_data, r"[^\d]", 5, True
                    ),
                    "Most Frequent Punctuation": self.get_character_counts(
                        column_data, r"[\w\d\s]", 5, True
                    ),
                    "Most Frequent Words": self.get_word_counts(
                        column_data, top_n=5, include_counts=True
                    )[:5],
                    "Average Word Length": _get_avg_word_lengths(
                        column_data, r"[^\s\w\d]"
                    ),
                    "Standard Deviation Word Length": _get_std_word_lengths(
                        column_data, r"[^\s\w\d]"
                    ),
                    "Average Sentence Length": column_data.dropna().apply(len).mean(),
                    "Standard Deviation Sentence Length": column_data.dropna()
                    .apply(len)
                    .std(),
                }
            )

        return description
