"""
Float Column Describer
"""

import math

import pandas as pd

from .integer_column_describer import IntegerColumnDescriber


class FloatColumnDescriber(IntegerColumnDescriber):
    """
    Describes float columns
    """

    def _get_precision(self, column_data: pd.Series) -> pd.Series:
        precision = column_data.apply(
            lambda x: len(str(x).split(".", maxsplit=1)[-1]) if not math.isnan(x) else 0
        )
        return precision

    def describe(self, column_data: pd.Series) -> dict:
        # This object is identical to the integer describer, but we also want to know details about
        # the decimal places of the float values.
        description = super().describe(column_data)

        # If the column is all missing, we don't need to do anything else
        if description["Percent Missing"] == 1:
            return description

        precision_by_record = self._get_precision(column_data)
        description.update(
            {
                "Highest Precision": int(precision_by_record.max()),
                "Average Precision": precision_by_record.mean(),
                "Lowest Precision": int(precision_by_record.min()),
            }
        )

        return description
