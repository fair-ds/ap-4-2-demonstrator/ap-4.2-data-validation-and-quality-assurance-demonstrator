"""
Date Column Describer
"""
import pandas as pd

from .idata_column_describer import IDataColumnDescriber


class DateColumnDescriber(IDataColumnDescriber):
    """
    Date Column Describer
    """

    def describe(self, column_data: pd.Series) -> dict:
        num_missing = column_data.isna().sum()
        percent_missing = column_data.isna().mean()

        description = {
            "Data Type": "date",
            "Count": len(column_data),
            "Unique": int(column_data.nunique()),
            "Percent Unique": column_data.nunique() / len(column_data),
            "Most Frequent Value": None,
            "Mean": None,
            "Standard Deviation": None,
            "25th Percentile": None,
            "Median": None,
            "75th Percentile": None,
            "Missing": int(num_missing),
            "Percent Missing": percent_missing,
            "Earliest": None,
            "Latest": None,
            "Range": None,
        }

        if percent_missing < 1:
            description.update(
                {
                    "Most Frequent Value": column_data.mode().iloc[0],
                    "Mean": column_data.mean(),
                    "Standard Deviation": column_data.std(),
                    "25th Percentile": column_data.quantile(0.25),
                    "Median": column_data.median(),
                    "75th Percentile": column_data.quantile(0.75),
                    "Earliest": column_data.min(),
                    "Latest": column_data.max(),
                    "Range": column_data.max() - column_data.min(),
                }
            )

        # Format all values that appear in the description as ISO dates
        for key, value in description.items():
            try:
                description[key] = value.isoformat()
            except AttributeError:
                pass

        return description
