"""
A column of boolean data from a DataFile
"""

from dataclasses import dataclass

import pandas as pd

from .data_column import DataColumn
from .data_column_describer import BooleanColumnDescriber

from ...visualizations import Visualization, BarPlot, PiePlot


def parse_boolean_from_strings(
    data: pd.Series,
    true_values: list = None,
    false_values: list = None,
    coerce: bool = True,
) -> pd.Series:
    """
    Parses a series of strings into a series of booleans

    Args:
        data (pd.Series): The series of strings to parse
        true_values (list, optional): A list of strings that should be considered True.
                                      Defaults to ["T", "True", "1", 1].
        false_values (list, optional): A list of strings that should be considered False.
                                       Defaults to ["F", "False", "0", 0].

    Returns:
        pd.Series: The series of booleans
    """
    if true_values is None:
        true_values = ["T", "True", "1", 1]
    if false_values is None:
        false_values = ["F", "False", "0", 0]

    # Create a mapping of true/false values to boolean
    mapping = {value: True for value in true_values + [True]}
    mapping.update({value: False for value in false_values + [False]})

    if coerce:
        result = data.map(mapping)
        result = result.where(pd.notnull(result), None)
    else:
        # Explicitly exclude empty data from the mapping to avoid a warning from pandas about
        # dropping non None values in future versions
        mapped_data = data.map(mapping)
        non_empty_mapped_data = mapped_data[mapped_data.notna()]
        result = non_empty_mapped_data.combine_first(non_empty_mapped_data)

    return result


@dataclass(kw_only=True)
class BooleanColumn(DataColumn):
    """
    Represents a column of boolean data
    """

    describer: BooleanColumnDescriber = BooleanColumnDescriber()

    def __post_init__(self):
        # If the parent of this column has a schema, use it to check if the user provided any
        # true/false values. If not, use the default values.
        if self.parent and self.parent.schema:
            field_schema = self.parent.schema.frictionless_schema.get_field(self.name)
            self.data = parse_boolean_from_strings(
                self.data,
                true_values=field_schema.true_values,
                false_values=field_schema.false_values,
            )
        else:
            self.data = parse_boolean_from_strings(self.data)

        super().__post_init__()

    @property
    def data_type(self) -> str:
        return "boolean"

    @property
    def my_plots(self) -> list[Visualization]:
        return [BarPlot(self), PiePlot(self)]
