"""
Utility functions related to Data columns
"""

import logging

import pandas as pd


from .exceptions import InvalidItemInColumnError
from .numeric_column import NumericColumn
from .text_column import TextColumn
from .boolean_column import BooleanColumn
from .date_column import DateColumn
from .geodata_column import GeodataColumn
from .categorical_column import CategoricalColumn

logger = logging.getLogger(__name__)

COLUMN_TYPES = {
    "number": NumericColumn,
    "integer": NumericColumn,
    "string": TextColumn,
    "object": TextColumn,
    "any": TextColumn,
    "boolean": BooleanColumn,
    "date": DateColumn,
    "time": DateColumn,
    "datetime": DateColumn,
    "geopoint": GeodataColumn,
    "category": CategoricalColumn,
}


def create_data_column(
    parent, name: str, col_data: pd.Series, dtype: str, my_format: str = "default"
):
    """
    Create a data column from a pandas Series
    """
    logger.info("Creating column %s with dtype %s", name, dtype)

    if dtype in COLUMN_TYPES:
        column_type = COLUMN_TYPES[dtype]

        try:
            column = column_type(
                parent=parent, name=name, data=col_data, format=my_format
            )
        except InvalidItemInColumnError:
            column = TextColumn(
                parent=parent, name=name, data=col_data, format=my_format
            )

        return column

    raise NotImplementedError(f"Unsupported column data type: {dtype}")
