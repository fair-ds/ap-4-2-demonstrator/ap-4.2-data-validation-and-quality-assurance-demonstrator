"""
A column of catagorical data from a DataFile
"""
from dataclasses import dataclass

from .data_column import DataColumn
from .data_column_describer import CategoricalColumnDescriber

from ...visualizations import BarPlot, PiePlot


@dataclass(kw_only=True)
class CategoricalColumn(DataColumn):
    """
    Represents a column of categorical data
    """

    describer: CategoricalColumnDescriber = CategoricalColumnDescriber()

    def __post_init__(self):
        self.data = self.data.astype("category")
        super().__post_init__()

    @property
    def data_type(self) -> str:
        return "categorical"

    @property
    def my_plots(self) -> list:
        return [BarPlot(self), PiePlot(self)]
