"""
Parent Object for all DataColumn child Types.
"""
from dataclasses import dataclass, field
from typing import TYPE_CHECKING

import pandas as pd

from .abstract_column import AbstractColumn
from ..errors import DataQualityTestCase
from ...visualizations import Visualization
from .data_column_describer import IDataColumnDescriber

from ..quality_issues import MissingValues, SingularValue, NoValues, PlaceholderValue

if TYPE_CHECKING:
    from ..datafile import DataFile

DESC_KEY_LOOKUP = {
    "top": "Top",
    "freq": "Frequency",
    "count": "Count",
    "mean": "Mean",
    "std": "Standard Deviation",
    "min": "Minimum Value",
    "25%": "25th Percentile",
    "50%": "Median",
    "75%": "75th Percentile",
    "max": "Maximum Value",
    "unique": "Unique",
}

QUALITY_CHECKS = [MissingValues, SingularValue, NoValues, PlaceholderValue]


@dataclass(kw_only=True)
class DataColumn(AbstractColumn):
    """
    Base class for all Data Column Objects. Contains properties and methods
    shared by all child types.
    """

    parent: "DataFile"
    name: str
    data: pd.Series
    format: str = None  # For use in date and geodata columns
    desc: dict = field(default_factory=dict)
    describer: IDataColumnDescriber = None

    def __post_init__(self):
        self.samples = list(self.data.sample(min(len(self.data), 10)).values)
        self.describe()
        self._format_keys()

    def __repr__(self):
        my_str = f"{self.name} ({type(self).__name__}):\n"
        for key, value in self.description.items():
            my_str += f" - {key}: {value}\n"
        return my_str

    @property
    def data_type(self) -> str:
        raise NotImplementedError

    @property
    def my_plots(self) -> list[Visualization]:
        raise NotImplementedError

    @property
    def description(self):
        """A dictionary containing a description of the column"""
        return self.desc

    def describe(self) -> dict:
        self.desc = self.describer.describe(self.data)
        return self.desc

    def sample_values(self) -> list:
        return self.samples

    def quality_issues(self) -> list[DataQualityTestCase]:
        errors = []
        for quality_check in QUALITY_CHECKS:
            check = quality_check(self)
            if check.violation:
                errors.append(check.error_message)
        return errors

    def _format_keys(self):
        # Replace the default keys with better formatted ones
        for old, new in DESC_KEY_LOOKUP.items():
            if old in self.desc:
                self.desc[new] = self.desc[old]
                self.desc.pop(old)

    def get_data(self):
        return self.parent.reader.read_column(self.name)

    def build_plots(self):
        """
        Build all of the plots that can be used to display data from this
        column.
        """
        return Visualization.combine_plots([plot.get() for plot in self.my_plots])
