"""
Tracks the assignments of DataFiles to their corresponding Schemas.

Designed to be instantiated alongside a SchemaAssignmentLoader.

Utilized by the SchemaAssignmentManager to monitor the associations between DataFiles and Schemas.

Also employed by the SchemaAssignmentValidator to ensure the correctness of SchemaAssignments.
"""

from dataclasses import dataclass
import logging

from .. import DataFile, Schema
from ...source.reader.ijson_data_reader import IJSONDataReader

logger = logging.getLogger(__name__)


@dataclass
class SchemaAssignment:
    """
    Tracks the assignments of DataFiles to their corresponding Schemas.
    """

    assignment: dict[Schema, list[DataFile]] = None
    reader: IJSONDataReader = None

    def __post_init__(self):
        if self.assignment is None:
            self.assignment = {}

    @property
    def raw_data(self) -> dict:
        """
        Get the raw data from which this SchemaAssignment was created.
        """
        if self.reader is None:
            return self.get_rich_file_contents()

        return self.reader.read_dict()

    @property
    def schemas(self) -> list[Schema]:
        """
        Get the Schemas tracked by this SchemaAssignment.
        """
        return list(self.assignment.keys())

    @property
    def data_files(self) -> list[DataFile]:
        """
        Get the DataFiles tracked by this SchemaAssignment.
        """
        return [
            data_file
            for data_files in self.assignment.values()
            for data_file in data_files
        ]

    def get_datafiles_by_schema(self, schema: Schema) -> list[DataFile]:
        """
        Get the DataFiles that are assigned to a Schema.
        """
        return self.assignment.get(schema, [])

    def get_schema_by_datafile(self, datafile: DataFile) -> Schema:
        """
        Get the Schema that a DataFile is assigned to.
        """
        for schema, data_files in self.assignment.items():
            if datafile in data_files:
                logger.debug("Schema found for datafile %s", datafile)
                return schema

        logger.debug("No schema found for datafile %s", datafile)
        return None

    def get_rich_file_contents(
        self, plots=True, validation_check=True, quality_check=True
    ) -> list[dict]:
        """
        Get the schema assignment data as a rich dictionary.

        Returns:
            list[dict]:
                A list of dictionaries, each containing the data_files and schema
        """
        result = [
            {
                "data_files": [
                    datafile.as_dict(
                        plots=plots,
                        validation_check=validation_check,
                        quality_check=quality_check,
                    )
                    for datafile in datafiles
                ],
                "schema": schema.as_dict(validation_check=validation_check),
            }
            for schema, datafiles in self.assignment.items()
        ]
        return result
