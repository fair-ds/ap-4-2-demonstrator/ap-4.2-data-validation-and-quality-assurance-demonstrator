"""
This module contains the SchemaAssignmentManager class, which is responsible for managing
SchemaAssignments.

We can provide the SchemaAssignmentManager with a schema assignment and a FileAuditor, and it will
use the FileAuditor to determine which files have been added, removed, or modified. It will then
update the schema assignment accordingly.

As we add additional objects to the SchemaAssigment, we apply the following rules:
- For every file that could be a schema, we check if it is a schema.
  - If that Schema does not exist in the SchemaAssignment, we add it.

- For every file that could be a data file
    - If that DataFile shares all fields with an existing Schema, we add it as a child of that
        Schema.
    - If that DataFile does not share all fields with an existing Schema, we create a new Schema
        and add it as a child of that Schema.

"""

import logging
from pathlib import Path
from typing import TYPE_CHECKING

from .. import DataFile, Schema
from ..datafile.datafile_factory import DataFileFactory
from .schema_assignment import SchemaAssignment
from ...source import VirtualSource

if TYPE_CHECKING:
    from ...file_auditor import FileAuditor

logger = logging.getLogger(__name__)


class SchemaAssignmentManager:
    """
    Class for managing SchemaAssignments.
    """

    def __init__(self, assignment: SchemaAssignment):
        self.assignment = assignment

    def update_schema_assignment(
        self,
        file_auditor: "FileAuditor",
        schema_file_patterns: list[str],
        data_file_patterns: list[str],
    ):
        """
        Compare the files in the FileAuditor to the files in the SchemaAssignment and update the
        SchemaAssignment accordingly.

        This method will add any new Schemas and DataFiles to the SchemaAssignment, and it will
        remove any Schemas and DataFiles that no longer exist.

        If a Datafile is encountered that does not share all fields with an existing Schema, a new
        Schema will be created and added as a child of that Schema.
        """

        # Get a list of all of the files in the FileAuditor that match the schema_file_patterns
        audited_schema_files = [
            Schema.from_resource(resource)
            for resource in file_auditor.all_files
            if any(
                Path(resource.filepath).match(pattern)
                for pattern in schema_file_patterns
            )
        ]

        # Get a list of all of the files in the FileAuditor that match the data_file_patterns
        audited_data_files = [
            DataFileFactory.from_resource(resource)
            for resource in file_auditor.all_files
            if any(
                Path(resource.filepath).match(pattern) for pattern in data_file_patterns
            )
        ]

        # If any of the schema files in the SchemaAssignment are not in the FileAuditor, remove
        # them from the SchemaAssignment
        for schema in self.assignment.schemas:
            if schema not in audited_schema_files:
                logger.info("Removing schema %s from the SchemaAssignment", schema)
                self.remove_schema(schema)

        # If any of the data files in the SchemaAssignment are not in the FileAuditor, remove them
        # from the SchemaAssignment
        for data_file in self.assignment.data_files:
            if data_file not in audited_data_files:
                logger.info(
                    "Removing data file %s from the SchemaAssignment", data_file
                )
                self.remove_datafile(data_file)

        # If any of the schema files in the FileAuditor are not in the SchemaAssignment, add them
        # to the SchemaAssignment
        for schema_file in audited_schema_files:
            if schema_file not in self.assignment.schemas:
                schema = Schema.from_resource(schema_file)
                logger.info("Adding schema %s to the SchemaAssignment", schema)
                self.add_schema(schema)

        # If any of the data files in the FileAuditor are not in the SchemaAssignment, add them to
        # the SchemaAssignment
        for data_file in audited_data_files:
            if data_file not in self.assignment.data_files:
                logger.info("Adding data file %s to the SchemaAssignment", data_file)
                self.add_datafile(DataFileFactory.from_resource(data_file))

        self.assign_schemas()

    def assign_schemas(self):
        """
        Iterate through all of the DataFiles in the SchemaAssignment and assign them to a Schema.

        If a DataFile is compatible with multiple Schemas, it will be assigned to the first Schema
        that it is compatible with.
        """
        for data_file in self.assignment.data_files:
            assigned_schema = self.assignment.get_schema_by_datafile(data_file)
            data_file.assign_schema(assigned_schema)

    def add_schema(self, schema: Schema):
        """
        Add a Schema to the SchemaAssignment.
        """
        self.assignment.assignment[schema] = []

    def add_datafile(self, datafile: DataFile):
        """
        Add a DataFile to the SchemaAssignment.
        """
        # Check all of the schema files currently in the assignemnt to see if one of them matches
        # the datafile
        for schema in self.assignment.schemas:
            if datafile.compatible_schema(schema):
                # If the datafile is compatible with this schema, add it to the assignment
                self.assignment.assignment[schema].append(datafile)
                return

        logger.info("No compatible schema found for datafile %s", datafile)
        new_schema = self.create_new_schema(datafile)
        self.add_schema(new_schema)
        self.assignment.assignment[new_schema].append(datafile)

    def create_new_schema(self, datafile: DataFile) -> Schema:
        """
        Create a new Schema from a DataFile.

        Args:
            datafile: The DataFile to create a Schema from.

        Returns:
            A new Schema object.
        """
        logger.debug("Creating new schema from datafile %s", datafile)
        return Schema.infer(datafile)

    def remove_datafile(self, datafile: DataFile):
        """
        Remove the given datafile from the SchemaAssignment.
        """

        # Check all of the schema files currently in the assignemnt to see if one of them matches
        # the datafile
        for schema in self.assignment.schemas:
            if datafile in self.assignment.assignment[schema]:
                self.assignment.assignment[schema].remove(datafile)
                return

    def remove_schema(self, schema: Schema):
        """
        Remove the given schema from the SchemaAssignment.
        """
        starting_assignment = self.assignment.assignment.copy()

        edited_assignment = {}
        for key, value in starting_assignment.items():
            if key != schema:
                edited_assignment[key] = value

        self.assignment.assignment = edited_assignment

    def get_all_unassigned_datafiles(
        self, data_file_patterns: list[str], file_auditor: "FileAuditor"
    ) -> list[DataFile]:
        """
        Get all DataFiles that have not been assigned to a Schema.
        """

        # Check all of the files that are present in the FileAuditor, and see if any of them are
        # datafiles. If they are, check if they are assigned to a Schema. If they are not, add them
        # to the list of unassigned datafiles.
        unassigned_datafiles = []

        # Get a list of all of the files in the FileAuditor that match the data_file_patterns
        all_datafiles = [
            resource
            for resource in file_auditor.all_existing_files
            if any(
                Path(resource.filepath).match(pattern) for pattern in data_file_patterns
            )
        ]

        # Compare the list of resources we got from the FileAuditor to the list of resources in
        # the SchemaAssignment. If any of the resources in the FileAuditor are not in the
        # SchemaAssignment, add them to the list of unassigned datafiles.

        for datafile in all_datafiles:
            found = False
            for assigned_datafile in self.assignment.data_files:
                if datafile == assigned_datafile:
                    found = True
                    break

            if not found:
                unassigned_datafiles.append(datafile)

        return unassigned_datafiles

    def get_all_virtual_schemas(self) -> list[Schema]:
        """
        Get all of the virtual schemas in the SchemaAssignment.

        Virtual Schemas are schemas which were inferred from a datafile. As a result, these are
        not based on a schema file and have no physical presence in any storage.
        """
        return [
            schema
            for schema in self.assignment.schemas
            if isinstance(schema.source, VirtualSource)
        ]
