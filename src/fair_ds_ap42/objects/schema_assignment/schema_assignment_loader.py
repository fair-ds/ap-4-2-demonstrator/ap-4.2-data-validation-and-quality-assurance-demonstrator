"""
This module contains the SchemaAssignmentLoader class, which is responsible for loading a
SchemaAssignment from a schemas.json file.

Ideally the JSON file should contain a nested list of dictionaries, where each dictionary
contains a schema and a list of data files that use that schema. For example:

```
[
    {
        "data_files": [
            {
                "filepath": "file1.csv",
                "source": {"type": "local_directory", "location": "./my_dir"},
                "size_in_bytes": 100,
                "last_modified": "2020-01-01T00:00:00Z",
            },
            {
                "filepath": "file2.csv",
                "source": {"type": "local_directory", "location": "./my_dir"},
                "size_in_bytes": 100,
                "last_modified": "2020-01-01T00:00:00Z",
            },
        ],
        "schema": {
            "filepath": "schema1.json",
            "source": {"type": "local_directory", "location": "./my_dir"},
            "size_in_bytes": 100,
            "last_modified": "2020-01-01T00:00:00Z",
        },
    },
    ...
```

"""

from . import SchemaAssignment
from .. import Schema, DataFile
from ...source.reader.ijson_data_reader import IJSONDataReader
from ...source.isource import ISource
from ...objects.schema.schema_factory import SchemaFactory
from ...objects.datafile.datafile_factory import DataFileFactory


class SchemaAssignmentLoaderFormatError(Exception):
    """
    General Excetion for when a SchemaAssignmentLoader encounters a file that is not formatted
    correctly.
    """


class SchemaAssignmentLoader:
    """
    Loader for a SchemaAssignment from a JSON file.

    Can handle JSON files that contain either a list of dictionaries, or a dictionary of lists.
    """

    @classmethod
    def from_reader(
        cls, reader: IJSONDataReader = None, sources: list[ISource] = None
    ) -> SchemaAssignment:
        """
        Create a SchemaAssignment from a JSON file.

        Args:
            reader: An IJSONDataReader that can read the JSON file.

        Returns:
            A SchemaAssignment.
        """
        if reader is None:
            return SchemaAssignment()

        if sources is None:
            sources = []

        try:
            my_schema_assignment = cls.parse_file(reader)
        except SchemaAssignmentLoaderFormatError:
            my_schema_assignment = cls.parse_low_information_file(reader)

        my_schema_assignment.reader = reader

        return my_schema_assignment

    @classmethod
    def parse_file(cls, reader: IJSONDataReader):
        """
        Parses a JSON file and returns a SchemaAssignment.

        Args:
            reader: An IJSONDataReader that can read the JSON file.

        Returns:
            A SchemaAssignment.

        Raises:
            SchemaAssignmentLoaderFormatError: If the JSON file is not formatted correctly.
        """

        # Initialize an empty SchemaAssignment dictionary to hold our Schemas and DataFiles
        my_assignment = {}

        try:
            # Create a Schema Object for each schema in the assignment and add it to the assignment
            for assignment in reader.read_dict():
                # Create a Schema from the assignment and add it to the assignment
                schema = Schema.from_dict(assignment["schema"])
                my_assignment[schema] = []

                # Create a DataFile Object for each data file in the assignment and add it to the
                # assignment
                for data_file in assignment["data_files"]:
                    # Create a DataFile from the assignment and add it to the assignment
                    data_file = DataFile.from_dict(data_file)
                    my_assignment[schema].append(data_file)

            # Return the SchemaAssignment
            return SchemaAssignment(my_assignment)

        except TypeError as err:
            if "string indices must be integers" in err.args[0]:
                raise SchemaAssignmentLoaderFormatError() from err

            raise err

    @classmethod
    def parse_low_information_file(
        cls,
        reader: IJSONDataReader,
    ) -> SchemaAssignment:
        """
        Parse a JSON file that contains only filepaths, and not any file metadata or source.

        We will attempt to locate the various files from among the provided sources.

        Args:
            filepath: The path to the JSON file.

        Returns:
            A SchemaAssignment.
        """

        my_assignment = {}
        schema_factory = SchemaFactory()
        datafile_factory = DataFileFactory()

        for schema_filepath, data_filepaths in reader.read_dict().items():
            schema = schema_factory.from_filepath(schema_filepath)

            my_assignment[schema] = []

            for data_filepath in data_filepaths:
                data_file = datafile_factory.from_filepath(data_filepath)

                my_assignment[schema].append(data_file)

        return SchemaAssignment(my_assignment)
