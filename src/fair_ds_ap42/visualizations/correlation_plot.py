""" Show a correlation between two columns as a scatter plot."""

import typing

import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig

if typing.TYPE_CHECKING:
    from ..objects.data_column import DataColumn


class CorrelationPlot(BasePlot):
    """Represents a Correlation Plot Visual"""

    def __init__(self, column1: "DataColumn", column2: "DataColumn"):
        # The datafile is needed to get the resource path - it should be the parent of the columns,
        # it doesn't matter which column we choose
        parent_datafile = column1.parent

        config = PlotConfig(
            title=f"Correlation Plot for {column1.name} and {column2.name}",
            xaxis_title=column1.name,
            yaxis_title=column2.name,
            filepath=(
                parent_datafile.resource_path
                + f"/plots/correlation_{column1.name}_{column2.name}.png"
            ),
        )
        super().__init__(config)

        self.column1 = column1
        self.column2 = column2

    def get(self) -> go.Figure:
        self._fig = px.scatter(x=self.column1.get_data(), y=self.column2.get_data())

        return super().get()
