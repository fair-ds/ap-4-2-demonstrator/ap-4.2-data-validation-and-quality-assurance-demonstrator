"""
Utility functions for visualizations.
"""

import pandas as pd


def get_counts(data, cutoff=5) -> pd.Series:
    """
    Get the most frequent values that appear in the data.

    If the data contains more than 5 values, then all other values are collected together under
    the "Other Values" category.

    Args:
        data (pd.Series): The data to get counts for
        cutoff (int, optional): The number of values to include in the counts. Defaults to 5.

    Returns:
        pd.Series: The counts of the data
    """
    counts = data.value_counts(dropna=False).fillna("(No Value)")

    if len(counts) > 6:
        other_col = pd.Series(counts.iloc[5:].sum(), ["Other Values"])
        counts = counts.iloc[:cutoff].append(other_col)

    return counts
