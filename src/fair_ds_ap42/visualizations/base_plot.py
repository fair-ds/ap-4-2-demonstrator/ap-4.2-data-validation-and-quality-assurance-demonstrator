"""
Base plot object for all visualizations.
"""

import io
import json
import logging
from pathlib import Path

import plotly
import plotly.graph_objects as go
import pympler.asizeof

from .plot_config import PlotConfig
from ..settings import settings

logger = logging.getLogger(__name__)


class BasePlot:
    """
    Base class for creating Plotly plots.
    """

    root_directory = "output"

    def __init__(self, config: PlotConfig):
        """
        Initializes a new instance of the BasePlot class.

        Args:
            title (str): The title of the plot.
            xaxis_title (str, optional): The title of the x-axis. Defaults to None.
            yaxis_title (str, optional): The title of the y-axis. Defaults to None.
        """
        self._fig = None
        self.config = config
        self.column = None

    @property
    def plot_type(self) -> str:
        """
        Returns the type of the plot.

        Returns:
            str: The type of the plot.
        """
        return self.__class__.__name__

    @property
    def as_json(self) -> dict:
        """
        Returns the plot as a JSON object.

        Returns:
            dict: The plot as a JSON object.
        """
        return json.dumps(self.get(), cls=plotly.utils.PlotlyJSONEncoder)

    @property
    def as_html_div(self) -> str:
        """
        Returns the plot as an HTML div.

        Returns:
            str: The plot as an HTML div.
        """
        plt = self.get()

        if plt is None:
            return "<div></div>"

        return plotly.offline.plot(plt, include_plotlyjs=False, output_type="div")

    def to_dict(self) -> dict:
        """
        Returns the plot as a dictionary.

        Returns:
            dict: The plot as a dictionary.
        """
        logger.debug("Converting plot '%s' to dictionary.", self.config.title)

        # Write the json data to a string buffer. This is needed because the standard
        # Figure.to_dict() method returns numpy arrays which are not serializable.
        stream = io.StringIO()
        plot_data = self.get()

        if plot_data is None:
            return {
                "error": "Plot data is None",
                "plot_type": self.plot_type,
            }

        plot_data_dict_in_bytes = pympler.asizeof.asizeof(plot_data)
        if plot_data_dict_in_bytes > settings.project.max_plot_size_in_bytes:
            logger.warning("Plot data is too large: %s bytes", plot_data_dict_in_bytes)
            self.to_file(self.config.filepath, plot_figure=plot_data)
            return {
                "error": f"Plot data is too large: {plot_data_dict_in_bytes:,} bytes",
                "plot_type": self.plot_type,
                "output_filepath": self.config.filepath,
                "resource_path": "plots/" + Path(self.config.filepath).name,
            }

        plot_data.write_json(stream)
        stream.seek(0)
        plot_dict = json.load(stream)
        plot_dict["plot_type"] = self.plot_type
        return plot_dict

    def get(self) -> go.Figure:
        """
        Returns the plot as a Plotly Figure.

        Returns:
            go.Figure: The plot as a Plotly Figure.
        """
        return self._fig.update_layout(
            title=self.config.title,
            title_font=self.config.title_font,
            xaxis_title=self.config.xaxis_title,
            yaxis_title=self.config.yaxis_title,
            plot_bgcolor=self.config.plot_bgcolor,
            paper_bgcolor=self.config.paper_bgcolor,
            width=self.config.figure_size[0],
            height=self.config.figure_size[1],
        )

    def update_traces(self, **kwargs) -> None:
        """
        Updates the traces of the plot using the provided keyword arguments.

        Args:
            **kwargs: The keyword arguments to update the traces with.
        """
        self._fig = self._fig.update_traces(**kwargs)

    def update_layout(self, **kwargs) -> None:
        """
        Updates the layout of the plot using the provided keyword arguments.

        Args:
            **kwargs: The keyword arguments to update the layout with.
        """
        self._fig = self._fig.update_layout(**kwargs)

    def to_file(self, filename: str, plot_figure=None):
        """
        Saves the plot to the provided filename.

        Args:
            filename (str): The filename to save the plot to.
        """
        full_path = self.root_directory / Path(filename)

        full_path.parent.mkdir(parents=True, exist_ok=True)

        if plot_figure is None:
            plot_figure = self.get()

        plot_figure.write_image(full_path)
        logger.warning("Plot saved to file: %s", full_path)
