"""Create a Pie Plot for the most common data points in a set of data"""

import pandas as pd
import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig


class TextPiePlot(BasePlot):
    """Represents a Pie Plot Visual"""

    def __init__(self, column):
        config = PlotConfig(
            title=f"Pie Plot for column {column.name} - Character Frequency",
            filepath=(
                column.parent.resource_path
                + f"/plots/char_freq_pie_plot_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        cutoff = 10

        letter_counts_df = pd.DataFrame(
            self.column.letter_counts, columns=["Letter", "Count"]
        )
        other_col = pd.DataFrame(
            {
                "Letter": ["Other Values"],
                "Count": [letter_counts_df.iloc[cutoff:].Count.sum()],
            }
        )
        data = pd.concat([letter_counts_df.iloc[:cutoff], other_col])

        self._fig = px.pie(data, values="Count", names="Letter")
        return super().get()
