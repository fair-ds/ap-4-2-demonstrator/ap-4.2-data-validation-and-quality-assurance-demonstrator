"""
Create a Missingness Matrix for a given datafile.
"""

import typing

import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig

if typing.TYPE_CHECKING:
    from ..objects.datafile import DataFile


class MissingnessMatrix(BasePlot):
    """Represents a Missingness Matrix Visualization"""

    def __init__(self, datafile: "DataFile"):
        config = PlotConfig(
            title=f"Missingness Matrix for {datafile.filepath}",
            filepath=datafile.resource_path + "/plots/missingness_matrix.png",
        )
        super().__init__(config)
        self.datafile = datafile

    def get(self) -> go.Figure:
        data = (
            self.datafile.get_dataframe()
            .replace("", None)
            .reset_index(drop=True)
            .notnull()
        )
        if data.all(axis=None):
            return None

        self._fig = px.imshow(data, color_continuous_scale="gray")
        self._fig.update_layout(coloraxis_showscale=False)

        return super().get()
