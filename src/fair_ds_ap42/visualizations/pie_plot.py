"""Create a Pie Plot for the most common data points in a set of data"""

import plotly.graph_objects as go

from .base_count_plot import BaseCountPlot
from .plot_config import PlotConfig


class PiePlot(BaseCountPlot):
    """Represents a Pie Plot Visual"""

    def __init__(self, column):
        config = PlotConfig(
            title=f"Pie Plot for column {column.name}",
            filepath=(
                column.parent.resource_path + f"/plots/pie_plot_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        data = self.counts.reset_index()
        data.columns = [self.column.name, "Count"]

        if "Other Values" in data[self.column.name]:
            pull_slices = [500, 0.2, 0.2, 0.2, 0.2, 0]
        else:
            pull_slices = None

        self._fig = go.Figure(
            data=[
                go.Pie(
                    labels=data[self.column.name],
                    values=data.Count,
                    pull=pull_slices,
                    rotation=90,
                )
            ]
        )

        return super().get()
