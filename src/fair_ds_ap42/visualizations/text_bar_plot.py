"""
Create a Bar Plot for a given column.
"""

from typing import TYPE_CHECKING

import pandas as pd
import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig

if TYPE_CHECKING:
    from ..objects.data_column import DataColumn


class TextBarPlot(BasePlot):
    """Creates a standard count plot for a DataColumn"""

    def __init__(self, column: "DataColumn"):
        config = PlotConfig(
            title=f"Bar Plot for column {column.name} - Word Frequency",
            filepath=(
                column.parent.resource_path + f"/plots/word_freq_bar_plot_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        cutoff = 10

        word_counts_df = pd.DataFrame(
            self.column.word_counts, columns=["Word", "Count"]
        )
        other_col = pd.DataFrame(
            {
                "Word": ["Other Values"],
                "Count": [word_counts_df.iloc[cutoff:].Count.sum()],
            }
        )
        data = pd.concat([word_counts_df.iloc[:cutoff], other_col])

        self._fig = px.bar(data, x="Word", y="Count")
        return super().get()
