"""
Use DBSCAN clustering to downsample a GeoSeries.
"""

from dataclasses import dataclass
import logging
import math

from sklearn.cluster import DBSCAN
import numpy as np

import geopandas as gpd

from .idownsampler import IDownsampler


logger = logging.getLogger(__name__)


def find_optimal_parameters(
    data: gpd.GeoSeries, desired_size: int
) -> tuple[float, int]:
    """
    Find optimal parameters (eps and min_samples) for downsampling using DBSCAN.

    Args:
        data (gpd.GeoSeries): GeoSeries containing geometries.

    Returns:
        Tuple[float, int]: Optimal values for eps and min_samples.
    """
    current_size = len(data)
    eps = 0.2
    min_samples = 5
    max_iterations = 20

    logger.info("Starting parameter estimation for downsampling...")
    logger.info("Initial data size: %s geometries", current_size)

    iteration = 0
    while current_size > desired_size and iteration < max_iterations:
        # Create a new instance of the downsampler with the current parameters
        downsampler = DBSCANDownsampler(eps=eps, min_samples=math.ceil(min_samples))

        # Downsample the data
        downsampled_data = downsampler.downsample(data)

        # Update the current size
        current_size = len(downsampled_data)

        logger.debug("Iteration: %s", iteration + 1)
        logger.debug(
            "Current parameters: eps=%s, min_samples=%s", eps, math.ceil(min_samples)
        )
        logger.debug("Downsampled data size: %s geometries", current_size)

        if current_size <= desired_size:
            logger.info("Desired size achieved. Stopping parameter estimation.")
            break

        # Adjust the parameters
        eps *= 0.8
        # min_samples *= 0.9
        iteration += 1

    return eps, math.ceil(min_samples)


@dataclass
class DBSCANDownsampler(IDownsampler):
    """
    Data Downsampler using DBSCAN clustering.
    """

    eps: float = 0.2
    min_samples: int = 5

    auto_estimate_parameters: bool = False
    desired_size: int = 2000

    def downsample(self, data: gpd.GeoSeries) -> gpd.GeoSeries:
        """
        Downsample a GeoSeries using DBSCAN clustering.

        Args:
            data (gpd.GeoSeries): GeoPandas GeoSeries containing geometries.

        Returns:
            gpd.GeoSeries: Downsampled GeoSeries.
        """
        if self.auto_estimate_parameters:
            self.eps, self.min_samples = find_optimal_parameters(
                data, self.desired_size
            )

        # Filter out empty geometries
        non_empty_geometries = data[~data.is_empty]

        # Extract coordinates from non-empty geometries
        coordinates = np.array([(geom.x, geom.y) for geom in non_empty_geometries])

        # Perform DBSCAN clustering
        clustering = DBSCAN(eps=self.eps, min_samples=self.min_samples).fit(coordinates)

        # Subset non-empty geometries by cluster labels
        downsampled_geometries = non_empty_geometries[clustering.labels_ >= 0]

        reduction_percentage = (
            1 - len(downsampled_geometries) / len(non_empty_geometries)
        ) * 100

        logger.info(
            "Downsampling completed. Reduction in size: %.2f%%", reduction_percentage
        )

        return downsampled_geometries
