"""
Downsamples a GeoSeries of points by aggregating them into a grid and
extracting a representative point from each grid cell.
"""

from dataclasses import dataclass

import geopandas as gpd
import numpy as np

from .idownsampler import IDownsampler


@dataclass
class GridDownsampler(IDownsampler):
    """
    Class for downsampling a GeoSeries of points by aggregating them into a grid and
    extracting a representative point from each grid cell.
    """

    grid_size: int = 0.1

    def downsample(self, data: gpd.GeoSeries) -> gpd.GeoSeries:
        data_copy = gpd.GeoDataFrame(geometry=data.copy())

        # Calculate the grid cell indices for each point
        data_copy["x_bin"] = np.floor(data.x / self.grid_size)
        data_copy["y_bin"] = np.floor(data.y / self.grid_size)

        # Aggregate points within each grid cell
        aggregated_gdf = (
            data_copy.groupby(["x_bin", "y_bin"])
            .agg(
                aggregated_geometry=("geometry", lambda x: x.unary_union),
                count=("geometry", "size"),
            )
            .reset_index()
        )

        # Filter out empty cells
        aggregated_gdf = aggregated_gdf[aggregated_gdf["count"] > 0]

        # Convert aggregated_geometry column to GeoSeries
        aggregated_gdf["aggregated_geometry"] = gpd.GeoSeries(
            aggregated_gdf["aggregated_geometry"]
        )

        # Set the active geometry column
        aggregated_gdf = aggregated_gdf.set_geometry("aggregated_geometry")

        # Extract representative point from each aggregated geometry
        aggregated_gdf["representative_point"] = aggregated_gdf[
            "aggregated_geometry"
        ].representative_point()

        # Access the representative_point column as a GeoSeries
        representative_points = aggregated_gdf["representative_point"]

        # Reset the index and drop unnecessary columns
        aggregated_gdf = aggregated_gdf.reset_index(drop=True).drop(
            columns=["count", "x_bin", "y_bin", "aggregated_geometry"]
        )

        return representative_points
