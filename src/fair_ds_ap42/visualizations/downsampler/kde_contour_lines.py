"""
Downsamples a GeoSeries of points using Kernel Density Estimation and contour lines.
"""

from dataclasses import dataclass
import logging

import geopandas as gpd
import numpy as np
from scipy.stats import gaussian_kde


from .idownsampler import IDownsampler

logger = logging.getLogger(__name__)


@dataclass
class KDEContourLines(IDownsampler):
    """
    Downsamples a GeoSeries of points using Kernel Density Estimation and contour lines.
    """

    bandwidth: float = 0.1

    def downsample(self, data: gpd.GeoSeries) -> gpd.GeoSeries:
        logger.info("Downsampling using KDE Contour Lines")

        # Filter out empty geometries
        non_empty_geometries = data[~data.is_empty]
        logger.warning("Number of non-empty geometries: %s", len(non_empty_geometries))

        # Extract coordinates from the geoseries
        coordinates = np.array(
            [(geom.x, geom.y) for geom in non_empty_geometries.geometry]
        )
        logger.warning("Extracted %s coordinates", len(coordinates))

        # Perform Kernel Density Estimation
        logger.info("Performing Kernel Density Estimation")
        kde = gaussian_kde(coordinates.T, bw_method=self.bandwidth)

        # Generate contour lines
        logger.info("Generating contour lines")
        xmin, ymin = coordinates.min(axis=0)
        xmax, ymax = coordinates.max(axis=0)
        x_values, y_values = np.mgrid[xmin:xmax:360j, ymin:ymax:180j]
        logger.warning("Generated grid with shape: %s", x_values.shape)

        positions = np.vstack([x_values.ravel(), y_values.ravel()])
        density = kde(positions).reshape(x_values.shape)
        logger.warning("Computed density matrix with shape: %s", density.shape)

        return x_values, y_values, density

        # Convert contour lines to a GeoSeries
        # logger.info("Converting contour lines to GeoSeries")
        # contours = gpd.GeoSeries.from_features(
        #     gpd.features.shapes(density, transform=data.crs), crs=data.crs
        # )
        # logger.warning("Generated %s contour lines", len(contours))

        # return contours
