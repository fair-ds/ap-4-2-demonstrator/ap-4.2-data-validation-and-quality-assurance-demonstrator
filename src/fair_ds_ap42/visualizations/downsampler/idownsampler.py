"""
Abstract class for downsampling a geometry.
"""

from abc import ABC, abstractmethod

import geopandas as gpd


class IDownsampler(ABC):
    """Class to downsample a geometry"""

    @abstractmethod
    def downsample(self, data: gpd.GeoSeries) -> gpd.GeoSeries:
        """
        Create a downsampled version of the input data.

        Args:
            data (gpd.GeoSeries): GeoPandas GeoSeries containing geometries.

        Returns:
            gpd.GeoSeries: Downsampled GeoSeries.
        """
        raise NotImplementedError("Downsampling not implemented in base class")
