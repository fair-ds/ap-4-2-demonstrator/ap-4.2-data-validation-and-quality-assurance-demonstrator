"""Represents a Bar Plot for DateTime Columns"""

import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig


class DateBarPlot(BasePlot):
    """A Bar Plot for DateTime Columns"""

    def __init__(self, column):
        config = PlotConfig(
            title=f"Bar Plot for column {column.name} - Day of Year",
            filepath=(
                column.parent.resource_path
                + f"/plots/bar_plot_{column.name}_day_of_year.png"
            ),
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        data = self.column.data.dt.dayofyear.value_counts().reset_index()
        data.columns = ["Day of Year", "Count"]
        self._fig = px.bar(data, x="Day of Year", y="Count")
        return super().get()
