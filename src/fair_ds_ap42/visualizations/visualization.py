"""Abstract base class from which all other visualizations are created."""

from abc import ABC, abstractmethod

from dataclasses import dataclass, field
import logging
from pathlib import Path
import time
import typing

import matplotlib.pyplot as plt

if typing.TYPE_CHECKING:
    from ..objects.data_column import DataColumn


logger = logging.getLogger(__name__)


class AbstractVisualization(ABC):
    """Abstract Base Class for all visualization objects"""

    @abstractmethod
    def get(self) -> plt.Figure:
        """Plot the provided data"""

    @abstractmethod
    def to_file(self, filepath: Path) -> None:
        """Plot the data and save it to the indicated filepath"""


@dataclass
class Visualization(AbstractVisualization):
    """Base Object for all Visualizatons"""

    column: "DataColumn" = field(repr=False)
    title: str
    xlabel: str = field(default="")
    ylabel: str = field(default="")

    @classmethod
    def combine_plots(cls, axes=list):
        """Combined multiple subplots into a single figure"""
        fig = plt.figure()
        for ax in axes:
            fig.axes.append(ax)
        # return fig

    def to_file(self, filepath: Path):
        """Save this object to file"""
        logger.info("saving plot to file %s", filepath)
        start = time.time()
        if self.get() is not None:
            logger.info("plot built in %0.02fs", time.time() - start)
            filepath.parent.mkdir(exist_ok=True, parents=True)
            plt.savefig(filepath)
            plt.close()
