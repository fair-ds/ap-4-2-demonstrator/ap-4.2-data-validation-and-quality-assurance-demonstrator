"""Intermediate Object that applies a helper function

Gives the object the property "counts", which gets value counts for the first
five most frequent values, then collects all other values in a category called
"Other Values"
"""

import pandas as pd

from .base_plot import BasePlot


class BaseCountPlot(BasePlot):
    """Gives a visualization the "counts" property."""

    @property
    def counts(self):
        """
        Gets the most frequent values that appear in the data. If the data
        contains more than 5 values, then all other values are collected
        together under the "Other Values" category.
        """

        counts = self.column.get_data().value_counts(dropna=False).fillna("(No Value)")

        if len(counts) > 6:
            other_col = pd.Series(counts.iloc[5:].sum(), ["Other Values"])
            counts = pd.concat([counts.iloc[:5], other_col])

        return counts
