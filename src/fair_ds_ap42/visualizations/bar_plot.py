"""
Create a Bar Plot for a given column.
"""

from typing import TYPE_CHECKING

import plotly.graph_objects as go
import plotly.express as px

from .base_count_plot import BaseCountPlot
from .plot_config import PlotConfig

if TYPE_CHECKING:
    from ..objects.data_column import DataColumn


class BarPlot(BaseCountPlot):
    """Creates a standard count plot for a DataColumn"""

    def __init__(self, column: "DataColumn"):
        config = PlotConfig(
            title=f"Bar Plot for column {column.name}",
            filepath=(
                column.parent.resource_path + f"/plots/bar_plot_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        data = self.counts.reset_index().fillna("(No Value)")
        data.columns = [self.column.name, "Count"]
        self._fig = px.bar(data, x=self.column.name, y="Count")
        return super().get()
