# pylint: disable=too-many-instance-attributes
"""
Defines a class to hold configuration for a plot.
"""

from dataclasses import dataclass


@dataclass
class PlotConfig:
    """
    Class to hold configuration for a plot.
    """

    title: str
    xaxis_title: str = None
    yaxis_title: str = None
    title_font: dict = None
    plot_bgcolor: str = "#DCDCDC"
    paper_bgcolor: str = "#ffffff"
    filepath: str = None
    figure_size: tuple = (800, 600)

    def __post_init__(self):
        # Set default values for title_font if not provided
        self.title_font = self.title_font or {"family": "Arial", "size": 24}
