"""
Create a Heatmap for the correlations in a given datafile
"""

import typing

import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from ..calculations.correlation import Correlation
from .plot_config import PlotConfig

if typing.TYPE_CHECKING:
    from ..objects.datafile import DataFile


class Heatmap(BasePlot):
    """
    Represents a Heatmap visualization.
    """

    def __init__(self, datafile: "DataFile", method: str):
        config = PlotConfig(
            title=f"{method.capitalize()} correlations for {datafile.filepath}",
            filepath=datafile.resource_path + f"/plots/{method}_correlation.png",
        )
        super().__init__(config)
        self.correlation = Correlation(datafile, method)

    def get(self) -> go.Figure:
        data = self.correlation.calculate_correlations()

        self._fig = px.imshow(data, text_auto=True, color_continuous_scale="Blues")

        return super().get()
