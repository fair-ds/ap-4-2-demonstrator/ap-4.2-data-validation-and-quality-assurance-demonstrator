"""
Create a Scatter plot from a Geodata column
"""

from typing import TYPE_CHECKING

import plotly.graph_objects as go

from .base_plot import BasePlot
from .plot_config import PlotConfig

if TYPE_CHECKING:
    from ..objects.data_column import GeodataColumn


class GeoScatterPlot(BasePlot):
    """Creates a Scatter plot for a GeodataColumn"""

    def __init__(self, column: "GeodataColumn", downsampler=None):
        config = PlotConfig(
            title="Scatter Plot for Geodata",
            filepath=(
                column.parent.resource_path
                + f"/plots/geo_scatter_plot_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.column = column
        self.downsampler = downsampler

    def _build_figure(self):
        geometry = self.column.get_geometry()

        if self.downsampler is not None:
            geometry = self.downsampler.downsample(geometry)

        self._fig = go.Figure()

        self._fig.add_trace(
            go.Scattergeo(
                lon=geometry.x,
                lat=geometry.y,
                mode="markers",
                marker={
                    "size": 2,
                    "opacity": 0.2,
                },
            )
        )

        self._fig.update_layout(
            mapbox={
                "style": "carto-positron",
                "zoom": 1,
                "center": {"lat": 0, "lon": 0},
            },
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
        )

    def get(self) -> go.Figure:
        self._build_figure()
        return super().get()
