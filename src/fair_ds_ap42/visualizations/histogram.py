"""
This module contains the Histogram class for creating a histogram plot.
"""

from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig

if TYPE_CHECKING:
    from ..objects.data_column import DataColumn


class Histogram(BasePlot):
    """
    Class for creating a histogram plot.
    """

    def __init__(self, column: "DataColumn"):
        config = PlotConfig(
            title=f"Histogram for column {column.name}",
            yaxis_title="Count",
            xaxis_title="Bin",
            filepath=(
                column.parent.resource_path + f"/plots/histogram_{column.name}.png"
            ),
        )
        super().__init__(config)
        self.data = column

    def get(self) -> go.Figure:
        data = self.data.get_data()

        if data.isna().all():
            return None

        bins = np.histogram_bin_edges(data.dropna(), bins="fd")
        y_data, x_data = np.histogram(data, bins)
        data_df = pd.DataFrame({"Bin": x_data[:-1], "Count": y_data})
        self._fig = px.bar(data_df, x="Bin", y="Count")
        return super().get()
