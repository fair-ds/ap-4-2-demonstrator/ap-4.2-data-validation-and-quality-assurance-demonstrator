"""Represents a Box Plot for Columns"""

from typing import TYPE_CHECKING

import plotly.graph_objects as go
import plotly.express as px

from .base_plot import BasePlot
from .plot_config import PlotConfig

if TYPE_CHECKING:
    from ..objects.data_column import DataColumn


class BoxPlot(BasePlot):
    """
    A Box Plot for Columns
    """

    def __init__(self, column: "DataColumn"):
        config = PlotConfig(
            title=f"Box Plot for column {column.name}",
            yaxis_title=column.name,
            filepath=column.parent.resource_path + f"/plots/box_plot_{column.name}.png",
        )
        super().__init__(config)
        self.column = column

    def get(self) -> go.Figure:
        self._fig = px.box(self.column.get_data())
        return super().get()
