"""
Lazy calculation of Correlations given a data file.
"""

from dataclasses import dataclass
import typing

import numpy as np
import pandas as pd

if typing.TYPE_CHECKING:
    from ..objects.datafile import DataFile


@dataclass
class Correlation:
    """
    Represents correlations for a datafile using a particular method.
    """

    datafile: "DataFile"
    method: str

    _correlations: pd.DataFrame = None

    def calculate_correlations(self):
        """
        Calculates and returns the correlations for the datafile using the specified method.
        """
        if self._correlations is None:
            self._correlations = self.datafile.get_dataframe().corr(
                method=self.method, numeric_only=True
            )
        return self._correlations

    def get_interesting_correlations(
        self, min_r: float = 0.5, top_n: int = 5
    ) -> dict[tuple[str, str], float]:
        """
        Returns the top n interesting correlations between columns in the datafile based on the
        correlation coefficient (Pearson's r), where the absolute value of the coefficient is above
        the minimum threshold.

        Args:
            min_r (float): The minimum absolute value of the correlation coefficient to consider.
                Default is 0.5.
            top_n (int): The maximum number of correlations to return. Default is 5.

        Returns:
            Dict[Tuple[str, str], float]: A dictionary of the top n correlations and their values.
        """
        corr = self.calculate_correlations()

        # Set the diagonal and lower triangle to NaN to avoid duplicates
        corr.values[np.tril_indices_from(corr)] = np.nan

        # Replace values below the threshold with NaN
        corr = corr.where(abs(corr) > min_r)

        # Drop any rows and columns that contain only NaNs
        corr = corr.dropna(how="all").dropna(axis=1, how="all")

        # Get the n largest correlations by absolute value
        result = corr.unstack().sort_values(key=abs, ascending=False)[:top_n]

        # Convert the Series to a dictionary of tuples
        result_dict = result.to_dict()

        # Remove any entries with NaN values
        result_dict = {(k[0], k[1]): v for k, v in result_dict.items() if pd.notnull(v)}

        return result_dict
