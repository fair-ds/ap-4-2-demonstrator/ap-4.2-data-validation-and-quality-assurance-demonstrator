"""
This file contains the Index class, which is used to generate the index.html page for the
FAIR-DS-AP42 HTML site. The index.html page contains an overview of the FAIR-DS-AP42
results, including the metadata and schema assignment for each data file.
"""

from ..state.result_parser import ResultParser
from .html_page import HTMLPage


class Index(HTMLPage):
    """
    A Class used to generate the index.html page for the FAIR-DS-AP42 HTML site.
    """

    def __init__(self, results: ResultParser):
        super().__init__()
        self.results = results
        self.template_name = "index.html.j2"
        self.output_filename = "index.html"

    def generate(self, *args, **kwargs) -> None:
        super().generate(
            metadata=self.results.full_results,
            schema_assignment=self.results.schema_assignment,
        )
