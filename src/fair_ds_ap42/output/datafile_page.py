"""
This file contains the DatafilePage class, which is used to generate an HTML page for a data file.
The HTML page contains information about the data file, including its field descriptions, plots,
and metadata. The DatafilePage class inherits from the HTMLPage class, which provides common
functionality for generating HTML pages.
"""

import os
from pathlib import Path

from .html_page import HTMLPage


class DatafilePage(HTMLPage):
    """
    A Class used to generate an HTML page for a data file.

    Attributes:
        json_data (dict): The datafile's JSON data
        schema_assignment (dict): The schema assignment for the datafile
        template_name (str): The name of the Jinja2 template file
        output_filename (str): The name of the output HTML file
    """

    def __init__(self, json_data: dict, schema_assignment: dict):
        super().__init__()
        self.json_data = json_data
        self.schema_assignment = schema_assignment
        self.template_name = "datafile.html.j2"
        self.output_filename = self.html_path

    @property
    def file_path(self) -> str:
        """
        The Filepath of the datafile.
        """
        return self.json_data["filepath"]

    @property
    def relative_path_to_root(self) -> str:
        """
        The path of this file relative to the root of the HTML site.

        Used to generate relative links to static files.
        """
        static_dir = "./"
        html_dir = os.path.dirname(self.html_path)
        relative_path = os.path.relpath(static_dir, start=html_dir)
        return relative_path.replace("\\", "/")

    @property
    def field_descriptions(self) -> dict:
        """
        Get a dictionary of field descriptions for the datafile

        Returns:
            dict: A dictionary of field descriptions
        """
        return self.json_data["field_descriptions"]

    @property
    def plots(self) -> dict:
        """
        Get a dictionary of plots for the datafile

        Returns:
            dict: A dictionary of plots
        """
        return self.json_data["plots"]

    @property
    def field_plots(self) -> dict:
        """
        Get a dictionary of field plots for the datafile

        Returns:
            dict: A dictionary of field plots
        """
        return self.json_data["field_plots"]

    @property
    def html_path(self) -> str:
        """
        Returns the path to the HTML file corresponding to the data file.

        Returns:
            str: The path to the HTML file.
        """
        path = Path(self.json_data["filepath"])
        new_path = path.with_suffix("").joinpath("index.html")
        return str(new_path)

    def generate(self, *args, **kwargs) -> None:
        super().generate(
            relative_path_to_root=self.relative_path_to_root,
            schema_assignment=self.schema_assignment,
            file_path=self.file_path,
            metadata=self.json_data,
            missingness_plot=self.plots.get("missingness"),
            correlation_plots=self.plots.get("corr_heatmaps"),
            field_correlations=self.plots.get("correlations"),
            field_descriptions=self.field_descriptions,
            field_plots=self.field_plots,
        )
