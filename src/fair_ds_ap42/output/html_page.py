"""
This file contains the HTMLPage class, which is used to generate HTML pages for the
FAIR-DS-AP42. This class is the parent class for all other HTML page classes, and
provides common functionality for generating HTML pages.
"""

from pathlib import Path
from importlib import resources

from jinja2 import Environment, FileSystemLoader


class HTMLPage:
    """
    A Class used to generate HTML pages for the FAIR-DS-AP42.

    Attributes:
        jinja_env (Environment): The Jinja2 environment
        template_name (str): The name of the Jinja2 template file
        output_filename (str): The name of the output HTML file
    """

    output_directory = Path("output")
    template_directory = resources.files("fair_ds_ap42") / "output/templates"

    def __init__(self):
        self.jinja_env = Environment(
            loader=FileSystemLoader(self.template_directory),
            autoescape=True,
        )
        self.template_name = ""
        self.output_filename = ""

    @property
    def output_filepath(self) -> Path:
        """
        Get the output file path by concatenating the output directory and the output filename.

        Returns:
            Path: The output file path
        """
        return self.output_directory / self.output_filename

    def generate(self, *args, **kwargs) -> None:
        """
        Genarate the HTML page using the Jinja2 template and the provided arguments.

        Will write the generated HTML to the pathe specified by the output_filepath attribute.
        All args are passed to the Jinja2 template.render() method.

        Args:
            *args: Variable length argument list
            **kwargs: Arbitrary keyword arguments

        Returns:
            None
        """
        self.output_directory.mkdir(exist_ok=True)

        template = self.jinja_env.get_template(self.template_name)

        html = template.render(*args, **kwargs)

        # Create the folder if the it doesn't exist
        self.output_filepath.parent.mkdir(parents=True, exist_ok=True)

        with open(self.output_filepath, "w", encoding="utf-8") as file_obj:
            file_obj.write(html)
