"""
stoplight_file.py

This module contains the StoplightFile class, which is used to load and parse the results of a
validation run in order to generate the "stoplight" summary of the validation results.
"""

import json


class StoplightFile:
    """
    Class to load and parse the results of a validation run in order to generate the "stoplight"
    summary of the validation results.
    """

    @classmethod
    def from_file(cls, file_path: str):
        """
        Class method to load a StoplightFile from a file.
        """
        with open(file_path, "r", encoding="utf-8") as file:
            return cls(json.load(file))

    def __init__(self, results: dict):
        # Iterate over the results to load the file by file summary

        self.summary = {}
        for datafile_results in results[0]["data_files"]:
            self.summary[datafile_results["filepath"]] = datafile_results[
                "validation_status"
            ]
