"""
Base class for singleton objects. Based on the example found at
https://refactoring.guru/design-patterns/singleton/python/example#example-0

The SingletonMeta class is a metaclass that provides the singleton pattern, allowing only one
instance of a class to exist at a time.

Attributes:

    _instances (class variable): A dictionary that keeps track of the single instance of each
    class.

Methods:

    __call__(*args, **kwargs):
        This method is called whenever an instance of a class is created. It first checks if an
        instance of the class already exists, and if not, it creates a new instance of the class
        and stores it in the _instances dictionary for future reference. It then returns the
        existing or newly created instance.

    clear():
        This method clears the instance of a class from the _instances dictionary, allowing a
        new instance to be created the next time the class is called.

Usage:
To use this metaclass, simply include it as the metaclass in your class definition:

class MyClass(metaclass=SingletonMeta):
    ...

This will ensure that only one instance of MyClass can exist at a time, regardless of how many
times the class is instantiated. To clear the instance of MyClass and allow a new one to be
created, simply call MyClass.clear().
"""


class SingletonMeta(type):
    """Class that provides the singleton pattern"""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

    def clear(cls):
        """
        Clears the cached instance of the Singleton class.

        This method removes the current instance of the Singleton class from the
        cache, allowing a new instance to be created when the class is next called.
        If the class has not yet been instantiated, this method will have no effect.

        Returns:
            None
        """
        try:
            del SingletonMeta._instances[cls]
        except KeyError:
            pass
