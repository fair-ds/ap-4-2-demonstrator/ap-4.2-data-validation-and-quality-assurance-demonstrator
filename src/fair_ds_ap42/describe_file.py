"""Collect descriptive statistics about a given file.

Iterates over the columns of a given CSV file and calculates some simple
descriptive statistics for each. Any particuarly notable issues, like columns
entirely/mostly missing or having single values will be noted and added to a
report XML.

Args:
    filename: The path of the file we are going to validate. This file must
        exist.
    report_filename: The path of the xml report to produce. If this file does
        not already exist, it will be created.
"""

import argparse
import json
import logging
import time

from .register_sources import register_all_sources
from .state.current_state import CurrentState
from .utils.logging import setup_logging
from .utils.file_io import find_non_serializable_values
from .settings import settings

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(description="Describe/Visualize a file")

    parser.add_argument("--n_processes", type=int, default=8)
    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def get_data_descriptions() -> None:
    """
    For all files which are flagged as "to_process" in the state file,
    get file and field descriptions for them.
    """
    state = CurrentState.load()

    all_descriptions = {}
    for data_file in state.schema_assignment.data_files:
        all_descriptions[data_file.filepath] = {
            "file_info": data_file.description,
            "fields": data_file.field_descriptions,
        }

    try:
        with open("out/desc.json", "w", encoding="utf-8") as file_obj:
            json.dump(all_descriptions, file_obj)
    except TypeError as exc:
        logger.error("Could not serialize data: %s", exc)
        logger.error(
            "Non-serializable values: %s",
            find_non_serializable_values(all_descriptions),
        )
        raise exc

    testsuites = state.xml.filter("data_quality")
    testsuites.to_file(settings.directories.xml_report)

    state.save()


if __name__ == "__main__":
    # set up logging
    setup_logging(file_logger=f"describe_file_{int(time.time())}.log")
    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    # Parse command line args
    args = _parse_cli()

    if args.debug:
        args.n_processes = 1

    register_all_sources()
    get_data_descriptions()
