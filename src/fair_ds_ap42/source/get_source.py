"""
Helper function to determine where a file exists.
"""

from pathlib import Path

from ..settings import settings
from .isource import ISource
from . import LocalDirectory, VirtualSource, S3Bucket
from ..utils.s3_utils import file_exists_s3
from .available_sources import find_file


def get_source(filepath: Path | str) -> "ISource":
    """
    Given a filepath, attempts to locate where that file exists.

    Args:
        filepath (Union[str, Path]):
            The filepath to search for

    Returns:
        Source
            If the file can be found on the local system, returns a
            LocalDirectory. If it is not local, but can be found on an S3 bucket
            using provided credentials, then returns an S3 Source. If we can't
            locate the file at all, we return a VirtualSource.
    """
    # Check registered sources first
    try:
        registered_source = find_file(filepath)
        if registered_source:
            return registered_source
    except FileNotFoundError:
        # We are allowed to fail here
        pass

    if Path(filepath).exists():
        return LocalDirectory()

    if settings.storage.s3.credentials_provided and file_exists_s3(filepath):
        return get_s3_bucket_from_settings()

    return VirtualSource()


def get_s3_bucket_from_settings():
    """
    Returns the S3 bucket name from the settings.

    Returns:
        str
            The S3 bucket name.
    """
    if settings.storage.s3.credentials_provided:
        return S3Bucket(
            bucket_name=settings.storage.s3.bucket,
            endpoint_url=settings.storage.s3.endpoint_url,
        )

    return None
