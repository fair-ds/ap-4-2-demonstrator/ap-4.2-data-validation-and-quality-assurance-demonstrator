"""
Interface for readers that need to implement methods for reading JSON data.
"""

from abc import ABC, abstractmethod


class IJSONDataReader(ABC):
    """
    Interface for readers that need to implement methods for reading JSON data.
    """
    @property
    def format(self) -> str:
        """
        Returns the format of the data that the reader reads.
        """
        return "json"

    @abstractmethod
    def read_dict(self) -> dict:
        """
        Reads the data from a file into a dictionary

        Args:
            filepath (str): The path to the file to read

        Returns:
            dict: The data from the file as a dictionary
        """
        raise NotImplementedError("This method must be implemented by a subclass")
