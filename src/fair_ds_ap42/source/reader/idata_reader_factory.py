"""
Abstract factory for creating IDataReader objects

This is used to create the appropriate IDataReader object for a given file path.
"""

from abc import ABC, abstractmethod
from pathlib import Path

from .idata_reader import IDataReader


class IDataReaderFactory(ABC):
    """
    Abstract factory for creating IDataReader objects
    """

    @abstractmethod
    def create_reader(self, file_path: str) -> IDataReader:
        """
        Creates an IDataReader object for the given file path

        Args:
            file_path (str): The path to the file

        Returns:
            IDataReader: The appropriate IDataReader object for the file
        """
        raise NotImplementedError("create_reader() must be implemented by subclasses")

    def _get_file_format_by_ext(self, file_path: str):
        """
        Get the correct file format based on the file extension
        """
        suffixes = Path(file_path).suffixes

        if len(suffixes) == 0:
            raise ValueError(f"File {file_path} has no suffix")

        if ".csv" in suffixes:
            return "csv"

        if ".tsv" in suffixes:
            return "tsv"

        if ".parquet" in suffixes:
            return "parquet"

        if ".json" in suffixes:
            return "json"

        raise NotImplementedError(f"File {file_path} is not a supported format")
