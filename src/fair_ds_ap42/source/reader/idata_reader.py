"""
Abstract class for data readers.

This class is used to define the interface for data readers. It is used to ensure that all data
readers have the same interface, and can be used interchangeably.

Attributes:
    None

Methods:
    read_data: Reads the data from a file into a pandas dataframe
    read_column: Reads a single column from a file into a pandas series

"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
import datetime


@dataclass(kw_only=True)
class IDataReader(ABC):
    """
    Abstract class for data readers.
    """

    filepath: str

    delimiter: str = ","
    encoding: str = "utf-8"

    @property
    @abstractmethod
    def format(self) -> str:
        """
        The format of this data reader.
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    @property
    @abstractmethod
    def source(self) -> str:
        """
        The source for this data reader.
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    @abstractmethod
    def get_size_in_bytes(self) -> int:
        """
        Gets the size of a file in bytes.

        Args:
            filepath (str): The path to the file to read

        Returns:
            int: The size of the file in bytes
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    def get_date_last_modified(self) -> datetime.datetime:
        """
        Gets the date that a file was last modified.

        Args:
            filepath (str): The path to the file to read

        Returns:
            datetime.datetime: The date that the file was last modified
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    def exists(self) -> bool:
        """
        Checks if a file exists.

        Args:
            filepath (str): The path to the file to read

        Returns:
            bool: True if the file exists, False otherwise
        """
        raise NotImplementedError("This method must be implemented by a subclass")
