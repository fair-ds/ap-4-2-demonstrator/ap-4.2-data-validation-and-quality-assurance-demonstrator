"""
Interface for readers that need to implement methods for reading tabular data.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass

import pandas as pd


@dataclass(kw_only=True)
class ITabularDataReader(ABC):
    """
    Interface for readers that need to implement methods for reading tabular data.
    """

    delimiter: str = ","

    @abstractmethod
    def read_data(self) -> pd.DataFrame:
        """
        Reads the data from a file into a pandas dataframe.

        Args:
            filepath (str): The path to the file to read

        Returns:
            pd.DataFrame: The data from the file as a pandas dataframe
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    @abstractmethod
    def read_column(self, column_name: str) -> pd.Series:
        """
        Reads a single column from a file into a pandas series.

        Args:
            filepath (str): The path to the file to read
            column_name (str): The name of the column to read

        Returns:
            pd.Series: The data from the column as a pandas series
        """
        raise NotImplementedError("This method must be implemented by a subclass")

    @abstractmethod
    def get_fields(self) -> list[str]:
        """
        Get the fields from a file.

        Args:
            filepath (str): The path to the file to read

        Returns:
            list[str]: The fields from the file
        """
        raise NotImplementedError("This method must be implemented by a subclass")
