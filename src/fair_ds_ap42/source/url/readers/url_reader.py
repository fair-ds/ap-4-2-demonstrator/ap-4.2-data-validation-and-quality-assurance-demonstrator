"""
Class for reading data from a URL.

Can be instantiated directly, but is typically created by the URLReaderFactory.

It is expected that URLs will point to exactly one file, so we do not need to
specify a filename.
"""

from dataclasses import dataclass

from ...local_directory.readers.local_reader import LocalReader
from ..url_source import URLSource


@dataclass(kw_only=True)
class URLReader(LocalReader):
    """
    URL Reader is nearly identical to LocalReader, but when we instantiate the Reader we
    download the file from the URL to a local directory.
    """

    url_source: URLSource

    def __post_init__(self):
        self.url_source.retrieve_file()
