"""
Defines the URLSource class.

This class represents a file that exists on a remote server which can be accessed via a URL.

As a result, in order to implement any of the methods in this class, we need to download the file
to a temporary location on the local machine first. Beyond this initial step, this class is
nearly identical to the LocalDirectory class.

Major Exception to this: The URLSource class only refers to a single file, whereas the
LocalDirectory class refers to a directory.
"""

from dataclasses import dataclass
import logging
import time
import urllib.request
from urllib.parse import urlparse


from .. import LocalDirectory
from ...source.reader.idata_reader import IDataReader
from ..local_directory.readers import LocalReaderFactory

logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class URLSource(LocalDirectory):
    """
    Represents a remote file that can be accessed via a URL.

    Attributes:
        url: The URL of the file.
        location: The location of the file on the local machine.
    """

    url: str
    location: str

    reader_factory: LocalReaderFactory = None

    @property
    def source_type(self) -> str:
        """
        The type of the source as a string
        """
        return "url"

    @property
    def name(self) -> str:
        """
        The name of the source as a string
        """
        return "url"

    @property
    def sort_rank(self):
        """
        The sort rank of the source.
        """
        return 3

    @property
    def filename(self) -> str:
        """
        Get the filename of the file.
        """
        parsed_url = urlparse(self.url)
        return parsed_url.path.split("/")[-1]

    @property
    def full_filepath(self) -> str:
        """
        Get the full filepath of the file.
        """
        return self.location + "/" + self.filename

    def __post_init__(self):
        self.reader_factory = LocalReaderFactory()

    def __str__(self) -> str:
        return "url"

    def get_reader(self, filepath: str = None, filetype: str = None) -> IDataReader:
        """
        Gets a reader for the file.

        Args:
            filepath (str): The filepath of the file to read. This is not used for URLSource.
            filetype (str): The type of file to read.

        Returns:
            IDataReader: The reader for the file.
        """
        if filepath is not None:
            logger.warning("filepath is not used for URLSource")

        # If the file doesn't yet exist, we need to download it
        if not self.exists(self.full_filepath):
            self.retrieve_file()

        reader = self.reader_factory.create_reader(self.full_filepath, filetype)
        return reader

    def retrieve_file(self) -> None:
        """
        Retrieves the file from the URL and stores it in the data directory.

        Args:
            None

        Returns:
            None
        """
        if self.exists(self.full_filepath):
            logger.info("%s already exists, not downloading again", self.full_filepath)
            return

        logger.info("Retrieving file from %s to %s", self.url, self.full_filepath)
        start_time = time.time()
        local_filename, response = urllib.request.urlretrieve(
            self.url, self.full_filepath
        )
        logger.debug("local_filename: %s", local_filename)
        logger.debug("response: %s", response)

        logger.info("Retrieved file in %.2f seconds", time.time() - start_time)

    def list_files(self, pattern: str = None) -> list[str]:
        """
        Get a list of all of the files in the source.

        This method is only implemented as a way to satisfy the ISource interface. The returned
        list will only ever contain a single file, which is the file that was downloaded from the
        URL.
        """
        if pattern is not None:
            logger.warning("pattern is not used for URLSource")

        return [self.filename]

    def write_json(self, filepath: str, data: dict) -> None:
        """
        Write a dictionary to a JSON file in the source.

        This method is not implemented for URLSource because it is not possible to write to a URL.
        """
        raise NotImplementedError("write_json is not implemented for URLSource")
