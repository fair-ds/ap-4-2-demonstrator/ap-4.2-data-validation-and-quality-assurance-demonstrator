"""
Factory for creating sources

Given basic information about a source, this factory will create the appropriate
source object.

Example usage:
    >>> from fair_ds_ap42.source import SourceFactory
    >>> source = SourceFactory.from_dict(
    ...     type="local_directory",
    ...     location="./my_dir"
    ... )
"""

from .isource import ISource

from . import LocalDirectory, S3Bucket, URLSource


class SourceFactory:
    """
    Factory Class for creating sources from dictionaries
    """

    @classmethod
    def from_dict(cls, **kwargs) -> ISource:
        """Creates a source from a dictionary"""
        if "type" not in kwargs:
            raise ValueError("Source type must be specified")

        # Store the type in a variable and remove it from the dictionary
        my_type = kwargs["type"]
        kwargs.pop("type")

        if my_type == "local_directory":
            return LocalDirectory(**kwargs)

        if my_type == "s3_bucket":
            return S3Bucket(**kwargs)

        if my_type == "url":
            return URLSource(**kwargs)

        raise ValueError("Invalid source type: {my_type}")
