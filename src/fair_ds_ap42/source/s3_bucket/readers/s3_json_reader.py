"""
Class used to read data from an S3 JSON file.

Can be instantiated directly, but is typically created by the S3ReaderFactory.
"""

from dataclasses import dataclass
import json

from .s3_reader import S3Reader
from ...reader.ijson_data_reader import IJSONDataReader


@dataclass(kw_only=True)
class S3JSONReader(S3Reader, IJSONDataReader):
    """
    Class used to read data from an S3 JSON file.
    """

    @property
    def format(self) -> str:
        return "json"

    def read_dict(self) -> dict:
        return json.loads(
            self.s3_bucket.get_contents(self.filepath, encoding=self.encoding)
        )
