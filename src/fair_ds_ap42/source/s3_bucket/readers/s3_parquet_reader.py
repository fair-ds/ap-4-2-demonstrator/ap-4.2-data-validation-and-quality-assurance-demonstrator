"""
Class used to read data from an S3 Parquet file.

Not intended to be instantiated directly, but rather to be created by the S3ReaderFactory.
"""

from dataclasses import dataclass
from io import BytesIO

import pandas as pd

from .s3_reader import S3Reader
from ...reader.itabular_data_reader import ITabularDataReader


@dataclass(kw_only=True)
class S3ParquetReader(S3Reader, ITabularDataReader):
    """
    Class used to read data from an S3 Parquet file.
    """

    @property
    def format(self) -> str:
        return "parquet"

    def read_data(self) -> pd.DataFrame:
        return pd.read_parquet(BytesIO(self.s3_bucket.get_bytes(self.filepath)))

    def read_column(self, column_name: str) -> pd.Series:
        return self.read_data()[column_name]

    def get_fields(self) -> list[str]:
        return list(self.read_data().columns)
