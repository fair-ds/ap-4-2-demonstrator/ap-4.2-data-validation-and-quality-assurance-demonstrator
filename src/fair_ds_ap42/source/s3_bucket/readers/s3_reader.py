"""
Base class for all S3 readers.
"""

from dataclasses import dataclass
import datetime
from typing import TYPE_CHECKING

from ...reader.idata_reader import IDataReader

if TYPE_CHECKING:
    from ..s3_bucket import S3Bucket


@dataclass(kw_only=True)
class S3Reader(IDataReader):
    """
    Base class containing shared functionality for all S3 readers.
    """

    s3_bucket: "S3Bucket"

    def get_size_in_bytes(self) -> int:
        """
        Gets the size of a file in bytes.

        Args:
            filepath (str): The path to the file to read

        Returns:
            int: The size of the file in bytes
        """
        return self.s3_bucket.get_size_in_bytes(self.filepath)

    def get_date_last_modified(self) -> datetime:
        """
        Gets the date that a file was last modified.

        Args:
            filepath (str): The path to the file to read

        Returns:
            datetime.datetime: The date that the file was last modified
        """
        return self.s3_bucket.get_date_last_modified(self.filepath)

    def exists(self) -> bool:
        """
        Checks if a file exists.

        Args:
            filepath (str): The path to the file to read

        Returns:
            bool: True if the file exists, False otherwise
        """
        return self.s3_bucket.exists(self.filepath)

    @property
    def source(self) -> str:
        """
        The source for this data reader.
        """
        return "s3"
