"""
Class used to read data from a CSV file in an S3 bucket.

Can be instantiated directly, but is typically created by the S3ReaderFactory.

Will cache the data after the first read, so subsequent reads will be faster.
"""

from dataclasses import dataclass
from io import StringIO
import logging

import pandas as pd

from .s3_reader import S3Reader
from ...reader.itabular_data_reader import ITabularDataReader

logger = logging.getLogger(__name__)


@dataclass(kw_only=True)
class S3CSVReader(S3Reader, ITabularDataReader):
    """
    Class used to read data from a CSV file in an S3 bucket.
    """

    _cached_data: pd.DataFrame = None

    @property
    def format(self) -> str:
        return "csv"

    def read_data(self) -> pd.DataFrame:
        if self._cached_data is not None:
            logger.debug("Returning cached data")
            return self._cached_data

        csv_string = StringIO(self.s3_bucket.get_contents(self.filepath, self.encoding))
        self._cached_data = pd.read_csv(csv_string, delimiter=self.delimiter)
        return self._cached_data

    def read_column(self, column_name: str) -> pd.Series:
        return self.read_data()[column_name]

    def get_fields(self) -> list[str]:
        first_row = self.s3_bucket.get_line_at_nth_position(
            filepath=self.filepath, line_number=0
        )
        return first_row.split(self.delimiter)
