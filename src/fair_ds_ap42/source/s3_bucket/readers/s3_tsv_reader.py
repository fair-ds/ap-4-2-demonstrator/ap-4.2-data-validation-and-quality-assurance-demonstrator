"""
Class used to read data from an S3 TSV file.

Can be instantiated directly, but is typically created by the S3ReaderFactory.
"""

from dataclasses import dataclass

from .s3_csv_reader import S3CSVReader


@dataclass(kw_only=True)
class S3TSVReader(S3CSVReader):
    """
    Class used to read data from an S3 TSV file.
    """

    delimiter: str = "\t"
