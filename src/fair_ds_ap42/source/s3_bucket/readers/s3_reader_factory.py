"""
Reader Factory for files stored in S3

This is used to create the appropriate IDataReader object for a given file path.
"""

from dataclasses import dataclass
from typing import TYPE_CHECKING

from ...reader.idata_reader_factory import IDataReaderFactory


from .s3_csv_reader import S3CSVReader
from .s3_json_reader import S3JSONReader
from .s3_tsv_reader import S3TSVReader
from .s3_parquet_reader import S3ParquetReader

from ....settings import settings

if TYPE_CHECKING:
    from ..s3_bucket import S3Bucket


@dataclass
class S3ReaderFactory(IDataReaderFactory):
    """
    Reader Factory for files stored in S3
    """

    s3_bucket: "S3Bucket"

    delimiter: str = ","
    encoding: str = "utf-8"

    def create_reader(self, file_path: str, file_format: str = None):
        if file_format is None:
            file_format = self._get_file_format_by_ext(file_path)

        # Get file delimiter and encoding from settings
        file_settings = settings.per_file.get_file_settings(file_path)

        if file_format == "csv":
            return S3CSVReader(
                filepath=file_path,
                s3_bucket=self.s3_bucket,
                delimiter=file_settings.delimiter,
                encoding=file_settings.encoding,
            )

        if file_format == "json":
            return S3JSONReader(
                filepath=file_path, s3_bucket=self.s3_bucket, encoding=self.encoding
            )

        if file_format == "tsv":
            return S3TSVReader(filepath=file_path, s3_bucket=self.s3_bucket)

        if file_format == "parquet":
            return S3ParquetReader(filepath=file_path, s3_bucket=self.s3_bucket)

        raise NotImplementedError(f"File format {file_format} not supported")
