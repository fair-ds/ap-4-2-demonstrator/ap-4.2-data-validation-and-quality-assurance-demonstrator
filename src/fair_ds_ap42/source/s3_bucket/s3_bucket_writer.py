"""
Class used to write various data types to an S3 bucket.

Usage:
    >>> from fair_ds_ap42.source.s3_bucket import S3Bucket
    >>> from fair_ds_ap42.source.s3_bucket import S3BucketWriter
    >>> s3_bucket = S3Bucket("my-bucket")
    >>> writer = S3BucketWriter(s3_bucket)
    >>> writer.write("data.txt", "Hello World!")
    >>> writer.write_json("data.json", {"key": "value"})
"""

import json

from .s3_bucket import S3Bucket
from ..writer.idata_writer import IDataWriter


class S3BucketWriter(IDataWriter):
    """
    Class used to write various data types to an S3 bucket.
    """

    def __init__(self, s3_bucket: S3Bucket):
        self.s3_bucket = s3_bucket

    def write(self, filepath: str, data: object) -> None:
        self.s3_bucket.client.put_object(
            Body=data, Bucket=self.s3_bucket.bucket_name, Key=filepath
        )

    def write_json(self, filepath: str, data: dict) -> None:
        self.s3_bucket.client.put_object(
            Body=json.dumps(data), Bucket=self.s3_bucket.bucket_name, Key=filepath
        )
