"""
Defines the S3Bucket class

This class is used to interact with files stored in an S3 bucket
"""

from dataclasses import dataclass
import json
import logging
import re

import boto3
from botocore.exceptions import ClientError, NoCredentialsError

from ..isource import ISource
from ..reader.idata_reader import IDataReader
from .readers import S3ReaderFactory


logger = logging.getLogger(__name__)


@dataclass
class S3Bucket(ISource):
    """
    Represents an S3 bucket
    """

    bucket_name: str
    endpoint_url: str = None
    prefix: str = None

    reader_factory: S3ReaderFactory = None

    def __str__(self):
        return "s3"

    def __post_init__(self):
        if self.reader_factory is None:
            self.reader_factory = S3ReaderFactory(self)
        if self.prefix is None:
            self.prefix = ""

    @property
    def sort_rank(self):
        return 2

    @property
    def name(self):
        """The type of source this is as a string object"""
        return "s3"

    @property
    def client(self):
        """S3 client for this bucket"""
        return boto3.client("s3", endpoint_url=self.endpoint_url)

    @property
    def accessible(self):
        """Check if the bucket is accessible"""
        try:
            self.client.list_buckets()
            logger.info("Successfully connected to S3 bucket %s", self.bucket_name)
            return True
        except (ClientError, NoCredentialsError) as err:
            logger.info("Could not connect to S3 bucket %s", self.bucket_name)
            logger.error(err)
            return False

    def get_reader(self, filepath: str, file_format: str = None) -> IDataReader:
        reader = self.reader_factory.create_reader(filepath, file_format)
        return reader

    def exists(self, filepath: str):
        try:
            self.client.head_object(Bucket=self.bucket_name, Key=filepath)
            return True
        except ClientError:
            return False

    def get_size_in_bytes(self, filepath: str):
        response = self.client.head_object(Bucket=self.bucket_name, Key=filepath)
        return response["ContentLength"]

    def get_date_last_modified(self, filepath: str):
        response = self.client.head_object(Bucket=self.bucket_name, Key=filepath)
        return response["LastModified"]

    def get_bytes(self, filepath: str) -> bytes:
        """Get the size of a file in bytes"""
        logger.debug("Getting bytes from %s in %s", filepath, self.bucket_name)
        obj = self.client.get_object(Bucket=self.bucket_name, Key=filepath)
        return obj["Body"].read()

    def get_contents(self, filepath: str, encoding="utf-8") -> str:
        logger.debug("Getting contents of %s from %s", filepath, self.bucket_name)
        return self.get_bytes(filepath).decode(encoding)

    def list_files(self, pattern: str = None) -> list[str]:
        # Use the paginator to get all objects in the bucket
        paginator = self.client.get_paginator("list_objects_v2")
        pages = paginator.paginate(Bucket=self.bucket_name, Prefix=self.prefix)

        # Get the keys from the pages
        keys = []
        for page in pages:
            if "Contents" in page:
                for obj in page["Contents"]:
                    keys.append(obj["Key"])

        # Filter the keys if a regex pattern is provided
        if pattern:
            keys = [key for key in keys if re.match(pattern, key)]

        return keys

    def preprocessing(self, filepath) -> None:
        return

    def get_line_at_nth_position(
        self, filepath: str, line_number: int, encoding="utf-8"
    ) -> str:
        """
        Get the line at the nth position in a file

        Args:
            filepath (str): The path to the file
            line_number (int): The line number to get
            encoding (str, optional): The encoding of the file. Defaults to "utf-8".

        Raises:
            IndexError: If the line number is not found in the file

        Returns:
            str: The line at the nth position in the file
        """
        body = self.client.get_object(Bucket=self.bucket_name, Key=filepath)["Body"]
        for i, line in enumerate(body.iter_lines()):
            if i == line_number:
                return line.decode(encoding)

        raise IndexError(f"Could not find line {line_number} in {filepath}")

    def to_json(self) -> dict:
        return {
            "type": "s3_bucket",
            "bucket_name": self.bucket_name,
        }

    def write_json(self, filepath: str, data: dict) -> None:
        """
        Writes the data to a file in the S3 bucket.

        Args:
            filepath (str): The filepath to write to.
            data (dict): The data to write.
        """
        # Write the data to a json file in the bucket
        self.client.put_object(
            Bucket=self.bucket_name,
            Key=filepath,
            Body=json.dumps(data),
        )
