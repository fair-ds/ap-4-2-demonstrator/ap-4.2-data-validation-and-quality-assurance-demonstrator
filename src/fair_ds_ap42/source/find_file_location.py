"""
For a given file path, check to see if the file:
- exists locally
    if so, reuturn the factory object for a local file
- if the user has provided S3 credentials, check to see if the file:
    - exists in S3
        if so, return the factory object for an S3 file

If new locations are added, they will need to be appended to the list of locations to check
"""
from pathlib import Path

from ..source import S3Bucket

from .reader.idata_reader import IDataReader
from .local_directory.readers.local_reader_factory import LocalReaderFactory
from .s3_bucket.readers.s3_reader_factory import S3ReaderFactory


def find_file_location(file_path: str, s3_bucket: S3Bucket = None) -> IDataReader:
    """
    Attempts to find the location of a file, and returns the appropriate reader object

    Args:
        file_path (str): The path to the file
        s3_bucket (S3Bucket, optional): The S3Bucket object to use for checking S3.

    Returns:
        IDataReader: The appropriate reader object for the file

    Raises:
        FileNotFoundError: If the file is not found in any of the locations
    """
    file_path = Path(file_path).as_posix()

    if Path(file_path).exists():
        reader = LocalReaderFactory()
        return reader.create_reader(file_path=file_path)

    if s3_bucket is not None and s3_bucket.exists(file_path):
        reader = S3ReaderFactory(s3_bucket=s3_bucket)
        return reader.create_reader(file_path=file_path)

    raise FileNotFoundError(f"File {file_path} not found")
