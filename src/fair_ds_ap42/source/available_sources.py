"""
Serves as a location where we can collect Source objects.

This is useful for when we want to iterate over all sources, or when we want to
find a specific source or a file in a specific source.
"""

from .isource import ISource

from .source_registry import SourceRegistry

# Create a global SourceRegistry object
source_registry = SourceRegistry()


def get_registered_sources() -> list[ISource]:
    """
    Get a list of all sources which have been registered so far.

    Returns:
        list[ISource]
            A list of all registered sources.
    """
    return source_registry.get_registered_sources()


def register_source(source: ISource):
    """
    Registers a source with the AVAILABLE_SOURCES list.

    Args:
        source (ISource):
            The source to register
    """
    source_registry.register_source(source)


def find_file(filepath: str, sources: list[ISource] = None) -> ISource:
    """
    Check all available sources, returning the first source that contains the
    given filepath.

    Sources will be checked in the following order:
    - Local Directories
    - S3 Buckets

    Args:
        filepath (str):
            The filepath to search for

    Returns:
        ISource
            The source that contains the given filepath
    """
    if sources is None:
        sources = get_registered_sources()

    return source_registry.find_file(filepath, sources)
