"""
Helper Module for writing data to local directory

Not fancy, but keeps things like this in line with the other source types
"""

import json

from ..writer.idata_writer import IDataWriter


class LocalDirectoryWriter(IDataWriter):
    """
    Helper class for writing data to a local directory.
    """

    def write(self, filepath: str, data: object) -> None:
        with open(filepath, "w", encoding="utf-8") as file_obj:
            file_obj.write(data)

    def write_json(self, filepath: str, data: dict) -> None:
        with open(filepath, "w", encoding="utf-8") as file_obj:
            json.dump(data, file_obj)
