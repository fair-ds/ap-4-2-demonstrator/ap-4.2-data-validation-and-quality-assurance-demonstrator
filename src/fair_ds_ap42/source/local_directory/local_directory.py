"""
Defines the LocalDirectory class.

This class represents the local machine running this code.
"""

from dataclasses import dataclass, field
import datetime
import json
import logging
from pathlib import Path

from ...source.reader.idata_reader import IDataReader

from ..isource import ISource
from .readers import LocalReaderFactory


logger = logging.getLogger(__name__)


@dataclass
class LocalDirectory(ISource):
    """
    Represents the local machine running this code.
    """

    location: str = ""
    reader_factory: LocalReaderFactory = field(default_factory=LocalReaderFactory)

    def __str__(self) -> str:
        return f"local ({self.location})"

    def __post_init__(self):
        if self.location is None:
            logger.warning(
                "Accessing a LocalDirectory without a location is being deprecated."
            )

    @property
    def name(self) -> str:
        return "local"

    @property
    def sort_rank(self):
        return 1

    @property
    def source_type(self) -> str:
        """
        Get the type of this source as a string
        """
        return "local_directory"

    def is_child_of(self, other: "LocalDirectory") -> bool:
        """
        Check if the given source is a parent of this source in the local directory.

        Args:
            parent (ISource): The source to check.

        Returns:
            bool:
                True if the given source is a parent of this source in the local directory.
        """
        parent_path = Path(other.location)
        child_path = Path(self.location)

        try:
            relative_path = child_path.relative_to(parent_path)
            logger.debug("%s is a child of %s", child_path, parent_path)
            logger.debug("relative path: %s", relative_path)
            return True
        except ValueError:
            logger.debug("%s is not a child of %s", child_path, parent_path)
            return False

    def is_child_or_parent_of(self, other: "LocalDirectory") -> bool:
        """
        Check if the given source is either a parent or a child of this source

        Args:
            other (ISource): The source to check.

        Returns:
            bool:
                True if the given source is either a parent or a child of this source.
        """
        return self.is_child_of(other) or other.is_child_of(self)

    def get_reader(self, filepath: str, file_format: str = None) -> IDataReader:
        true_filepath = self._get_relative_path(filepath).as_posix()

        reader = self.reader_factory.create_reader(true_filepath, file_format)
        return reader

    def _get_relative_path(self, filepath: str) -> Path:
        # If the first directory in a path's parent is the same as the location, strip it
        if self.location != "" and str(filepath).startswith(self.location):
            filepath = Path(filepath).relative_to(self.location)

        return Path(self.location, filepath)

    def exists(self, filepath: str) -> bool:
        return self._get_relative_path(filepath).exists()

    def get_size_in_bytes(self, filepath: str) -> int:
        return self._get_relative_path(filepath).stat().st_size

    def get_date_last_modified(self, filepath: str) -> datetime.datetime:
        lm_timestamp = self._get_relative_path(filepath).stat().st_mtime
        return datetime.datetime.fromtimestamp(lm_timestamp)

    def get_contents(self, filepath: str) -> str:
        with open(self._get_relative_path(filepath), "r", encoding="utf-8") as file_obj:
            return file_obj.read()

    def list_files(self, pattern: str = None) -> list[str]:
        file_paths = [
            filepath.as_posix()
            for filepath in Path(self.location).rglob(pattern or "*")
            if filepath.is_file()
        ]
        return file_paths

    def preprocessing(self, filepath) -> None:
        raise NotImplementedError(f"preprocessing for local file {filepath}")

    def to_json(self) -> dict:
        return {
            "location": self.location,
            "type": self.source_type,
        }

    def write_json(self, filepath: str, data: dict) -> None:
        """
        Write the data to a file in the local directory.

        The filepath is relative to the local directory.

        Args:
            filepath (str): The filepath to write to.
            data (dict): The data to write.
        """

        # get the relative path given the location
        filepath = self._get_relative_path(filepath)

        # Ensure that all of the directories exist
        filepath.parent.mkdir(parents=True, exist_ok=True)

        # Write the data to the file
        with open(filepath, "w", encoding="utf-8") as file_obj:
            json.dump(data, file_obj)
