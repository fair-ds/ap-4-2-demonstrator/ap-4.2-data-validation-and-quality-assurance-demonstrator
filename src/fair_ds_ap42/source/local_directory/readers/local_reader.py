"""
Base class for all local readers.

Not intended to be instantiated directly, but rather to be used as a base class for
other local readers.
"""

from dataclasses import dataclass

import datetime
from pathlib import Path

from ...reader.idata_reader import IDataReader


@dataclass(kw_only=True)
class LocalReader(IDataReader):
    """
    Base class containing shared functionality for all local readers.
    """

    def get_size_in_bytes(self):
        return Path(self.filepath).stat().st_size

    def get_date_last_modified(self):
        return datetime.datetime.fromtimestamp(Path(self.filepath).stat().st_mtime)

    def exists(self) -> bool:
        return Path(self.filepath).exists()

    @property
    def source(self) -> str:
        return "local"
