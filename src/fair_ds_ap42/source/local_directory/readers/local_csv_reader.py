"""
Class used to read data from a local CSV file.

Can be instantiated directly, but is typically created by the LocalReaderFactory.
"""

from dataclasses import dataclass

import pandas as pd

from .local_reader import LocalReader
from ...reader.itabular_data_reader import ITabularDataReader


@dataclass(kw_only=True)
class LocalCSVReader(LocalReader, ITabularDataReader):
    """
    Class for reading data from a local CSV file.
    """

    @property
    def format(self) -> str:
        return "csv"

    def read_data(self):
        return pd.read_csv(
            self.filepath, delimiter=self.delimiter, encoding=self.encoding
        )

    def read_column(self, column_name):
        return pd.read_csv(
            self.filepath,
            delimiter=self.delimiter,
            encoding=self.encoding,
            usecols=[column_name],
        )[column_name]

    def get_fields(self):
        return list(
            pd.read_csv(
                self.filepath, delimiter=self.delimiter, encoding=self.encoding, nrows=0
            ).columns
        )
