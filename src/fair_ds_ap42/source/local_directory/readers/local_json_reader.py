"""
Class for reading data from a local JSON file.

Can be instantiated directly, but is typically created by the LocalReaderFactory.
"""

from dataclasses import dataclass
import json

from .local_reader import LocalReader
from ...reader.ijson_data_reader import IJSONDataReader


@dataclass(kw_only=True)
class LocalJSONReader(LocalReader, IJSONDataReader):
    """
    Class for reading data from a local JSON file.
    """

    @property
    def format(self) -> str:
        return "json"

    def read_dict(self) -> dict:
        with open(self.filepath, "r", encoding="utf-8") as file_obj:
            data = json.load(file_obj)

        return data
