"""
Reader factory for local files

This class is used to create the appropriate reader object for a local file
"""

from dataclasses import dataclass

from ...reader.idata_reader_factory import IDataReaderFactory

from .local_csv_reader import LocalCSVReader
from .local_json_reader import LocalJSONReader
from .local_tsv_reader import LocalTSVReader
from .local_parquet_reader import LocalParquetReader


@dataclass
class LocalReaderFactory(IDataReaderFactory):
    """
    Reader factory for local files
    """

    delimiter: str = None
    encoding: str = "utf-8"

    def create_reader(self, file_path: str, file_format: str = None):
        if file_format is None:
            file_format = self._get_file_format_by_ext(file_path)

        if file_format == "csv":
            return LocalCSVReader(
                filepath=file_path,
                delimiter=(self.delimiter if self.delimiter else ","),
                encoding=self.encoding,
            )

        if file_format == "json":
            return LocalJSONReader(filepath=file_path, encoding=self.encoding)

        if file_format == "tsv":
            return LocalTSVReader(
                filepath=file_path,
                delimiter=(self.delimiter if self.delimiter else "\t"),
                encoding=self.encoding,
            )

        if file_format == "parquet":
            return LocalParquetReader(
                filepath=file_path,
            )

        raise NotImplementedError(f"File format {file_format} not supported")
