"""
Class for reading parquet files from a local directory.

Can be instantiated directly, but is typically created by the LocalReaderFactory.
"""

from dataclasses import dataclass
import pandas as pd

from .local_reader import LocalReader
from ...reader.itabular_data_reader import ITabularDataReader


@dataclass(kw_only=True)
class LocalParquetReader(LocalReader, ITabularDataReader):
    """
    Class for reading parquet files from a local directory.
    """

    @property
    def format(self) -> str:
        return "parquet"

    def read_data(self) -> pd.DataFrame:
        return pd.read_parquet(self.filepath)

    def read_column(self, column_name: str) -> pd.Series:
        return pd.read_parquet(self.filepath, columns=[column_name])[column_name]

    def get_fields(
        self,
    ) -> list:
        return list(pd.read_parquet(self.filepath).columns)
