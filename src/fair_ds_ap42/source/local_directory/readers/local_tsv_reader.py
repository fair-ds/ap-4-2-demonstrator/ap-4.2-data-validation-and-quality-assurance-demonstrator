"""
Class for reading data from a local TSV file.

Can be instantiated directly, but is typically created by the LocalReaderFactory.
"""

from dataclasses import dataclass

from .local_csv_reader import LocalCSVReader


@dataclass(kw_only=True)
class LocalTSVReader(LocalCSVReader):
    """
    Class for reading data from a local TSV file.
    """

    delimiter = "\t"
