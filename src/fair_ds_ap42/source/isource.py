"""
Interface for a source of data. This could be a local file or a remote file.
"""

from abc import ABC, abstractmethod
import datetime
from functools import total_ordering
from pathlib import Path
from .reader.idata_reader import IDataReader


@total_ordering
class ISource(ABC):
    """Represents the physical location of the resource"""

    @property
    @abstractmethod
    def sort_rank(self):
        """The sort rank of this source. Lower numbers are higher priority."""
        raise NotImplementedError("sort_rank property not implemented in subclass.")

    def __lt__(self, other):
        return self.sort_rank < other.sort_rank

    def __eq__(self, other):
        return self.sort_rank == other.sort_rank

    @abstractmethod
    def __str__(self) -> str:
        """The name of this source"""

    @property
    @abstractmethod
    def name(self) -> str:
        """The name of this source"""

    @abstractmethod
    def get_reader(self, filepath: str, file_format: str = None) -> "IDataReader":
        """Gets a reader for a file from this source"""

    @abstractmethod
    def exists(self, filepath: Path | str) -> bool:
        """Returns True if the object can be found"""

    @abstractmethod
    def get_size_in_bytes(self, filepath: Path | str) -> int:
        """Gets the size of this object in bytes"""

    @abstractmethod
    def get_date_last_modified(self, filepath: Path | str) -> datetime.datetime:
        """Gets the date this object was last modified"""

    @abstractmethod
    def get_contents(self, filepath: Path | str) -> str:
        """Gets the contents of the file as a string"""

    @abstractmethod
    def list_files(self, pattern: str = None) -> list[str]:
        """List all files that match the given pattern"""

    @abstractmethod
    def preprocessing(self, filepath: Path | str) -> None:
        """Any preprocessing that needs to occur after initization"""

    @abstractmethod
    def to_json(self) -> dict:
        """Converts the source to a dictionary"""

    @abstractmethod
    def write_json(self, filepath: str, data: dict) -> None:
        """Writes the given dictionary to the given filepath in this source"""
