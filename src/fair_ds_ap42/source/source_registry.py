"""
This file contains the SourceRegistry class, which acts as a central object that holds
all available sources.

Usage:
```python
    source_registry = SourceRegistry()

    my_source = LocalDirectory("my_source", "/path/to/local/directory")
    source_registry.register_source(my_source)

    source_registry.get_registered_sources()
    >>> [my_source]

    source_registry.find_file(my_file_in_my_source)
    >>> my_source
```
"""

from .isource import ISource


class SourceRegistry:
    """
    This object acts as a central object that holds all available sources.

    This is useful for when we want to iterate over all sources, or when we want to
    find a file in a specific source.
    """

    def __init__(self):
        self.available_sources = []

    def get_registered_sources(self) -> list[ISource]:
        """
        Get a list of all sources which have been registered so far.

        Returns:
            list[ISource]
                A list of all registered sources.
        """
        return self.available_sources

    def register_source(self, source: ISource):
        """
        Registers a source with the available_sources list.

        Args:
            source (ISource):
                The source to register
        """
        self.available_sources.append(source)

    def find_file(self, filepath: str, sources: list[ISource] = None) -> ISource:
        """
        Check all available sources, returning the first source that contains the
        given filepath.

        Sources will be checked in the following order:
        - Local Directories
        - S3 Buckets

        Args:
            filepath (str):
                The filepath to search for

        Returns:
            ISource
                The source that contains the given filepath
        """
        if sources is None:
            sources = self.get_registered_sources()

        for source in sorted(sources):
            if source.exists(filepath):
                return source

        raise FileNotFoundError(f"File {filepath} not found in any source")
