"""
Abstract class that defines the interface for the writer class.
"""

from abc import ABC, abstractmethod


class IDataWriter(ABC):
    """
    Abstract class that defines the interface for the writer class.

    Writer classes are used to save data to a file in a given source.
    """

    @abstractmethod
    def write(self, filepath: str, data: object) -> None:
        """
        Writes the data to a file in the given source.

        Args:
            filepath: The path to the file to write to.
            data: The data to write to the file.
        """
        raise NotImplementedError

    @abstractmethod
    def write_json(self, filepath: str, data: dict) -> None:
        """
        Writes the data to a JSON file in the given source.

        Args:
            filepath: The path to the file to write to.
            data: The data to write to the file.
        """
        raise NotImplementedError
