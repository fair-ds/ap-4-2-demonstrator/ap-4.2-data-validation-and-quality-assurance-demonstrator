"""
Stand-in reader for virtual files.

Virtual files are files that are not stored on disk, but are instead generated
on the fly. This reader is used to represent these files in the same way as
other files, but it does not actually read any data.
"""

from dataclasses import dataclass
import datetime

from ...reader.idata_reader import IDataReader


@dataclass(kw_only=True)
class VirtualReader(IDataReader):
    """
    Represents a virtual file.
    """

    def get_size_in_bytes(self) -> int:
        return 0

    def get_date_last_modified(self) -> datetime:
        return datetime.datetime.now()

    def exists(self) -> bool:
        return False

    @property
    def source(self) -> str:
        return "virtual"

    @property
    def format(self) -> str:
        return "virtual"
