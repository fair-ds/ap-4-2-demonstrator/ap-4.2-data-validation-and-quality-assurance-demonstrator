"""
Defines the VirtualSource class.

This class represents a souce which is only in the memory of the current device.
"""

import datetime
import logging

from ..isource import ISource
from .reader.virtual_reader import VirtualReader

logger = logging.getLogger(__name__)


class VirtualSource(ISource):
    """
    Represents a souce which is only in the memory of the current device.

    As such, the file size is 0 and the date modified is the current time.
    """

    def __str__(self) -> str:
        return "Virtual"

    @property
    def sort_rank(self):
        return 0

    @property
    def name(self) -> str:
        return "Virtual"

    def get_reader(self, filepath: str, file_format: str = None):
        if file_format is not None:
            logger.warning("File format %s is not used in VirtualSources.", file_format)
        return VirtualReader(filepath=filepath)

    def exists(self, filepath=None) -> bool:
        if filepath is not None:
            logger.warning("Filepath %s is not used in VirtualSources.", filepath)
        return False

    def get_size_in_bytes(self, filepath=None) -> int:
        if filepath is not None:
            logger.warning("Filepath %s is not used in VirtualSources.", filepath)
        return 0

    def get_date_last_modified(self, filepath=None) -> datetime.datetime:
        if filepath is not None:
            logger.warning("Filepath %s is not used in VirtualSources.", filepath)
        return datetime.datetime.now()

    def get_contents(self, filepath=None) -> str:
        raise NotImplementedError

    def list_files(self, pattern: str = None) -> list[str]:
        raise NotImplementedError

    def preprocessing(self, filepath) -> None:
        raise NotImplementedError

    def to_json(self) -> dict:
        return {
            "type": "virtual",
        }

    def write_json(self, filepath: str, data: dict) -> None:
        raise NotImplementedError("Cannot write to virtual source.")
