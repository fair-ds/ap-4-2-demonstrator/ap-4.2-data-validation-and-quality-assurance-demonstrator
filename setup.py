import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fair-ds-AP4.2-demonstrator-jonhartm",
    version="0.0.1",
    author="Jonathan Hartman",
    author_email="hartman@itc.rwth-aachen.de",
    description="AP 4.2 Demonstrator for FAIR Dataspaces Project",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.9",
    include_package_data=True,
    package_data={"": ["output/static/*.*"]},
    install_requires=[
        "numpy==1.22.3",
        "pandas==1.3.5",
        "scipy==1.7.3",
        "geopandas==0.10.2",
        "matplotlib==3.5.0",
        "frictionless==4.23.2",
        "rtree==1.0.0",
    ],
)
