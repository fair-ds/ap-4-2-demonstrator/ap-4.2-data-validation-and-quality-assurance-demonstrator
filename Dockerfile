# This file is a template, and might need editing before it works on your project.
FROM python:3.10

WORKDIR /usr/src/app

ARG JUNIT_XML_DEPLOY
ARG CI_JOB_TOKEN
ARG BRANCH

RUN pip install git+https://gitlab-ci-token:${CI_JOB_TOKEN}@git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/ap-4.2-data-validation-and-quality-assurance-demonstrator.git@${BRANCH}

ADD /src/fair_ds_ap42/output/static pages/static

RUN pip list

# For some other command
CMD ["python", "-V"]
